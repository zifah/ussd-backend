﻿using System.Collections.Generic;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;
using System;
using System.Configuration;
using UssdFrontend.Interfaces;

namespace UssdFrontend.Pages
{
    public class Billing : Page, ITransactionalPage
    {
        private const int _selectItemsPerPage = 5;

        public Billing() : base() { }

        public Billing(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "debit", this);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;

            if (string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                GeneralHelper.SetTransactionAccount(_session);
            }

            // get biller information
            _session.SelectedBiller = GeneralHelper.GetBiller(_session.BillerCode);

            if (_session.SelectedBiller == null)
            {
                if (SessionObject._allBillers == null || SessionObject._allBillers.Count == 0)
                {
                    SessionObject._allBillers = GeneralHelper.GetAllBillers();
                }

                var categories = SessionObject._allBillers.Select(x => x.category).Distinct().ToList();
                return GeneralHelper.DoSelectItem("biller category", categories);
            }

            var noOfPlans = _session.SelectedBiller.Plans.Count;

            if (noOfPlans == 1)
            {
                _session.SelectedBillerPlan = _session.SelectedBiller.Plans[0];
                return CollectCustomerId();
            }

            // present plan selection menu
            _session.EnteredValue = null;
            return ProcessSession();
        }

        public UssdResponse Step3()
        {
            if (_session.SelectedBiller == null)
            {
                // did not select biller at inception
                Object selectedBillerCategory;

                _session.SelectDictionary.TryGetValue(SessionConstants.SelectedBillerCategory, out selectedBillerCategory);

                if (selectedBillerCategory == null)
                {
                    // the customer has not chosen a category
                    try
                    {
                        // _session.NextStep = 3;
                        var selectedIndex = Convert.ToInt32(_session.EnteredValue) - 1;
                        var category = SessionObject._allBillers.Select(x => x.category).Distinct().ElementAt(selectedIndex);
                        _session.SelectDictionary[SessionConstants.SelectedBillerCategory] = category;

                        var billers = SessionObject._allBillers.Where(x => x.category.Equals(category));
                        return GeneralHelper.DoSelectItem("biller", billers.Select(x => x.short_name).ToList());
                    }

                    catch
                    {
                        return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
                    }
                }
                // category is selected, select biller
                try
                {
                    var selectedIndex = Convert.ToInt32(_session.EnteredValue) - 1;

                    var selectedBiller = SessionObject._allBillers.Where(x => x.category.Equals((string)selectedBillerCategory)).ElementAt(selectedIndex);
                    _session.BillerCode = selectedBiller.code;
                    _session.NextStep = 2;
                    return ProcessSession();
                }

                catch
                {
                    return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
                }
            }

            // a biller has been selected. Go ahead to choose a plan
            var plans = _session.SelectedBiller.Plans;

            if (plans.Count == 1)
            {
                _session.SelectedBillerPlan = plans[0];
                return CollectCustomerId();
            }

            else if (_session.EnteredValue == null && _session.SelectedBillerPlan == null)
            {
                // TODO: Test that code that falls into this block works fine
                // first time, show first plan page
                _session.SelectListCurrentPage = 1;
                // _session.NextStep = 3;
                return GeneralHelper.DoSelectItem("plan", GetPlanSelectPage());
            }

            else
            {
                // TODO: Test that code that falls into this block works fine
                try
                {
                    var newPage = SetSelectedPlanOrGetNewPage();

                    if (_session.SelectedBillerPlan == null)
                    {
                        _session.SelectListCurrentPage = newPage;
                        return GeneralHelper.DoSelectItem("plan", GetPlanSelectPage());
                    }

                    return CollectCustomerId();
                }

                catch (Exception ex)
                {
                    return GeneralHelper.EndSessionWithMessage(ex.Message, _session.SessionKey);
                }
            }
        }

        public UssdResponse Step4()
        {
            var biller = _session.SelectedBiller;
            var plan = _session.SelectedBillerPlan;

            if (string.IsNullOrWhiteSpace(_session.BillingCustomerId))
            {
                // if BillingCustomerId was not supplied as part of the short code
                _session.BillingCustomerId = _session.EnteredValue;
            }

            if (_session.Amount >= 1 || plan.IsAmountFixed || plan.UseValidationAmount)
            {
                _session.Amount = plan.IsAmountFixed ? plan.Amount : _session.Amount;

                string validationText = biller.RequiresValidation ? null : _session.BillingCustomerId;

                // do validation if applies, display validation info and collect PIN
                if (biller.RequiresValidation)
                {
                    // get validation name and add it to PIN collection text
                    var retrievedValidator = ValidateBill();
                    decimal validationAmount = 0;
                    bool isAmountInvalid = false;

                    if (retrievedValidator != null)
                    {
                        validationAmount = retrievedValidator.validation_amount;
                        validationText = retrievedValidator.validation_text;
                        isAmountInvalid = plan.UseValidationAmount && validationAmount < 1;
                    }

                    if (string.IsNullOrWhiteSpace(validationText) || isAmountInvalid)
                    {
                        string validationFailureMessage = $"Sorry. We could not validate {_session.BillingCustomerId}. Please, check and try again later.";
                        return GeneralHelper.EndSessionWithMessage(validationFailureMessage, _session.SessionKey);
                    }

                    if (plan.UseValidationAmount)
                    {
                        _session.Amount = _session.ValidationAmount = validationAmount;
                    }
                }

                _session.ValidationName = validationText;
                _session.NextStep = 6;

                string confirmationText = string.Format("Paying N{0:N2} for {1} {2} on behalf of {3}||Surcharge: N{4:N2}",
                    _session.Amount,
                    biller.ShortName,
                    plan.Name.Split('-')[0].Trim(),
                    validationText,
                    biller.Surcharge);

                return GeneralHelper.CollectPin(confirmationText);
            }

            // collect amount
            _session.NextStep = 5;
            return GeneralHelper.CollectValue(null, "Enter amount");
        }

        /// <summary>
        /// Collect amount
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step5()
        {
            try
            {
                _session.Amount = Convert.ToInt32(_session.EnteredValue);
                _session.NextStep = 4;
                return ProcessSession();
            }

            catch
            {
                return GeneralHelper.CollectValue("Invalid amount entered", "Enter amount");
            }
        }

        public UssdResponse Step6()
        {
            return GeneralHelper.ProcessTransaction(_session, this);
        }

        public UssdResponse DoTransaction()
        {
            var requestBody = GetSessionBillingRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMBilling" : "Billing";

            var response = WebHelper.DoDuisPostRequest<BillingResponse>(requestBody, endpoint);

            if (response == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {

                string advertContent = GeneralHelper.GetAd(_session, "BillPayment");

                return GeneralHelper.EndSessionWithTemplate("Billing\\successful.txt", new Dictionary<string, object> {
                    { "billerName", _session.SelectedBiller.ShortName },
                    { "planName", _session.SelectedBillerPlan.Name.Split('-')[0].Trim() },
                    { "advert", advertContent }
                }, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisInsufficientFundsCode)
            {
                if (_session.SelectedBillerPlan.IsAmountFixed || _session.UserType != UserType.InstantBanking)
                {
                    return GeneralHelper.EndSessionWithMessage(Constants.ConstInsufficientFunds, _session.SessionKey);
                }

                return GeneralHelper.ProcessInsufficientFunds("/Billing/GetAffordable", _session, "pay", 6);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                if (_session.SelectedBillerPlan.IsAmountFixed || _session.UserType != UserType.InstantBanking)
                {
                    return GeneralHelper.EndSessionWithMessage(Constants.ConstDailyLimitExceededMessage, _session.SessionKey);
                }

                return GeneralHelper.ProcessLimitExceeded("/Billing/GetLimits", _session, "pay", 6);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                _session.NextStep = 6;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }

        private UssdResponse CollectCustomerId()
        {
            var resultXml = string.Empty;
            _session.NextStep = 4;

            if (string.IsNullOrWhiteSpace(_session.BillingCustomerId))
            {
                string customerIdCollectionText = string.Format("Enter your {0} {1}", _session.SelectedBiller.Name, _session.SelectedBiller.CustomerIdDescriptor);
                return GeneralHelper.CollectValue(string.Empty, customerIdCollectionText);
            }

            return ProcessSession();
        }

        private BillingResponse ValidateBill()
        {
            string result = string.Empty;

            var input = GetSessionBillingRequest();

            var response = WebHelper.DoDuisPostRequest<BillingResponse>(input, "/Billing/ValidateBill",
                Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]));

            return response;
        }

        private Dictionary<string, string> GetPlanSelectList()
        {
            // at every three items, insert a null value
            var result = new Dictionary<string, string>();
            var plans = _session.SelectedBiller.Plans;

            int pageNumber = 0;

            for (int i = 0; i < plans.Count; i++)
            {
                if (i % _selectItemsPerPage == 0)
                {
                    pageNumber++;
                }

                result.Add(string.Format("{0}{1}", pageNumber, (i) % (_selectItemsPerPage) + 1), plans[i].Name);
            }

            return result;
        }

        private int SetSelectedPlanOrGetNewPage()
        {
            Plan result = null;

            int newPageNumber = 0;

            int selectedValue = 0;

            bool isInt = Int32.TryParse(_session.EnteredValue, out selectedValue);

            if (isInt)
            {
                var noOfPages = _session.GetNoOfSelectPages(_session.SelectedBiller.Plans.Count, _selectItemsPerPage);

                try
                {
                    var planSelectList = GetPlanSelectList();
                    var selectPlanName = planSelectList[string.Format("{0}{1}", _session.SelectListCurrentPage, selectedValue)];
                    result = _session.SelectedBiller.Plans.SingleOrDefault(x => x.Name == selectPlanName);
                }

                catch
                {
                    if (selectedValue - _selectItemsPerPage == 1)
                    {
                        //next page request
                        newPageNumber = _session.SelectListCurrentPage + 1;
                    }

                    else if (selectedValue == 0)
                    {
                        // previous page request
                        newPageNumber = _session.SelectListCurrentPage - 1;
                    }

                    if (result == null && (newPageNumber == 0 || newPageNumber > noOfPages))
                    {
                        throw new ArgumentException(Constants.ConstInvalidSelectionMessage);
                    }
                }
            }

            else
            {
                throw new ArgumentException(Constants.ConstInvalidSelectionMessage);
            }

            _session.SelectedBillerPlan = result;
            return newPageNumber;
        }

        private IList<string> GetPlanSelectPage()
        {
            int currentPage = _session.SelectListCurrentPage;

            var planSelectList = GetPlanSelectList();

            var result = planSelectList.Where(x => x.Key.StartsWith(currentPage.ToString())).Select(x => x.Value).ToList();

            if (result.Count > _selectItemsPerPage)
            {
                result = result.Take(_selectItemsPerPage).ToList();
            }

            int numberOPages = _session.GetNoOfSelectPages(planSelectList.Count, _selectItemsPerPage);

            if (currentPage < numberOPages)
            {
                result.Add("Next");
            }

            if (currentPage > 1)
            {
                result.Add("Previous");
            }

            return result;
        }

        private BillingRequest GetSessionBillingRequest()
        {
            var billingRequest = new BillingRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                biller_code = _session.SelectedBiller.Code,
                customer_id = _session.BillingCustomerId,
                msisdn = _session.Msisdn,
                validation_text = _session.ValidationName,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                selected_plan_code = _session.SelectedBillerPlan.Code,
                is_via_direct_code = _session.IsViaDirectCode,
                token = _session.TransactionToken
            };

            return billingRequest;
        }
    }
}