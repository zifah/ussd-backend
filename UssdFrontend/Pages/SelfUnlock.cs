﻿using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class SelfUnlock : Page
    {
        public SelfUnlock() : base() { }

        public SelfUnlock(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            string instruction = "Enter your NUBAN account number";
            return GeneralHelper.CollectValue(null, instruction);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            _session.SourceAccount = _session.EnteredValue.Trim();
            string instruction = "Enter your birthday in format: DDMMYY. 1st December 1995 is 011295";
            return GeneralHelper.CollectValue(null, instruction);
        }

        public UssdResponse Step3()
        {
            _session.NextStep = 4;
            
            // the customer's birthday is stored in BillingCustomerId property of session
            _session.BillingCustomerId = _session.EnteredValue.Trim();
            if (_session.BillingCustomerId.All(x => char.IsNumber(x)) && (_session.BillingCustomerId.Length == 6 || _session.BillingCustomerId.Length == 8))
            {
                _session.BillingCustomerId = 
                    $"{_session.BillingCustomerId.Substring(0, 2)} {_session.BillingCustomerId.Substring(2, 2)} {_session.BillingCustomerId.Substring(4)}";
            }

            string instruction = "Enter the last four digits of your Debit Card Number";
            return GeneralHelper.CollectValue(null, instruction);
        }

        public UssdResponse Step4()
        {
            // The last four digits of the customer's PAN are stored in Password field
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var request = new SelfUnlockRequest
            {
                is_via_direct_code = _session.IsViaDirectCode,
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                birthday_DdMmYy = _session.BillingCustomerId,
                pan_last_four_digits = _session.TransactionPassword,
                reference_number = _session.SessionKey
            };

            var response = WebHelper.DoDuisPostRequest<SelfUnlockResponse>(request, "/Accounts/SelfUnlock");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                return GeneralHelper.EndSessionWithTemplate("Account\\self-unlock-failed-auth.txt", new Dictionary<string, object> {
                    { "accountNumber", request.account_number },
                    { "birthday", request.birthday_DdMmYy },
                    { "cardNumber", request.pan_last_four_digits }
                }, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string pinResetCode = $"{_session.ShortCodePrefix}{ConfigurationManager.AppSettings[Constants.ConfPinResetEndpointCode]}#";

                return GeneralHelper.EndSessionWithTemplate("Account\\self-unlock-successful.txt", new Dictionary<string, object> {
                    { "pinResetCode", pinResetCode }
                }, _session.SessionKey);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }
    }
}