﻿using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;
using System.Collections.Generic;

namespace UssdFrontend.Pages
{
    public class MobileData : Page
    {
        private const int _selectItemsPerPage = 3;
        private const string _constCategoryName = "Mobile Data";

        private readonly List<string> _rechargeTypes = new List<string>()
        {
            Constants.ConfConstantThisPhone,
            Constants.ConfConstantAnotherPhone
        };

        public MobileData() : base() { }

        public MobileData(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectItem("phone to renew", _rechargeTypes);
        }

        public UssdResponse Step2()
        {
            string selectedItem = GeneralHelper.GetSelectedItem(_rechargeTypes, _session);

            if (selectedItem == Constants.ConfConstantThisPhone)
            {
                _session.BillingCustomerId = _session.Msisdn;
                return SetBillerByNetwork(_session.MsisdnNetwork, 5);
            }

            else if (selectedItem == Constants.ConfConstantAnotherPhone)
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectValue(null, "Enter phone to renew");
            }

            // return to Step 1
            _session.NextStep = 1;
            return ProcessSession();
        }

        public UssdResponse Step3()
        {
            string resultXml = string.Empty;

            try
            {
                _session.NextStep = 4;
                if (string.IsNullOrWhiteSpace(_session.BillingCustomerId))
                {
                    _session.BillingCustomerId = _session.EnteredValue.Trim();
                }

                return GeneralHelper.DoSelectItem("network", Constants.Networks);               
            }

            catch
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectValue("Incorrect phone number", "Enter phone to renew");
            }
        }


        public UssdResponse Step4()
        {
            try
            {
                string network = GeneralHelper.GetSelectedItem(Constants.Networks, _session);
                return SetBillerByNetwork(network, 5);
            }

            catch
            {
                return GeneralHelper.DoSelectItem("network", Constants.Networks);
            }
        }

        public UssdResponse Step5()
        {
            _session.NextStep = 6;
            return GeneralHelper.DoSelectAccount(_session, "debit", this);
        }

        public UssdResponse Step6()
        {
            if (string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                GeneralHelper.SetTransactionAccount(_session);
            }

            return RedirectToBilling();
        }



        public UssdResponse SetBillerByNetwork(string networkName, int nextStep)
        {
            string resultXml = string.Empty;

            if (SessionObject._allBillers == null || SessionObject._allBillers.Count == 0)
            {
                SessionObject._allBillers = GeneralHelper.GetAllBillers();
            }

            try
            {
                _session.BillerCode = SessionObject
                    ._allBillers
                    .Single(x => x.category.Equals(_constCategoryName) && x.name.ToLower().StartsWith(networkName.ToLower()))
                    .code;

                _session.SelectedBiller = GeneralHelper.GetBiller(_session.BillerCode);

                _session.NextStep = nextStep;

                return ProcessSession();
            }

            catch
            {
                string message = string.Format("{0} data is currently not available. Please bear with us", networkName);
                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }
        }

        public UssdResponse RedirectToBilling()
        {
            _session.NextPageName = "Billing";
            _session.EnteredValue = null;

            _session.NextStep = 3;

            return Router.LoadPage();
        }
    }
}