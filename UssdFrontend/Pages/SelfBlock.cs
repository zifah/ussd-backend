﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class SelfBlock : Page
    {
        public SelfBlock() : base() { }

        public SelfBlock(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;

            if (string.IsNullOrWhiteSpace(_session.TransactionPassword))
            {
                return GeneralHelper.CollectPin($"Please, enter the Instant Banking PIN for {_session.BillingCustomerId}");
            }
            
            return ProcessSession();            
        }

        public UssdResponse Step2()
        {
            _session.TransactionPassword = string.IsNullOrWhiteSpace(_session.EnteredValue) ?
                _session.TransactionPassword : _session.EnteredValue.Trim();

            var blockRequest = new SelfBlockRequest
            {
                is_via_direct_code = _session.IsViaDirectCode,
                msisdn = _session.Msisdn,
                beneficiary_msisdn = _session.BillingCustomerId,
                password = _session.TransactionPassword
            };

            var blockResponse = WebHelper.DoDuisPostRequest<SelfBlockResponse>(blockRequest, "/Accounts/SelfBlock");

            if (blockResponse == null || string.IsNullOrWhiteSpace(blockResponse.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);

            }

            else if (blockResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (blockResponse.response_code == Constants.DuisSuccessfulCode)
            {
                return GeneralHelper.EndSessionWithTemplate("Account\\successful-block.txt", new Dictionary<string, object> {
                    { "phone", blockRequest.beneficiary_msisdn },
                    { "customer_care", ConfigurationManager.AppSettings[Constants.ConfCustomerCareName] },
                    { "customer_care_phone",  ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone] },
                }, _session.SessionKey);
            }

            else if (blockResponse.response_code == Constants.DuisFailedAuthenticationCode && _session.FailedLoginAttempts < 4)
            {
                // allow PIN re-entry
                _session.NextStep = 2;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(blockResponse.response_message);
            }

            else if (blockResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(blockResponse.response_code, blockResponse.response_message, _session.SessionKey);
        }
    }
}