﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class MicroPay : Page
    {
        public MicroPay() : base() { }

        public MicroPay(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "payment", this);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;

            GeneralHelper.SetTransactionAccount(_session);

            var validationRequest = GetRequestPayload();

            var validateResponse = WebHelper.DoDuisPostRequest<MicroPayResponse>(validationRequest, "/MicroPay/Validate",
                Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]));

            if (validateResponse != null && validateResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string productName = $"{validateResponse.biller_name} {validateResponse.payment_item_name}";
                _session.ValidationName = validateResponse.biller_name;
                _session.Amount = validateResponse.payment_amount;
                _session.ValidationFee = validateResponse.surcharge_amount;

                string surchargeText = validateResponse.surcharge_amount > 0 ? $"||Surcharge: {validateResponse.surcharge_amount:N2}" : string.Empty;
                
                string displayMessage = $"Pay N{_session.Amount:N2} to {_session.ValidationName}{surchargeText}";
                return GeneralHelper.CollectPin($"{displayMessage}|");
            }

            else
            {
                string validationError = validateResponse != null && !string.IsNullOrWhiteSpace(validateResponse.response_message) ?
                    validateResponse.response_message :
                    $"We could not verify merchant #{_session.BillerCode} at this time; please check and retry shortly";

                return GeneralHelper.EndSessionWithMessage(validationError, _session.SessionKey);
            }
        }

        public UssdResponse Step3()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var paymentRequest = GetRequestPayload();

            var microPayResponse = WebHelper.DoDuisPostRequest<MicroPayResponse>(paymentRequest, "/MicroPay/Pay");

            if (microPayResponse == null || microPayResponse.response_code == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string message = string.Format("Your payment of N{0:N2} to {1} was successful.",
                    _session.Amount, paymentRequest.merchant_name, _session.BillingCustomerId);

                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry         
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(microPayResponse.response_message);
            }

            else if (microPayResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(microPayResponse.response_code, microPayResponse.response_message, _session.SessionKey);
        }

        private MicroPayRequest GetRequestPayload()
        {
            var request = new MicroPayRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                surcharge = _session.ValidationFee,
                msisdn = _session.Msisdn,
                merchant_name = _session.ValidationName,
                merchant_code = _session.BillerCode,
                msisdn_network = _session.MsisdnNetwork,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
                token = _session.TransactionToken
            };

            return request;
        }
    }
}