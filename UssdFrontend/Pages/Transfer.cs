﻿using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using UssdFrontend.Interfaces;
using UssdFrontend.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System;
using UssdFrontend.Exceptions;

namespace UssdFrontend.Pages
{
    public class Transfer : Page, ITransactionalPage
    {
        public Transfer() : base() { }

        public Transfer(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            PreventPrematureTransfers();
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "debit", this);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;

            GeneralHelper.SetTransactionAccount(_session);

            // get account banks
            // put the banks into a list
            var accountBanks = WebHelper.DoDuisGetRequest<AccountBanks>("/accounts/GetAccountBanks", new Dictionary<string, object> {
                { "account_number", _session.DestinationAccount },
                { "sender_msisdn", _session.Msisdn }
            });

            if (accountBanks == null || accountBanks.banks.Count < 1)
            {
                return GeneralHelper.EndSessionWithMessage("Destination account number is invalid", _session.SessionKey);
            }

            for (int i = 1; i <= accountBanks.banks.Count; i++)
            {
                _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.BeneficiaryBankSelectPrefix, i), accountBanks.banks[i - 1]);
            }

            if (accountBanks.banks.Count == 1)
            {
                return ProcessSession();
            }

            return GeneralHelper.DoSelectItem("destination bank", accountBanks.banks.Select(x => x.name).ToList());
        }

        public UssdResponse Step3()
        {
            object selectedBank = null;
            _session.SelectDictionary.TryGetValue(string.Format("{0}{1}", SessionConstants.BeneficiaryBankSelectPrefix, _session.EnteredValue), out selectedBank);

            _session.SelectedBank = (Bank)selectedBank;

            if (_session.SelectedBank == null)
            {
                var beneficiaryBanks = _session.SelectDictionary.Where(x => x.Key.StartsWith(SessionConstants.BeneficiaryBankSelectPrefix)).ToDictionary(x => x.Key, x => x.Value);

                if (beneficiaryBanks.Count == 1)
                {
                    _session.SelectedBank = (Bank)beneficiaryBanks.ElementAt(0).Value;
                }

                else
                {
                    return GeneralHelper.EndSessionWithMessage("Your selection is invalid.", _session.SessionKey);
                }
            }

            // get account name
            try
            {
                if (_session.CollectNarration == 1)
                {
                    string numNarrationCharacters = ConfigurationManager.AppSettings[Constants.ConfNumberNarrationCharacters];
                    string narrationText = $"Enter narration ({numNarrationCharacters} characters max) e.g. March Pocket money from Mummy";
                    _session.NextStep = 5;
                    return GeneralHelper.CollectValue(null, narrationText);
                }

                return GeneralHelper.ValidateCollectPinTransfer(_session, 4);

            }

            catch (TimeoutException ex)
            {
                return GeneralHelper.EndSessionWithMessage(ex.Message, _session.SessionKey);
            }
        }

        public UssdResponse Step4()
        {
            return GeneralHelper.ProcessTransaction(_session, this);
        }

        public UssdResponse Step5()
        {
            int numNarrationChars = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfNumberNarrationCharacters]);
            _session.Narration = _session.EnteredValue.Trim();

            if (numNarrationChars < _session.Narration.Length)
            {
                string narrationText = string.Format("Narration cannot be more than {0} characters", numNarrationChars);
                return GeneralHelper.CollectValue(narrationText, "Retry");
            }

            return GeneralHelper.ValidateCollectPinTransfer(_session, 4);
        }

        public UssdResponse DoTransaction()
        {
            var requestBody = new
            {
                msisdn = _session.Msisdn,
                source_nuban = _session.SourceAccount,
                password = _session.TransactionPassword,
                token = _session.TransactionToken,
                dest_nuban = _session.DestinationAccount,
                dest_account_name = _session.ValidationName,
                dest_bank_name = _session.SelectedBank.name_short,
                dest_bank_code = _session.SelectedBank.code_v2,
                dest_bank_alpha_code = _session.SelectedBank.name_short,
                reference_number = string.Format("{0}{1}", _session.SessionKey, _session.RetryCount == 0 ? string.Empty : "R" + _session.RetryCount),
                amount = _session.Amount,
                narration = _session.Narration,
                is_via_direct_code = _session.IsViaDirectCode
            };

            var response = WebHelper.DoDuisPostRequest<FundsTransferResponse>(requestBody, "/FundsTransfer");

            if (response == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = GeneralHelper.GetAd(_session, "FundsTransfer");

                return GeneralHelper.EndSessionWithTemplate("Transfer\\transfer-done.txt", new Dictionary<string, object>() {
                    { "beneficiary_name", _session.ValidationName },
                    { "advert", advertContent }
                }, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisInsufficientFundsCode)
            {
                var ownBankTransferCode = ConfigurationManager.AppSettings[Constants.ConfOwnBankTransferCode];

                string interOrIntra = requestBody.dest_bank_code.Equals(ownBankTransferCode) ? "Intra" : "Inter";

                return GeneralHelper.ProcessInsufficientFunds($"/FundsTransfer/GetAffordable{interOrIntra}", _session, "transfer", 4);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                var ownBankTransferCode = ConfigurationManager.AppSettings[Constants.ConfOwnBankTransferCode];
                string interOrIntra = requestBody.dest_bank_code.Equals(ownBankTransferCode) ? "Intra" : "Inter";
                return GeneralHelper.ProcessLimitExceeded($"/FundsTransfer/GetLimits{interOrIntra}", _session, "transfer", 4);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }

        /// <summary>
        /// Stop customers who enrolled during the weekend from doing transfers until Monday
        /// </summary>
        private void PreventPrematureTransfers()
        {
            // Get enrolment date
            // check if today is Saturday or Sunday
            // if today is Saturday, confirm block the transaction if the customer enrolled today
            // if today is Sunday, block the transaction if the customer enrolled yesterday or today
            bool block = false;
            var now = DateTime.Now;
            var today = now.Date;

            if (_session.UserEnrolmentTime.HasValue)
            {
                var enrolmentDate = _session.UserEnrolmentTime.Value.Date;

                if(new DayOfWeek[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(today.DayOfWeek) || (today.DayOfWeek == DayOfWeek.Monday && now.Hour < 8))
                {
                    block = enrolmentDate >= today.AddDays(today.DayOfWeek == DayOfWeek.Saturday ? 0 : -(today.DayOfWeek + 7 - DayOfWeek.Saturday));
                }
            }

            if (block)
            {
                throw new UssdException("Unable to complete.");
            }
        }
    }
}