﻿using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class ContactAccountOfficer : Page
    {
        public ContactAccountOfficer() : base() { }

        public ContactAccountOfficer(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "an", this);
        }

        public UssdResponse Step2()
        {
            GeneralHelper.SetTransactionAccount(_session);

            var request = GetRequestPayload();

            var response = WebHelper.DoDuisPostRequest<GetAccountOfficerResponse>(request, "/AccountOfficer");

            if (response == null || response.response_code == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                if (string.IsNullOrWhiteSpace(response.account_officer_name) ||
                    (string.IsNullOrWhiteSpace(response.account_officer_phone) && string.IsNullOrWhiteSpace(response.account_officer_email)))
                {
                    return GeneralHelper.EndSessionWithTemplate("account\\account-officer-empty.txt", new Dictionary<string, object> {
                        { "customer_care", ConfigurationManager.AppSettings[Constants.ConfCustomerCareName] },
                        { "customer_care_phone", ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone] }
                    }, _session.SessionKey);
                }

                else
                {
                    return GeneralHelper.EndSessionWithTemplate("account\\account-officer-details.txt", new Dictionary<string, object> {
                        { "name", response.account_officer_name },
                        { "phone", response.account_officer_phone },
                        { "email", response.account_officer_email }
                    }, _session.SessionKey);
                }
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            else
            {
                return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
            }
        }

        private GetAccountOfficerRequest GetRequestPayload()
        {
            var request = new GetAccountOfficerRequest
            {
                account_number = _session.SourceAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
            };

            return request;
        }
    }
}