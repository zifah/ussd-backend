﻿using UssdFrontend.HelperObjects;

namespace UssdFrontend.Pages
{
    public class Index : Page
    {
        public Index() : base() { }

        public Index(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            // Put Instant banking menu as USSD homepage
            return new UssdResponse
            {
                RedirectInstruction = new Redirect
                {
                    // redirect to the Instant banking menu
                    ShortCode = $"{_session.ShortCodePrefix}1#"
                }
            };

            //bool useNewMobileMoney = false;

            //Boolean.TryParse(ConfigurationManager.AppSettings[Constants.ConfUseNewMobileMoney], out useNewMobileMoney);

            //string mobileMoneyUrl = useNewMobileMoney ? string.Format("{0}/MMMenu", Router._defaultEndpoint) :
            //    ConfigurationManager.AppSettings[Constants.ConfMobileMoneyUrl];

            //string duisMenuUrl = string.Format("{0}/Menu", Router._defaultEndpoint);

            //return GeneralHelper.EndSessionWithMessage("This page is under construction", _session.SessionKey);
        }
    }
}