﻿using System;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Text.RegularExpressions;

namespace UssdFrontend.Pages
{
    public class CollectRechargePhone : Page
    {
        public CollectRechargePhone() : base() { }

        public CollectRechargePhone(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.CollectValue(null, "Enter phone number (11-digits)");
        }

        public UssdResponse Step2()
        {
            string resultXml = string.Empty;

            string phone = _session.EnteredValue.Trim();

            if (Regex.IsMatch(phone, "^[0-9]{11}$"))
            {
                _session.BillingCustomerId = phone;

                _session.NextStep = 1;
                _session.NextPageName = "CollectRechargeAmount";
                return Router.LoadPage();
            }

            return GeneralHelper.CollectValue($"{phone} is not a valid phone number|", "Enter phone number (11-digits)");
        }
    }
}