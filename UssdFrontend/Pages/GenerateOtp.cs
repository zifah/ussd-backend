﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class GenerateOtp : Page
    {
        public GenerateOtp() : base() { }

        public GenerateOtp(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            decimal convenienceFee = GetConvenienceFee();
            string username = GetOtpUsername();
            username = $"{username.Substring(0, 2)}***{username.Substring(username.Length - 2, 2)}";
            return GeneralHelper.CollectPin($"Generating one-time password for user: {username}. This will cost N{convenienceFee:N2}");
        }

        public UssdResponse Step2()
        {
            var otpResponse = GetOtp();

            if (otpResponse == null || string.IsNullOrWhiteSpace(otpResponse.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisSuccessfulCode)
            {
                return GeneralHelper.EndSessionWithTemplate("GenerateOtp\\successful.txt", new Dictionary<string, object> {
                    { "otp", otpResponse.otp },
                    { "expirationTime", otpResponse.expiration_time.ToString("HH:mm")},
                    { "expirationDate", otpResponse.expiration_time.ToString("dd MMM yyyy") }
                }, _session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(otpResponse.response_message);
            }

            else if (otpResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            else
            {
                return GeneralHelper.EndSessionWithGenericFailure(otpResponse.response_code, otpResponse.response_message, _session.SessionKey);
            }
        }

        private string GetOtpUsername()
        {
            var result = WebHelper.DoDuisGetRequest<OtpResponse>("/otp/getusername", new Dictionary<string, object> { { "msisdn", _session.Msisdn } });
            return result.username;
        }

        private decimal GetConvenienceFee()
        {
            var result = WebHelper.DoDuisGetRequest<OtpResponse>("/otp/getcharge", new Dictionary<string, object> { { "msisdn", _session.Msisdn } });
            return result.convenience_fee;
        }

        private OtpResponse GetOtp()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var result = WebHelper.DoDuisPostRequest<OtpResponse>(new OtpRequest
            {
                password = _session.TransactionPassword,
                msisdn = _session.Msisdn,
                is_via_direct_code = _session.IsViaDirectCode,
                reference_number = _session.SessionKey
            }, "/otp/generate");

            return result;
        }
    }
}