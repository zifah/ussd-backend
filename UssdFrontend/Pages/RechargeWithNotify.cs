﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class RechargeWithNotify : Page
    {
        public RechargeWithNotify() : base() { }

        public RechargeWithNotify(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextPageName = typeof(Recharge).Name;
            _session.NextStep = 1;
            return Router.LoadPage();
        }
    }
}