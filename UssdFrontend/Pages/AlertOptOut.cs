﻿using System.Collections.Generic;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;
using System;

namespace UssdFrontend.Pages
{
    public class AlertOptOut : Page
    {
        private const int _selectItemsPerPage = 5;

        public AlertOptOut() : base() { }

        public AlertOptOut(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, null, this, "account to opt-out of alert");
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            GeneralHelper.SetTransactionAccount(_session);

            var alertProducts = GetAlertProducts();

            if (alertProducts == null || alertProducts.Count == 0)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstServiceUnavailableMessage, _session.SessionKey);
            }

            _session.SelectDictionary.Add(SessionConstants.AlertProducts, alertProducts);
            return DisplayAlertProducts(1);
        }

        public UssdResponse Step3()
        {
            string enteredValue = _session.EnteredValue.Trim();

            try
            {
                var alertProducts = (Dictionary<string, string>)_session.SelectDictionary[SessionConstants.AlertProducts];

                var index = Convert.ToInt32(enteredValue);

                if (index == 0)
                {
                    _session.NextStep = 3;
                    return DisplayAlertProducts(_session.SelectListCurrentPage - 1);
                }

                else if (index == _selectItemsPerPage + 1)
                {
                    _session.NextStep = 3;
                    return DisplayAlertProducts(_session.SelectListCurrentPage + 1);
                }

                _session.NextStep = 4;
                int itemIndex = index * _session.SelectListCurrentPage - 1;
                var alertProduct = alertProducts.ElementAt(itemIndex);
                _session.SelectDictionary[SessionConstants.SelectedAlertProduct] = alertProduct;

                var isSubscription = _session.AlertSubscriptionAction.Equals(1);

                return GeneralHelper.CollectPin($"Stopping {alertProduct.Value} alert on your account: {_session.SourceAccountMasked}",
                    "Enter your PIN to confirm");
            }

            catch
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
            }
        }

        public UssdResponse Step4()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var theAlertProduct = ((KeyValuePair<string, string>)_session.SelectDictionary[SessionConstants.SelectedAlertProduct]);

            // compose alert subscription message
            var requestBody = new AlertSubscriptionRequest
            {
                account_number = _session.SourceAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                product_id = theAlertProduct.Key,
                subscribe = false,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };

            // post it to Duis
            var response = WebHelper.DoDuisPostRequest<AlertSubscriptionResponse>(requestBody, "/AlertSubscription");

            // display result to customer (allow re-entry of PIN)
            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                return GeneralHelper
                    .EndSessionWithMessage(
                    $"You will no longer receive {theAlertProduct.Value} on your account: {_session.SourceAccountMasked}", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }
            
            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }


        public InfoUpdateRequest GetAlertSubscriptionUpdateRequest()
        {
            return new InfoUpdateRequest
            {
                info_type = _session.NewInformationKey,
                msisdn = _session.Msisdn,
                new_value = _session.NewInformation,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey
            };
        }

        private Dictionary<string, string> GetAlertProducts()
        {
            var result = WebHelper.DoDuisGetRequest<Dictionary<string, string>>("/AlertSubscription", new Dictionary<string, object> { });
            return result;
        }

        private UssdResponse DisplayAlertProducts(int pageToShow)
        {
            var alertProducts = (Dictionary<string, string>)_session.SelectDictionary[SessionConstants.AlertProducts];
            var selectList = alertProducts.Skip((pageToShow - 1) * _selectItemsPerPage).Take(_selectItemsPerPage).Select(x => x.Value).ToList();

            _session.SelectListCurrentPage = pageToShow;

            int noOfPages = _session.GetNoOfSelectPages(alertProducts.Count, _selectItemsPerPage);

            if (_session.ShouldAddNext(pageToShow, noOfPages))
            {
                selectList.Add("Next");
            }

            if (_session.ShouldAddPrevious(1))
            {
                selectList.Add("Previous");
            }

            return GeneralHelper.DoSelectItem("alert", selectList);
        }
    }
}