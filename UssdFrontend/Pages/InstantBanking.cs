﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Text.RegularExpressions;

namespace UssdFrontend.Pages
{
    public class InstantBanking : Page
    {
        private readonly int _selectItemsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfMenuItemsPerPage]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');

        public InstantBanking() : base() { }

        public InstantBanking(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            return DisplayMenu(0);
        }

        public UssdResponse Step2()
        {
            try
            {
                string enteredValue = _session.EnteredValue.Trim();
                int selectedMenuItem = Convert.ToInt32(enteredValue);

                var displayedItems = GetMenuItems(0);

                if (selectedMenuItem == 0)
                {
                    return DisplayMenu(-1);
                }

                if (displayedItems[selectedMenuItem - 1] == "Next")
                {
                    return DisplayMenu(1);
                }

                PageSelector selectedPage = DestinationPages.
                    Single(x => x.DisplayName == displayedItems[selectedMenuItem - 1]);

                _session.MenuSelectedPageName = selectedPage.PageName;

                if (selectedPage.SessionVariables.Count == 0)
                {
                    return RedirectToSelectedPage();
                }

                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = selectedPage.SessionVariables[0].Name;
                return GeneralHelper.CollectValue(null, $"Enter {selectedPage.SessionVariables[0].DisplayName}");
            }

            catch
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
            }
        }


        public UssdResponse Step3()
        {
            string enteredValue = _session.EnteredValue.Trim();

            PageSelector selectedPage = DestinationPages
                .Where(x => !_closedPages.Contains(x.PageName))
                .Single(x => x.PageName == _session.MenuSelectedPageName);


            var sessionObjectType = typeof(SessionObject);

            var sessionPropertyInfo = sessionObjectType.GetProperty(_session.NextMenuVariableToCollect);

            var sessionVariable = selectedPage.SessionVariables.Single(x => x.Name == _session.NextMenuVariableToCollect);

            var index = selectedPage.SessionVariables.IndexOf(sessionVariable);

            try
            {
                if (!Regex.IsMatch(enteredValue, sessionVariable.RegexValidator))
                {
                    throw new Exception();
                }

                sessionPropertyInfo.SetValue(_session, Convert.ChangeType(enteredValue, sessionPropertyInfo.PropertyType));
            }

            catch
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = sessionVariable.Name;

                return GeneralHelper.CollectValue(null, $"The {sessionVariable.DisplayName} entered was invalid. Please try again");
            }

            if (index == selectedPage.SessionVariables.Count - 1)
            {
                return RedirectToSelectedPage();
            }

            _session.NextStep = 3;
            _session.NextMenuVariableToCollect = selectedPage.SessionVariables[index + 1].Name;
            
            return GeneralHelper.CollectValue(null, string.Format("Enter {0}", selectedPage.SessionVariables[index + 1].DisplayName));

        }

        public UssdResponse DisplayMenu(int increment)
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectItem("menu", GetMenuItems(increment));
        }

        public List<string> GetMenuItems(int currentPageIncrement)
        {
            _session.SelectListCurrentPage += currentPageIncrement;
            int currentPage = _session.SelectListCurrentPage;
            var items = DestinationPages.Where(x => !_closedPages.Contains(x.PageName)).Select(x => x.DisplayName).ToList();
            var noOfItems = items.Count;

            var startIndex = Math.Max(0, _selectItemsPerPage * (currentPage - 1));
            var endIndex = startIndex + _selectItemsPerPage - 1;

            var result = items.Where(x => items.IndexOf(x) >= startIndex && items.IndexOf(x) <= endIndex).ToList();

            if (noOfItems - endIndex > 1)
            {
                result.Add("Next");
            }

            if (currentPage > 1)
            {
                result.Add("Previous");
            }

            return result;
        }

        private UssdResponse RedirectToSelectedPage()
        {
            _session.NextStep = 1;
            if (Router._anonymousPages.Contains(_session.MenuSelectedPageName) || _session.IsUserLoaded)
            {
                _session.NextPageName = _session.MenuSelectedPageName;
            }

            else
            {
                // refer user to registration stage
                _session.NextPageName = "NewUser";

                // where to redirect to after creation
                _session.RedirectStepNumber = 1;
                _session.RedirectPageName = _session.MenuSelectedPageName;
            }

            return Router.LoadPage();
        }

        private List<PageSelector> DestinationPages = new List<PageSelector>()
        {
             new PageSelector{ PageName = "AccountOpening", DisplayName = "Open Account",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "Balance", DisplayName = "Balance",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "CollectRechargeType", DisplayName = "Airtime",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new PageSelector{ PageName = "Transfer", DisplayName = "Transfer",
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "DestinationAccount", DisplayName = "Destination account", RegexValidator = "^[0-9]{10}$" },
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,7}$" }
                 }
             },
             new PageSelector{ PageName = "Cashout", DisplayName = "Dial4Cash",
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,4}000$" }
                 }
             },
             new PageSelector{ PageName = "MobileData", DisplayName = "Data Plan",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new PageSelector{ PageName = "Billing", DisplayName = "Bill Payments",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new PageSelector{ PageName = "Airline", DisplayName = "Airline Tickets",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new PageSelector{ PageName = "AccountReminderService", DisplayName = "Get Account Number/BVN",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new PageSelector{ PageName = "GenerateOtp", DisplayName = "Generate OTP",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new PageSelector{ PageName = "Kyc", DisplayName = "Info Update",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "PinChange", DisplayName = "PIN change",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "PinReset", DisplayName = "Forgot PIN",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "ControlCard", DisplayName = "Block card",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "ContactAccountOfficer", DisplayName = "Contact Account Officer",
                 SessionVariables = new List<PageSessionVariable> { } }
        };
    }

    public class PageSelector
    {
        public string PageName { set; get; }
        public string DisplayName { set; get; }
        public List<PageSessionVariable> SessionVariables { set; get; }
    }

    public class PageSessionVariable
    {
        public string Name { set; get; }
        public string DisplayName { set; get; }
        public string RegexValidator { set; get; }
    }
}