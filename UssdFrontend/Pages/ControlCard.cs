﻿using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;
using System;

namespace UssdFrontend.Pages
{
    public class ControlCard : Page
    {
        private readonly string[] _blockUnblock = new string[] { _block };
        private const string _block = "Block";
        private const string _unblock = "Unblock";

        public ControlCard() : base() { }

        public ControlCard(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;

            var activeCards = GetActiveCards();

            if (activeCards == null || activeCards.cards.Count == 0)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstServiceUnavailableMessage, _session.SessionKey);
            }

            _session.SelectDictionary.Add(SessionConstants.AccountCards, activeCards);

            if (activeCards.cards.Count == 1)
            {
                _session.BillingCustomerId = activeCards.cards[0].masked_pan;
                return ProcessSession();
            }

            return GeneralHelper.DoSelectItem("card to manage", activeCards.cards.Select(x => x.display_name).ToList());
        }

        public UssdResponse Step2()
        {
            Card selectedCard = null;
            var accountCards = (CardEnquiryResponse)_session.SelectDictionary[SessionConstants.AccountCards];

            if (_session.BillingCustomerId == null)
            {
                int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;

                selectedCard = accountCards.cards[selectedIndex];
                _session.BillingCustomerId = selectedCard.masked_pan;
            }

            else
            {
                selectedCard = GetSelectedCard();
            }

            _session.NextStep = 3;

            if (_blockUnblock.Length == 1)
            {
                _session.EnteredValue = "1";
                return ProcessSession();
            }

            return GeneralHelper.DoSelectItem($"action on {selectedCard.display_name}", _blockUnblock);
        }
        public UssdResponse Step3()
        {
            int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;
            bool isHotlist = _blockUnblock[selectedIndex].Equals(_block);
            var selectedCard = GetSelectedCard();

            var payload = new CardControlRequest
            {
                account_number = selectedCard.account_number,
                masked_pan = selectedCard.masked_pan,
                msisdn = _session.Msisdn,
                reference_number = _session.SessionKey,
                is_unblock = !isHotlist,
                is_via_direct_code = _session.IsViaDirectCode
            };

            _session.SelectDictionary[SessionConstants.CardControlRequest] = payload;

            _session.NextStep = 4;

            string displayText = $"{(isHotlist ? "Blocking" : "Unblocking")} {selectedCard.display_name}|";

            return GeneralHelper.CollectPin(displayText);
        }

        public UssdResponse Step4()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var requestBody = (CardControlRequest)_session.SelectDictionary[SessionConstants.CardControlRequest];
            requestBody.password = _session.TransactionPassword;

            var response = WebHelper.DoDuisPostRequest<CardControlResponse>(requestBody, "/Cards/Control");


            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                var card = GetSelectedCard();
                return GeneralHelper.EndSessionWithMessage($"Your card: {_session.BillingCustomerId} has been {(requestBody.is_unblock ? _unblock : _block)}ed successfully.", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }

        private Card GetSelectedCard()
        {
            var customerCards = (CardEnquiryResponse)_session.SelectDictionary[SessionConstants.AccountCards];
            var theCard = customerCards.cards.First(x => x.masked_pan.Equals(_session.BillingCustomerId));
            return theCard;
        }


        private CardEnquiryResponse GetActiveCards()
        {
            var result = WebHelper.DoDuisGetRequest<CardEnquiryResponse>("/cards", new Dictionary<string, object> {
                { "msisdn", _session.Msisdn },
                { "account_number", _session.DefaultAccount }
            });

            return result;
        }
    }
}