﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class Kyc : Page
    {
        private const string _constTitle = "Info update";
        private const int _selectItemsPerPage = 3;

        public Kyc() : base() { }

        public Kyc(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;

            var updatables = GetUpdatableInformation();

            if (updatables == null || updatables.Count == 0)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstServiceUnavailableMessage, _session.SessionKey);
            }

            else
            {
                for (int i = 1; i <= updatables.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.KycUpdatableSelectPrefix, i), updatables[i - 1]);
                }

                return GeneralHelper.DoSelectItem("information to update", updatables);
            }
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            string enteredValue = _session.EnteredValue.Trim();

            try
            {
                _session.NewInformationKey = (string)_session.SelectDictionary[string.Format("{0}{1}", SessionConstants.KycUpdatableSelectPrefix, enteredValue)];
                return GeneralHelper.CollectValue(null, $"Enter your {_session.NewInformationKey}");
            }

            catch
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
            }
        }

        public UssdResponse Step3()
        {
            _session.NextStep = 4;
            _session.NewInformation = _session.EnteredValue.Trim();
            return GeneralHelper.CollectValue(null, $"Please, re-enter your {_session.NewInformationKey}");
        }

        public UssdResponse Step4()
        {
            if (_session.NewInformation == _session.EnteredValue)
            {
                _session.NextStep = 5;
                return GeneralHelper.CollectPin($"Your { _session.NewInformationKey} will be set to {_session.NewInformation}");
            }

            _session.NextStep = 3;
            return GeneralHelper.CollectValue($"Both {_session.NewInformationKey}s do not match", $"Please, enter your {_session.NewInformationKey}");
        }

        public UssdResponse Step5()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var requestBody = GetKycUpdateRequest();

            var response = WebHelper.DoDuisPostRequest<InfoUpdateResponse>(requestBody, "/InfoUpdate");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                return GeneralHelper.EndSessionWithMessage("Information was updated successfully", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.NextStep = 5;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_message, response.response_code, _session.SessionKey);            
        }

        public InfoUpdateRequest GetKycUpdateRequest()
        {
            return new InfoUpdateRequest
            {
                info_type = _session.NewInformationKey,
                msisdn = _session.Msisdn,
                new_value = _session.NewInformation,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };
        }

        private List<string> GetUpdatableInformation()
        {
            var result = WebHelper.DoDuisGetRequest<List<string>>("/InfoUpdate", new Dictionary<string, object> { });
            return result;
        }
    }
}