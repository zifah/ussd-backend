﻿using UssdFrontend.HelperObjects;
using UssdFrontend.Interfaces;

namespace UssdFrontend.Pages
{
    public abstract class Page : IUssdPage
    {
        public SessionObject _session;

        public Page()
        {

        }

        public Page(SessionObject session)
        {
            _session = session;
        }

        public UssdResponse ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (UssdResponse)method.Invoke(this, null);
            return result;
        }

    }
}