﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;

namespace UssdFrontend.Pages
{
    public class Cashout : Page
    {
        private const string _constCashoutChannelsList = "CashoutChannelsList";
        /// <summary>
        /// The selected channel
        /// </summary>
        private const string _constCashoutChannel = "CashoutChannel";
        private const int _constCashoutPinLength = 4;

        public Cashout() : base() { }

        public Cashout(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "debit", this);
        }

        public UssdResponse Step2()
        {
            GeneralHelper.SetTransactionAccount(_session);
            _session.NextStep = 3;

            decimal cashoutMinimum = Convert.ToDecimal(ConfigurationManager.AppSettings[Constants.ConfCashoutMinimumFactor]);

            if (_session.Amount < cashoutMinimum)
            {
                return GeneralHelper.CollectValue(null, $"Enter amount in multiples of {cashoutMinimum}");
            }

            return ProcessSession();
        }

        public UssdResponse Step3()
        {
            _session.NextStep = 4;

            decimal cashoutMinimum = Convert.ToDecimal(ConfigurationManager.AppSettings[Constants.ConfCashoutMinimumFactor]);

            if (_session.Amount < cashoutMinimum)
            {
                try
                {
                    var amount = Convert.ToDecimal(_session.EnteredValue.Trim());

                    if (amount < cashoutMinimum || amount % cashoutMinimum != 0)
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    _session.Amount = amount;
                }

                catch
                {
                    _session.NextStep = 3;
                    return GeneralHelper.CollectValue(_session.SessionKey, "Amount is not valid", $"Enter amount in multiples of {cashoutMinimum}");
                }
            }

            return CollectCashoutPin(false);
        }

        public UssdResponse Step4()
        {
            var cashoutPin = _session.EnteredValue.Trim();

            if (cashoutPin.All(char.IsDigit) && cashoutPin.Length == _constCashoutPinLength)
            {
                _session.CashoutPin = cashoutPin;
                _session.NextStep = 5;
                return ProcessSession();
            }

            return CollectCashoutPin(true);
        }

        /// <summary>
        /// Save cashout PIN and collect user PIN
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step5()
        {
            _session.NextStep = 6;
            return GeneralHelper.CollectPin($"Cardless withdrawal of NGN {_session.Amount:N2}|");
        }

        public UssdResponse Step6()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var requestBody = GetSessionCashoutRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMCashout" : "/Cashout";

            var response = WebHelper.DoDuisPostRequest<CashoutResponse>(requestBody, endpoint);

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = GeneralHelper.GetAd(_session, "Cashout");

                return GeneralHelper.EndSessionWithTemplate("Cashout\\successful.txt", new Dictionary<string, object> {
                    { "paycode", response.paycode },
                    { "advert", advertContent },
                    { "channel", requestBody.withdrawal_channel }
                }, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.NextStep = 6;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }

        private UssdResponse CollectCashoutPin(bool isRetry)
        {
            var resultXml = string.Empty;
            _session.NextStep = 4;
            string customerIdCollectionText = "Enter a one-time 4-digit PIN for your cardless withdrawal";

            return GeneralHelper.CollectValue(isRetry ? "One-time PIN must be a 4-digit numeric code" : string.Empty, customerIdCollectionText);
        }

        private CashoutRequest GetSessionCashoutRequest()
        {
            var CashoutRequest = new CashoutRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey,
                transaction_pin = _session.CashoutPin,
                is_via_direct_code = _session.IsViaDirectCode,
                token = _session.TransactionToken,
                withdrawal_channel = "ATM"
            };

            return CashoutRequest;
        }
    }
}