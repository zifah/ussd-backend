﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class PinReset : Page
    {
        private const string _constTitle = "Pin Reset";

        public PinReset() : base() { }

        public PinReset(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            string message = TemplateHelper.ParseTemplate("Account\\start-pin-reset.txt", new Dictionary<string, object>() { });
            return GeneralHelper.CollectValue(null, message, _constTitle);
        }

        /// <summary>
        /// Collect birthday in DdMmYy format
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            _session.SourceAccount = _session.EnteredValue.Trim();
            string message = TemplateHelper.ParseTemplate("Account\\pin-reset-token.txt", new Dictionary<string, object>() { });
            return GeneralHelper.CollectValue(null, message, _constTitle);
        }

        /// <summary>
        /// Collect new PIN
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step3()
        {
            _session.NextStep = 4;
            _session.PinResetToken = _session.EnteredValue.Trim();
            return GeneralHelper.CollectPin(null, "Select a new PIN with 4 or more numbers or letters", _constTitle);
        }

        /// <summary>
        /// Confirm new PIN
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step4()
        {
            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = GeneralHelper.IsPasswordValid(newPassword, _session.SourceAccount);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 5;
                _session.NewPassword = newPassword;
                return GeneralHelper.CollectPin(null, "Confirm your new PIN", _constTitle);
            }

            _session.NextStep = 4;
            return GeneralHelper.CollectPin($"{isPasswordValid.reason}|", "Please, enter a PIN with 4 or more numbers or letters", _constTitle);
        }


        public UssdResponse Step5()
        {
            _session.NextStep = 5;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                var response = WebHelper.DoDuisPostRequest<UssdUser>(new
                {
                    msisdn = _session.Msisdn,
                    password = _session.NewPasswordConfirm.Trim(),
                    account_number = _session.SourceAccount,
                    birthday_DdMmYy = _session.PinResetToken,
                    is_via_direct_code = _session.IsViaDirectCode
                }, "/accounts/resetpassword");

                if (response == null || string.IsNullOrWhiteSpace(response.response_code))
                {
                    return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
                }

                else if (response.response_code == Constants.DuisDailyLimitExceededCode)
                {
                    return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisTimeoutCode)
                {
                    return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisSuccessfulCode)
                {
                    string advertContent = GeneralHelper.GetAd(_session, "PinReset");
                    return GeneralHelper.EndSessionWithTemplate("PinReset\\successful.txt",
                        new Dictionary<string, object> {
                            { "advert", advertContent }
                        }, _session.SessionKey);
                }


                else if (response.response_code == Constants.DuisAccountBlockedCode)
                {
                    return GeneralHelper.RedirectToBlocked(_session);
                }

                return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
            }

            else
            {
                _session.NextStep = 4;
                return GeneralHelper.CollectPin("Your PINs do not match!", "Select a PIN with 4 or more numbers or letters", _constTitle);
            }
        }
    }
}