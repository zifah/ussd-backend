﻿using System.Collections.Generic;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class PageClosed : Page
    {
        public PageClosed() : base() { }

        public PageClosed(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            return GeneralHelper.EndSessionWithTemplate("General\\page-temporarily-closed.txt", 
                new Dictionary<string, object>() {}, _session.SessionKey);
        }
    }
}