﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class NewUser : Page
    {
        public NewUser() : base() { }

        public NewUser(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            if (_session.IsUserLoaded)
            {
                //Not eligible to access this menu
                return GeneralHelper.EndSessionWithTemplate("Account\\user-already-enrolled.txt", new Dictionary<string, object>(){
                    { "mainMenuCode", ConfigurationManager.AppSettings[Constants.ConfMainMenuCode] }
                }, _session.SessionKey);
            }

            if (_session.ReferrerPhone != null)
            {
                _session.NextStep = 2;
                return GeneralHelper.DoProceedOrNot(_session.SessionKey, $"Confirm referrer phone as {_session.ReferrerPhone}");
            }

            _session.NextStep = 4;

            return StartEnrolment();
        }

        public UssdResponse Step2()
        {
            bool proceed = GeneralHelper.IsResponsePositive(_session);

            if (proceed)
            {
                return StartEnrolment();
            }

            else
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectValue(null, "Enter referrer phone number");
            }
        }

        public UssdResponse Step3()
        {
            _session.ReferrerPhone = _session.EnteredValue.Trim();
            _session.NextStep = 1;
            return Router.LoadPage();
        }

        /// <summary>
        /// Check account validity, eligibility and msisdn match
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step4()
        {
            _session.NextStep = 5;
            _session.SourceAccount = _session.EnteredValue.Trim();

            var canEnrolResponse = WebHelper.DoDuisPostRequest<UssdUser>(new
            {
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount
            }, "/accounts/CanEnrol");


            if (canEnrolResponse.response_code == Constants.DuisSuccessfulCode)
            {
                return GeneralHelper.CollectPin(null, "Select a PIN with 4 or more numbers or letters");
            }

            else if (canEnrolResponse.response_code == Constants.DuisEnrolmentClosedCode)
            {
                var enrolClosedMessage = "System is temporarily under maintenance<br/><br/>Please, retry after 15 minutes";
                return GeneralHelper.EndSessionWithMessage(enrolClosedMessage, _session.SessionKey);
            }

            else
            {
                return GeneralHelper.EndSessionWithGenericFailure(canEnrolResponse.response_code, canEnrolResponse.response_message, _session.SessionKey);
            }
        }


        public UssdResponse Step5()
        {
            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = GeneralHelper.IsPasswordValid(newPassword, _session.SourceAccount);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 6;
                _session.NewPassword = newPassword;
                return GeneralHelper.CollectPin(null, "Confirm your new PIN");
            }

            _session.NextStep = 5;
            return GeneralHelper.CollectPin($"{isPasswordValid.reason}|", "Please, enter a PIN with 4 or more numbers or letters");

        }


        public UssdResponse Step6()
        {
            _session.NextStep = 6;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                var response = WebHelper.DoDuisPostRequest<UssdUser>(new
                {
                    msisdn = _session.Msisdn,
                    password = _session.NewPasswordConfirm,
                    account_number = _session.SourceAccount,
                    msisdn_network = _session.MsisdnNetwork,
                    is_via_direct_code = _session.IsViaDirectCode,
                    referrer_phone = _session.ReferrerPhone,
                    reference_number = _session.SessionKey
                }, "/accounts");

                if (response == null)
                {
                    return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
                }

                else if (response.response_code == Constants.DuisTimeoutCode)
                {
                    return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisSuccessfulCode)
                {
                    _session.UserEnrolmentTime = DateTime.Now;

                    // redirect to the original action
                    if (_session.RedirectPageName == this.GetType().Name)
                    {
                        _session.RedirectPageName = Constants.ConstInstantBanking;
                    }

                    _session.NextStep = 1;
                    _session.DefaultAccount = _session.SourceAccount;
                    _session.NextPageName = _session.RedirectPageName;
                    return Router.LoadPage();
                }

                else if (response.response_code == Constants.DuisWeakPinCode)
                {
                    // restart PIN collection
                    _session.NextStep = 5;
                    return GeneralHelper.CollectPin(null, "Invalid PIN. Please, select a PIN with 4 or more numbers or letters");
                }

                return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
            }

            _session.NextStep = 5;
            return GeneralHelper.CollectPin(null, "Your PINs do not match. Please, select a PIN with 4 or more numbers or letters");
        }

        public UssdResponse StartEnrolment()
        {
            return new UssdResponse
            {
                Lines = TemplateHelper.ParseTemplate("Account\\start-enrolment.txt", new Dictionary<string, object>() { }),
                InputLabel = "Please enter your account number",
                Title = "Welcome to Fidelity Instant Banking"
            };
        }
    }
}