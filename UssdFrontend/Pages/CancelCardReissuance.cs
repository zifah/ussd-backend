﻿using System.Collections.Generic;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class CancelCardReissuance : Page
    {
        public CancelCardReissuance() : base() { }

        public CancelCardReissuance(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "your card", this);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            GeneralHelper.SetTransactionAccount(_session);
            string displayMessage = "Cancelling auto-renewal for your expired debit card|";
            return GeneralHelper.CollectPin(displayMessage);
        }

        public UssdResponse Step3()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var request = GetRequestPayload();

            var response = WebHelper.DoDuisPostRequest<CancelCardReissuanceResponse>(request, "/Cards/CancelReissuance");
                        
            if (response == null || response.response_code == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advert = GeneralHelper.GetAd(_session, null);

                return GeneralHelper.EndSessionWithTemplate("Cards\\card-reissuance-cancelled.txt", new Dictionary<string, object> {
                    { "account_last_four", FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.LastCharacters(request.account_number, 4) },
                    { "advert", advert }
                }, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }
            
            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }

        private CancelCardReissuanceRequest GetRequestPayload()
        {
            var request = new CancelCardReissuanceRequest
            {
                account_number = _session.SourceAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
            };

            return request;
        }
    }
}