﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class RemitaOtpGenerate : Page
    {
        private const string _remitaApp = "Remita App";
        public RemitaOtpGenerate() : base() { }

        public RemitaOtpGenerate(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, _remitaApp, this);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            GeneralHelper.SetTransactionAccount(_session);
            return GeneralHelper.CollectPin($"You are about to generate an OTP for {_remitaApp} activation");
        }

        public UssdResponse Step3()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var request = new ThirdPartyOtpRequest
            {
                is_via_direct_code = _session.IsViaDirectCode,
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                reference_number = _session.SessionKey,
                password = _session.TransactionPassword,
                purpose = _remitaApp
            };

            var response = WebHelper.DoDuisPostRequest<ThirdPartyOtpResponse>(request, "ThirdPartyOtp/Generate");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = GeneralHelper.GetAd(_session, "AirtimeRecharge");

                return GeneralHelper.EndSessionWithTemplate("ThirdPartyOtp\\successful.txt", new Dictionary<string, object> {
                    { "purpose", _remitaApp },
                    { "advert", advertContent }
                }, _session.SessionKey);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }
    }
}