﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;

namespace UssdFrontend.Pages
{
    public class Airline : Page
    {
        private const int _selectItemsPerPage = 3;
        private const string _constCategoryName = "Airline";

        public Airline() : base() { }

        public Airline(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "debit", this);
        }

        public UssdResponse Step2()
        {
            if (string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                GeneralHelper.SetTransactionAccount(_session);
            }

            // select biller
            if (SessionObject._allBillers == null || SessionObject._allBillers.Count == 0)
            {
                SessionObject._allBillers = GeneralHelper.GetAllBillers();
            }

            var airlineBillers = SessionObject._allBillers.Where(x => x.category.Equals(_constCategoryName));

            _session.NextStep = 3;
            return GeneralHelper.DoSelectItem("an airline", airlineBillers.Select(x => x.short_name).ToList());
        }

        public UssdResponse Step3()
        {
            var airlineBillers = SessionObject._allBillers.Where(x => x.category.Equals(_constCategoryName));

            try
            {
                var selectedBiller = GeneralHelper.GetSelectedItem(airlineBillers, _session);
                _session.EnteredValue = null;
                _session.SelectedBiller = GeneralHelper.GetBiller(selectedBiller.code);
                _session.NextPageName = "Billing";
                _session.NextStep = 3;
                return Router.LoadPage();
            }

            catch
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
            }
        }
    }
}