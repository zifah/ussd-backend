﻿using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Collections.Generic;
using System;

namespace UssdFrontend.Pages
{
    public class CollectRechargeType : Page
    {
        /// <summary>
        /// <para>Index 0: This phone</para>
        /// <para>Index 1: Another phone</para>
        /// </summary>
        private readonly List<string> _rechargeTypes = new List<string>()
        {
            Constants.ConfConstantThisPhone,
            Constants.ConfConstantAnotherPhone
        };

        public CollectRechargeType() : base() { }

        public CollectRechargeType(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectItem("phone to recharge", _rechargeTypes);
        }

        public UssdResponse Step2()
        {
            string resultXml = string.Empty;

            int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;

            string selectedItem = _rechargeTypes[selectedIndex];

            if (selectedItem == Constants.ConfConstantThisPhone)
            {
                _session.NextStep = 1;
                _session.NextPageName = "CollectRechargeAmount";
                return Router.LoadPage();
            }

            else if (selectedItem == Constants.ConfConstantAnotherPhone)
            {
                _session.NextStep = 1;
                _session.NextPageName = "CollectRechargePhone";
                return Router.LoadPage();
            }

            return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
        }
    }
}