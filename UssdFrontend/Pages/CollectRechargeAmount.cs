﻿using System;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Text.RegularExpressions;

namespace UssdFrontend.Pages
{
    public class CollectRechargeAmount : Page
    {
        public CollectRechargeAmount() : base() { }

        public CollectRechargeAmount(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.CollectValue(null, "Enter amount");
        }

        public UssdResponse Step2()
        {
            string resultXml = string.Empty;

            string amount = _session.EnteredValue.Trim();

            if (Regex.IsMatch(amount, "^[1-9]{1}[0-9]{0,7}$"))
            {
                _session.Amount = Convert.ToDecimal(amount);

                _session.NextStep = 1;

                switch (_session.UserType)
                {
                    case UserType.MobileMoney:
                        _session.NextPageName = "MMRecharge";
                        break;

                    case UserType.InstantBanking:
                    default:
                        _session.NextPageName = "Recharge";
                        break;
                }

                return Router.LoadPage();
            }
            
            return GeneralHelper.CollectValue($"{amount} is not a valid amount|", "Enter amount");            
        }
    }
}