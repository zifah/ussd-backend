﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Interfaces;
using UssdFrontend.Utilities;
using System.Linq;

namespace UssdFrontend.Pages
{
    public class Recharge : Page, ITransactionalPage
    {
        public Recharge() : base() { }

        public Recharge(SessionObject session) : base(session) { }

        private ThirdPartyRechargeSmsMode SmsMode
        {
            get
            {
                ThirdPartyRechargeSmsMode smsState;
                Enum.TryParse<ThirdPartyRechargeSmsMode>(ConfigurationManager.AppSettings[Constants.ConfThirdPartyRechargeSmsMode], out smsState);
                return smsState;
            }
        }

        public UssdResponse Step1()
        {

            if(Convert.ToBoolean(ConfigurationManager
                    .AppSettings[Constants.ConfRedirectRechargeToClickatell]))
            {
                string beneficiary = _session.BillingCustomerId ?? _session.Msisdn;
                bool isThirdPartyRecharge = !_session.Msisdn.Equals(beneficiary);

                var firstPartOfCode = Regex.Match(_session.ShortCode, "^\\*\\d+\\*").Value;

                string rechargeRedirectTemplate = ConfigurationManager
                    .AppSettings[Constants.ConfRechargeCodeTemplate]
                    .Split('|')
                    .First(x => x.StartsWith(firstPartOfCode))
                    .Replace("Amount", $"{_session.Amount}");

                return new UssdResponse
                {
                    RedirectInstruction = new Redirect
                    {
                        ShortCode = isThirdPartyRecharge ? 
                        rechargeRedirectTemplate.Replace("Msisdn", $"{_session.BillingCustomerId}") : 
                        rechargeRedirectTemplate.Replace("*Msisdn", "")
                    }
                };
            }

            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "debit", this);
        }

        public UssdResponse Step2()
        {
            _session.NextStep = 3;

            if (string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                GeneralHelper.SetTransactionAccount(_session);
            }

            string beneficiary = _session.BillingCustomerId ?? _session.Msisdn;
            bool isThirdPartyRecharge = !_session.Msisdn.Equals(beneficiary);

            if (isThirdPartyRecharge && (_session.NotifyBeneficiary == 1 || SmsMode == ThirdPartyRechargeSmsMode.On))
            {
                _session.SendRechargeSms = true;
                return ProcessSession();
            }

            if (isThirdPartyRecharge && SmsMode == ThirdPartyRechargeSmsMode.Optional)
            {
                string smsCost = ConfigurationManager.AppSettings[Constants.ConfSmsCost];
                return GeneralHelper.DoProceedOrNot(_session.SessionKey, string.Format("Tell beneficiary who sent them airtime (SMS costs N{0})?", smsCost));
            }

            return ProcessSession();
        }

        public UssdResponse Step3()
        {
            string beneficiary = _session.BillingCustomerId ?? _session.Msisdn;
            bool isThirdPartyRecharge = !_session.Msisdn.Equals(beneficiary);
            if (!_session.SendRechargeSms && isThirdPartyRecharge && SmsMode == ThirdPartyRechargeSmsMode.Optional)
            {
                _session.SendRechargeSms = GeneralHelper.IsResponsePositive(_session);               
            }

            if (_session.SendRechargeSms && _session.Narration == null)
            {
                // ask user for the name which the user notification should contain
                _session.NextStep = 4;
                return GeneralHelper.CollectValue("Who sent me airtime?|", "Enter your short name");
            }

            var requestContent = ComposeRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMRecharge/RequiresAuthentication" : "Recharge/RequiresAuthentication";

            var requiresPinResponse = WebHelper.DoDuisPostRequest(requestContent, endpoint);

            if (requiresPinResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _session.EnteredValue = null;
                var requiresPin = JsonConvert.DeserializeObject<bool>(requiresPinResponse.Content);

                if (requiresPin)
                {
                    _session.NextStep = 6;
                    string pinEntryMessage =
                        string.Format("Recharging {0} with N{1:N2} airtime{2}", requestContent.beneficiary_msisdn, _session.Amount,
                        _session.Narration == null ? "" : 
                        $@"||SMS name: {_session.Narration}|SMS cost: N{ConfigurationManager.AppSettings[Constants.ConfSmsCost]}|");
                    return GeneralHelper.CollectPin(pinEntryMessage);
                }

                else
                {
                    //ask for recharge confirmation if PIN is not going to be entered
                    _session.NextStep = 5;
                    string confirmationMessage = $"Confirm airtime recharge of N{_session.Amount:N2} for {requestContent.beneficiary_msisdn}";
                    return GeneralHelper.DoProceedOrNot(_session.SessionKey, confirmationMessage);
                }
            }

            return GeneralHelper.EndSessionWithMessage("Service temporarily unavailable", _session.SessionKey);            
        }


        /// <summary>
        /// Collect sender custom name
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step4()
        {
            var resultXml = string.Empty;

            if (_session.EnteredValue.Length > 20)
                return GeneralHelper.CollectValue("Name is too long", "Enter name (up to 20 characters)");

            _session.Narration = _session.EnteredValue;
            _session.NextStep = 3;
            return ProcessSession();
        }

        public UssdResponse Step5()
        {
            var resultXml = string.Empty;

            _session.NextStep = 6;

            bool proceed = GeneralHelper.IsResponsePositive(_session);

            string advertContent = GeneralHelper.GetAd(_session, "AirtimeRecharge");

            if (proceed)
            {
                return ProcessSession();
            }

            return GeneralHelper.EndSessionWithTemplate("Recharge\\cancelled.txt", new Dictionary<string, object> {
                { "advert", advertContent },
                { "mainMenuCode", ConfigurationManager.AppSettings[Constants.ConfMainMenuCode] }
                }, _session.SessionKey);
        }

        public UssdResponse Step6()
        {
            return GeneralHelper.ProcessTransaction(_session, this);
        }

        private RechargeRequest ComposeRequest()
        {
            RechargeRequest request = new RechargeRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                beneficiary_msisdn = _session.BillingCustomerId ?? _session.Msisdn,
                msisdn = _session.Msisdn,
                msisdn_network = _session.MsisdnNetwork,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
                password = _session.TransactionPassword,
                token = _session.TransactionToken,
                send_beneficiary_sms = _session.SendRechargeSms,
                sender_name = _session.Narration
            };

            return request;
        }

        public UssdResponse DoTransaction()
        {
            string resultXml = string.Empty;

            var requestContent = ComposeRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMRecharge" : "/Recharge/BuyAirtime";

            var rechargeResponse = WebHelper.DoDuisPostRequest<RechargeResponse>(requestContent, endpoint);

            if (rechargeResponse == null || rechargeResponse.response_code == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (rechargeResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = GeneralHelper.GetAd(_session, "AirtimeRecharge");

                return GeneralHelper.EndSessionWithTemplate("Recharge\\successful.txt", new Dictionary<string, object> {
                    { "beneficiary", requestContent.beneficiary_msisdn },
                    { "amount", _session.Amount.ToString("N2") },
                    { "advert", advertContent }
                }, _session.SessionKey);
            }

            else if (rechargeResponse.response_code == Constants.DuisInsufficientFundsCode)
            {
                return _session.UserType == UserType.MobileMoney ?
                    GeneralHelper.EndSessionWithMessage("Insufficient funds", _session.SessionKey) :
                    GeneralHelper.ProcessInsufficientFunds("/Recharge/GetAffordable", _session, "recharge", 6);
            }

            else if (rechargeResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (rechargeResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return _session.UserType == UserType.MobileMoney ?
                    GeneralHelper.EndSessionWithMessage("Daily limit exceeded", _session.SessionKey) :
                    GeneralHelper.ProcessLimitExceeded("/Recharge/GetLimits", _session, "recharge", 6);
            }

            else if (rechargeResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.NextStep = 6;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(rechargeResponse.response_message);
            }

            else if (rechargeResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(rechargeResponse.response_code, rechargeResponse.response_message, _session.SessionKey);
        }
    }

    public enum ThirdPartyRechargeSmsMode { Off, On, Optional }
}