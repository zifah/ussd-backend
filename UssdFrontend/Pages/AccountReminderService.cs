﻿using System.Collections.Generic;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;

namespace UssdFrontend.Pages
{
    public class AccountReminderService : Page
    {
        public AccountReminderService() : base() { }

        public AccountReminderService(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            decimal charge = GeneralHelper.GetServiceCharge(_session.Msisdn);
            string confirmationMessage = string.Format("Requesting for your account and BVN numbers. This service costs N{0:N2}|", charge);
            return GeneralHelper.CollectPin(confirmationMessage);
        }

        public UssdResponse Step2()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var serviceRequest = new ServiceRequest
            {
                account_number = _session.DefaultAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey
            };

            var serviceResponse = WebHelper.DoDuisPostRequest<MsisdnAccount>(serviceRequest, "Accounts/RememberAccount");

            if (serviceResponse == null || string.IsNullOrWhiteSpace(serviceResponse.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisSuccessfulCode)
            {
                return GeneralHelper.EndSessionWithTemplate("Service\\account-reminder.txt", new Dictionary<string, object> {
                    { "bvn", serviceResponse.bvn },
                    //TODO: Accounts should be a dictionary of: Account number, Account type or just form the list here
                    { "accounts", serviceResponse.accounts.Select(x => string.Format("{0} ({1})", x.account_number, x.account_type)) },
                }, _session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(serviceResponse.response_message);
            }

            else if (serviceResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(serviceResponse.response_code, serviceResponse.response_message, _session.SessionKey);
        }
    }
}