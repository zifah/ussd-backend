﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class RemitaPayment : Page
    {
        public RemitaPayment() : base() { }

        public RemitaPayment(SessionObject session) : base(session) { }

        /// <summary>
        /// Select payment account
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "payment", this);
        }

        /// <summary>
        /// Verify transaction
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step2()
        {
            _session.NextStep = 3;

            GeneralHelper.SetTransactionAccount(_session);

            var validationRequest = new RemitaPayRequest
            {
                rrr = _session.BillingCustomerId,
                amount = _session.Amount
            };

            var validateResponse = WebHelper.DoDuisPostRequest<RemitaPayResponse>(validationRequest, "/RemitaPayment/Validate",
                Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]));

            if (validateResponse != null && validateResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string productName = string.Format("{0} {1}",
                    validateResponse.biller_name,
                    validateResponse.payment_item_name);

                _session.Amount = validateResponse.payment_amount;

                string displayMessage = string.Format("Pay N{0:N2} for {1} #{2} ({3})",
                    _session.Amount,
                    productName,
                    _session.BillingCustomerId,
                    validateResponse.payer_name);
                
                #region Save the payment request to session
                var paymentRequest = new RemitaPayRequest
                {
                    rrr = _session.BillingCustomerId,
                    amount = _session.Amount,
                    account_number = _session.SourceAccount,
                    is_via_direct_code = _session.IsViaDirectCode,
                    msisdn = _session.Msisdn,
                    reference_number = _session.SessionKey,
                    plan_code = validateResponse.plan_code,
                    product_name = productName
                };

                _session.SelectDictionary[SessionConstants.RemitaPayRequest] = paymentRequest;
                #endregion

                return GeneralHelper.CollectPin($"{displayMessage}|");
            }

            else
            {
                string validationError = validateResponse != null && !string.IsNullOrWhiteSpace(validateResponse.response_message) ?
                    validateResponse.response_message :
                    $"We could not verify RRR #{_session.BillingCustomerId} at this time; please check and retry shortly";

                return GeneralHelper.EndSessionWithMessage(validationError, _session.SessionKey);
            }
        }


        public UssdResponse Step3()
        {
            var password = string.IsNullOrWhiteSpace(_session.EnteredValue) ? string.Empty : _session.EnteredValue.Trim();

            var paymentRequest = ComposeRequest(password);

            var remitaPaymentResponse = WebHelper.DoDuisPostRequest<RemitaPayResponse>(paymentRequest, "/RemitaPayment/Pay");

            if (remitaPaymentResponse == null || remitaPaymentResponse.response_code == null)
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisSuccessfulCode)
            {
                var message = $"Your payment of N{_session.Amount:N2} for {paymentRequest.product_name} #{_session.BillingCustomerId} was successful.";
                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);                
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.NextStep = 3;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(remitaPaymentResponse.response_message);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(remitaPaymentResponse.response_code, remitaPaymentResponse.response_message, _session.SessionKey);            
        }


        private RemitaPayRequest ComposeRequest(string password)
        {
            RemitaPayRequest request = (RemitaPayRequest)_session.SelectDictionary[SessionConstants.RemitaPayRequest];

            request.password = password;
            request.token = _session.TransactionToken;

            return request;
        }
    }
}