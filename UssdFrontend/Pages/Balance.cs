﻿using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class Balance : Page
    {
        public Balance() : base() { }

        public Balance(SessionObject session) : base(session) { }

        /// <summary>
        /// GetAccountToSelectIfApplicable
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectAccount(_session, "inquiry", this);
        }

        /// <summary>
        /// Determine enquiry account and request PIN
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step2()
        {
            UssdResponse result;
            _session.NextStep = 3;

            decimal convenienceFee = 0;

            try
            {
                GeneralHelper.SetTransactionAccount(_session);
                convenienceFee = GetConvenienceFee();

                result = GeneralHelper.CollectPin($"Balance inquiry attracts a convenience fee of N{convenienceFee:N2}|");
            }

            catch
            {
                result = GeneralHelper.EndSessionWithMessage("An error occured. Please, try again later.", _session.SessionKey);
            }

            return result;
        }

        /// <summary>
        /// Do the inquiry
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step3()
        {
            UssdResponse result;

            var password = _session.EnteredValue;

            var requestBody = new
            {
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                password = password,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };

            var enquiryResponse = WebHelper.DoDuisPostRequest<AccountEnquiryResponse>(requestBody, "/accounts/GetAccountBalance");

            if (enquiryResponse == null)
            {
                string message = TemplateHelper.ParseTemplate("General\\system-error.txt", new Dictionary<string, object>());
                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }


            else if (enquiryResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = $"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {trueservePhone}";
                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string balanceInfo = TemplateHelper.ParseTemplate("General\\view-balance.txt", new Dictionary<string, object>() {
                    {"account", enquiryResponse.account_details.masked_account_number},
                    {"available", enquiryResponse.account_details.account_balance },
                    {"cleared", enquiryResponse.account_details.cleared_balance },
                    {"uncleared", enquiryResponse.account_details.uncleared_balance },
                    {"lien", enquiryResponse.account_details.lien },
                    {"minimum", enquiryResponse.account_details.minimum_balance }
                });

                return GeneralHelper.EndSessionWithMessage(balanceInfo, _session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.NextStep = 3;
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(enquiryResponse.response_message);
            }

            else if (enquiryResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                result = GeneralHelper.RedirectToBlocked(_session);
            }

            else
            {
                result = GeneralHelper.EndSessionWithGenericFailure(enquiryResponse.response_code, enquiryResponse.response_message, _session.SessionKey);
            }

            return result;
        }

        private decimal GetConvenienceFee()
        {
            var result = WebHelper.DoDuisGetRequest<AccountEnquiryResponse>("/accounts/getbalenqcharge", new Dictionary<string, object> { { "phone", _session.Msisdn } });
            return result.amount;
        }
    }
}