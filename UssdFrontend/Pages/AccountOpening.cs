﻿using System.Collections.Generic;
using System;
using System.Configuration;
using FidelityBank.CoreLibraries.Utility.Engine;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using UssdFrontend.Dtos;

namespace UssdFrontend.Pages
{
    public class AccountOpening : Page
    {
        #region Constants
        private const string _constFirstName = "AccountOpening_FirstName";
        private const string _constLastName = "AccountOpening_LastName";
        private const string _constBirthday = "AccountOpening_Birthday";
        private const string _constBvn = "AccountOpening_Bvn";
        private const string _constEmail = "AccountOpening_Email";
        #endregion

        public AccountOpening() : base() { }

        public AccountOpening(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.DoProceedOrNot(_session.SessionKey, "Do you have a Fidelity Bank account?");
        }

        public UssdResponse Step2()
        {
            bool isResponsePositive = GeneralHelper.IsResponsePositive(_session) || _session.IsUserLoaded;

            if (isResponsePositive)
            {
                string message = TemplateHelper.ParseTemplate("AccountOpening\\existing-customer.txt", new Dictionary<string, object>());
                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }

            else
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectValue(null, "First name");
            }
        }

        public UssdResponse Step3()
        {
            string resultXml = string.Empty;
            _session.SelectDictionary.Add(_constFirstName, _session.EnteredValue.Trim().ToTitleCase());
            _session.NextStep = 4;
            return GeneralHelper.CollectValue(null, "Last name");
        }

        public UssdResponse Step4()
        {
            string resultXml = string.Empty;
            _session.SelectDictionary.Add(_constLastName, _session.EnteredValue.Trim().ToTitleCase());
            _session.NextStep = 5;
            return GeneralHelper.CollectValue(null, "Enter your email or 0 if none");
        }

        public UssdResponse Step5()
        {
            var theEmail = _session.EnteredValue.Trim().ToLower();

            bool noEmail = theEmail == "0";

            if (noEmail || ValidationUtil.IsValidEmail(theEmail))
            {
                _session.SelectDictionary.Add(_constEmail, noEmail ? null : theEmail);
                _session.NextStep = 6;

                return GeneralHelper.CollectValue(null, "DOB (DD-MM-YYYY)");
            }

            else
            {
                return GeneralHelper.CollectValue(null, "Invalid email. Enter your email or 0 if none");
            }
        }

        public UssdResponse Step6()
        {
            var dobSplit = _session.EnteredValue.Trim().Split('-');
            DateTime? birthday = null;

            try
            {
                birthday = new DateTime(Convert.ToInt32(dobSplit[2]), Convert.ToInt32(dobSplit[1]), Convert.ToInt32(dobSplit[0]));
                _session.SelectDictionary.Add(_constBirthday, birthday);
            }

            catch
            {
                return GeneralHelper.CollectValue("Incorrect date format. Please retry with specified format", "DOB (DD-MM-YYYY)");               
            }

            _session.NextStep = 7;

            string firstName = (string)_session.SelectDictionary[_constFirstName];
            string lastName = (string)_session.SelectDictionary[_constLastName];

            string instruction = $"Name: {firstName} {lastName}|DOB: {birthday:dd MMM yyyy}";

            return GeneralHelper.CollectValue(instruction, "Enter your BVN to confirm or 0 if none");
        }

        public UssdResponse Step7()
        {
            string bvn = _session.EnteredValue.Trim();
            bvn = bvn == "0" ? null : bvn;

            _session.SelectDictionary.Add(_constBvn, bvn);

            string firstName = (string)_session.SelectDictionary[_constFirstName];
            string lastName = (string)_session.SelectDictionary[_constLastName];
            string email = (string)_session.SelectDictionary[_constEmail];

            DateTime birthday = (DateTime)_session.SelectDictionary[_constBirthday];

            if (bvn == null)
            {
                return DoAccountCreation(firstName, lastName, birthday, bvn, email);
            }

            var timeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]);

            var bvnResponse = WebHelper.DoDuisGetRequest<BvnResponse>("Bvn", new Dictionary<string, object> {
                { "bvn", bvn },
                { "msisdn", _session.Msisdn },
                { "reference_number", _session.SessionKey },
            }, timeoutSeconds);

            // Compare BVN response to name and determine verdict

            if (bvnResponse == null || bvnResponse.response_code != Constants.DuisSuccessfulCode)
            {
                return GeneralHelper.EndSessionWithMessage("Sorry. We could not validate your BVN. Please, try again.", _session.SessionKey);
            }

            else if (bvnResponse == null || bvnResponse.first_name == null || bvnResponse.last_name == null ||
                bvnResponse.first_name.Trim().ToTitleCase() != firstName ||
                bvnResponse.last_name.Trim().ToTitleCase() != lastName ||
                bvnResponse.birthday != birthday ||
                bvnResponse.phone != _session.Msisdn)
            {
                return GeneralHelper.EndSessionWithMessage("The details supplied do not match the BVN. Please, try again.", _session.SessionKey);
            }

            return DoAccountCreation(firstName, lastName, birthday, bvn, email);
        }

        private UssdResponse DoAccountCreation(string firstName, string lastName, DateTime birthday, string bvn, string email)
        {
            // Do Account Creation
            var response = WebHelper.DoDuisPostRequest<OpenAccountResponse>(new OpenAccountRequest
            {
                birthday = birthday.ToString("dd MMM yyyy"),
                bvn = bvn,
                first_name = firstName,
                last_name = lastName,
                msisdn = _session.Msisdn,
                reference_number = _session.SessionKey,
                referrer_code = _session.BillingCustomerId,
                branch_code = _session.BillerCode,
                email = email,
            }, "BankAccount");


            // display result to customer (allow re-entry of PIN)
            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage("Your request is being processed. We will notify you shortly.", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                var message = TemplateHelper.ParseTemplate("AccountOpening\\successful.txt", new Dictionary<string, object> {
                    { "accountNumber", response.account_number }
                });

                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }

            else
            {
                return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
            }
        }
    }
}