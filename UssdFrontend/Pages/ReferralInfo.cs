﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class ReferralInfo : Page
    {
        public ReferralInfo() : base() { }

        public ReferralInfo(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            string confirmationMessage = TemplateHelper.ParseTemplate("Referral\\confirmation.txt", new Dictionary<string, object> { });
            return GeneralHelper.CollectPin(confirmationMessage);
        }

        public UssdResponse Step2()
        {
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var serviceRequest = new ServiceRequest
            {
                account_number = _session.DefaultAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };

            var response = WebHelper.DoDuisPostRequest<ReferralPoint>(serviceRequest, "Referral/GetPoints");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string referralCode = $"{_session.ShortCodePrefix}{_session.Msisdn}#";
                return GeneralHelper.EndSessionWithTemplate("Referral\\successful.txt", new Dictionary<string, object> {
                    { "points", response.points },
                    { "referralCode", referralCode }
                }, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                _session.FailedLoginAttempts++;
                return GeneralHelper.ShowReenterPin(response.response_message);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }
    }
}