﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;
using System.Linq;
using System.Text.RegularExpressions;

namespace UssdFrontend.Pages
{
    public class MobileMoney : Page
    {
        private readonly int _selectItemsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfMenuItemsPerPage]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');

        public MobileMoney() : base() { }

        public MobileMoney(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            var theUser = GetUser();

            if (theUser != null && theUser.response_code == Constants.DuisNotEnrolledCode)
            {
                _session.NextPageName = "MMRegister";
                return Router.LoadPage();
            }

            else if (theUser != null && theUser.response_code == Constants.DuisSuccessfulCode)
            {
                _session.UserType = UserType.MobileMoney;
                _session.IsUserLoaded = true;
                _session.DefaultAccount = GeneralHelper.GetMobileAccount(_session.Msisdn);
                return DisplayMenu(0);
            }

            return GeneralHelper.EndSessionWithMessage(Constants.ConstServiceUnavailableMessage, _session.SessionKey);
        }

        public UssdResponse Step2()
        {
            try
            {
                string enteredValue = _session.EnteredValue.Trim();
                int selectedMenuItem = Convert.ToInt32(enteredValue);

                var displayedItems = GetMenuItems(0);

                if (selectedMenuItem == 0)
                {
                    return DisplayMenu(-1);
                }

                if (displayedItems[selectedMenuItem - 1] == "Next")
                {
                    return DisplayMenu(1);
                }

                PageSelector selectedPage = DestinationPages.
                    Single(x => x.DisplayName == displayedItems[selectedMenuItem - 1]);

                _session.MenuSelectedPageName = selectedPage.PageName;

                if (selectedPage.SessionVariables.Count == 0)
                {
                    _session.NextStep = 1;
                    _session.NextPageName = selectedPage.PageName;
                    return Router.LoadPage();
                }

                else
                {
                    _session.NextStep = 3;
                    _session.NextMenuVariableToCollect = selectedPage.SessionVariables[0].Name;
                    return GeneralHelper.CollectValue(null, $"Enter {selectedPage.SessionVariables[0].DisplayName}");
                }
            }

            catch
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstInvalidSelectionMessage, _session.SessionKey);
            }
        }


        public UssdResponse Step3()
        {
            string enteredValue = _session.EnteredValue.Trim();

            PageSelector selectedPage = DestinationPages
                .Where(x => !_closedPages.Contains(x.PageName))
                .Single(x => x.PageName == _session.MenuSelectedPageName);


            var sessionObjectType = typeof(SessionObject);

            var sessionPropertyInfo = sessionObjectType.GetProperty(_session.NextMenuVariableToCollect);

            var sessionVariable = selectedPage.SessionVariables.Single(x => x.Name == _session.NextMenuVariableToCollect);

            var index = selectedPage.SessionVariables.IndexOf(sessionVariable);

            try
            {
                if (!Regex.IsMatch(enteredValue, sessionVariable.RegexValidator))
                {
                    throw new Exception();
                }

                sessionPropertyInfo.SetValue(_session, Convert.ChangeType(enteredValue, sessionPropertyInfo.PropertyType));
            }

            catch
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = sessionVariable.Name;
                return GeneralHelper.CollectValue(null, $"The {sessionVariable.DisplayName} entered was invalid. Please try again");
            }

            if (index == selectedPage.SessionVariables.Count - 1)
            {
                // redirect to the main page since all variables have been collected
                _session.NextStep = 1;
                _session.NextPageName = selectedPage.PageName;
                return Router.LoadPage();
            }

            else
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = selectedPage.SessionVariables[index + 1].Name;
                return GeneralHelper.CollectValue(null, $"Enter {selectedPage.SessionVariables[index + 1].DisplayName}");
            }
        }

        public UssdResponse DisplayMenu(int increment)
        {
            _session.NextStep = 2;
            return GeneralHelper.DoSelectItem("menu", GetMenuItems(increment));
        }

        private MMUser GetUser()
        {
            var response = WebHelper.DoDuisGetRequest<MMUser>("/mmaccounts", new Dictionary<string, object>() {
                { "msisdn", _session.Msisdn }
            });

            return response;
        }

        public List<string> GetMenuItems(int currentPageIncrement)
        {
            _session.SelectListCurrentPage += currentPageIncrement;
            int currentPage = _session.SelectListCurrentPage;
            var items = DestinationPages.Where(x => !_closedPages.Contains(x.PageName)).Select(x => x.DisplayName).ToList();
            var noOfItems = items.Count;

            var startIndex = Math.Max(0, _selectItemsPerPage * (currentPage - 1));
            var endIndex = startIndex + _selectItemsPerPage - 1;

            var result = items.Where(x => items.IndexOf(x) >= startIndex && items.IndexOf(x) <= endIndex).ToList();

            if (noOfItems - endIndex > 1)
            {
                result.Add("Next");
            }

            if (currentPage > 1)
            {
                result.Add("Previous");
            }

            return result;
        }

        private List<PageSelector> DestinationPages = new List<PageSelector>()
        {
             new PageSelector{ PageName = "MMBalance", DisplayName = "Balance",
                 SessionVariables = new List<PageSessionVariable> { } },

             new PageSelector{ PageName = "MMTransfer", DisplayName = "Transfer from Wallet",
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,7}$" }
                 }
             },

             new PageSelector{ PageName = "CollectRechargeType", DisplayName = "Airtime",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },

             new PageSelector{ PageName = "MMBilling", DisplayName = "Pay Bill",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },

             new PageSelector{ PageName = "PinChange", DisplayName = "Change PIN",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },

             new PageSelector{ PageName = "MMHistory", DisplayName = "Last three transactions",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },

             new PageSelector{ PageName = "MMCashout", DisplayName = "Cashout",
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,7}$" }
                 }
             }
        };
    }
}