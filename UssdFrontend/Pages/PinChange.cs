﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class PinChange : Page
    {
        private const string _constTitle = "Pin Change";

        public PinChange() : base() { }

        public PinChange(SessionObject session) : base(session) { }

        /// <summary>
        /// Collect current password for auth and do the password change
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.CollectPin(null, "Enter your old PIN");
        }


        public UssdResponse Step2()
        {
            _session.NextStep = 3;
            _session.TransactionPassword = _session.EnteredValue.Trim();
            return GeneralHelper.CollectPin(null, "Please, enter a new PIN with 4 or more numbers or letters");
        }


        public UssdResponse Step3()
        {
            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = GeneralHelper.IsPasswordValid(newPassword, _session.DefaultAccount, _session.UserType);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 4;
                _session.NewPassword = newPassword;
                return GeneralHelper.CollectPin(null, "Confirm your new PIN");
            }

            else
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectPin($"{isPasswordValid.reason}|", "Please, enter a new PIN with 4 or more numbers or letters");
            }
        }


        public UssdResponse Step4()
        {
            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                var response = WebHelper.DoDuisPostRequest<UssdUser>(new
                {
                    msisdn = _session.Msisdn,
                    new_password = _session.NewPasswordConfirm,
                    password = _session.TransactionPassword,
                    account_number = _session.DefaultAccount,
                    is_via_direct_code = _session.IsViaDirectCode
                }, _session.UserType == UserType.MobileMoney ? "/mmaccounts/changepassword" : "accounts/changepassword");


                if (response == null || string.IsNullOrWhiteSpace(response.response_code))
                {
                    return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
                }

                else if (response.response_code == Constants.DuisTimeoutCode)
                {
                    return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisDailyLimitExceededCode)
                {
                    return GeneralHelper.EndSessionWithMessage(GeneralHelper.GetLimitExceededMessage(), _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisSuccessfulCode)
                {
                    string advertContent = GeneralHelper.GetAd(_session, "PinChange");
                    return GeneralHelper.EndSessionWithTemplate("PinChange\\successful.txt", new Dictionary<string, object> {
                        { "advert", advertContent }
                    }, _session.SessionKey);
                }


                else if (response.response_code == Constants.DuisAccountBlockedCode)
                {
                    return GeneralHelper.RedirectToBlocked(_session);
                }

                else if (response.response_code == Constants.DuisFailedAuthenticationCode)
                {
                    return GeneralHelper.EndSessionWithMessage("Old PIN was incorrect!||We recommend that you use the Forgot PIN menu if you cannot remember your old PIN",
                        _session.SessionKey);
                }

                return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
            }

            _session.NextStep = 3;
            return GeneralHelper.CollectPin(null, "Your PINs do not match. Please, enter a new PIN with 4 or more numbers or letters");
        }
    }
}