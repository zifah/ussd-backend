﻿using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class UserInactive : Page
    {
        public UserInactive() : base() { }

        public UserInactive(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            return GeneralHelper.EndSessionWithTemplate("General\\pin-tries-exceeded.txt", new Dictionary<string, object>() {
                    {"trueservePhone", ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone]},
                    {"selfUnlockCode",
                    $"{_session.ShortCodePrefix}{ConfigurationManager.AppSettings[Constants.ConfSelfUnlockEndpointCode]}#"}
                }, _session.SessionKey);
        }
    }
}