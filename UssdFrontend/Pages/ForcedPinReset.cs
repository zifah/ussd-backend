﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class ForcedPinReset : Page
    {
        public ForcedPinReset() : base() { }

        public ForcedPinReset(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;

            int displayNameLength = 20;
            string displayName = _session.AccountName.Substring(0, Math.Min(displayNameLength, _session.AccountName.Length));
            displayName = FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.ToTitleCase(displayName);

            if (_session.AccountName.Length > displayNameLength)
            {
                displayName += "..";
            }

            return GeneralHelper.CollectValue($"Hi {displayName},||For security reasons, you need to choose a new PIN or password before you proceed|",
                "Please enter your account number");
        }

        public UssdResponse Step2()
        {
            string enteredValue = _session.EnteredValue.Trim();

            if (_session.DefaultAccount.Equals(enteredValue))
            {
                _session.SourceAccount = enteredValue;
                _session.NextStep = 3;

                // proceed to PIN selection
                return GeneralHelper.CollectPin(null, "Select a new PIN with 4 or more numbers or letters");

            }

            else
            {
                _session.FailedLoginAttempts += 1;

                int failedAccountValidationLimit = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfFailedAccountValidationLimit]);

                if (_session.FailedLoginAttempts == failedAccountValidationLimit)
                {
                    // lockout user
                    WebHelper.DoDuisPostRequest<LockoutResponse>(new LockoutRequest
                    {
                        msisdn = _session.Msisdn,
                        account_number = _session.DefaultAccount,
                        reason = string.Format("Account validation failed {0} times", failedAccountValidationLimit),
                        is_via_direct_code = _session.IsViaDirectCode
                    }, "/accounts/lock");

                    return GeneralHelper.RedirectToBlocked(_session);
                }

                else
                {
                    // display incorrect account message  
                    // allow re-entry
                    _session.NextStep = 2;
                    return GeneralHelper.CollectValue($"{enteredValue} is invalid|", "Please, enter the account you chose during sign-up");
                }
            }
        }

        /// <summary>
        /// Confirm new PIN
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step3()
        {
            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = GeneralHelper.IsPasswordValid(newPassword, _session.SourceAccount);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 4;
                _session.NewPassword = newPassword;
                return GeneralHelper.CollectPin(null, "Confirm your new PIN");
            }

            else
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectPin($"{isPasswordValid.reason}|", "Please, enter a PIN with 4 or more numbers or letters");
            }
        }

        public UssdResponse Step4()
        {
            _session.NextStep = 4;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword != _session.NewPasswordConfirm)
            {
                _session.NextStep = 3;
                return GeneralHelper.CollectPin("Your PINs do not match!|", "Enter a PIN with 4 or more numbers or letters.");
            }

            _session.PinResetToken = WebHelper.DoDuisGetRequest<Account>("/accounts", new Dictionary<string, object> { { "accountNumber", _session.DefaultAccount } }).birthday_ddMmYy;

            var response = WebHelper.DoDuisPostRequest<UssdUser>(new
            {
                msisdn = _session.Msisdn,
                password = _session.NewPasswordConfirm.Trim(),
                account_number = _session.SourceAccount,
                birthday_DdMmYy = _session.PinResetToken,
                is_via_direct_code = _session.IsViaDirectCode
            }, "/accounts/resetpassword");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                return GeneralHelper.EndSessionWithSystemError(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                return GeneralHelper.EndSessionWithMessage(Constants.ConstTransactionProcessingMessage, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                _session.RedirectNotifyMessage = "PIN reset was successful.";
                _session.NextStep = 1;
                _session.NextPageName = _session.RedirectPageName;
                return Router.LoadPage();
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                return GeneralHelper.RedirectToBlocked(_session);
            }

            return GeneralHelper.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
        }
    }
}