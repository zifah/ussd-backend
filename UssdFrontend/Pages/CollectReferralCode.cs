﻿using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    public class CollectReferralCode : Page
    {
        public CollectReferralCode() : base() { }

        public CollectReferralCode(SessionObject session) : base(session) { }

        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            return GeneralHelper.CollectValue(null, "Enter Staff Id");
        }

        public UssdResponse Step2()
        {
            string resultXml = string.Empty;
            _session.BillingCustomerId = _session.EnteredValue.Trim();
            _session.NextPageName = typeof(AccountOpening).Name;
            _session.NextStep = 1;
            return Router.LoadPage();
        }
    }
}