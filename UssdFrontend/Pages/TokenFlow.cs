﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class TokenFlow : Page
    {
        private readonly List<string> _yesOrNoToken = new List<string>() { _yesToken, _noToken };
        private const string _yesToken = "I have a token";
        private const string _noToken = "I do not have a token";

        public TokenFlow() : base() { }

        public TokenFlow(SessionObject session) : base(session) { }

        /// <summary>
        /// Decide whether or not the customer has a hard/physical token for USSD banking
        /// </summary>
        /// <returns></returns>
        public UssdResponse Step1()
        {
            _session.NextStep = 2;
            string instruction = string.Format("You need a Fidelity Hard Token to spend up to N{0:N2}", _session.Amount);

            return GeneralHelper.DoSelectItemSpecial(instruction, _yesOrNoToken);
        }


        public UssdResponse Step2()
        {
            string resultXml = string.Empty;
            bool hasToken = false;

            try
            {
                int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;
                hasToken = _yesOrNoToken[selectedIndex].Equals(_yesToken);
            }

            catch
            {
                _session.NextStep = 1;
                return Router.LoadPage();
            }

            _session.NextPageName = _session.RedirectPageName;
            _session.NextStep = _session.RedirectStepNumber;
            _session.RetryCount++;

            if (hasToken)
            {
                // show token entry page, then fire new transaction with token
                _session.TransactionRetryType = RetryType.TokenRetry;
                return GeneralHelper.CollectValue(null, "Enter your token");
            }

            // show page with options: how do I get a token; enter lower amount
            _session.TransactionRetryType = RetryType.AmountRetry;
            _session.MaxAmount = _session.MaxBeforeTokenAmount;

            if (_session.MaxAmount == 0)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"This transaction must be verified with a token. Please visit the nearest Fidelity bank branch to get your Hard Token");
                return GeneralHelper.EndSessionWithMessage(message, _session.SessionKey);
            }

            string labelText = string.Format(@"Enter an amount between N{0:N2} and N{1:N2} to continue. To do more, please visit the nearest Fidelity bank branch to get your Hard Token", _session.MinAmount, _session.MaxAmount);
            return GeneralHelper.CollectValue(string.Empty, labelText);
        }
    }
}