﻿Account balance for {{account}}:
{% if available != null %}||Available: N{{available}}{% endif %}
{% if cleared != null %}||Cleared: N{{cleared}}{% endif %}
{% if uncleared != null %}||Uncleared: N{{uncleared}}{% endif %}
{% if lien != null %}||Lien: N{{lien}}{% endif %}
{% if minimum != null %}||Minimum: N{{minimum}}{% endif %}