﻿using System.Collections.Generic;
using System.Linq;

namespace UssdFrontend.HelperObjects
{
    public class UssdResponse
    {
        public UssdResponse()
        {

        }
        /// <summary>
        /// Title > Lines > Selectable > Input label
        /// </summary>
        public string Title { set; get; }
        /// <summary>
        /// Lines are separated by a pipe (|)
        /// </summary>
        public string Lines { set; get; }
        public IList<string> Selectables { set; get; }
        public string InputLabel { set; get; }
        public Redirect RedirectInstruction { set; get; }
    }

    public class Redirect
    {
        public string ShortCode { set; get; }
        public string Url { set; get; }
    }
}