﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend
{
    public class Constants
    {
        public const string DuisFailedAuthenticationCode = "07";
        public const string DuisSuccessfulCode = "00";
        public const string DuisInsufficientFundsCode = "51";
        public const string DuisNotEnrolledCode = "05";
        public const string DuisAccountBlockedCode = "06";
        public const string DuisTimeoutCode = "98";
        public const string DuisWeakPinCode = "04";
        public const string DuisPasswordExpiredCode = "14";
        public const string DuisEnrolmentClosedCode = "97";
        public const string DuisDailyLimitExceededCode = "08";
        public const string DuisPhoneAccountMismatchCode = "01";

        public const string ConstMobileMoney = "MobileMoney";
        public const string ConstAirtel = "AIRTEL";
        public const string ConstMtn = "MTN";
        public const string ConstGlo = "GLO";
        public const string ConstEtisalat = "ETISALAT";
        public const string ConstInstantBanking = "InstantBanking";
        public const string ConstApiRoot = "api/v1";
        public const string ConstRouterUrl = "Master"; // former value was "master", I'm experimenting
        public const string ConstSessionKey = "SessionKey";
        public const string ConstInvalidSelectionMessage = "Your selection was not valid";
        public const string ConstTransactionProcessingMessage = "Transaction is being processed; you will be notified shortly";

        public static readonly string[] Networks = new string[] { ConstMtn, ConstAirtel, ConstEtisalat, ConstGlo };


        public const string ConfCustomerCarePhone = "CustomerCarePhone";
        public const string ConfShortCodePrefix = "ShortCodePrefix";
        public const string ConfShortCodePrefixes = "ShortCodePrefixes";
        public const string ConfPinResetEndpointCode = "PinResetEndpointCode";
        public const string ConfSelfUnlockEndpointCode = "SelfUnlockEndpointCode";
        public const string ConfMainMenuCode = "MainMenuCode";
        public const string ConfMenuItemsPerPage = "MenuItemsPerPage";
        public const string ConfMobileMoneyUrl = "MobileMoneyUrl";
        public const string ConfLoggingRootFolder = "LoggingRootFolder";
        public const string ConfDuisWaitTimeoutSeconds = "DuisWaitTimeoutSeconds";
        public const string ConfOwnBankTransferCode = "OwnBankTransferCode";
        public const string ConfUseNewMobileMoney = "UseNewMobileMoney";

        public const string ConfQriosEnteredValueHeader = "whoisd-ussd-message";
        public const string ConfNameEnquiryTimeoutSeconds = "NameEnquiryTimeoutSeconds";
        public const string ConfGeneralValidationTimeoutSeconds = "GeneralValidationTimeoutSeconds";
        public const string ConfSystemErrorMessage = "An error occurred, please retry shortly";
        public const string ConfClosedPages = "ClosedPages";
        /// <summary>
        /// List of menus (comma delimited) which an unregistered user can access
        /// </summary>
        public const string ConfAnonymousPages = "AnonymousPages";
        public const string ConfFailedAccountValidationLimit = "FailedAccountValidationLimit";
        public const string ConfCustomerCareName = "CustomerCareName";

        public const string ConfConstantThisPhone = "This phone";
        public const string ConfConstantAnotherPhone = "Another phone";

        public const string ConfNumberNarrationCharacters = "NumberNarrationCharacters";
        public const string ConfThirdPartyRechargeSmsMode = "ThirdPartyRechargeSmsMode";
        public const string ConfSmsCost = "SmsCost";
        public const string ConfCashoutMinimumFactor = "CashoutMinimumFactor";

        public const string ConfRechargeCodeTemplate = "RechargeCodeTemplate";
        public const string ConfRedirectRechargeToClickatell = "RedirectRechargeToClickatell";

        public const string ConfDuisRootUrl = "DuisRootUrl";
        public const string ConstInsufficientFunds = "Insufficient funds";
        public const string ConstDailyLimitExceededMessage = "Sorry. You have exceeded your daily limit!";
        public const string ConstServiceUnavailableMessage = "Service temporarily unavailable. Please, try again later";
    }
}