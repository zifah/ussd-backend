﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace UssdFrontend.HelperObjects
{
    /* 
     * REQUEST
     * <ussd>
     * <msisdn>M</msisdn>    
     * <sessionid>S</sessionid>    
     * <type>T</type>    
     * <network>N</network>* This is an optional parameter, see Appendix D for value layout    
     * <msg>MSG</msg>
     * </ussd > 
     * 
     * RESPONSE (premium node is deprecated)
     * <ussd>
     * <type>T</type>
     * <msg>MSG</msg>
     * <premium>
     * <cost>C</cost>
     * <ref>R</ref>
     * </premium>
     * </ussd>
     *    */
    [XmlRoot("ussd")]
    public class ClickatellRequest
    {
        [XmlElement("msisdn")]
        public string Phone { set; get; }

        [XmlElement("type")]
        public int Type { set; get; }

        [XmlElement("msg")]
        public string Message { set; get; }

        [XmlElement("sessionid")]
        public string SessionId { set; get; }

        [XmlElement("network")]
        public int Network { set; get; }

        public string NetworkString
        {
            get
            {
                if (Enum.IsDefined(typeof(ClickatellNetwork), Network))
                {
                    return ((ClickatellNetwork)Network).ToString();
                }

                return null;
            }
        }
    }

    /*
     * RESPONSE (premium node is deprecated)
     * <ussd>
     * <type>T</type>
     * <msg>MSG</msg>
     * <premium>
     * <cost>C</cost>
     * <ref>R</ref>
     * </premium>
     * </ussd>
     */

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("ussd")]
    public class ClickatellResponse
    {
        [XmlElement("type")]
        public int Type { set; get; }

        [XmlElement("msg")]
        public string Message { set; get; }
    }

    public enum ClickatellRequestType { REQUEST = 1, RESPONSE = 2, RELEASE = 3, TIMEOUT = 4, CHARGE = 10 }

    public enum ClickatellResponseType { RESPONSE = 2, RELEASE = 3, REDIRECT = 5 }

    public enum ClickatellNetwork { Etisalat = 1, Glo = 2, Airtel = 3, MTN = 4 }
}