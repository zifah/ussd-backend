﻿using System;
using System.Collections.Generic;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;

namespace UssdFrontend
{
    public class SessionObject
    {
        public static List<BillerDto> _allBillers;

        public SessionObject()
        {
            CreationTime = DateTime.Now;
            NextStep = 1;
            SelectDictionary = new Dictionary<string, object>();
            SelectListCurrentPage = 1;
            MMParameters = new MobileMoney();
            Banks = new List<Bank> { };
        }
        public virtual bool IsViaDirectCode { set; get; }
        /// <summary>
        /// Will be in all requests
        /// </summary>
        public virtual string Msisdn { set; get; }
        public virtual string MsisdnNetwork { get; set; }
        public virtual string PinResetToken { set; get; }
        public virtual string DefaultAccount { set; get; }
        public virtual bool IsUserActive { set; get; }
        public virtual DateTime? UserEnrolmentTime { set; get; }
        /// <summary>
        /// For use during debit/inquiry transaction
        /// </summary>
        public virtual string SourceAccount { set; get; }
        public virtual string SourceAccountMasked
        {
            get
            {
                return SourceAccount == null || SourceAccount.Length != 10 ? SourceAccount :
                    string.Format("{0}*****{1}", SourceAccount.Substring(0, 2), SourceAccount.Substring(7, 3));
            }
        }
        /// <summary>
        /// For transfers
        /// </summary>
        public virtual string DestinationAccount { set; get; }
        public virtual DateTime CreationTime { set; get; }
        /// <summary>
        /// Increment this by one on successful completion of each step
        /// </summary>
        public virtual string NextPageName { set; get; }
        public virtual int NextStep { set; get; }
        public virtual string TransactionPassword { set; get; }
        public virtual string TransactionToken { set; get; }

        /// <summary>
        /// <para>Account1, BoxedAccountObject</para>  
        /// <para>Account2, BoxedAccountObject</para>  
        /// <para>BeneficiaryBank1, BoxedBankObject</para>  
        /// <para>BeneficiaryBank2, BoxedBankObject</para>  
        /// <para>BeneficiaryBank3, BoxedbankObject</para>  
        /// </summary>
        public virtual Dictionary<string, object> SelectDictionary { set; get; }
        public virtual string SessionKey { set; get; }
        public virtual string ShortCode { set; get; }
        public virtual string ShortCodePrefix { set; get; }
        public virtual string NewPassword { set; get; }
        public virtual string NewPasswordConfirm { set; get; }
        public virtual int FailedLoginAttempts { set; get; }
        public virtual Bank SelectedBank { set; get; }
        public virtual Plan SelectedBillerPlan { set; get; }
        /// <summary>
        /// E.g. recharge phone, smartcard number, account opening referrer staff id
        /// </summary>
        public virtual string BillingCustomerId { set; get; }
        public virtual decimal Amount { set; get; }
        public virtual decimal MinAmount { set; get; }
        public virtual decimal MaxAmount { set; get; }
        public virtual decimal MaxBeforeTokenAmount { get; set; }
        public virtual int RetryCount { set; get; }
        public virtual RetryType? TransactionRetryType { set; get; }
        public virtual string ValidationName { set; get; }
        public virtual decimal ValidationAmount { set; get; }
        public virtual decimal ValidationFee { set; get; }
        public virtual Biller SelectedBiller { set; get; }
        public virtual string EnteredValue { set; get; }
        public virtual string NewInformation { set; get; }
        public virtual string NewInformationKey { set; get; }
        public virtual string CashoutPin { set; get; }
        public virtual string BillerCode { set; get; }
        public virtual string RedirectPageName { get; set; }
        public virtual int RedirectStepNumber { get; set; }
        public virtual string MenuSelectedPageName { set; get; }
        public virtual int SelectListCurrentPage { set; get; }
        public virtual int SelectItemsPerPage { set; get; }
        public virtual string NextMenuVariableToCollect { set; get; }
        /// <summary>
        /// Contains a whitespace trimmed version of the current user's account name
        /// </summary>
        public virtual string AccountName { set; get; }
        public virtual string RedirectNotifyMessage { set; get; }

        /// <summary>
        /// Collect narration for Transfer if value is '1', else don't
        ///
        /// </summary>
        public virtual int CollectNarration { set; get; }
        public virtual string Narration { set; get; }

        /// <summary>
        /// 1 for subscribe, 2 for unsubscribe
        /// </summary>
        public virtual int AlertSubscriptionAction { set; get; }

        public virtual bool SendRechargeSms { set; get; }

        public virtual int NotifyBeneficiary { set; get; }

        public virtual IList<Bank> Banks { set; get; }

        public virtual int GetNoOfSelectPages(int noOfItems, int selectItemsPerPage)
        {
            if(noOfItems % selectItemsPerPage == 0)
            {
                return noOfItems / selectItemsPerPage;
            }

            return noOfItems / selectItemsPerPage  + 1;
        }

        public bool ShouldAddNext(int currentPage, int numberOfPages)
        {
            return currentPage < numberOfPages;
        }

        public bool ShouldAddPrevious(int currentPage)
        {
            return currentPage > 1;
        }

        public bool IsTokenRetry
        {
            get
            {
                return TransactionRetryType == RetryType.TokenRetry;
            }
        }

        public bool IsAmountRetry
        {
            get
            {
                return TransactionRetryType == RetryType.AmountRetry;
            }
        }

        public MobileMoney MMParameters { set; get; }

        /// <summary>
        /// <para>True if the user who initiated this session is an existing user</para>
        /// </summary>
        public bool IsUserLoaded { get; set; }
        public string ReferrerPhone { set; get; }
        public string ReferrerCode { set; get; }
        public UserType? UserType { set; get; }
        public UssdResponse Response { set; get; }
    }

    public class SessionConstants
    {
        public const string AccountSelectPrefix = "Account";
        public const string BeneficiaryBankSelectPrefix = "BeneficiaryBank";
        public const string BillingPlanSelectPrefix = "BillingPlan";
        public const string KycUpdatableSelectPrefix = "KycUpdatable";
        public const string AlertProducts = "AlertProducts";
        public const string SelectedAlertProduct = "SelectedAlertProduct";
        public const string AccountCards = "AccountCards";
        public const string SelectedBillerCategory = "SelectedBillerCategory";
        public const string RemitaPayRequest = "RemitaPayRequest";
        public const string CardControlRequest = "CardControlRequest";
    }

    public enum RetryType { AmountRetry = 1, TokenRetry = 2 };
    public enum UserType {
        /// <summary>
        /// Leave at this (first) position as default is InstantBanking
        /// </summary>
        InstantBanking, 
        MobileMoney };

    public class MobileMoney
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }

        public string DobDdMmYyyy { get; set; }
    }
}