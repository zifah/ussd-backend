﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using UssdFrontend.Exceptions;
using UssdFrontend.HelperObjects;
using UssdFrontend.Interfaces;
using UssdFrontend.Pages;
using FidelityCoreEngine = FidelityBank.CoreLibraries.Utility.Engine;

namespace UssdFrontend.Utilities
{
    public class Router
    {
        private readonly NameValueCollection _headers = HttpContext.Current.Request.Headers;
        public static readonly string _defaultEndpoint = string.Format("{0}/{1}/{2}",
            HttpContext.Current.Request.ApplicationPath, Constants.ConstApiRoot, Constants.ConstRouterUrl).Replace("//", "/");
        private readonly string _shortCode;
        private readonly string _subscriber;
        private readonly string _subscriberNetwork;
        private const int _sessionValidityMinutes = 10;
        private static readonly FidelityCoreEngine.LoggingUtil _logger =
            new FidelityCoreEngine.LoggingUtil(ConfigurationManager.AppSettings[Constants.ConfLoggingRootFolder]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');
        internal static readonly string[] _anonymousPages = ConfigurationManager.AppSettings[Constants.ConfAnonymousPages].Split(',');
        private const string _backwardNavCharacter = ".";

        public Router()
        {

        }

        public Router(string subscriber, string subscriberNetwork, string shortCode)
        {
            _shortCode = shortCode;
            _subscriber = subscriber;
            _subscriberNetwork = subscriberNetwork;
        }

        internal UssdResponse LoadInstantBankingMenu()
        {
            var resultXml = RouteRequest(null, Constants.ConstInstantBanking);
            return resultXml;
        }

        internal UssdResponse LoadMobileMoneyMenu()
        {
            var resultXml = RouteRequest(null, Constants.ConstMobileMoney);
            return resultXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionObject"></param>
        /// <param name="nextPage"></param>
        /// <param name="sessionKey">If this is supplied, a session key will not be auto-generated for a new session</param>
        /// <returns></returns>
        public UssdResponse RouteRequest(SessionObject sessionObject, string nextPage = null, string sessionKey = null)
        {
            var httpContext = HttpContext.Current;
            var cache = httpContext.Cache;
            var isNewSession = sessionObject == null || string.IsNullOrWhiteSpace(sessionObject.SessionKey);

            string generalLogFile = string.Format("General\\{0:dd-MMM-yyyy}.txt", DateTime.Today);

            if (isNewSession)
            {
                sessionKey = sessionKey ?? string.Format("Session{0}", Guid.NewGuid().ToString());
                SetSessionKey(sessionKey);

                sessionObject = new SessionObject();

                sessionObject.SessionKey = sessionKey;
                sessionObject.ShortCode = _shortCode;
                sessionObject.Msisdn = _subscriber;
                sessionObject.MsisdnNetwork = _subscriberNetwork == null ? null : _subscriberNetwork.ToUpper();
                sessionObject.ShortCodePrefix = ConfigurationManager.AppSettings[Constants.ConfShortCodePrefix];


                #region Set shortcode prefix
                string shortCodePrefixes = ConfigurationManager.AppSettings[Constants.ConfShortCodePrefixes];

                if (!string.IsNullOrWhiteSpace(shortCodePrefixes))
                {
                    var thePrefix = shortCodePrefixes.Split(',').FirstOrDefault(x => sessionObject.ShortCode.StartsWith(x.TrimEnd('*')));
                    sessionObject.ShortCodePrefix = thePrefix == null ? sessionObject.ShortCodePrefix : thePrefix;
                }
                #endregion

                var shortCodeMinusPrefix = _shortCode.Replace(sessionObject.ShortCodePrefix, string.Empty);
                var endpoint = _shortCode == null ? new ShortCodeResult() : GetEndpoint(shortCodeMinusPrefix);

                nextPage = nextPage ?? endpoint.ClassName;

                if (nextPage == Constants.ConstMobileMoney && !_closedPages.Contains(nextPage))
                {
                    sessionObject.NextPageName = nextPage;
                }

                else if (!string.IsNullOrWhiteSpace(nextPage) && !_closedPages.Contains(nextPage))
                {
                    sessionObject.IsViaDirectCode = !string.IsNullOrWhiteSpace(endpoint.ClassName);
                    var user = GeneralHelper.GetUser(_subscriber);

                    if (user == null)
                    {
                        return GeneralHelper.EndSessionWithMessage("Service temporarily unavailable", sessionKey);
                    }

                    else if (user.response_code == Constants.DuisSuccessfulCode)
                    {
                        sessionObject.DefaultAccount = user.default_account.Trim();
                        sessionObject.IsUserActive = user.is_active;
                        sessionObject.UserEnrolmentTime = user.enrolment_time;
                        sessionObject.IsUserLoaded = true;
                        sessionObject.UserType = UserType.InstantBanking;

                        if (user.is_active)
                        {
                            if (user.should_reset_password)
                            {
                                sessionObject.AccountName = user.name == null ? null : user.name.Trim();
                                sessionObject.NextPageName = "ForcedPinReset";
                                sessionObject.RedirectPageName = nextPage;
                            }

                            else
                            {
                                sessionObject.NextPageName = nextPage;
                            }
                        }

                        else if (_anonymousPages.Contains(nextPage))
                        {
                            sessionObject.NextPageName = nextPage;
                        }

                        else
                        {
                            sessionObject.NextPageName = "UserInactive";
                        }
                    }

                    else if (_anonymousPages.Contains(nextPage))
                    {
                        sessionObject.NextPageName = nextPage;
                    }

                    else if (user.response_code == Constants.DuisNotEnrolledCode)
                    {
                        // allow unregistered users to proceed to Instant banking menu
                        if (nextPage == Constants.ConstInstantBanking)
                        {
                            sessionObject.NextPageName = nextPage;
                        }

                        else
                        {
                            sessionObject.NextPageName = "NewUser";
                            sessionObject.RedirectPageName = nextPage;
                        }
                    }

                    else
                    {
                        return GeneralHelper.EndSessionWithMessage("Service temporarily unavailable", sessionKey);
                    }

                    foreach (var pair in endpoint.KeyValuePairs)
                    {
                        var property = typeof(SessionObject).GetProperty(pair.Key);

                        if (property != null)
                        {
                            var value = Convert.ChangeType(pair.Value, property.PropertyType);
                            property.SetValue(sessionObject, value);
                        }
                    }
                }

                else
                {
                    // UNKNOWN CODE
                    sessionObject.NextPageName = _closedPages.Contains(nextPage) ? "PageClosed" : "Index";
                }

                var sessionList = new List<SessionObject>() { };
                cache.Insert(sessionKey, sessionList, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(_sessionValidityMinutes));
            }

            else
            {
                var enteredValue = sessionObject.EnteredValue;
                var suppliedSessionKey = sessionObject.SessionKey;
                SetSessionKey(suppliedSessionKey);

                var sessionList = (List<SessionObject>)cache[sessionObject.SessionKey];

                var last = sessionList.Last();

                if (enteredValue.Equals(_backwardNavCharacter) && sessionList.Count > 1)
                {
                    sessionList.Remove(last);
                    sessionObject = sessionList.Last();
                    string logFile = string.Format("Sessions\\{0:dd-MMM-yyyy}\\{1}\\{2}.txt", DateTime.Today, sessionObject.Msisdn, sessionObject.SessionKey);
                    _logger.LogMessage(string.Format("Replaying previous session step with response: \r\n {0}", JsonConvert.SerializeObject(sessionObject.Response)), logFile);
                    return sessionObject.Response;
                }

                else
                {
                    sessionObject = last;
                }

                if (sessionObject == null)
                {
                    _logger.LogMessage(httpContext.Request.RawUrl, generalLogFile);
                    return GeneralHelper.EndSessionWithMessage("Session ended abruptly!", suppliedSessionKey);
                }

                else
                {
                    sessionObject.EnteredValue = enteredValue;
                }
            }

            string sessionLogFile = string.Format("Sessions\\{0:dd-MMM-yyyy}\\{1}\\{2}.txt", DateTime.Today, sessionObject.Msisdn, sessionObject.SessionKey);

            try
            {
                string urlWithoutEnteredValue = FidelityCoreEngine
                    .StringManipulation
                    .RemoveQueryStringByKey(httpContext.Request.Url.AbsoluteUri, "enteredValue");
                _logger.LogMessage(string.Format("Starting session for request: {0}", urlWithoutEnteredValue), sessionLogFile);

                // freeze the latest session object at session start 
                var sessionList = (List<SessionObject>)cache[sessionObject.SessionKey];
                var newObject = sessionObject.Copy();  //JsonConvert.DeserializeObject<SessionObject>(serialized);
                sessionList.Add(newObject);

                var ussdResponse = LoadPage();
                _logger.LogMessage(string.Format("Ending session with response: \r\n {0}", JsonConvert.SerializeObject(ussdResponse)), sessionLogFile);
                return ussdResponse;
            }

            catch (Exception ex)
            {
                if (ex.InnerException is UssdException)
                {
                    UssdException ussdException = (UssdException)ex.InnerException;
                    return GeneralHelper.EndSessionWithMessage(string.Format("{0}{1}", 
                        ussdException.ErrorMessage, ussdException.ErrorCode == null ? null : $" ({ussdException.ErrorCode})"),
                    sessionObject.SessionKey);
                }

                _logger.LogException(ex, sessionLogFile);
                return GeneralHelper.EndSessionWithMessage("Oops; an unexpected error occured!", sessionObject.SessionKey);
            }
        }

        internal static string GetSessionKey()
        {
            return Convert.ToString(HttpContext.Current.Items[Constants.ConstSessionKey]);
        }

        private static void SetSessionKey(string sessionKey)
        {
            HttpContext.Current.Items[Constants.ConstSessionKey] = sessionKey;
        }

        /// <summary>
        /// Load the page indicated by Session.NextPageName
        /// </summary>
        /// <returns></returns>
        public static UssdResponse LoadPage()
        {
            var sessionObject = GetSessionObject();

            var pageType = Activator
                .CreateInstance(null, $"{typeof(Router).Assembly.GetName().Name}.Pages.{sessionObject.NextPageName}")
                .Unwrap()
                .GetType();

            var pageInstance = (IUssdPage)Activator.CreateInstance(pageType, sessionObject);
            // Call the handler method
            sessionObject.Response = pageInstance.ProcessSession();
            return sessionObject.Response;
        }

        public static SessionObject GetSessionObject(string sessionKey = null)
        {
            var httpContext = HttpContext.Current;

            if (sessionKey == null)
                sessionKey = GetSessionKey();

            var theList = (List<SessionObject>)httpContext.Cache.Get(sessionKey);

            if (theList != null && theList.Count > 0)
            {
                return theList.Last();
            }
            return null;
        }

        internal static void EndSession(string sessionKey)
        {
            // SAVE THE SESSION STATE TO FILE (remove entered value)
            HttpContext.Current.Cache.Remove(sessionKey);
        }

        public static ShortCodeResult GetEndpoint(string shortCodeMinusPrefix)
        {
            // *770*5050114937*1000# - funds transfer: ten_numeric_characters*number between 1 and 99 999 999 (no leading zeros)# ^[0-9]{10}*[1-9]{1}[0-9]{0,7}#$
            // *770*08033209433*1000# - recharge: eleven_numeric_characters*number between 1 and 99 999 999 (no leading zeros)#  ^[0-9]{11}*[1-9]{1}[0-9]{0,7}#$
            // *770*1000# - recharge: number between 1 and 99 999 999 (no leading zeros)# ^[1-9]{1}[0-9]{0,7}#$
            // *770*8*1000# - cash out: 8*number between 1 and 99 999 999 (no leading zeros)# ^8*[1-9]{1}[0-9]{0,7}#$
            // *770*00# - pin change: 00# ^00#$
            // *770*01# - pin reset: 01# ^01#$
            // *770*02# - Info update: 02# ^02#$
            // *770*0#  - balance enquiry: 0# ^0#$
            // *770*1946*1000# - bill payment: number between 1000 and 9999*number between 1 and 99 999 999 (no leading zeros)# ^[1-9]{1}[0-9]{3}*[1-9]{1}[0-9]{0,7}#$
            // *770*03*1# or *770*03*2# - alert subscription: 1 is subscription, 2 is subscription
            // *770*33*12345678# - alert subscription: 1 is subscription, 2 is subscription

            string regexInput = shortCodeMinusPrefix;

            var result = new ShortCodeResult
            {
                ShortCode = shortCodeMinusPrefix,
            };

            // do not rearrange indiscriminately
            IDictionary<string, string> shortCodes = new Dictionary<string, string>() {
                {"^(?!DestinationAccount*Amount#)[0-9]{10}\\*[1-9]{1}[0-9]{0,7}#$", "Transfer"},
                {"^(?!DestinationAccount*Amount*CollectNarration#)[0-9]{10}\\*[1-9]{1}[0-9]{0,7}\\*[0,1]{1}#$", "Transfer"},
                {"^(?!BillingCustomerId#)[0-9]{7}([0-9]{5})?#$", "RemitaPayment"},
                {"^(?!22*BillerCode*Amount#)22\\*[0-9]{8}\\*[1-9]{1}[0-9]{0,7}#$", "MicroPay"},
                {"^911#$", "ControlCard"}, //must be positioned over recharge due to precedence
                {"^1#$", "InstantBanking"},
                {"^2#$", "AlertOptOut"}, //must be positioned over recharge due to precedence
                {"^3#$", Constants.ConstMobileMoney}, //must be positioned over recharge due to precedence
                {"^5#$", "AccountReminderService"}, //must be positioned over recharge due to precedence
                {"^6#$", "CancelCardReissuance"}, //must be positioned over recharge due to precedence
                {"^7#$", "ReferralInfo" }, //must be positioned over recharge due to precedence
                {"^(?!8*Amount#)8\\*[1-9]{1}[0-9]{0,4}000#$", "Cashout"},
                {"^(?!8*Amount#)8\\*[1-9]{1}[0-9]{0,4}000\\*1#$", "VervePaycode"},
                {"^911\\*9#$", "SelfUnlock"},
                {"^(?!911*BillingCustomerId#)911\\*[0-9]{11}#$", "SelfBlock"},
                {"^(?!911*BillingCustomerId*TransactionPassword#)911\\*[0-9]{11}\\*[0-9]{4,10}#$", "SelfBlock"},
                {"^(?!BillingCustomerId*Amount#)[0-9]{11}\\*[1-9]{1}[0-9]{0,7}#$", "Recharge"},
                {"^(?!BillingCustomerId*Amount*NotifyBeneficiary#)[0-9]{11}\\*[1-9]{1}[0-9]{0,7}\\*1#$", "RechargeWithNotify"},
                {"^(?!Amount#)[1-9]{1}[0-9]{0,7}#$", "Recharge"},
                {"^00#$", "PinChange"},
                {"^01#$", "PinReset"},
                {"^02#$", "Kyc"},
                {"^03#$", "ContactAccountOfficer"},
                {"^04#$", "RemitaOtpGenerate"},
                {"^0#$", "Balance"},
                {"^(?!BillerCode*Amount#)[1-9]{1}[0-9]{3}\\*[1-9]{1}[0-9]{0,7}#$", "Billing"},
                {"^(?!BillerCode*BillingCustomerId*Amount#)[1-9]{1}[0-9]{3}\\*[0-9]+\\*[1-9]{1}[0-9]{0,7}#$", "Billing"},
                {"^(?!ReferrerPhone#)[0-9]{11}#$", "NewUser"},
                {"^000#$", "AccountOpening"},
                // BillerCode is the agent's SolId (or branch code)
                {"^(?!000*BillerCode#)000\\*[0-9]{3}#$", "CollectReferralCode"}
            };


            var match = shortCodes.FirstOrDefault(x => Regex.IsMatch(regexInput, x.Key));

            var doesMatchExist = !match.Equals(new KeyValuePair<string, string>());

            result.ClassName = doesMatchExist ? match.Value : string.Empty;

            if (doesMatchExist)
            {
                var kvpDictionary = new Dictionary<string, string>();
                var sections = match.Key.Split(new[] { "(?!", "#)" }, StringSplitOptions.RemoveEmptyEntries);

                if (sections.Count() == 3)
                {
                    var format = sections[1];
                    var value = regexInput.Replace("#", string.Empty);

                    var formatSplit = format.Split('*');
                    var valueSplit = value.Split('*');

                    for (int i = 0; i < formatSplit.Length; i++)
                    {
                        if (formatSplit[i] != valueSplit[i])
                        {
                            kvpDictionary.Add(formatSplit[i], valueSplit[i]);
                        }
                    }
                }
                result.KeyValuePairs = kvpDictionary;
            }

            return result;
        }

        /// <summary>
        /// <para>Assumes that the phone number supplied is in 234... format. This is the current format which Qrios provides</para>
        /// </summary>
        /// <param name="msisdn234"></param>
        /// <returns></returns>
        internal static string ConvertToElevenDigitFormat(string msisdn234)
        {
            var result = msisdn234;

            if (msisdn234.StartsWith("234") && msisdn234.Length == 13)
            {
                result = string.Format("0{0}", msisdn234.Remove(0, 3));
            }

            return result;
        }
    }

    public class ShortCodeResult
    {
        public string ShortCode { set; get; }
        public string ClassName { set; get; }
        public Dictionary<string, string> KeyValuePairs { set; get; }

        public ShortCodeResult()
        {
            KeyValuePairs = new Dictionary<string, string>();
        }
    }
}