﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;

namespace UssdFrontend.Utilities
{
    public class WebHelper
    {

        internal static T DoDuisGetRequest<T>(string endpoint, Dictionary<string, object> queryParameters, int timeoutSeconds = 0) where T : new()
        {
            var client = GetDuisClient();

            var request = new RestRequest(endpoint, Method.GET);

            foreach (var param in queryParameters)
            {
                request.AddParameter(param.Key, param.Value);
            }

            if (timeoutSeconds > 0)
            {
                request.Timeout = timeoutSeconds * 1000;
            }

            var response = client.Execute<T>(request);

            return response.Data;
        }

        internal static IRestResponse DoDuisGetRequest(string endpoint, Dictionary<string, object> queryParameters)
        {
            var client = GetDuisClient();

            var request = new RestRequest(endpoint, Method.GET);

            foreach (var param in queryParameters)
            {
                request.AddParameter(param.Key, param.Value);
            }

            var response = client.Execute(request);
            return response;
        }

        internal static T DoDuisPostRequest<T>(dynamic requestBody, string endpoint, int timeoutSeconds = 0) where T : new()
        {
            var duisClient = GetDuisClient();

            var request = new RestRequest(endpoint, Method.POST);

            request.AddJsonBody(requestBody);

            if (timeoutSeconds > 0)
            {
                request.Timeout = timeoutSeconds * 1000;
            }

            var response = duisClient.Execute<T>(request);

            return response.Data;
        }


        internal static IRestResponse DoDuisPostRequest(dynamic requestBody, string endpoint)
        {
            var duisClient = GetDuisClient();

            var request = new RestRequest(endpoint, Method.POST);

            request.AddJsonBody(requestBody);

            var response = duisClient.Execute(request);

            return response;
        }

        internal static RestSharp.RestClient GetDuisClient()
        {
            //ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
            var duisRootUrl = ConfigurationManager.AppSettings[Constants.ConfDuisRootUrl];
            RestClient client = new RestClient(duisRootUrl);
            return client;
        }
    }
}