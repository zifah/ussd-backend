﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UssdFrontend.HelperObjects;

namespace UssdFrontend.Utilities
{
    public class UIHelper
    {
        internal static UssdResponse CollectPin(string description, string inputLabel = "Enter PIN")
        {
            UssdResponse result = new UssdResponse {
                 InputLabel = inputLabel,
                  Lines = description
            } ;

            return result;
        }
    }
}