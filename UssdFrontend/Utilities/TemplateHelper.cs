﻿using DotLiquid;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace UssdFrontend.Utilities
{
    public class TemplateHelper
    {

        private static readonly string _templatesRootPath = ConfigurationManager.AppSettings["TemplatesRootPath"];

        public static string ParseTemplate(string templatePath, IDictionary<string, object> filler)
        {
            templatePath = string.Format("{0}\\{1}", _templatesRootPath, templatePath);
            var template = LoadTemplate(templatePath);

            var result = template.Render(Hash.FromDictionary(filler)).Replace("\r" ,"").Replace("\n", "");

            return result;

        }

        private static Template LoadTemplate(string filePath)
        {
            var fileContent = File.ReadAllText(filePath);
            return Template.Parse(fileContent);
        }
    }
}