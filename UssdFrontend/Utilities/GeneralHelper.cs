﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdFrontend.Dtos;
using UssdFrontend.HelperObjects;
using UssdFrontend.Interfaces;

namespace UssdFrontend.Utilities
{
    public class GeneralHelper
    {
        private const string _yes = "Yes";
        private const string _no = "No";
        private static readonly string[] _yesOrNo = new string[] { _yes, _no };


        public static UssdResponse DoProceedOrNot(string sessionKey, string instruction)
        {
            var response = DoSelectItemSpecial(instruction, _yesOrNo);
            return response;
        }

        internal static UssdResponse DoSelectItemSpecial(string instruction, IList<string> selectItems)
        {
            UssdResponse response = new UssdResponse
            {
                Lines = instruction,
                Selectables = selectItems
            };

            return response;
        }

        internal static UssdResponse DoSelectItem(string itemName, IList<string> selectItems)
        {
            return DoSelectItemSpecial($"Select {itemName}", selectItems);
        }

        /// <summary>
        /// Will default to no (negative if an invalid option is selected)
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static bool IsResponsePositive(SessionObject session)
        {
            bool isPositive = false;

            try
            {
                int index = Convert.ToInt32(session.EnteredValue.Trim()) - 1;
                isPositive = _yesOrNo[index] == _yes;
            }

            catch
            {

            }

            return isPositive;
        }

        internal static List<BillerDto> GetAllBillers()
        {
            var billers = WebHelper.DoDuisGetRequest<List<BillerDto>>("/Billing", new Dictionary<string, object>());
            return billers;
        }

        /// <summary>
        /// Gets the selected item number from the session object and returns the corresponding object from the supplied list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="theList"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        internal static T GetSelectedItem<T>(IEnumerable<T> theList, SessionObject session)
        {
            var selectedIndex = Convert.ToInt32(session.EnteredValue) - 1;

            return theList.ElementAt(selectedIndex);
        }

        /// <summary>
        /// Returns null if could not get biller details
        /// </summary>
        /// <returns></returns>
        public static Biller GetBiller(string billerCode)
        {
            Biller result = null;

            var response = WebHelper.DoDuisGetRequest<BillingResponse>("/Billing/GetBiller", new Dictionary<string, object> {
                { "billerCode", billerCode }
            });


            var billerDetails = response;

            if (billerDetails != null && billerDetails.response_code == Constants.DuisSuccessfulCode)
            {
                result = new Biller
                {
                    Code = billerCode,
                    Name = billerDetails.biller_name,
                    RequiresValidation = billerDetails.requires_id_validation,
                    Surcharge = billerDetails.surcharge,
                    Plans = billerDetails.plans, //.Select(x => new Plan { Code = x.Key, Name = x.Value }).ToList(),
                    CustomerIdDescriptor = billerDetails.customer_id_descriptor,
                    ShortName = billerDetails.biller_short_name
                };
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="senderPhone">Needed to determine the transaction charge for the user</param>
        /// <param name="destinationAccount">Needed to determine the transaction type (i.e. account or mobile) based on the account number</param>
        /// <returns></returns>
        public static Bank GetOwnBank(string senderPhone, string destinationAccount)
        {
            var response = WebHelper.DoDuisGetRequest<AccountBanks>("/MMTransfer/GetOwnBank", new Dictionary<string, object> {
                { "sender_msisdn",  senderPhone },
                { "account_number", destinationAccount }
            });

            return response != null && response.response_code == Constants.DuisSuccessfulCode ? response.banks[0] : null;
        }
        
        public static decimal GetServiceCharge(string msisdn)
        {
            decimal charge = 0;

            var response = WebHelper.DoDuisGetRequest<AccountEnquiryResponse>("/Service/GetCharge", new Dictionary<string, object> {
                { "msisdn",  msisdn}
            });

            charge = response.amount;
            //var billerDetails = response;

            //if (billerDetails != null && billerDetails.response_code == Constants.DuisSuccessfulCode)
            //{
            //    result = new Biller
            //    {
            //        Code = billerCode,
            //        Name = billerDetails.biller_name,
            //        RequiresValidation = billerDetails.requires_id_validation,
            //        Surcharge = billerDetails.surcharge,
            //        Plans = billerDetails.plans, //.Select(x => new Plan { Code = x.Key, Name = x.Value }).ToList(),
            //        CustomerIdDescriptor = billerDetails.customer_id_descriptor,
            //        ShortName = billerDetails.biller_short_name
            //    };
            //}

            return charge;
        }

        /// <summary>
        /// Throws an exception with customer friendly message in account validation fails
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="bankCode">Used in the request to the name enquiry API</param>
        /// <param name="bankName">Used in composing failure message; should be concise; short name preferred</param>
        /// <returns></returns>
        public static string GetAccountName(string accountNumber, string bankCode, string bankName)
        {
            string result = accountNumber;

            var timeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfNameEnquiryTimeoutSeconds]);

            var nameEnqOutput = WebHelper.DoDuisGetRequest<NameEnquiryResponse>("/accounts/GetAccountName", new Dictionary<string, object> {
                {"accountNumber", accountNumber},
                { "bankCodeNipV2", bankCode}
            }, timeoutSeconds);

            if (nameEnqOutput != null && nameEnqOutput.response_code == Constants.DuisSuccessfulCode)
            {
                result = nameEnqOutput.account_name;
            }

            else
            {
                throw new TimeoutException(string.Format("Sorry, we had trouble validating {0} {1} at this time. Please, retry shortly", bankName, accountNumber));
            }

            return result;
        }

        public static UssdResponse RedirectToBlocked(SessionObject session)
        {
            session.NextStep = 1;
            session.NextPageName = "UserInactive";
            return Router.LoadPage();
        }

        public static UssdResponse RedirectToPage<TPageClass>(SessionObject session, int pageNumber = 1) where TPageClass : IUssdPage
        {
            session.NextStep = pageNumber;
            session.NextPageName = typeof(TPageClass).Name;
            return Router.LoadPage();
        }

        public static UssdResponse DoMobileMoneyTransfer(SessionObject session, int transactionStep)
        {
            var result = new UssdResponse { };

            session.TransactionPassword = session.EnteredValue.Trim();

            FundsTransferRequest request = GenerateMobileTransferPayload(session);

            var response = WebHelper.DoDuisPostRequest<FundsTransferResponse>(request, "/mmtransfer");

            if (response == null)
            {
                result = EndSessionWithSystemError(session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = GetAd(session, "MMFundsTransfer");

                string message = TemplateHelper.ParseTemplate("Transfer\\transfer-done.xml", new Dictionary<string, object>() {
                    { "beneficiary_name", session.ValidationName },
                    { "advert", advertContent }
                });

                result = EndSessionWithMessage(message, session.SessionKey);
            }

            else if (response.response_code == Constants.DuisInsufficientFundsCode)
            {
                result = EndSessionWithMessage("Insufficient funds", session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                result = EndSessionWithMessage("Transaction is being processed; you will be notified shortly", session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                result = EndSessionWithMessage("You have exceeded your daily limit", session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                result = ShowReenterPin(response.response_message);
                session.NextStep = transactionStep;
                session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                result = RedirectToBlocked(session);
            }

            else
            {
                result = EndSessionWithGenericFailure(response.response_code, response.response_message, session.SessionKey);
            }

            return result;
        }

        public static UssdResponse EndSessionWithGenericFailure(string responseCode, string responseMessage, string sessionId)
        {
            var message = TemplateHelper.ParseTemplate("General\\generic-failure.txt", new Dictionary<string, object>() {
                    {"responseCode", responseCode},
                    {"responseMessage", responseMessage}
                });

            return EndSessionWithMessage(message, sessionId);
        }

        private static FundsTransferRequest GenerateMobileTransferPayload(SessionObject session)
        {
            return new FundsTransferRequest
            {
                msisdn = session.Msisdn,
                source_account = session.Msisdn,
                password = session.TransactionPassword,
                dest_account = session.DestinationAccount,
                dest_bank_name = session.SelectedBank.name_short,
                dest_bank_code = session.SelectedBank.code_v2, //_session.SelectedBank.code_v2,
                dest_bank_alpha_code = session.SelectedBank.name_short,
                reference_number = string.Format("{0}{1}", session.SessionKey, session.RetryCount == 0 ? string.Empty : "R" + session.RetryCount),
                amount = session.Amount,
                narration = session.Narration,
                is_via_direct_code = session.IsViaDirectCode

            };
        }

        public static UssdResponse ValidateCollectPinTransfer(SessionObject session, int nextStep)
        {
            session.NextStep = nextStep;
            string accountName = string.Empty;
            accountName = GetAccountName(session.DestinationAccount, session.SelectedBank.code_v2, session.SelectedBank.name_short);

            session.ValidationName = accountName;

            string validationText = string.Format(@"Transferring N{0} to {1} at {2}. This will cost N{3:N2}{4}",
                            session.Amount.ToString("N2"),
                            HttpUtility.HtmlEncode(accountName.Substring(0, Math.Min(30, accountName.Length))),
                            session.SelectedBank.name,
                            session.SelectedBank.transfer_charge,
                            session.CollectNarration == 1 ? string.Format("||Narration: {0}|", session.Narration) : string.Empty
                            );

            return CollectPin(validationText);            
        }

        public static string GetMobileAccount(string phoneNumberElevenDigits)
        {
            // The account number is the last ten digits of the phone number
            return phoneNumberElevenDigits.Substring(1, 10);
        }


        /// <summary>
        /// Ends the session and display the entered message on customer's screen
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public static UssdResponse EndSessionWithMessage(string message, string sessionId)
        {
            Router.EndSession(sessionId);

            var response = new UssdResponse
            {
                Lines = message
            };

            return response;
        }

        /// <summary>
        /// <para>The response object should not be null even if the msisdn does not match a user</para>
        /// <para>If response object is null, there is a system error at the server</para>
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        internal static UssdUser GetUser(string msisdn)
        {
            var response = WebHelper.DoDuisGetRequest<UssdUser>("/accounts/GetUser", new Dictionary<string, object> {
                { "msisdn", msisdn }
            });

            return response;
        }

        /// <summary>
        /// Check validity of password for purposes of password update (set/reset)
        /// </summary>
        /// <param name="password"></param>
        /// <param name="account"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        internal static PasswordValidationResponse IsPasswordValid(string password, string account, UserType? userType = UserType.InstantBanking)
        {
            var endpoint = userType == UserType.MobileMoney ? "/mmaccounts/ispasswordvalid" : "/accounts/ispasswordvalid";

            var response = WebHelper.DoDuisGetRequest<PasswordValidationResponse>(endpoint, new Dictionary<string, object> {
            { "password", password },
            { "account", account }
            });

            return response;
        }

        /// <summary>
        /// Error message to be displayed to user after entering PIN incorrectly
        /// </summary>
        /// <param name="loginTriesLeftString">Number of unused PIN tries for this user</param>
        /// <returns></returns>
        internal static string GetBadPinMessage(string loginTriesLeftString)
        {
            int loginTriesLeft = Convert.ToInt32(loginTriesLeftString);

            string result = string.Empty;

            if (loginTriesLeft <= 2)
            {
                result = string.Format("Incorrect PIN||You have only {0} tr{1} left until the system locks you out. Consider choosing a new PIN using the 'Forgot PIN' menu|",
                    loginTriesLeft, loginTriesLeft == 1 ? "y" : "ies");
            }

            else
            {
                result = "Incorrect PIN|";
            }

            return result;
        }


        internal static RestClient GetDuisClient()
        {
            RestClient client = new RestClient(ConfigurationManager.AppSettings["DuisRootUrl"]);
            return client;
        }

        internal static UssdResponse ProcessLimitExceeded(string endpoint, SessionObject session, string transactionAction, int nextStep)
        {
            UssdResponse result = null;

            var limit = WebHelper.DoDuisGetRequest<LimitResponse>(endpoint, new Dictionary<string, object>
                    {
                        { "msisdn", session.Msisdn },
                        { "amount", session.Amount }
                    });

            if (limit == null || limit.max_amount < 1 || limit.count_left < 1)
            {
                string message = GeneralHelper.GetLimitExceededMessage();
                result = EndSessionWithMessage(message, session.SessionKey);
            }

            else
            {
                session.MinAmount = limit.min_amount;
                session.MaxAmount = limit.max_amount;
                session.MaxBeforeTokenAmount = limit.max_before_token_amount;

                if (limit.is_limit_exceeded)
                {
                    session.NextStep = nextStep;
                    session.RetryCount += 1;

                    string displayText = string.Format("Amount is too {0}. You can {1} between N{2:N2} and  N{3:N2}<br/>",
                        limit.is_exceeded_limit_minimum ? "low" : "high", transactionAction, session.MinAmount, session.MaxAmount);

                    result = CollectValue(displayText, "Enter amount");
                    session.TransactionRetryType = RetryType.AmountRetry;
                }

                else if (limit.is_token_limit_exceeded)
                {
                    session.RedirectPageName = session.NextPageName;
                    session.RedirectStepNumber = session.NextStep;
                    session.NextStep = 1;
                    session.NextPageName = "TokenFlow";
                    result = Router.LoadPage();
                }
            }

            return result;
        }

        internal static string GetLimitExceededMessage()
        {
            string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
            string message = TemplateHelper.ParseTemplate("General\\limit-exceeded.txt", new Dictionary<string, object> {
                    { "trueserve_phone", trueservePhone }
                });
            return message;
        }

        /// <summary>
        /// Returns the ad content or null if we could not get the ad
        /// </summary>
        /// <param name="session"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        internal static string GetAd(SessionObject session, string module)
        {
            if (session.UserType == UserType.MobileMoney)
            {
                return null;
            }

            var advert = WebHelper.DoDuisGetRequest<AdvertResponse>("/Adverts", new Dictionary<string, object> {
                { "msisdn", session.Msisdn },
                { "module", module }
            });

            string content = advert == null || advert.response_code != Constants.DuisSuccessfulCode ? null : advert.content;

            return content;
        }

        /// <summary>
        /// </summary>
        /// <param name="_session"></param>
        /// <param name="accountApplication">This will be used by default to compose the prompt as follows: "Select your <accountApplication> account"</param>
        /// <param name="caller"></param>
        /// <param name="fullAccountDescription">If supplied, it will be used to prompt the customer's selection i.e. "Select <fullAccountDescription>"</param>
        /// <returns></returns>
        internal static UssdResponse DoSelectAccount(SessionObject _session, string accountApplication, IUssdPage caller, string fullAccountDescription = null)
        {
            UssdResponse result;

            var request = new RestRequest("/accounts/GetPhoneAccounts", Method.GET);

            request.AddParameter("phone", _session.Msisdn);

            var duisClient = GetDuisClient();

            var response = duisClient.Execute<MsisdnAccount>(request);

            var msisdnAccount = response.Data;

            if (msisdnAccount != null)
            {
                for (int i = 1; i <= msisdnAccount.accounts.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.AccountSelectPrefix, i), msisdnAccount.accounts[i - 1]);
                }

                if (msisdnAccount.accounts.Count > 1)
                {
                    result = new UssdResponse
                    {
                        Title = $"Select {fullAccountDescription ?? accountApplication + " account"}",
                        Selectables = msisdnAccount.accounts.Select(x => x.masked_account_number).ToList()
                    };
                }

                else
                {
                    result = caller.ProcessSession();
                }
            }

            else
            {
                result = caller.ProcessSession();
            }

            return result;
        }
        
        internal static UssdResponse ProcessTransaction(SessionObject session, ITransactionalPage caller)
        {
            UssdResponse resultXml = null;

            if (session.RetryCount > 0 && session.IsAmountRetry)
            {
                decimal amount = 0;

                Decimal.TryParse(session.EnteredValue, out amount);

                if (amount >= session.MinAmount && amount <= session.MaxAmount)
                {
                    session.Amount = amount;
                    resultXml = caller.DoTransaction();
                }

                else
                {
                    string displayText = string.Format("Please, enter an amount between N{0:N2} and  N{1:N2}|",
                        session.MinAmount, session.MaxAmount);

                    resultXml = CollectValue(displayText, "Enter amount");
                }
            }

            else if (session.RetryCount > 0 && session.IsTokenRetry)
            {
                session.TransactionToken = session.EnteredValue == null ? null : session.EnteredValue.Trim();
                resultXml = caller.DoTransaction();
            }

            else
            {
                session.TransactionPassword = session.EnteredValue == null ? null : session.EnteredValue.Trim();
                resultXml = caller.DoTransaction();
            }

            return resultXml;
        }

        internal static UssdResponse ProcessInsufficientFunds(string endpoint, SessionObject session, string transactionAction, int nextStep)
        {
            UssdResponse result;

            var affordable = WebHelper.DoDuisGetRequest<AffordableResponse>(endpoint, new Dictionary<string, object>
                    {
                        { "accountNumber", session.SourceAccount }
                    });

            if (affordable == null || affordable.max_amount < 1)
            {
                result = EndSessionWithMessage(string.Format("Insufficient funds"), session.SessionKey);
            }

            else
            {
                session.NextStep = nextStep;
                session.MinAmount = affordable.min_amount;
                session.MaxAmount = affordable.max_amount;

                string displayText = string.Format("Your account balance is insufficient for transaction. But you can still {0} between N{1:N2} and  N{2:N2}|",
                    transactionAction, session.MinAmount, session.MaxAmount);

                result = CollectValue(displayText, "Enter amount");
                session.RetryCount += 1;
                session.TransactionRetryType = RetryType.AmountRetry;
            }

            return result;
        }

        internal static UssdResponse CollectValue(string inputDescription, string inputLabel, string title = null)
        {
            return new UssdResponse
            {
                InputLabel = inputLabel,
                Lines = inputDescription,
                Title = title
            };
        }

        internal static UssdResponse CollectPin(string inputDescription, string inputLabel = "Enter PIN", string title = null)
        {
            return CollectValue(inputDescription, inputLabel, title);
        }

        internal static UssdResponse ShowReenterPin(string pinTriesLeftString)
        {
            var instruction = GetBadPinMessage(pinTriesLeftString);
            return CollectPin(instruction);
        }

        /// <summary>
        /// If account selection is invalid, use the default profile account
        /// </summary>
        /// <param name="session"></param>
        internal static void SetTransactionAccount(SessionObject session)
        {
            string txnAccount = session.DefaultAccount;

            if (!string.IsNullOrWhiteSpace(session.EnteredValue))
            {
                object selectedAccount = null;
                session.SelectDictionary.TryGetValue(string.Format("{0}{1}", SessionConstants.AccountSelectPrefix, session.EnteredValue.Trim()), out selectedAccount);
                txnAccount = (Account)selectedAccount == null ? txnAccount : ((Account)selectedAccount).account_number;
            }

            session.SourceAccount = txnAccount;
        }

        internal static UssdResponse EndSessionWithSystemError(string sessionKey)
        {
            string message = TemplateHelper.ParseTemplate("General\\system-error.txt", new Dictionary<string, object>());
            return EndSessionWithMessage(message, sessionKey);
        }

        internal static UssdResponse EndSessionWithTemplate(string templatePath, Dictionary<string, object> filler, string sessionKey)
        {
            var message = TemplateHelper.ParseTemplate(templatePath, filler);
            return EndSessionWithMessage(message, sessionKey);
        }
    }
}