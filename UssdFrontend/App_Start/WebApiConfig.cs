﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace UssdFrontend
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            var apiRoot = Constants.ConstApiRoot;

            var routes = config.Routes;
            routes.MapHttpRoute("DefaultApiWithId", apiRoot + "/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            routes.MapHttpRoute("DefaultApiWithAction", apiRoot + "/{controller}/{action}");
            routes.MapHttpRoute("DefaultApiGet", apiRoot + "/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            routes.MapHttpRoute("DefaultApiPost", apiRoot + "/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });


            GlobalConfiguration.Configuration.Formatters.XmlFormatter.UseXmlSerializer = true;
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            //config.Formatters.XmlFormatter.UseXmlSerializer = true;
        }
    }
}
