﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UssdFrontend.Startup))]
namespace UssdFrontend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
