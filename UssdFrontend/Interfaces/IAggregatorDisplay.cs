﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UssdFrontend.HelperObjects;

namespace UssdFrontend.Interfaces
{
    public interface IAggregatorDisplay
    {
        HttpResponseMessage GenerateResponse(UssdResponse responseSpecifier); 
    }
}
