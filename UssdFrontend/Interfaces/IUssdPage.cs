﻿
using UssdFrontend.HelperObjects;

namespace UssdFrontend.Interfaces
{
    public interface IUssdPage
    {
        UssdResponse ProcessSession();
    }
}