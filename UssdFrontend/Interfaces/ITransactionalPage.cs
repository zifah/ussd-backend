﻿using UssdFrontend.HelperObjects;

namespace UssdFrontend.Interfaces
{
    public interface ITransactionalPage
    {
        UssdResponse DoTransaction();
    }
}