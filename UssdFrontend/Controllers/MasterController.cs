﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UssdFrontend.HelperObjects;
using UssdFrontend.Utilities;

namespace UssdFrontend.Controllers
{

    public class MasterController : ApiController
    {
        private const string _qriosHeaderShortCode = "X-Connector-Description";

        public HttpResponseMessage Get([FromUri]QriosInput value)
        {
            UssdResponse result = null;

            if (value != null && value.protocol != null && value.protocol.ToLower() == "ussd")
            {
                // new session
                if (string.IsNullOrWhiteSpace(value.sessionKey))
                {
                    var cleanSubscriber = Router.ConvertToElevenDigitFormat(value.subscriber ?? value.abonent);
                    var shortCode = HttpContext.Current.Request.Headers[_qriosHeaderShortCode];
                    var router = new Router(cleanSubscriber, value._operator, shortCode);
                    result = router.RouteRequest(null);
                }

                else
                {
                    var router = new Router();
                    var enteredValue = HttpContext.Current.Request.Headers[Constants.ConfQriosEnteredValueHeader];
                    result = router.RouteRequest(new SessionObject { SessionKey = value.sessionKey, EnteredValue = enteredValue });
                }
            }

            return new QriosDisplay().GenerateResponse(result); //XmlHelper.GetXmlStringHttpResponse(result);
        }

        /// <summary>
        /// Instant Banking Menu; alas. We never knew Mobile Money was coming to join this club
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Menu([FromUri]QriosInput value)
        {
            UssdResponse result = null;

            if (value != null && value.protocol != null && value.protocol.ToLower() == "ussd")
            {
                var cleanSubscriber = Router.ConvertToElevenDigitFormat(value.subscriber ?? value.abonent);
                var shortCode = HttpContext.Current.Request.Headers[_qriosHeaderShortCode];
                var router = new Router(cleanSubscriber, value._operator, shortCode);
                result = router.LoadInstantBankingMenu();
            }

            return new QriosDisplay().GenerateResponse(result);
        }

        /// <summary>
        /// MMMenu means MobileMoneyMenu
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage MMMenu([FromUri]QriosInput value)
        {
            UssdResponse result = null;

            if (value != null && value.protocol != null && value.protocol.ToLower() == "ussd")
            {
                var cleanSubscriber = Router.ConvertToElevenDigitFormat(value.subscriber ?? value.abonent);
                var shortCode = HttpContext.Current.Request.Headers[_qriosHeaderShortCode];
                var router = new Router(cleanSubscriber, value._operator, shortCode);
                result = router.LoadMobileMoneyMenu();
            }

            return new QriosDisplay().GenerateResponse(result);
        }

        [HttpPost]
        public HttpResponseMessage Clickatell([FromBody]ClickatellRequest input)
        {
            UssdResponse result = null;

            if (input != null)
            {
                string sessionKey = $"Session{input.SessionId}{input.Phone}";
                var inputType = (ClickatellRequestType)input.Type;

                // new session
                if (inputType == ClickatellRequestType.REQUEST)
                {
                    var cleanSubscriber = Router.ConvertToElevenDigitFormat(input.Phone);
                    var shortCode = input.Message;
                    var router = new Router(cleanSubscriber, input.NetworkString, shortCode);
                    result = router.RouteRequest(null, null, sessionKey);
                }

                else if(inputType == ClickatellRequestType.RELEASE)
                {
                    result = GeneralHelper.EndSessionWithMessage("GOODBYE!", sessionKey);
                }

                else
                {
                    var router = new Router();
                    var enteredValue = input.Message;
                    result = router.RouteRequest(new SessionObject { SessionKey = sessionKey, EnteredValue = enteredValue });
                }
            }

            return new ClickatellDisplay().GenerateResponse(result);
        }
    }

    public class QriosInput
    {
        public string subscriber { set; get; }
        public string abonent { set; get; }
        public string protocol { set; get; }
        public string _operator { set; get; }
        public string sessionKey { set; get; }
        public string enteredValue { set; get; }
        public string page { set; get; }
    }
}
