﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using UssdFrontend.HelperObjects;
using UssdFrontend.Interfaces;
using UssdFrontend.Utilities;

namespace UssdFrontend
{
    public class QriosDisplay : IAggregatorDisplay
    {
        public HttpResponseMessage GenerateResponse(UssdResponse responseSpecifier)
        {
            // TITLE > LINES > SELECTABLE > INPUT LABEL
            if(responseSpecifier == null)
            {
                responseSpecifier = GeneralHelper.EndSessionWithSystemError(Router.GetSessionKey());
            }

            var xmlString = TemplateHelper.ParseTemplate("General\\qrios.xml", new Dictionary<string, object> {
                { "title", responseSpecifier.Title  },
                { "selectList", responseSpecifier.Selectables  },
                { "inputLabel", responseSpecifier.InputLabel  },
                { "sessionKey", Router.GetSessionKey() },
                { "endpoint", Router._defaultEndpoint  },
                { "description", responseSpecifier.Lines == null ? null : System.Web.HttpUtility.HtmlEncode(responseSpecifier.Lines).Replace("|", "<br/>")}
            });

            return new HttpResponseMessage()
            {
                Content = new StringContent(xmlString, Encoding.UTF8, "application/xml")
            };
        }
    }
}