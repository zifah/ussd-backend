﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using UssdFrontend.HelperObjects;
using UssdFrontend.Interfaces;
using UssdFrontend.Utilities;

namespace UssdFrontend
{
    public class ClickatellDisplay : IAggregatorDisplay
    {
        public HttpResponseMessage GenerateResponse(UssdResponse responseSpecifier)
        {
            if (responseSpecifier == null)
            {
                responseSpecifier = GeneralHelper.EndSessionWithSystemError(Router.GetSessionKey());
            }

            ClickatellResponse clickatellMessage = null;

            if (responseSpecifier.RedirectInstruction != null)
            {
                clickatellMessage = new ClickatellResponse
                {
                    Type = (int)ClickatellResponseType.REDIRECT,
                    Message = responseSpecifier.RedirectInstruction.ShortCode
                };
            }

            else
            {
                string message = ComposeMessage(responseSpecifier); clickatellMessage = new ClickatellResponse
                {
                    // if there are no selectables and no input label, then it's the end of the request
                    Type = (responseSpecifier.Selectables == null || responseSpecifier.Selectables.Count == 0)
                  && string.IsNullOrWhiteSpace(responseSpecifier.InputLabel) ? (int)ClickatellResponseType.RELEASE : (int)ClickatellResponseType.RESPONSE,

                    Message = message
                };
            }

            var xmlString = FidelityBank.CoreLibraries.Utility.Engine.XmlHelper.Serialize(clickatellMessage);

            return new HttpResponseMessage()
            {
                Content = new StringContent(xmlString, Encoding.UTF8, "application/xml")
            };
        }

        private string ComposeMessage(UssdResponse responseSpecifier)
        {
            // TITLE > LINES > SELECTABLE > INPUT LABEL
            string titleLine = string.IsNullOrWhiteSpace(responseSpecifier.Title) ? string.Empty : $"{responseSpecifier.Title}:\r\n";

            string lines = string.IsNullOrWhiteSpace(responseSpecifier.Lines)
                ? string.Empty : $"{responseSpecifier.Lines.Replace("|", "\r\n")}\r\n";

            string optionsLine = GenerateOptionsList(responseSpecifier.Selectables);

            string labelLine = string.IsNullOrWhiteSpace(responseSpecifier.InputLabel) ? string.Empty : $"{responseSpecifier.InputLabel}:";

            string message = $"{titleLine}{lines}{optionsLine}{labelLine}";
            return message;
        }

        private string GenerateOptionsList(IList<string> selectables)
        {
            if (selectables == null || selectables.Count == 0)
            {
                return string.Empty;
            }

            string response = string.Empty;

            for (int i = 0; i < selectables.Count; i++)
            {
                string option = selectables[i];
                // Index of an item called "Previous" and which is at the end of the list should be 0
                int serialNumber = option.Equals("Previous") && i == selectables.Count - 1 ? 0 : i + 1;
                response += $"{serialNumber}>{option}\r\n";
            }

            return response;
        }
    }
}