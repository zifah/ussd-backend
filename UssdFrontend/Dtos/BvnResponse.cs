﻿using System;
using System.Collections.Generic;
using UssdFrontend.Models;

namespace UssdFrontend.Dtos
{
    public class BvnResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public string middle_name { set; get; }
        public DateTime? birthday { set; get; }
        /// <summary>
        /// Standard format i.e. +234...
        /// </summary>
        public string phone { set; get; }
    }
}