﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class FundsTransferRequest
    {
        public string msisdn { set; get; }
        public string source_account { set; get; }
        public string password { set; get; }
        public string token { set; get; }
        public string dest_account { set; get; }
        public string dest_account_name { set; get; }
        public string dest_bank_name { set; get; }
        public string dest_bank_code { set; get; }
        public string dest_bank_alpha_code { set; get; }

        public string reference_number { set; get; }
        public decimal amount { set; get; }
        public string narration { set; get; }
        public bool is_via_direct_code { set; get; }
    }
}