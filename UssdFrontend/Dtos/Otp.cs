﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class OtpRequest
    {
        public string msisdn { set; get; }
        public string password { set; get; }
        public bool is_via_direct_code { set; get; }
        public string reference_number { set; get; }
    }

    public class OtpResponse
    {
        public string username { set; get; }
        public string otp { set; get; }
        public decimal convenience_fee { set; get; }
        public DateTime expiration_time { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}