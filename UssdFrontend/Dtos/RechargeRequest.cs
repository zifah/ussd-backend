﻿namespace UssdFrontend.Dtos
{
    public class RechargeRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string msisdn_network { set; get; }
        public string password { set; get; }
        public string token { set; get; }
        public decimal amount { set; get; }
        public string beneficiary_msisdn { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { get; set; }
        public bool send_beneficiary_sms { set; get; }
        /// <summary>
        /// The custom name of bank customer for a third-party recharge transaction
        /// </summary>
        public string sender_name { set; get; }
    }

}