﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class OpenAccountRequest
    {
        public string email { set; get; }
        public string msisdn { set; get; }
        public string reference_number { set; get; }
        public string account_number { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public string birthday { set; get; }
        public string bvn { set; get; }
        public string branch_code { set; get; }
        public string referrer_code { set; get; }
    }
}