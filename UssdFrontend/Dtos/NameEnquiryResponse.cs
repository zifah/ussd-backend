﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class NameEnquiryResponse
    {
        public string account_name { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }

    }
}