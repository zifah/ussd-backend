﻿using System.Collections.Generic;
namespace UssdFrontend.Dtos
{
    public class CardEnquiryResponse
    {
        public CardEnquiryResponse()
        {
            cards = new List<Card>();
        }

        public string account_number { set; get; }
        public List<Card> cards { set; get; }
    }
}