﻿namespace UssdFrontend.Dtos
{
    public class AlertSubscriptionResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}