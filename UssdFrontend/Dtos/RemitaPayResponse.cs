﻿namespace UssdFrontend.Dtos
{
    public class RemitaPayResponse
    {
        public RemitaPayResponse()
        {

        }

        public string response_code { set; get; }
        public string response_message { set; get; }
        public string payment_reference { set; get; }
        public decimal payment_amount { set; get; }
        public string plan_code { set; get; }
        public string payer_name { set; get; }
        public string biller_name { set; get; }
        public string payment_item_name { set; get; }
    }
}