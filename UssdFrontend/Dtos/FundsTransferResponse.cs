﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class FundsTransferResponse
    {
        public string payment_reference { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }

    }
}