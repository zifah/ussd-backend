﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{

    public class MicroPayRequest
    {
        public string msisdn { set; get; }
        public string msisdn_network { set; get; }
        public string password { set; get; }
        public string account_number { set; get; }
        public decimal amount { set; get; }
        public decimal surcharge { set; get; }
        public string reference_number { set; get; }
        public string merchant_code { set; get; }
        public string merchant_name { set; get; }
        public bool is_via_direct_code { get; set; }
        public string token { set; get; }
    }

    public class MicroPayResponse
    {

        public string response_code { set; get; }
        /// <summary>
        /// <para>For validation, the content of this field should be displayed except it is null</para>
        /// <para>In that case, a generic validation failure message should be displayed</para>
        /// </summary>
        public string response_message { set; get; }
        public string payment_reference { set; get; }
        public decimal payment_amount { set; get; }
        public decimal surcharge_amount { set; get; }
        public string biller_name { set; get; }
        public string payment_item_name { set; get; }
    }
}