﻿using System.Collections.Generic;
namespace UssdFrontend.Dtos
{
    public class CardControlResponse
    {
        public CardControlResponse()
        {

        }

        public string response_code { set; get; }
        public string response_message { set; get; }
        public string display_information { set; get; }
    }
}