﻿namespace UssdFrontend.Dtos
{
    public class Account
    {
        public string account_number { set; get; }
        public string account_name { set; get; }
        public string account_type { set; get; }
        /// <summary>
        /// Masked account number; actually
        /// </summary>
        public string masked_account_number { set; get; }

        public string account_balance { set; get; }
        public string cleared_balance { set; get; }
        public string uncleared_balance { set; get; }
        public string lien { set; get; }
        public string minimum_balance { set; get; }
        public string birthday_ddMmYy { set; get; }
    }
}