﻿namespace UssdFrontend.Dtos
{
    public class InfoUpdateRequest
    {
        public string msisdn { set; get; }
        public string password { set; get; }
        /// <summary>
        /// possible values: email, bvn, address
        /// </summary>
        public string info_type { set; get; }
        public string new_value { set; get; }
        public string reason { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { get; set; }
    }

}