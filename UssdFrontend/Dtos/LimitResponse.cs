﻿namespace UssdFrontend.Dtos
{
    public class LimitResponse
    {
        public LimitResponse()
        {

        }

        public decimal min_amount { set; get; }
        public decimal max_amount { set; get; }
        public decimal max_before_token_amount { set; get; }
        public int count_left { set; get; }
        public bool is_limit_exceeded { set; get; }
        public bool is_token_limit_exceeded { set; get; }
        public bool is_exceeded_limit_minimum { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}