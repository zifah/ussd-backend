﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class MMRegisterRequest
    {
        public string msisdn { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public string dob_DdMmYyyy { set; get; }
        public string pin { set; get; }
        public string network { set; get; }

    }
    public class MMRegisterResponse
    {
        public string responseCode { set; get; }
        public string responseMessage { set; get; }
    }
}