﻿using System;

namespace UssdFrontend.Dtos
{
    public class ThirdPartyOtpResponse
    {
        public string otp { set; get; }
        public DateTime? expiration_time { set; get; }
        public decimal convenience_fee { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}