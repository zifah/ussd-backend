﻿
namespace UssdFrontend.Dtos
{

    public class PhoneEnquiryResponse
    {
        public string msisdn { set; get; }
        public string network { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }

    }
}