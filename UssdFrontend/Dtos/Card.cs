﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class Card
    {
        public string masked_pan { set; get; }
        public string clear_pan { set; get; }
        public string card_network { set; get; }
        public string name_on_card { set; get; }
        public string display_name { set; get; }
        public string account_number { set; get; }
    }
}