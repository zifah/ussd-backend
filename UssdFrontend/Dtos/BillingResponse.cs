﻿using System.Collections.Generic;
using UssdFrontend.Models;

namespace UssdFrontend.Dtos
{
    public class BillingResponse
    {
        public BillingResponse()
        {

        }

        public string response_code { set; get; }
        public string response_message { set; get; }
        public string biller_name { set; get; }
        public string biller_short_name { set; get; }
        public string product_name { set; get; }
        public bool requires_id_validation { set; get; }
        public bool requires_plan_selection { get { return plans.Count > 0; } }

        /// <summary>
        /// key:    plan code
        /// value:  plan name: name (amount)
        /// </summary>
        public List<Plan> plans { set; get; }
        public string validation_text { set; get; }
        public decimal validation_amount { set; get; }
        public decimal surcharge { set; get; }
        public string payment_reference { set; get; }
        public string customer_id_descriptor { set; get; }
    }
}