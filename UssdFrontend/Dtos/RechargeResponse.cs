﻿namespace UssdFrontend.Dtos
{
    public class RechargeResponse
    {
        public RechargeResponse()
        {

        }

        public string response_code { set; get; }
        public string response_message { set; get; }
        public string network { set; get; }
        public string payment_reference { set; get; }
    }
}