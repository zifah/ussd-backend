﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class Biller
    {
        public string Name { set; get; }
        public string ShortName { set; get; }
        public string Code { set; get; }
        public string CustomerIdDescriptor { set; get; }
        public List<Plan> Plans { set; get; }
        public bool RequiresValidation { set; get; }
        public decimal Surcharge { set; get; }
    }
}