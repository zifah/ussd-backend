﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdFrontend.Dtos
{
    public class SelfBlockRequest
    {
        public string beneficiary_msisdn { set; get; }
        public string msisdn { set; get; }
        public string password { set; get; }
        public string reason { set; get; }
        public bool is_via_direct_code { get; set; }
    }

    public class SelfBlockResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}