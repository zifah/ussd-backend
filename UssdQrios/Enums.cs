﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios
{
    /// <summary>
    /// Arrangement of enum items should not be changed. Off is first so it can be the default value
    /// </summary>
    public enum ThirdPartyRechargeSmsMode { Off, On, Optional }
}