﻿using DotLiquid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using UssdQrios.Pages;

namespace UssdQrios.Utilities
{
    public class XmlHelper
    {
        private static readonly string _xmlPagesRootPath = ConfigurationManager.AppSettings["XmlPagesRootPath"];

        public static string ParseTemplate(string templatePath, IDictionary<string, object> filler)
        {
            templatePath = string.Format("{0}\\{1}", _xmlPagesRootPath, templatePath);
            var template = LoadTemplate(templatePath);

            var result = template.Render(Hash.FromDictionary(filler));

            return result;

        }

        private static Template LoadTemplate(string filePath)
        {
            var fileContent = File.ReadAllText(filePath);
            return Template.Parse(fileContent);
        }

        internal static string ComposeGenericFailure(string responseCode, string responseMessage)
        {
            var resultXml = XmlHelper.ParseTemplate("General\\generic-failure.xml", new Dictionary<string, object>() { 
                    {"responseCode", responseCode},
                    {"responseMessage", responseMessage}
                });

            return resultXml;
        }

        internal static HttpResponseMessage GetXmlStringHttpResponse(string xml)
        {
            var finalResponse = new HttpResponseMessage()
            {
                Content = new StringContent(xml, Encoding.UTF8, "application/xml")
            };

            return finalResponse;
        }

        internal static string ShowReenterPin(string sessionKey, Router router, string pinTriesLeftString)
        {
            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"pinDescription", router.GetBadPinMessage(pinTriesLeftString)},
                        {"sessionKey", sessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

            var result = ParseTemplate("General\\enter-pin.xml", templateDictionary);

            return result;
        }

        internal static string CollectValue(string sessionKey, string inputDescription, string inputLabel)
        {
            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"description", inputDescription},
                        {"sessionKey", sessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"inputLabel", inputLabel}
                    };

            var result = ParseTemplate("General\\enter-value.xml", templateDictionary);

            return result;
        }

        internal static string CollectPin(string sessionKey, string description, string inputLabel = "Enter PIN")
        {
            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"title", ""},                        
                        {"sessionKey", sessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", inputLabel},
                        {"pinDescription", description}
                    };

            string resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", templateDictionary);

            return resultXml;
        }

        /// <summary>
        /// <para>"Select " will be added in front of supplied selectItem value on the display</para>
        /// </summary>
        /// <param name="selectItem"></param>
        /// <param name="sessionKey"></param>
        /// <param name="selectItems"></param>
        /// <returns></returns>
        internal static string DoSelectItem(string selectItem, string sessionKey, IList<string> selectItems)
        {
            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"itemName",  selectItem},
                        {"sessionKey", sessionKey},
                        {"selectList", selectItems},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

            return XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
        }

        internal static string DoSelectItemSpecial(string instruction, string sessionKey, IList<string> selectItems)
        {
            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"instruction",  instruction},
                        {"sessionKey", sessionKey},
                        {"selectList", selectItems},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

            return XmlHelper.ParseTemplate("General\\select-item-special.xml", templateDictionary);
        }
    }
}