﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Models;
using UssdQrios.Pages;

namespace UssdQrios.Utilities
{
    public class GeneralHelper
    {
        private const string _yes = "Yes";
        private const string _no = "No";
        private static readonly string[] _yesOrNo = new string[] { _yes, _no };


        public static string DoProceedOrNot(string sessionKey, string instruction)
        {
            string resultXml = XmlHelper.DoSelectItemSpecial(instruction, sessionKey, _yesOrNo);

            return resultXml;
        }

        public static bool IsResponsePositive(SessionObject session)
        {
            bool isPositive = false;

            try
            {
                int index = Convert.ToInt32(session.EnteredValue.Trim()) - 1;
                isPositive = _yesOrNo[index] == _yes;
            }

            catch
            {

            }

            return isPositive;
        }

        internal static List<BillerDto> GetAllBillers()
        {
            var billers = Router.DoDuisGetRequest<List<BillerDto>>("/Billing", new Dictionary<string, object>());
            return billers;
        }

        /// <summary>
        /// Gets the selected item number from the session object and returns the corresponding object from the supplied list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="theList"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        internal static T GetSelectedItem<T>(IEnumerable<T> theList, SessionObject session)
        {
            var selectedIndex = Convert.ToInt32(session.EnteredValue) - 1;

            return theList.ElementAt(selectedIndex);
        }

        /// <summary>
        /// Returns null if could not get biller details
        /// </summary>
        /// <returns></returns>
        public static Biller GetBiller(string billerCode)
        {
            Biller result = null;

            var response = Router.DoDuisGetRequest<BillingResponse>("/Billing/GetBiller", new Dictionary<string, object> {
                { "billerCode", billerCode }
            });


            var billerDetails = response;

            if (billerDetails != null && billerDetails.response_code == Constants.DuisSuccessfulCode)
            {
                result = new Biller
                {
                    Code = billerCode,
                    Name = billerDetails.biller_name,
                    RequiresValidation = billerDetails.requires_id_validation,
                    Surcharge = billerDetails.surcharge,
                    Plans = billerDetails.plans, //.Select(x => new Plan { Code = x.Key, Name = x.Value }).ToList(),
                    CustomerIdDescriptor = billerDetails.customer_id_descriptor,
                    ShortName = billerDetails.biller_short_name
                };
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="senderPhone">Needed to determine the transaction charge for the user</param>
        /// <param name="destinationAccount">Needed to determine the transaction type (i.e. account or mobile) based on the account number</param>
        /// <returns></returns>
        public static Bank GetOwnBank(string senderPhone, string destinationAccount)
        {
            var response = Router.DoDuisGetRequest<AccountBanks>("/MMTransfer/GetOwnBank", new Dictionary<string, object> {
                { "sender_msisdn",  senderPhone },
                { "account_number", destinationAccount }
            });

            return response != null && response.response_code == Constants.DuisSuccessfulCode ? response.banks[0] : null;
        }

        public static decimal GetServiceCharge(string msisdn)
        {
            decimal charge = 0;

            var response = Router.DoDuisGetRequest<AccountEnquiryResponse>("/Service/GetCharge", new Dictionary<string, object> {
                { "msisdn",  msisdn}
            });

            charge = response.amount;
            //var billerDetails = response;

            //if (billerDetails != null && billerDetails.response_code == Constants.DuisSuccessfulCode)
            //{
            //    result = new Biller
            //    {
            //        Code = billerCode,
            //        Name = billerDetails.biller_name,
            //        RequiresValidation = billerDetails.requires_id_validation,
            //        Surcharge = billerDetails.surcharge,
            //        Plans = billerDetails.plans, //.Select(x => new Plan { Code = x.Key, Name = x.Value }).ToList(),
            //        CustomerIdDescriptor = billerDetails.customer_id_descriptor,
            //        ShortName = billerDetails.biller_short_name
            //    };
            //}

            return charge;
        }

        /// <summary>
        /// Throws an exception with customer friendly message in account validation fails
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="bankCode">Used in the request to the name enquiry API</param>
        /// <param name="bankName">Used in composing failure message; should be concise; short name preferred</param>
        /// <returns></returns>
        public static string GetAccountName(string accountNumber, string bankCode, string bankName)
        {
            string result = accountNumber;

            var timeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfNameEnquiryTimeoutSeconds]);

            var nameEnqOutput = Router.DoDuisGetRequest<NameEnquiryResponse>("/accounts/GetAccountName", new Dictionary<string, object> { 
                {"accountNumber", accountNumber}, {"bankCodeNipV2", bankCode}
            }, timeoutSeconds);

            if (nameEnqOutput != null && nameEnqOutput.response_code == Constants.DuisSuccessfulCode)
            {
                result = nameEnqOutput.account_name;
            }

            else
            {
                throw new TimeoutException(string.Format("Sorry, we had trouble validating {0} {1} at this time. Please, retry shortly", bankName, accountNumber));
            }

            return result;
        }

        public static string RedirectToBlocked(SessionObject session, Router router)
        {
            session.NextStep = 1;
            session.NextPageName = "UserInactive";
            return router.LoadPage();
        }

        public static string DoMobileMoneyTransfer(SessionObject session, Router router, int transactionStep)
        {
            string resultXml = string.Empty;

            session.TransactionPassword = session.EnteredValue.Trim();

            FundsTransferRequest request = GenerateMobileTransferPayload(session);

            var response = router.DoDuisPostRequest<FundsTransferResponse>(request, "/mmtransfer");

            if (response == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                router.EndSession(session.SessionKey);

            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = router.GetAd(session, "MMFundsTransfer");

                resultXml = XmlHelper.ParseTemplate("Transfer\\transfer-done.xml", new Dictionary<string, object>() { 
                    { "beneficiary_name", HttpUtility.HtmlEncode(session.ValidationName) },                    
                    { "advert", HttpUtility.HtmlEncode(advertContent) }
                });

                router.EndSession(session.SessionKey);
            }

            else if (response.response_code == Constants.DuisInsufficientFundsCode)
            {
                resultXml = router.EndSessionWithMessage("Insufficient funds", session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                resultXml = router.EndSessionWithMessage("You have exceeded your daily limit", session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(session.SessionKey, router, response.response_message);
                session.NextStep = transactionStep;
                session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(session, router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                router.EndSession(session.SessionKey);
            }

            return resultXml;
        }

        private static FundsTransferRequest GenerateMobileTransferPayload(SessionObject session)
        {
            return new FundsTransferRequest
            {
                msisdn = session.Msisdn,
                source_account = session.Msisdn,
                password = session.TransactionPassword,
                dest_account = session.DestinationAccount,
                dest_bank_name = session.SelectedBank.name_short,
                dest_bank_code = session.SelectedBank.code_v2, //_session.SelectedBank.code_v2,
                dest_bank_alpha_code = session.SelectedBank.name_short,
                reference_number = string.Format("{0}{1}", session.SessionKey, session.RetryCount == 0 ? string.Empty : "R" + session.RetryCount),
                amount = session.Amount,
                narration = session.Narration,
                is_via_direct_code = session.IsViaDirectCode

            };
        }

        public static string ValidateCollectPinTransfer(SessionObject session, int nextStep)
        {
            session.NextStep = nextStep;
            string accountName = string.Empty;
            accountName = GetAccountName(session.DestinationAccount, session.SelectedBank.code_v2, session.SelectedBank.name_short);

            session.ValidationName = accountName;

            string validationText = string.Format(@"<div>Transferring N{0} to {1} at {2}. This will cost N{3:N2}</div>{4}",
                            session.Amount.ToString("N2"),
                            HttpUtility.HtmlEncode(accountName.Substring(0, Math.Min(30, accountName.Length))),
                            session.SelectedBank.name,
                            session.SelectedBank.transfer_charge,
                            session.CollectNarration == 1 ? string.Format("<br/><div>Narration: {0}</div>", session.Narration) : string.Empty
                            );

            string resultXml = XmlHelper.CollectPin(session.SessionKey, validationText);
            return resultXml;
        }

        public static string GetMobileAccount(string phoneNumberElevenDigits)
        {
            // The account number is the last ten digits of the phone number
            return phoneNumberElevenDigits.Substring(1, 10);
        }
        
        public static string RedirectToClosed(SessionObject session, Router router)
        {
            session.NextStep = 1;
            session.NextPageName = "PageClosed";
            return router.LoadPage();
        }
    }
}