﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class ServiceRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { set; get; }
    }
}