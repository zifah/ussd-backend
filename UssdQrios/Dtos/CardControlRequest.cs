﻿namespace UssdQrios.Dtos
{
    public class CardControlRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public string masked_pan { set; get; }
        public string card_network { set; get; }
        public bool is_unblock { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { get; set; }
    }

}