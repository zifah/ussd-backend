﻿namespace UssdQrios.Dtos
{
    public class CashoutResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string paycode { set; get; }
        public string payment_reference { set; get; }
    }
}