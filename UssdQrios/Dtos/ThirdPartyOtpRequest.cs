﻿namespace UssdQrios.Dtos
{
    public class ThirdPartyOtpRequest
    {
        public string msisdn { set; get; }
        public string password { set; get; }
        public string account_number { set; get; }
        public string purpose { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { set; get; }
    }

}
