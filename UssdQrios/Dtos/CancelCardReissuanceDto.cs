﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class CancelCardReissuanceRequest
    {
        public string reference_number { set; get; }
        public bool is_via_direct_code { get; set; }

        public string account_number { set; get; }
        public string msisdn { set; get; }
        public string password { set; get; }
    }
    public class CancelCardReissuanceResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}