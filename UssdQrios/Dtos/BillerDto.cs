﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class BillerDto
    {
        public string name { set; get; }
        public string short_name { set; get; }
        public string code { set; get; }
        public string category { set; get; }
    }
}