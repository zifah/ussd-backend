﻿namespace UssdQrios.Dtos
{
    public class BillingRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public string biller_code { set; get; }
        public decimal amount { set; get; }
        public string selected_plan_code { set; get; }
        public string customer_id { set; get; }
        public string validation_text { get; set; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { get; set; }
        public string token { get; set; }
    }

}