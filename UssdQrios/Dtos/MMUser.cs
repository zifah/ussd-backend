﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class MMUser
    {
        public string msisdn { set; get; }
        public string customer_id { set; get; }
        public bool is_active { set; get; }
        public bool is_valid { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string name { set; get; }
    }
}