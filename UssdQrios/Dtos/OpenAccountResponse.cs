﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class OpenAccountResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string account_number { set; get; }
        public string customer_id { set; get; }
    }
}