﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class LockoutRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string reason { set; get; }
        public bool is_via_direct_code { get; set; }
    }

    public class LockoutResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}