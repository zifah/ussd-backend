﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class AccountEnquiryResponse
    {
        public Account account_details { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
        public decimal amount { set; get; }

    }
}