﻿namespace UssdQrios.Dtos
{
    public class AffordableResponse
    {
        public AffordableResponse()
        {

        }

        public decimal min_amount { set; get; }
        public decimal max_amount { set; get; }
        public string account_number { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}