﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class SelfUnlockRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string pan_last_four_digits { set; get; }
        public string birthday_DdMmYy { set; get; }
        public bool is_via_direct_code { get; set; }
        public string reference_number { set; get; }

        //public DateTime? birthday
        //{
        //    get
        //    {
        //        DateTime? result = null;

        //        try
        //        {
        //            var day = Convert.ToInt32(birthday_DdMmYy.Substring(0, 2));
        //            var month = Convert.ToInt32(birthday_DdMmYy.Substring(2, 2));
        //            var year = Convert.ToInt32(birthday_DdMmYy.Substring(4, 4));

        //            result = new DateTime(year, month, day);
        //        }

        //        catch
        //        {

        //        }

        //        return result;                   
        //    }
        //}
    }

    public class SelfUnlockResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}