﻿using System;
using System.Collections.Generic;
using UssdQrios.Models;

namespace UssdQrios.Dtos
{
    public class ReferralPoint
    {
        public ReferralPoint()
        {

        }

        public string response_code { set; get; }
        public string response_message { set; get; }
        public double points { set; get; }
        public DateTime expiration_date { set; get; }
        public string expiration_day { set; get; }

    }
}