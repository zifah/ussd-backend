﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class NameEnquiryResponse
    {
        public string account_name { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }

    }
}