﻿namespace UssdQrios.Dtos
{
    public class GetAccountOfficerResponse
    {
        public GetAccountOfficerResponse()
        {

        }
        public string account_officer_name { set; get; }
        public string account_officer_phone { set; get; }
        public string account_officer_email { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}