﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class AdvertRequest
    {
        public string msisdn { set; get; }
        public string module { set; get; }
    }
    public class AdvertResponse
    {
        public string content { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}