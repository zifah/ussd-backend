﻿namespace UssdQrios.Dtos
{
    public class InfoUpdateResponse
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}