﻿namespace UssdQrios.Dtos
{
    public class CashoutRequest
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public decimal amount { set; get; }
        public string transaction_pin { set; get; }
        public string withdrawal_channel { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { get; set; }
        public string token { get; set; }
    }

}