﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UssdQrios.Models;

namespace UssdQrios.Dtos
{
    public class AccountBanks
    {
        public AccountBanks()
        {
            banks = new List<Bank>();
        }

        public string account_number { set; get; }
        public List<Bank> banks { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}