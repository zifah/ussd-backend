﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UssdQrios.Models;

namespace UssdQrios.Dtos
{
    public class UssdUser
    {
        public string msisdn { set; get; }
        public string default_account { set; get; }
        public bool is_active { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
        public bool should_reset_password { set; get; }
        public string name { set; get; }
        public UserType type;
        public DateTime? enrolment_time { set; get; }
    }
}