﻿using System.Collections.Generic;
namespace UssdQrios.Dtos
{
    public class PasswordValidationResponse
    {
        public bool is_valid { set; get; }
        /// <summary>
        /// reason why the password is not valid
        /// </summary>
        public string reason { set; get; }
    }
}