﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UssdQrios.Dtos
{
    public class MsisdnAccount
    {
        public MsisdnAccount()
        {
            accounts = new List<Account>();
        }

        public string msisdn { set; get; }
        public List<Account> accounts { set; get; }
        public string bvn { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}