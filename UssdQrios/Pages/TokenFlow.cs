﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Collects phone number for third party recharge
    /// </summary>
    public class TokenFlow : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private readonly List<string> _yesOrNoToken = new List<string>() { _yesToken, _noToken };
        private const string _yesToken = "I have a token";
        private const string _noToken = "I do not have a token";

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public TokenFlow()
        {

        }

        public TokenFlow(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Decide whether or not the customer has a hard/physical token for USSD banking
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            string instruction = string.Format("You need a Fidelity Hard Token to spend up to N{0:N2}", _session.Amount);

            resultXml = XmlHelper.DoSelectItemSpecial(instruction, _session.SessionKey, _yesOrNoToken);
            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            bool hasToken = false;

            try
            {
                int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;
                hasToken = _yesOrNoToken[selectedIndex].Equals(_yesToken);
            }

            catch
            {
                _session.NextStep = 1;
                return _router.LoadPage();
            }

            _session.NextPageName = _session.RedirectPageName;
            _session.NextStep = _session.RedirectStepNumber;
            _session.RetryCount++;

            if (hasToken)
            {
                // show token entry page, then fire new transaction with token
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, "Enter your token");
                _session.TransactionRetryType = RetryType.TokenRetry;
            }

            else
            {
                // show page with options: how do I get a token; enter lower amount
                _session.TransactionRetryType = RetryType.AmountRetry;
                _session.MaxAmount = _session.MaxBeforeTokenAmount;

                if (_session.MaxAmount == 0)
                {
                    string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                    string message = string.Format(@"This transaction must be verified with a token. Please visit the nearest Fidelity bank branch to get your Hard Token");
                    resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
                }

                else
                {
                    string labelText = string.Format(@"Enter an amount between N{0:N2} and N{1:N2} to continue. To do more, please visit the nearest Fidelity bank branch to get your Hard Token", _session.MinAmount, _session.MaxAmount);
                    resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, labelText);
                }

            }

            return resultXml;
        }
    }
}