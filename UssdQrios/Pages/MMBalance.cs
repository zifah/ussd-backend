﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using System.Linq;
using UssdQrios.Dtos;
using System.Web;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MMBalance : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public MMBalance()
        {

        }

        public MMBalance(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            _session.SourceAccount = GeneralHelper.GetMobileAccount(_session.Msisdn);
            resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Format("Checking balance on {0} mobile account", _session.Msisdn));
            return resultXml;
        }

        public string Step2()
        {
            var resultXml = string.Empty;
            var password = _session.EnteredValue.Trim();

            var requestBody = new
            {
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                password = password,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };

            var request = new RestRequest("/MMAccounts/Balance", Method.POST);

            request.AddJsonBody(requestBody);

            var response = _duisClient.Execute<AccountEnquiryResponse>(request);

            var enquiryResponse = response.Data;

            if (enquiryResponse == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }


            else if (enquiryResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = string.Format("Balance on mobile account {0} is {1:N2}", _session.Msisdn, 
                    enquiryResponse.account_details.account_balance);
                _router.EndSession(_session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                var templateDictionary = new Dictionary<string, object>
                    {   
                        {"pinDescription", _router.GetBadPinMessage(enquiryResponse.response_message)},
                        {"sessionKey", _session.SessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", templateDictionary);
                _session.NextStep = 2;
                _session.FailedLoginAttempts++;
            }

            else if (enquiryResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(enquiryResponse.response_code, enquiryResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}