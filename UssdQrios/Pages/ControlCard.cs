﻿
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class ControlCard : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private readonly string[] _blockUnblock = new string[] { _block };
        private const string _block = "Block";
        private const string _unblock = "Unblock";

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public ControlCard()
        {

        }

        public ControlCard(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime ControlCard input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        /// <summary>
        /// <para>Determine if its self ControlCard or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;

            var activeCards = GetActiveCards();

            if (activeCards == null || activeCards.cards.Count == 0)
            {
                resultXml = _router.EndSessionWithMessage("Service temporarily unavailable. Please, try again later", _session.SessionKey);
            }

            else
            {
                _session.SelectDictionary.Add(SessionConstants.AccountCards, activeCards);

                if (activeCards.cards.Count == 1)
                {
                    _session.BillingCustomerId = activeCards.cards[0].masked_pan;
                    resultXml = ProcessSession();
                }

                else
                {
                    resultXml = XmlHelper.DoSelectItem("card to manage", _session.SessionKey, activeCards.cards.Select(x => x.display_name).ToList());
                }

            }

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            Card selectedCard = null;
            var accountCards = (CardEnquiryResponse)_session.SelectDictionary[SessionConstants.AccountCards];

            if (_session.BillingCustomerId == null)
            {
                int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;

                selectedCard = accountCards.cards[selectedIndex];
                _session.BillingCustomerId = selectedCard.masked_pan;
            }

            else
            {
                selectedCard = GetSelectedCard();
            }

            _session.NextStep = 3;

            if (_blockUnblock.Length == 1)
            {
                _session.EnteredValue = "1";
                resultXml = ProcessSession();
            }

            else
            {
                resultXml = XmlHelper.DoSelectItem(string.Format("action on {0}", selectedCard.display_name), _session.SessionKey, _blockUnblock);
            }

            return resultXml;
        }

        /// <summary>
        /// Ask customer for card control action: Hotlist/Unblock
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            string resultXml = string.Empty;

            int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;
            bool isHotlist = _blockUnblock[selectedIndex].Equals(_block);
            var selectedCard = GetSelectedCard();

            var payload = new CardControlRequest
            {
                account_number = selectedCard.account_number,
                masked_pan = selectedCard.masked_pan,
                msisdn = _session.Msisdn,
                reference_number = _session.SessionKey,
                is_unblock = !isHotlist,
                is_via_direct_code = _session.IsViaDirectCode,
            };

            _session.SelectDictionary[SessionConstants.CardControlRequest] = payload;

            _session.NextStep = 4;

            string displayText = string.Format("{0} {1}<br/>", isHotlist ? "Blocking" : "Unblocking", selectedCard.display_name);

            resultXml = XmlHelper.CollectPin(_session.SessionKey, displayText);
            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var requestBody = (CardControlRequest)_session.SelectDictionary[SessionConstants.CardControlRequest];
            requestBody.password = _session.TransactionPassword;

            var response = _router.DoDuisPostRequest<CardControlResponse>(requestBody, "/Cards/Control");

            // display result to customer (allow re-entry of PIN)
            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                var card = GetSelectedCard();
                resultXml = _router.EndSessionWithMessage(string.Format("Your card: {0} has been {1}ed successfully.", _session.BillingCustomerId,
                    requestBody.is_unblock ? _unblock : _block), _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                var templateDictionary = new Dictionary<string, object>
                    {   
                        {"pinDescription", _router.GetBadPinMessage(response.response_message)},
                        {"sessionKey", _session.SessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", templateDictionary);
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure( response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private Card GetSelectedCard()
        {
            var customerCards = (CardEnquiryResponse)_session.SelectDictionary[SessionConstants.AccountCards];
            var theCard = customerCards.cards.First(x => x.masked_pan.Equals(_session.BillingCustomerId));
            return theCard;
        }


        private CardEnquiryResponse GetActiveCards()
        {
            var result = Router.DoDuisGetRequest<CardEnquiryResponse>("/cards", new Dictionary<string, object> { 
                { "msisdn", _session.Msisdn },
                { "account_number", _session.DefaultAccount }
            });

            return result;
        }
    }
}