﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Collects phone number for third party recharge
    /// </summary>
    public class CollectRechargePhone : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public CollectRechargePhone()
        {

        }

        public CollectRechargePhone(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            _session.NextStep = 2;
            string resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, "Enter phone number (11-digits)");
            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            
            string phone = _session.EnteredValue.Trim();

            if(Regex.IsMatch(phone, "^[0-9]{11}$"))
            {
                _session.BillingCustomerId = phone;

                _session.NextStep = 1;
                _session.NextPageName = "CollectRechargeAmount";
                resultXml = _router.LoadPage();
            }

            else
            {
                _session.NextStep = 2;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Format("{0} is not a valid phone number<br/>", phone)
                    , "Enter phone number (11-digits)");
            }
            
            return resultXml;
        }
    }
}