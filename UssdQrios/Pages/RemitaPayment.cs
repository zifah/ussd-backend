﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class RemitaPayment : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public RemitaPayment()
        {

        }

        public RemitaPayment(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// <para>Select payment account</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 3;
            resultXml = _router.DoSelectAccount(_session, "payment", this);
            return resultXml;
        }

        /// <summary>
        /// Verify transaction
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            var resultXml = string.Empty;

            _session.NextStep = 4;

            _router.SetTransactionAccount(_session);

            var validationRequest = new RemitaPayRequest
                {
                    rrr = _session.BillingCustomerId,
                    amount = _session.Amount
                };

            var validateResponse = _router.DoDuisPostRequest<RemitaPayResponse>(validationRequest, "/RemitaPayment/Validate",
                Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]));

            if (validateResponse != null && validateResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string productName = string.Format("{0} {1}",
                    validateResponse.biller_name,
                    validateResponse.payment_item_name);

                _session.Amount = validateResponse.payment_amount;

                string displayMessage = string.Format("Pay N{0:N2} for {1} #{2} ({3})",
                    _session.Amount,
                    productName,
                    _session.BillingCustomerId,
                    validateResponse.payer_name);

                displayMessage = HttpUtility.HtmlEncode(displayMessage);

                #region Save the payment request to session
                var paymentRequest = new RemitaPayRequest
                        {
                            rrr = _session.BillingCustomerId,
                            amount = _session.Amount,
                            account_number = _session.SourceAccount,
                            is_via_direct_code = _session.IsViaDirectCode,
                            msisdn = _session.Msisdn,
                            reference_number = _session.SessionKey,
                            plan_code = validateResponse.plan_code,
                            product_name = productName
                        };

                _session.SelectDictionary[SessionConstants.RemitaPayRequest] = paymentRequest;
                #endregion

                resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Format("{0}{1}", displayMessage, "<br/>"));
            }

            else
            {
                string validationError = validateResponse != null && !string.IsNullOrWhiteSpace(validateResponse.response_message) ? 
                    validateResponse.response_message : 
                    string.Format("We could not verify RRR #{0} at this time; please check and retry shortly", _session.BillingCustomerId);
                
                resultXml = _router.EndSessionWithMessage(validationError, _session.SessionKey);
            }            

            return resultXml;
        }

        public string Step4()
        {
            var resultXml = string.Empty;

            var password = string.IsNullOrWhiteSpace(_session.EnteredValue) ? string.Empty : _session.EnteredValue.Trim();

            var paymentRequest = ComposeRequest(password);

            var remitaPaymentResponse = _router.DoDuisPostRequest<RemitaPayResponse>(paymentRequest, "/RemitaPayment/Pay");

            if (remitaPaymentResponse == null || remitaPaymentResponse.response_code == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = _router.EndSessionWithMessage(
                    string.Format(
                    "Your payment of N{0:N2} for {1} #{2} was successful.", _session.Amount, HttpUtility.HtmlEncode(paymentRequest.product_name), _session.BillingCustomerId),
                    _session.SessionKey);
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, remitaPaymentResponse.response_message);
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
            }

            else if (remitaPaymentResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(remitaPaymentResponse.response_code, remitaPaymentResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private RemitaPayRequest ComposeRequest(string password)
        {
            RemitaPayRequest request = (RemitaPayRequest)_session.SelectDictionary[SessionConstants.RemitaPayRequest];

            request.password = password;
            request.token = _session.TransactionToken;

            return request;
        }
    }
}