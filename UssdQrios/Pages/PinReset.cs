﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class PinReset : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public PinReset()
        {

        }

        public PinReset(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            _session.NextStep = 2;

            resultXml = XmlHelper.ParseTemplate("Account\\start-pin-reset.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint }
                });

            return resultXml;
        }

        /// <summary>
        /// Collect birthday in DdMmYy format
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            string resultXml = string.Empty;

            _session.NextStep = 3;

            _session.SourceAccount = _session.EnteredValue.Trim();

            resultXml = XmlHelper.ParseTemplate("Account\\pin-reset-token.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint }

                });

            return resultXml;
        }

        /// <summary>
        /// Collect new PIN
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            string resultXml = string.Empty;

            _session.NextStep = 4;

            _session.PinResetToken = _session.EnteredValue.Trim();

            resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Select a new PIN with 4 or more numbers or letters" }

                });

            return resultXml;
        }

        /// <summary>
        /// Confirm new PIN
        /// </summary>
        /// <returns></returns>
        public string Step4()
        {
            string resultXml = string.Empty;

            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = _router.IsPasswordValid(newPassword, _session.SourceAccount);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 5;
                _session.NewPassword = newPassword;

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Confirm your new PIN" }
            });
            }

            else
            {
                _session.NextStep = 4;

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Please, enter a PIN with 4 or more numbers or letters" },
                    {"pinDescription", string.Format("{0}<br/>", isPasswordValid.reason)}
                });
            }

            return resultXml;
        }

        public string Step5()
        {
            string resultXml = string.Empty;

            _session.NextStep = 5;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                var response = _router.DoDuisPostRequest<UssdUser>(new
                {
                    msisdn = _session.Msisdn,
                    password = _session.NewPasswordConfirm.Trim(),
                    account_number = _session.SourceAccount,
                    birthday_DdMmYy = _session.PinResetToken,
                    is_via_direct_code = _session.IsViaDirectCode
                }, "/accounts/resetpassword");

                if (response == null || string.IsNullOrWhiteSpace(response.response_code))
                {
                    resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                    _router.EndSession(_session.SessionKey);
                }

                else if (response.response_code == Constants.DuisDailyLimitExceededCode)
                {
                    string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                    string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                    resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisTimeoutCode)
                {
                    resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisSuccessfulCode)
                {
                    string advertContent = _router.GetAd(_session, "PinReset");

                    var failedEnrolmentMessage = XmlHelper.ParseTemplate("PinReset\\successful.xml", new Dictionary<string, object> {                  
                    { "advert", HttpUtility.HtmlEncode(advertContent) }
                });

                    resultXml = _router.EndSessionWithXml(failedEnrolmentMessage, _session.SessionKey);
                }


                else if (response.response_code == Constants.DuisAccountBlockedCode)
                {
                    resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
                }

                else
                {
                    resultXml = _router.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
                }
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinDescription", "Your PINs do not match!" },
                    {"pinLabel", "Select a PIN with 4 or more numbers or letters"}
                });

                _session.NextStep = 4;
            }

            return resultXml;
        }
    }
}