﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using System.Linq;
using UssdQrios.Dtos;
using System.Web;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MMMobileTransfer : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private readonly int _selectItemsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfMenuItemsPerPage]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');

        private static List<string> _segments = new List<string> { 
            "A-E", "F-J", "K-O", "P-T", "U-Z"
        };

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public MMMobileTransfer()
        {

        }

        public MMMobileTransfer(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Select mobile money scheme
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            // fetch MMO from server
            string resultXml = string.Empty;

            if (_session.SelectedBank == null)
            {
                // A-E, F-J, K-O, P-T, U-Z
                _session.NextStep = 2;
                resultXml = XmlHelper.DoSelectItem("Mobile Money", _session.SessionKey, _segments);
            }

            else
            {
                _session.NextStep = 3;
                resultXml = _router.LoadPage();
            }


            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            _session.NextStep = 3;

            try
            {
                string enteredValue = _session.EnteredValue.Trim();
                int selectedIndex = Convert.ToInt32(enteredValue) - 1;

                _session.Banks = GetMMOs().OrderBy(x => x.name).ToList();

                if(_session.Banks.Count == 0)
                {
                    return _router.EndSessionWithMessage("No mobile money operators were found. Please try again later.", _session.SessionKey);
                }

                var selectedSegment = _segments[selectedIndex];
                var range = selectedSegment.Split('-');
                var bottomChar = range[0][0];
                var topChar = range[1][0];

                var banksToDisplay = _session.Banks.Where(x =>
                {
                    var firstNameCharacter = x.name.ToUpper()[0];
                    return firstNameCharacter >= bottomChar && firstNameCharacter <= topChar;
                }).ToList();

                _session.Banks = banksToDisplay;

                resultXml = XmlHelper.DoSelectItem("Mobile Money", _session.SessionKey, _session.Banks.Select(x => x.name).ToList());
            }

            catch
            {
                resultXml = XmlHelper.DoSelectItemSpecial("Invalid selection. Select Mobile Money", _session.SessionKey, _segments);
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;
            _session.NextStep = 4;

            if (_session.SelectedBank == null)
            {
                _session.SelectedBank = GeneralHelper.GetSelectedItem(_session.Banks, _session);
            }

            string confirmation = HttpUtility.HtmlEncode(string.Format("Transferring N{0} to {1} on {2}", _session.Amount, _session.DestinationAccount, _session.SelectedBank.name));

            resultXml = XmlHelper.CollectPin(_session.SessionKey, confirmation);

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = GeneralHelper.DoMobileMoneyTransfer(_session, _router, 4);
            return resultXml;
        }

        private IList<Bank> GetMMOs()
        {
            var accountBanks = 
                Router.DoDuisGetRequest<AccountBanks>("/mmtransfer/getmmos", new Dictionary<string, object> { { "sender_msisdn", _session.Msisdn } });

            return accountBanks == null || accountBanks.response_code != Constants.DuisSuccessfulCode ? new List<Bank> { } : accountBanks.banks;
        }
    }
}