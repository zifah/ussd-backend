﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class Cashout : IUssdPage
    {
        private const string _constCashoutChannelsList = "CashoutChannelsList";
        /// <summary>
        /// The selected channel
        /// </summary>
        private const string _constCashoutChannel = "CashoutChannel";

        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public Cashout()
        {

        }

        public Cashout(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime Cashout input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        /// <summary>
        /// <para>Determine if its self Cashout or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "debit", this);
            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            _router.SetTransactionAccount(_session);
            _session.NextStep = 3;

            decimal cashoutMinimum = Convert.ToDecimal(ConfigurationManager.AppSettings[Constants.ConfCashoutMinimumFactor]);

            if (_session.Amount < cashoutMinimum)
            {
                resultXml = XmlHelper.CollectValue(_session.SessionKey, null, string.Format("Enter amount in multiples of {0}", cashoutMinimum));
            }

            else
            {
                resultXml = ProcessSession();
            }

            return resultXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            var resultXml = string.Empty;
            _session.NextStep = 34;

            decimal cashoutMinimum = Convert.ToDecimal(ConfigurationManager.AppSettings[Constants.ConfCashoutMinimumFactor]);

            if (_session.Amount < cashoutMinimum)
            {
                try
                {
                    var amount = Convert.ToDecimal(_session.EnteredValue.Trim());

                    if (amount < cashoutMinimum || amount % cashoutMinimum != 0)
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    _session.Amount = amount;
                }

                catch
                {
                    _session.NextStep = 3;
                    resultXml = XmlHelper.CollectValue(_session.SessionKey, "Amount is not valid",
                        string.Format("Enter amount in multiples of {0}", cashoutMinimum));
                    return resultXml;
                }
            }

            resultXml = CollectCashoutPin(false);

            return resultXml;
        }


        public string Step34()
        {
            string resultXml = string.Empty;

            var cashoutPin = _session.EnteredValue.Trim();

            if (cashoutPin.All(char.IsDigit) && cashoutPin.Length == 4)
            {
                _session.CashoutPin = cashoutPin;
                _session.NextStep = 4;
                resultXml = ProcessSession();
            }

            else
            {
                resultXml = CollectCashoutPin(true);
            }

            return resultXml;
        }

        /// <summary>
        /// Save cashout PIN and collect user PIN
        /// </summary>
        /// <returns></returns>
        public string Step4()
        {
            string resultXml = string.Empty;
            
            _session.NextStep = 5;
            resultXml = XmlHelper.CollectValue(_session.SessionKey,
                string.Format("<div>Cardless withdrawal of NGN {0:N2}</div>", _session.Amount), "Enter PIN");

            return resultXml;
        }

        public string Step5()
        {
            var resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var requestBody = GetSessionCashoutRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMCashout" : "/Cashout";

            var response = _router.DoDuisPostRequest<CashoutResponse>(requestBody, endpoint);

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = _router.GetAd(_session, "Cashout");

                resultXml = XmlHelper.ParseTemplate("Cashout\\successful.xml", new Dictionary<string, object> {
                    { "paycode", response.paycode },
                    { "advert", HttpUtility.HtmlEncode(advertContent) },
                    { "channel", HttpUtility.HtmlEncode(requestBody.withdrawal_channel) }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, response.response_message);
                _session.NextStep = 5;
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = XmlHelper.ParseTemplate("General\\pin-tries-exceeded.xml", new Dictionary<string, object>() {
                    {"trueservePhone", ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone]}
                });

                _router.EndSession(_session.SessionKey);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private string CollectCashoutPin(bool isRetry)
        {
            var resultXml = string.Empty;
            _session.NextStep = 34;
            string customerIdCollectionText = "Enter a one-time 4-digit PIN for your cardless withdrawal";
            resultXml = XmlHelper.CollectValue(_session.SessionKey,
                isRetry ? "One-time PIN must be a 4-digit numeric code" : string.Empty, customerIdCollectionText);
            return resultXml;
        }

        private CashoutRequest GetSessionCashoutRequest()
        {
            var CashoutRequest = new CashoutRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey,
                transaction_pin = _session.CashoutPin,
                is_via_direct_code = _session.IsViaDirectCode,
                token = _session.TransactionToken,
                withdrawal_channel = "ATM"
            };

            return CashoutRequest;
        }
    }
}