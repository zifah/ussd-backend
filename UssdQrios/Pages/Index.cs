﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class Index : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public Index()
        {

        }

        public Index(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            bool useNewMobileMoney = false;

            Boolean.TryParse(ConfigurationManager.AppSettings[Constants.ConfUseNewMobileMoney], out useNewMobileMoney);

            string mobileMoneyUrl = useNewMobileMoney ? string.Format("{0}/MMMenu", Router._defaultEndpoint) : 
                ConfigurationManager.AppSettings[Constants.ConfMobileMoneyUrl];

            string duisMenuUrl = string.Format("{0}/Menu", Router._defaultEndpoint);

            resultXml = XmlHelper.ParseTemplate("General\\main-menu.xml", new Dictionary<string, object>() { 
                    {"duisMenuUrl", duisMenuUrl },
                    {"mobileMoneyUrl", mobileMoneyUrl }

                });

            _router.EndSession(_session.SessionKey);

            return resultXml;
        }
    }
}