﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class Kyc : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private const int _selectItemsPerPage = 3;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public Kyc()
        {

        }

        public Kyc(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime Kyc input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        /// <summary>
        /// <para>Determine if its self Kyc or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;

            var updatables = GetUpdatableInformation();

            if (updatables == null || updatables.Count == 0)
            {
                resultXml = _router.EndSessionWithMessage("Service temporarily unavailable. Please, try again later", _session.SessionKey);
            }

            else
            {
                for (int i = 1; i <= updatables.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.KycUpdatableSelectPrefix, i), updatables[i - 1]);
                }

                var templateDictionary = new Dictionary<string, object>
                    {   
                        {"itemName", "information to update"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", updatables},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

                resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
            }

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            _session.NextStep = 3;
            string enteredValue = _session.EnteredValue.Trim();

            try
            {
                _session.NewInformationKey = (string)_session.SelectDictionary[string.Format("{0}{1}", SessionConstants.KycUpdatableSelectPrefix, enteredValue)];
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, string.Format("Enter your {0}", _session.NewInformationKey));
            }

            catch
            {
                resultXml = _router.EndSessionWithMessage(Constants.InvalidSelectionMessage, _session.SessionKey);
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;
            _session.NextStep = 4;
            _session.NewInformation = _session.EnteredValue.Trim();

            resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, string.Format("Please, re-enter your {0}",
                _session.NewInformationKey));

            return resultXml;
        }

        /// <summary>
        /// Confirm new information
        /// </summary>
        /// <returns></returns>
        public string Step4()
        {
            string resultXml = string.Empty;

            if (_session.NewInformation == _session.EnteredValue)
            {
                _session.NextStep = 5;
                resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Format("Your {0} will be set to {1}",
                    _session.NewInformationKey, _session.NewInformation));
            }

            else
            {
                _session.NextStep = 3;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Format("Both {0}s do not match", _session.NewInformationKey), 
                    string.Format("Please, enter your {0}", _session.NewInformationKey));
            }

            return resultXml;
        }

        public string Step5()
        {
            var resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var requestBody = GetKycUpdateRequest();

            var response = _router.DoDuisPostRequest<InfoUpdateResponse>(requestBody, "/InfoUpdate");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = _router.EndSessionWithMessage("Information was updated successfully", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                var templateDictionary = new Dictionary<string, object>
                    {   
                        {"pinDescription", _router.GetBadPinMessage(response.response_message)},
                        {"sessionKey", _session.SessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", templateDictionary);
                _session.NextStep = 5;
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_message, response.response_code);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        public InfoUpdateRequest GetKycUpdateRequest()
        {
            return new InfoUpdateRequest
                {
                    info_type = _session.NewInformationKey,
                    msisdn = _session.Msisdn,
                    new_value = _session.NewInformation,
                    password = _session.TransactionPassword,
                    reference_number = _session.SessionKey,
                    is_via_direct_code = _session.IsViaDirectCode
                };
        }

        private List<string> GetUpdatableInformation()
        {
            var result = Router.DoDuisGetRequest<List<string>>("/InfoUpdate", new Dictionary<string, object> { });
            return result;
        }
    }
}