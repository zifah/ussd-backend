﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class AlertOptOut : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private const int _selectItemsPerPage = 5;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public AlertOptOut()
        {

        }

        public AlertOptOut(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime AlertSubscription input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "alert", this, "account to opt-out");
            return resultXml;
        }

        /// <summary>
        /// <para>Determine if its self AlertSubscription or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            string resultXml = string.Empty;
            _session.NextStep = 3;
            _router.SetTransactionAccount(_session);

            var alertProducts = GetAlertProducts();

            if (alertProducts == null || alertProducts.Count == 0)
            {
                resultXml = _router.EndSessionWithMessage("Service temporarily unavailable. Please, try again later", _session.SessionKey);
            }

            else
            {
                _session.SelectDictionary.Add(SessionConstants.AlertProducts, alertProducts);
                resultXml = DisplayAlertProducts(1);
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;
            string enteredValue = _session.EnteredValue.Trim();

            try
            {
                var alertProducts = (Dictionary<string, string>)_session.SelectDictionary[SessionConstants.AlertProducts];

                var index = Convert.ToInt32(enteredValue);

                if (index == 0)
                {
                    _session.NextStep = 3;
                    resultXml = DisplayAlertProducts(_session.SelectListCurrentPage - 1);
                }

                else if (index == _selectItemsPerPage + 1)
                {
                    _session.NextStep = 3;
                    resultXml = DisplayAlertProducts(_session.SelectListCurrentPage + 1);
                }

                else
                {
                    int itemIndex = index * _session.SelectListCurrentPage - 1;
                    var alertProduct = alertProducts.ElementAt(itemIndex);
                    _session.SelectDictionary[SessionConstants.SelectedAlertProduct] = alertProduct;

                    var isSubscription = _session.AlertSubscriptionAction.Equals(1);

                    resultXml = XmlHelper.CollectPin(_session.SessionKey,
                        string.Format("Stopping {0} alert on your account: {1}", alertProduct.Value, _session.SourceAccountMasked),
                        "Enter your PIN to confirm");

                    _session.NextStep = 4;
                    //resultXml = _router.EndSessionWithMessage
                    //    (string.Format("You have successfully chosen an alert product: {0}", alertProduct.Value), _session.SessionKey);
                }
            }

            catch
            {
                resultXml = _router.EndSessionWithMessage(Constants.InvalidSelectionMessage, _session.SessionKey);
            }

            return resultXml;
        }

        /// <summary>
        /// Send subscribe/unsubscribe action to Duis, with auth data
        /// </summary>
        /// <returns></returns>
        public string Step4()
        {
            string resultXml = string.Empty;
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var theAlertProduct = ((KeyValuePair<string, string>)_session.SelectDictionary[SessionConstants.SelectedAlertProduct]);

            // compose alert subscription message
            var requestBody = new AlertSubscriptionRequest
            {
                account_number = _session.SourceAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                product_id = theAlertProduct.Key,
                subscribe = false,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };

            // post it to Duis
            var response = _router.DoDuisPostRequest<AlertSubscriptionResponse>(requestBody, "/AlertSubscription");
            
            // display result to customer (allow re-entry of PIN)
            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = _router.EndSessionWithMessage(string.Format("You will no longer receive {0} on your account: {1}", 
                    theAlertProduct.Value, _session.SourceAccountMasked), _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                var templateDictionary = new Dictionary<string, object>
                    {   
                        {"pinDescription", _router.GetBadPinMessage(response.response_message)},
                        {"sessionKey", _session.SessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", templateDictionary);
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        public InfoUpdateRequest GetAlertSubscriptionUpdateRequest()
        {
            return new InfoUpdateRequest
                {
                    info_type = _session.NewInformationKey,
                    msisdn = _session.Msisdn,
                    new_value = _session.NewInformation,
                    password = _session.TransactionPassword,
                    reference_number = _session.SessionKey
                };
        }

        private Dictionary<string, string> GetAlertProducts()
        {
            var result = Router.DoDuisGetRequest<Dictionary<string, string>>("/AlertSubscription", new Dictionary<string, object> { });
            return result;
        }

        private string DisplayAlertProducts(int pageToShow)
        {
            var alertProducts = (Dictionary<string, string>)_session.SelectDictionary[SessionConstants.AlertProducts];
            var selectList = alertProducts.Skip((pageToShow - 1) * _selectItemsPerPage).Take(_selectItemsPerPage).Select(x => x.Value).ToList();

            _session.SelectListCurrentPage = pageToShow;

            int noOfPages = _session.GetNoOfSelectPages(alertProducts.Count, _selectItemsPerPage);

            if (_session.ShouldAddNext(pageToShow, noOfPages))
            {
                selectList.Add("Next");
            }

            if (_session.ShouldAddPrevious(1))
            {
                selectList.Add("Previous");
            }

            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"itemName", "alert"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", selectList},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

            string resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
            return resultXml;
        }
    }
}