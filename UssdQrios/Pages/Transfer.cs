﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Exceptions;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class Transfer : IUssdPage, ITransactionalPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public Transfer()
        {

        }

        public Transfer(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// GetAccountToSelectIfApplicable
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            bool preventPrematureTransfers = false;
            bool.TryParse(ConfigurationManager.AppSettings[Constants.ConfPreventPrematureTransfers], out preventPrematureTransfers);

            if (preventPrematureTransfers)
            {
                PreventPrematureTransfers();
            }

            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "debit", this);
            return resultXml;
        }

        /// <summary>
        /// Determine debit account and request destination bank selection
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;
            _session.NextStep = 3;

            _router.SetTransactionAccount(_session);

            // get account banks
            // put the banks into a list

            var request = new RestRequest("/accounts/GetAccountBanks", Method.GET);

            request.AddParameter("account_number", _session.DestinationAccount);
            request.AddParameter("sender_msisdn", _session.Msisdn);

            var response = _duisClient.Execute<AccountBanks>(request);

            var accountBanks = response.Data;

            if (accountBanks != null && accountBanks.banks.Count > 0)
            {
                for (int i = 1; i <= accountBanks.banks.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.BeneficiaryBankSelectPrefix, i), accountBanks.banks[i - 1]);
                }

                if (accountBanks.banks.Count == 1)
                {
                    resultXml = Step3();
                }

                else
                {
                    var templateDictionary = new Dictionary<string, object>
                    {
                        {"itemName", "destination bank"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", accountBanks.banks.Select(x => x.name)},
                        {"routerUrl", Router._defaultEndpoint},
                    };

                    resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
                }
            }

            else
            {
                resultXml = _router.EndSessionWithMessage("Destination account number is invalid", _session.SessionKey);
            }

            return resultXml;
        }

        /// <summary>
        /// Determine destination bank, validate account
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            var resultXml = string.Empty;

            object selectedBank = null;
            _session.SelectDictionary.TryGetValue(string.Format("{0}{1}", SessionConstants.BeneficiaryBankSelectPrefix, _session.EnteredValue), out selectedBank);

            _session.SelectedBank = (Bank)selectedBank;

            if (_session.SelectedBank == null)
            {
                var beneficiaryBanks = _session.SelectDictionary.Where(x => x.Key.StartsWith(SessionConstants.BeneficiaryBankSelectPrefix)).ToDictionary(x => x.Key, x => x.Value);

                if (beneficiaryBanks.Count == 1)
                {
                    _session.SelectedBank = (Bank)beneficiaryBanks.ElementAt(0).Value;
                }

                else
                {
                    return _router.EndSessionWithMessage("Your selection is invalid.", _session.SessionKey);
                }
            }

            // get account name


            try
            {
                if (_session.CollectNarration == 1)
                {
                    string numNarrationCharacters = ConfigurationManager.AppSettings[Constants.ConfNumberNarrationCharacters];
                    string narrationText = string.Format("Enter narration ({0} characters max) e.g. March Pocket money from Mummy", numNarrationCharacters);
                    resultXml = XmlHelper.CollectValue(_session.SessionKey, null, narrationText);
                    _session.NextStep = 5;
                }

                else
                {
                    resultXml = GeneralHelper.ValidateCollectPinTransfer(_session, 4);
                }
            }

            catch (TimeoutException ex)
            {
                resultXml = _router.EndSessionWithMessage(ex.Message, _session.SessionKey);
            }

            return resultXml;
        }


        /// <summary>
        /// Do the transfer
        /// </summary>
        /// <returns></returns>
        public string Step4()
        {
            var resultXml = string.Empty;
            resultXml = _router.ProcessTransaction(_session, this);
            return resultXml;
        }

        /// <summary>
        /// Collect narration
        /// </summary>
        /// <returns></returns>
        public string Step5()
        {
            var resultXml = string.Empty;
            int numNarrationChars = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfNumberNarrationCharacters]);
            _session.Narration = _session.EnteredValue.Trim();

            if (numNarrationChars < _session.Narration.Length)
            {
                string narrationText = string.Format("Narration cannot be more than {0} characters", numNarrationChars);
                resultXml = XmlHelper.CollectValue(_session.SessionKey, narrationText, "Retry");
            }

            else
            {
                resultXml = GeneralHelper.ValidateCollectPinTransfer(_session, 4);
            }

            return resultXml;
        }

        public string DoTransaction()
        {
            string resultXml = string.Empty;

            var requestBody = new
            {
                msisdn = _session.Msisdn,
                source_nuban = _session.SourceAccount,
                password = _session.TransactionPassword,
                token = _session.TransactionToken,
                dest_nuban = _session.DestinationAccount,
                dest_account_name = _session.ValidationName,
                dest_bank_name = _session.SelectedBank.name_short,
                dest_bank_code = _session.SelectedBank.code_v2, //_session.SelectedBank.code_v2,
                dest_bank_alpha_code = _session.SelectedBank.name_short,
                reference_number = string.Format("{0}{1}", _session.SessionKey, _session.RetryCount == 0 ? string.Empty : "R" + _session.RetryCount),
                amount = _session.Amount,
                narration = _session.Narration,
                is_via_direct_code = _session.IsViaDirectCode
            };

            var response = _router.DoDuisPostRequest<FundsTransferResponse>(requestBody, "/FundsTransfer");

            if (response == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = _router.GetAd(_session, "FundsTransfer");

                resultXml = XmlHelper.ParseTemplate("Transfer\\transfer-done.xml", new Dictionary<string, object>() {
                    { "beneficiary_name", HttpUtility.HtmlEncode(_session.ValidationName) },
                    { "advert", HttpUtility.HtmlEncode(advertContent) }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisInsufficientFundsCode)
            {
                var ownBankTransferCode = ConfigurationManager.AppSettings[Constants.ConfOwnBankTransferCode];

                string interOrIntra = requestBody.dest_bank_code.Equals(ownBankTransferCode) ? "Intra" : "Inter";

                resultXml = _router.ProcessInsufficientFunds(string.Format("/FundsTransfer/GetAffordable{0}", interOrIntra), _session, "transfer", 4);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                var ownBankTransferCode = ConfigurationManager.AppSettings[Constants.ConfOwnBankTransferCode];
                string interOrIntra = requestBody.dest_bank_code.Equals(ownBankTransferCode) ? "Intra" : "Inter";
                resultXml = _router.ProcessLimitExceeded(string.Format("/FundsTransfer/GetLimits{0}", interOrIntra), _session, "transfer", 4);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, response.response_message);
                _session.NextStep = 4;
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        /// <summary>
        /// Stop customers who enrolled during the weekend from doing transfers until Monday
        /// </summary>
        private void PreventPrematureTransfers()
        {
            // Get enrolment date
            // check if today is Saturday or Sunday
            // if today is Saturday, confirm block the transaction if the customer enrolled today
            // if today is Sunday, block the transaction if the customer enrolled yesterday or today
            bool block = false;
            var now = DateTime.Now;
            var today = now.Date;

            if (_session.UserEnrolmentTime.HasValue)
            {
                var enrolmentDate = _session.UserEnrolmentTime.Value.Date;

                if (new DayOfWeek[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(today.DayOfWeek) || (today.DayOfWeek == DayOfWeek.Monday && now.Hour < 8))
                {
                    block = enrolmentDate >= today.AddDays(today.DayOfWeek == DayOfWeek.Saturday ? 0 : -(today.DayOfWeek + 7 - DayOfWeek.Saturday));
                }
            }

            if (block)
            {
                throw new UssdException("Unable to complete.");
            }
        }
    }
}