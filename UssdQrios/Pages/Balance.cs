﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class Balance : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public Balance()
        {

        }

        public Balance(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// GetAccountToSelectIfApplicable
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            _session.NextStep = 2;

            var request = new RestRequest("/accounts/GetPhoneAccounts", Method.GET);

            request.AddParameter("phone", _session.Msisdn);

            var response = _duisClient.Execute<MsisdnAccount>(request);

            var msisdnAccount = response.Data;

            if (msisdnAccount != null)
            {
                for (int i = 1; i <= msisdnAccount.accounts.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.AccountSelectPrefix, i), msisdnAccount.accounts[i - 1]);
                }

                if (msisdnAccount.accounts.Count > 1)
                {
                    var templateDictionary = new Dictionary<string, object>
                    {   
                        {"itemName", "inquiry account"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", msisdnAccount.accounts.Select(x => x.masked_account_number)},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

                    resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
                }

                else
                {
                    resultXml = Step2();
                }
            }

            else
            {
                resultXml = Step2();
            }

            return resultXml;
        }

        /// <summary>
        /// Determine enquiry account and request PIN
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;
            _session.NextStep = 3;

            string inquiryAccount = _session.DefaultAccount;

            decimal convenienceFee = 0;

            try
            {
                convenienceFee = GetConvenienceFee();
                object selectedAccount = null;
            _session.SelectDictionary.TryGetValue(string.Format("{0}{1}", SessionConstants.AccountSelectPrefix, _session.EnteredValue), out selectedAccount);
            inquiryAccount = (Account)selectedAccount == null ? inquiryAccount : ((Account)selectedAccount).account_number;

            _session.SourceAccount = inquiryAccount;

            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"title", ""},                        
                        {"sessionKey", _session.SessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

            resultXml = XmlHelper.CollectPin(_session.SessionKey,
                string.Format("Balance inquiry attracts a convenience fee of N{0:N2}<br/>", convenienceFee));
            }

            catch
            {
                resultXml = _router.EndSessionWithMessage("An error occured. Please, try again later.", _session.SessionKey);
            }            

            return resultXml;
        }

        private decimal GetConvenienceFee()
        {
            var result = Router.DoDuisGetRequest<AccountEnquiryResponse>("/accounts/getbalenqcharge", new Dictionary<string, object> { { "phone", _session.Msisdn } });
            return result.amount;
        }

        /// <summary>
        /// Do the inquiry
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            var resultXml = string.Empty;

            var password = _session.EnteredValue;

            var requestBody = new
            {
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                password = password,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode
            };

            var request = new RestRequest("/accounts/GetAccountBalance", Method.POST);

            request.AddJsonBody(requestBody);

            var response = _duisClient.Execute<AccountEnquiryResponse>(request);

            var enquiryResponse = response.Data;

            if (enquiryResponse == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (enquiryResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }


            else if (enquiryResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = XmlHelper.ParseTemplate("General\\view-balance.xml", new Dictionary<string, object>() { 
                    {"account", enquiryResponse.account_details.masked_account_number},
                    {"available", enquiryResponse.account_details.account_balance },
                    {"cleared", enquiryResponse.account_details.cleared_balance },
                    {"uncleared", enquiryResponse.account_details.uncleared_balance },
                    {"lien", enquiryResponse.account_details.lien },
                    {"minimum", enquiryResponse.account_details.minimum_balance }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (enquiryResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                var templateDictionary = new Dictionary<string, object>
                    {   
                        {"pinDescription", _router.GetBadPinMessage(enquiryResponse.response_message)},
                        {"sessionKey", _session.SessionKey},                       
                        {"routerUrl", Router._defaultEndpoint},
                        {"pinLabel", "Enter PIN"}
                    };

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", templateDictionary);
                _session.NextStep = 3;
                _session.FailedLoginAttempts++;
            }

            else if (enquiryResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(enquiryResponse.response_code, enquiryResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}