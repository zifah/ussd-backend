﻿using RestSharp;
using System;
using System.Collections.Generic;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Collects phone number for third party recharge
    /// </summary>
    public class CollectRechargeType : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>Index 0: This phone</para>
        /// <para>Index 1: Another phone</para>
        /// </summary>
        private readonly List<string> _rechargeTypes = new List<string>()
        {
            Constants.ConfConstantThisPhone,
            Constants.ConfConstantAnotherPhone
        };

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public CollectRechargeType()
        {

        }

        public CollectRechargeType(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            _session.NextStep = 2;
            string resultXml = XmlHelper.DoSelectItem("phone to recharge", _session.SessionKey, _rechargeTypes);
            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            int selectedIndex = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;

            string selectedItem = _rechargeTypes[selectedIndex];

            if (selectedItem == Constants.ConfConstantThisPhone)
            {
                _session.NextStep = 1;
                _session.NextPageName = "CollectRechargeAmount";
                resultXml = _router.LoadPage();
            }

            else if (selectedItem == Constants.ConfConstantAnotherPhone)
            {
                _session.NextStep = 1;
                _session.NextPageName = "CollectRechargePhone";
                resultXml = _router.LoadPage();
            }

            else
            {
                resultXml = _router.EndSessionWithMessage(Constants.InvalidSelectionMessage, _session.SessionKey);
            }

            return resultXml;
        }
    }
}