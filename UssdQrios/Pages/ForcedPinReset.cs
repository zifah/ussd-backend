﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class ForcedPinReset : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public ForcedPinReset()
        {

        }

        public ForcedPinReset(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            _session.NextStep = 2;

            int displayNameLength = 20;
            string displayName = _session.AccountName.Substring(0, Math.Min(displayNameLength, _session.AccountName.Length));
            displayName = FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.ToTitleCase(displayName);
            displayName = HttpUtility.HtmlEncode(displayName);

            if (_session.AccountName.Length > displayNameLength)
            {
                displayName += "..";
            }

            resultXml = XmlHelper.CollectValue(_session.SessionKey, 
                string.Format("Hi {0},<br/><br/>For security reasons, you need to choose a new PIN or password before you proceed<br/>",
               displayName), "Please enter your account number");

            return resultXml;
        }

        /// <summary>
        /// Collect birthday in DdMmYy format
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            string resultXml = string.Empty;

            string enteredValue = _session.EnteredValue.Trim();

            if (_session.DefaultAccount.Equals(enteredValue))
            {
                _session.SourceAccount = enteredValue;
                _session.NextStep = 3;

                // proceed to PIN selection
                resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Empty, "Select a new PIN with 4 or more numbers or letters");

            }

            else
            {
                _session.FailedLoginAttempts += 1;

                int failedAccountValidationLimit = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfFailedAccountValidationLimit]);

                if (_session.FailedLoginAttempts == failedAccountValidationLimit)
                {
                    // lockout user
                    _router.DoDuisPostRequest<LockoutResponse>(new LockoutRequest
                    {
                        msisdn = _session.Msisdn,
                        account_number = _session.DefaultAccount,
                        reason = string.Format("Account validation failed {0} times", failedAccountValidationLimit),
                        is_via_direct_code = _session.IsViaDirectCode
                    }, "/accounts/lock");

                    resultXml = XmlHelper.ParseTemplate("General\\pin-tries-exceeded.xml", new Dictionary<string, object>() { 
                    {"trueservePhone", ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone]}
                });

                    _router.EndSession(_session.SessionKey);
                }

                else
                {
                    // display incorrect account message
                    resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Format("{0} is invalid<br/>", enteredValue),
                        "Please, enter the account you chose during sign-up");
                    // allow re-entry
                    _session.NextStep = 2;
                }
            }

            return resultXml;
        }

        /// <summary>
        /// Confirm new PIN
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            string resultXml = string.Empty;

            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = _router.IsPasswordValid(newPassword, _session.SourceAccount);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 4;
                _session.NewPassword = newPassword;
                resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Empty, "Confirm your new PIN");
            }

            else
            {
                _session.NextStep = 3;

                resultXml = XmlHelper.CollectPin(_session.SessionKey,
                     string.Format("{0}<br/>", isPasswordValid.reason), "Please, enter a PIN with 4 or more numbers or letters");

            }

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            _session.NextStep = 4;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                _session.PinResetToken = Router.DoDuisGetRequest<Account>("/accounts", new Dictionary<string, object> { { "accountNumber", _session.DefaultAccount } }).birthday_ddMmYy;

                var response = _router.DoDuisPostRequest<UssdUser>(new
                {
                    msisdn = _session.Msisdn,
                    password = _session.NewPasswordConfirm.Trim(),
                    account_number = _session.SourceAccount,
                    birthday_DdMmYy = _session.PinResetToken,
                    is_via_direct_code = _session.IsViaDirectCode
                }, "/accounts/resetpassword");

                if (response == null || string.IsNullOrWhiteSpace(response.response_code))
                {
                    resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                    _router.EndSession(_session.SessionKey);
                }

                else if (response.response_code == Constants.DuisTimeoutCode)
                {
                    resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisSuccessfulCode)
                {
                    _session.RedirectNotifyMessage = "PIN reset was successful.";
                    _session.NextStep = 1;
                    _session.NextPageName = _session.RedirectPageName;
                    resultXml = _router.LoadPage();
                }

                else if (response.response_code == Constants.DuisAccountBlockedCode)
                {
                    resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
                }

                else
                {
                    resultXml = _router.EndSessionWithGenericFailure(response.response_code, response.response_message, _session.SessionKey);
                }
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinDescription", "Your PINs do not match!<br/>" },
                    {"pinLabel", "Enter a PIN with 4 or more numbers or letters."}
                });

                _session.NextStep = 3;
            }

            return resultXml;
        }
    }
}