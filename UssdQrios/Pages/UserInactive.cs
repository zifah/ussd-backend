﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class UserInactive : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public UserInactive()
        {

        }

        public UserInactive(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }
        
        public string Step1()
        {
            string resultXml = string.Empty;

            resultXml = XmlHelper.ParseTemplate("General\\pin-tries-exceeded.xml", new Dictionary<string, object>() { 
                    {"trueservePhone", ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone]},
                    {"selfUnlockCode", string.Format("{0}{1}#", 
                    ConfigurationManager.AppSettings[Constants.ConfQriosUssdPrefix],
                    ConfigurationManager.AppSettings[Constants.ConfSelfUnlockEndpointCode])}
                });

            _router.EndSession(_session.SessionKey);

            return resultXml;
        }
    }
}