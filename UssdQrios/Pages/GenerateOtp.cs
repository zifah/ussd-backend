﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class GenerateOtp : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public GenerateOtp()
        {

        }

        public GenerateOtp(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime GenerateOtp input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        /// <summary>
        /// <para>Determine if its self GenerateOtp or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;

            decimal convenienceFee = GetConvenienceFee();
            string username = GetOtpUsername();
            username = string.Format("{0}***{1}", username.Substring(0, 2), username.Substring(username.Length - 2, 2));

            resultXml = XmlHelper.CollectPin(_session.SessionKey,
                string.Format("Generating one-time password for user: {0}. This will cost N{1:N2}", username, convenienceFee));

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            var otpResponse = GetOtp();

            if (otpResponse == null || string.IsNullOrWhiteSpace(otpResponse.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (otpResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = XmlHelper.ParseTemplate("GenerateOtp\\successful.xml", new Dictionary<string, object> { 
                    { "otp", otpResponse.otp },                    
                    { "expirationTime", otpResponse.expiration_time.ToString("HH:mm")},                    
                    { "expirationDate", otpResponse.expiration_time.ToString("dd MMM yyyy") }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (otpResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, otpResponse.response_message);
                _session.FailedLoginAttempts++;
            }

            else if (otpResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(otpResponse.response_code, otpResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private string GetOtpUsername()
        {
            var result = Router.DoDuisGetRequest<OtpResponse>("/otp/getusername", new Dictionary<string, object> { { "msisdn", _session.Msisdn } });
            return result.username;
        }

        private decimal GetConvenienceFee()
        {
            var result = Router.DoDuisGetRequest<OtpResponse>("/otp/getcharge", new Dictionary<string, object> { { "msisdn", _session.Msisdn } });
            return result.convenience_fee;
        }

        private OtpResponse GetOtp()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var result = _router.DoDuisPostRequest<OtpResponse>(new OtpRequest
            {
                password = _session.TransactionPassword,
                msisdn = _session.Msisdn,
                is_via_direct_code = _session.IsViaDirectCode,
                reference_number = _session.SessionKey
            }, "/otp/generate");

            return result;
        }
    }
}