﻿
using RestSharp;
using System.Linq;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class RechargeWithNotify : IUssdPage
    {
        private SessionObject _session;
        private Router _router;
        private const string _categoryName = "RechargeWithNotify";

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public RechargeWithNotify()
        {

        }

        public RechargeWithNotify(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            _session.NextPageName = typeof(Recharge).Name;
            _session.NextStep = 1;
            return _router.LoadPage();            
        }
    }
}