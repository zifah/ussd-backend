﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class CancelCardReissuance : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public CancelCardReissuance()
        {

        }

        public CancelCardReissuance(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// <para>Select payment account</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "your card", this);
            return resultXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;

            _session.NextStep = 3;

            _router.SetTransactionAccount(_session);

            string displayMessage = string.Format("Cancelling auto-renewal for your expired debit card");

            resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Format("{0}<br/>", displayMessage));

            return resultXml;
        }

        public string Step3()
        {
            var resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var request = GetRequestPayload();

            var cancelReissuanceResponse = _router.DoDuisPostRequest<CancelCardReissuanceResponse>(request, "/Cards/CancelReissuance");

            if (cancelReissuanceResponse == null || cancelReissuanceResponse.response_code == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (cancelReissuanceResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (cancelReissuanceResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (cancelReissuanceResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = _router.GetAd(_session, null);

                resultXml = XmlHelper.ParseTemplate("Cards\\card-reissuance-cancelled.xml", new Dictionary<string, object> {
                    { "account_last_four", FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.LastCharacters(request.account_number, 4) },
                    { "advert", HttpUtility.HtmlEncode(advertContent) }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (cancelReissuanceResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, cancelReissuanceResponse.response_message);
                _session.NextStep = 3;
                _session.FailedLoginAttempts++;
            }

            else if (cancelReissuanceResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(cancelReissuanceResponse.response_code, cancelReissuanceResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private CancelCardReissuanceRequest GetRequestPayload()
        {
            var request = new CancelCardReissuanceRequest
            {
                account_number = _session.SourceAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
            };

            return request;
        }
    }
}