﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class SelfUnlock : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public SelfUnlock()
        {

        }

        public SelfUnlock(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;

            string instruction = "Enter your NUBAN account number";
            resultXml = XmlHelper.CollectValue(_session.SessionKey, null, instruction);

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            _session.NextStep = 3;

            _session.SourceAccount = _session.EnteredValue.Trim();

            string instruction = "Enter your birthday in format: DDMMYY. 1st December 1995 is 011295";
            resultXml = XmlHelper.CollectValue(_session.SessionKey, null, instruction);

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;
            _session.NextStep = 4;

            // the customer's birthday is stored in BillingCustomerId property of session
            _session.BillingCustomerId = _session.EnteredValue.Trim();
            if(_session.BillingCustomerId.All(x => char.IsNumber(x)) && (_session.BillingCustomerId.Length == 6 || _session.BillingCustomerId.Length == 8))
            {
                _session.BillingCustomerId = $"{_session.BillingCustomerId.Substring(0, 2)} {_session.BillingCustomerId.Substring(2, 2)} {_session.BillingCustomerId.Substring(4)}";
            }

            string instruction = "Enter the last four digits of your Debit Card Number";
            resultXml = XmlHelper.CollectValue(_session.SessionKey, null, instruction);

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            // The last four digits of the customer's PAN are stored in Password field
            _session.TransactionPassword = _session.EnteredValue.Trim();

            var request = new SelfUnlockRequest
            {
                is_via_direct_code = _session.IsViaDirectCode,
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                birthday_DdMmYy = _session.BillingCustomerId,
                pan_last_four_digits = _session.TransactionPassword,
                reference_number = _session.SessionKey
            };

            var response = _router.DoDuisPostRequest<SelfUnlockResponse>(request, "/Accounts/SelfUnlock");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                resultXml = XmlHelper.ParseTemplate("Account\\self-unlock-failed-auth.xml", new Dictionary<string, object> {
                    { "accountNumber", request.account_number },
                    { "birthday", request.birthday_DdMmYy },
                    { "cardNumber", request.pan_last_four_digits }
                });
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string pinResetCode = string.Format("{0}{1}#", ConfigurationManager.AppSettings[Constants.ConfQriosUssdPrefix],
                    ConfigurationManager.AppSettings[Constants.ConfPinResetEndpointCode]);

                resultXml = XmlHelper.ParseTemplate("Account\\self-unlock-successful.xml", new Dictionary<string, object> {
                    { "pinResetCode", pinResetCode } });

                _router.EndSession(_session.SessionKey);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}