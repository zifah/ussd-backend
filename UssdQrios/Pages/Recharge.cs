﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class Recharge : IUssdPage, ITransactionalPage
    {
        private const string _yes = "Yes";
        private const string _no = "No";
        private readonly string[] _yesOrNo = new string[] { _yes, _no };

        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public Recharge()
        {

        }

        private ThirdPartyRechargeSmsMode SmsMode
        {
            get
            {
                ThirdPartyRechargeSmsMode smsState;
                Enum.TryParse<ThirdPartyRechargeSmsMode>(ConfigurationManager.AppSettings[Constants.ConfThirdPartyRechargeSmsMode], out smsState);
                return smsState;
            }
        }

        public Recharge(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime recharge input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        /// <summary>
        /// <para>Determine if its self recharge or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "debit", this);
            return resultXml;
        }

        public string Step2()
        {
            var resultXml = string.Empty;
            _session.NextStep = 3;

            if(string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                _router.SetTransactionAccount(_session);
            }

            string beneficiary = _session.BillingCustomerId ?? _session.Msisdn;
            bool isThirdPartyRecharge = !_session.Msisdn.Equals(beneficiary);

            if (isThirdPartyRecharge && (_session.NotifyBeneficiary == 1 || SmsMode == ThirdPartyRechargeSmsMode.On))
            {
                _session.SendRechargeSms = true;
                return ProcessSession();
            }

            if (isThirdPartyRecharge && SmsMode == ThirdPartyRechargeSmsMode.Optional)
            {
                string smsCost = ConfigurationManager.AppSettings[Constants.ConfSmsCost];
                return XmlHelper.DoSelectItemSpecial(string.Format("Tell beneficiary who sent them airtime (SMS costs N{0})?", smsCost),
                    _session.SessionKey, _yesOrNo);
            }

            resultXml = ProcessSession();
            return resultXml;
        }

        public string Step3()
        {
            var resultXml = string.Empty;
            string beneficiary = _session.BillingCustomerId ?? _session.Msisdn;
            bool isThirdPartyRecharge = !_session.Msisdn.Equals(beneficiary);
            if (!_session.SendRechargeSms && isThirdPartyRecharge && SmsMode == ThirdPartyRechargeSmsMode.Optional)
            {
                try
                {
                    int index = Convert.ToInt32(_session.EnteredValue.Trim()) - 1;
                    _session.SendRechargeSms = _yesOrNo[index] == _yes;
                }

                catch
                {
                    // do nothing leave in its default state
                }
            }

            if(_session.SendRechargeSms && _session.Narration == null)
            {
                // ask user for the name which the user notification should contain
                _session.NextStep = 4;
                return XmlHelper.CollectValue(_session.SessionKey, "Who sent me airtime?<br/>", "Enter your short name");
            }

            var requestContent = ComposeRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMRecharge/RequiresAuthentication" : "Recharge/RequiresAuthentication";

            var requiresPinResponse = _router.DoDuisPostRequest(requestContent, endpoint);

            if (requiresPinResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _session.EnteredValue = null;
                var requiresPin = JsonConvert.DeserializeObject<bool>(requiresPinResponse.Content);

                string pinEntryMessage =
                        string.Format("Recharging {0} with N{1:N2} airtime{2}", requestContent.beneficiary_msisdn, _session.Amount,
                        _session.Narration == null ? "" : 
                        $"<br/><br/>SMS name: {_session.Narration}<br/>SMS cost: N{ConfigurationManager.AppSettings[Constants.ConfSmsCost]}<br/>");

                string confirmationMessage =
                        string.Format("Confirm airtime recharge of N{0:N2} for {1}", _session.Amount, requestContent.beneficiary_msisdn);

                if (requiresPin)
                {
                    //var network = _router.DoDuisGetRequest<PhoneEnquiryResponse>("/Recharge/Network", new Dictionary<string, object>{
                    //{"msisdn", requestContent.beneficiary_msisdn}
                    //});
                    _session.NextStep = 6;

                    resultXml = XmlHelper.CollectPin(_session.SessionKey, pinEntryMessage);
                }

                else
                {
                    //ask for recharge confirmation if PIN is not going to be entered
                    _session.NextStep = 5;
                    resultXml = GeneralHelper.DoProceedOrNot(_session.SessionKey, string.Format("{0}", confirmationMessage));
                }
            }

            else
            {
                return _router.EndSessionWithMessage("Service temporarily unavailable", _session.SessionKey);
            }

            return resultXml;
        }

        /// <summary>
        /// Collect sender custom name
        /// </summary>
        /// <returns></returns>
        public string Step4()
        {
            var resultXml = string.Empty;

            if(_session.EnteredValue.Length > 20)
                return XmlHelper.CollectValue(_session.SessionKey, "Name is too long", "Enter name (up to 20 characters)");

            _session.Narration = _session.EnteredValue;
            _session.NextStep = 3;
            return ProcessSession();
        }

        public string Step5()
        {
            var resultXml = string.Empty;

            _session.NextStep = 6;

            bool proceed = GeneralHelper.IsResponsePositive(_session);

            string advertContent = _router.GetAd(_session, "AirtimeRecharge");

            if(proceed)
            {
                resultXml = ProcessSession();
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("Recharge\\cancelled.xml", new Dictionary<string, object> {
                { "advert", HttpUtility.HtmlEncode(advertContent) },
                { "mainMenuCode", HttpUtility.HtmlEncode(ConfigurationManager.AppSettings[Constants.ConfMainMenuCode]) }
                });

            _router.EndSession(_session.SessionKey); 
            }

            return resultXml;
        }

        public string Step6()
        {
            var resultXml = string.Empty;
            resultXml = _router.ProcessTransaction(_session, this);
            return resultXml;
        }

        public string DoTransaction()
        {
            string resultXml = string.Empty;

            var requestContent = ComposeRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMRecharge" : "/Recharge/BuyAirtime";

            var rechargeResponse = _router.DoDuisPostRequest<RechargeResponse>(requestContent,endpoint);

            if (rechargeResponse == null || rechargeResponse.response_code == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (rechargeResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = _router.GetAd(_session, "AirtimeRecharge");

                resultXml = XmlHelper.ParseTemplate("Recharge\\successful.xml", new Dictionary<string, object> {
                    { "beneficiary", requestContent.beneficiary_msisdn },
                    { "amount", _session.Amount.ToString("N2") },
                    { "advert", HttpUtility.HtmlEncode(advertContent) }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (rechargeResponse.response_code == Constants.DuisInsufficientFundsCode)
            {
                resultXml = _session.UserType == UserType.MobileMoney ? 
                    _router.EndSessionWithMessage("Insufficient funds", _session.SessionKey) :
                    _router.ProcessInsufficientFunds("/Recharge/GetAffordable", _session, "recharge", 6);
            }

            else if (rechargeResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (rechargeResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                resultXml = _session.UserType == UserType.MobileMoney ? 
                    _router.EndSessionWithMessage("Daily limit exceeded", _session.SessionKey) :
                    _router.ProcessLimitExceeded("/Recharge/GetLimits", _session, "recharge", 6);
            }

            else if (rechargeResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, rechargeResponse.response_message);
                _session.NextStep = 6;
                _session.FailedLoginAttempts++;
            }

            else if (rechargeResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(rechargeResponse.response_code, rechargeResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private RechargeRequest ComposeRequest()
        {
            RechargeRequest request = new RechargeRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                beneficiary_msisdn = _session.BillingCustomerId ?? _session.Msisdn,
                msisdn = _session.Msisdn,
                msisdn_network = _session.MsisdnNetwork,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
                password = _session.TransactionPassword,
                token = _session.TransactionToken,
                send_beneficiary_sms = _session.SendRechargeSms, 
                sender_name = _session.Narration
            };

            return request;
        }
    }
}