﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class InstantBanking : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private readonly int _selectItemsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfMenuItemsPerPage]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public InstantBanking()
        {

        }

        public InstantBanking(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string DisplayMenu(int increment)
        {
            string resultXml = string.Empty;

            var templateDictionary = new Dictionary<string, object>
                    {
                        {"itemName", "menu"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", GetMenuItems(increment)},
                        {"routerUrl", Router._defaultEndpoint},
                    };

            _session.NextStep = 2;

            resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);

            return resultXml;
        }

        /// <summary>
        /// <para>Determine if its self InstantBanking or otherwise</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            return DisplayMenu(0);
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            try
            {
                string enteredValue = _session.EnteredValue.Trim();
                int selectedMenuItem = Convert.ToInt32(enteredValue);

                var displayedItems = GetMenuItems(0);

                if (selectedMenuItem == 0)
                {
                    return DisplayMenu(-1);
                }

                if (displayedItems[selectedMenuItem - 1] == "Next")
                {
                    return DisplayMenu(1);
                }

                Page selectedPage = DestinationPages.
                    Single(x => x.DisplayName == displayedItems[selectedMenuItem - 1]);

                _session.MenuSelectedPageName = selectedPage.PageName;

                if (selectedPage.SessionVariables.Count == 0)
                {
                    resultXml = RedirectToSelectedPage();
                }

                else
                {
                    _session.NextStep = 3;
                    _session.NextMenuVariableToCollect = selectedPage.SessionVariables[0].Name;
                    resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty,
                        string.Format("Enter {0}", selectedPage.SessionVariables[0].DisplayName));
                }
            }

            catch
            {
                resultXml = _router.EndSessionWithMessage("Invalid selection", _session.SessionKey);
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;

            string enteredValue = _session.EnteredValue.Trim();

            Page selectedPage = DestinationPages.Where(x => !_closedPages.Contains(x.PageName)).Single(x => x.PageName == _session.MenuSelectedPageName);


            var sessionObjectType = typeof(SessionObject);

            var sessionPropertyInfo = sessionObjectType.GetProperty(_session.NextMenuVariableToCollect);

            var sessionVariable = selectedPage.SessionVariables.Single(x => x.Name == _session.NextMenuVariableToCollect);

            var index = selectedPage.SessionVariables.IndexOf(sessionVariable);

            try
            {
                if (!Regex.IsMatch(enteredValue, sessionVariable.RegexValidator))
                {
                    throw new Exception();
                }

                sessionPropertyInfo.SetValue(_session, Convert.ChangeType(enteredValue, sessionPropertyInfo.PropertyType));
            }

            catch
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = sessionVariable.Name;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty,
                    string.Format("The {0} entered was invalid. Please try again", sessionVariable.DisplayName));

                return resultXml;
            }

            if (index == selectedPage.SessionVariables.Count - 1)
            {
                // redirect to the main page since all variables have been collected
                //_session.NextStep = 1;
                //_session.NextPageName = selectedPage.PageName;
                //resultXml = _router.LoadPage();
                resultXml = RedirectToSelectedPage();
            }

            else
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = selectedPage.SessionVariables[index + 1].Name;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty,
                    string.Format("Enter {0}", selectedPage.SessionVariables[index + 1].DisplayName));
            }

            return resultXml;
        }

        public List<string> GetMenuItems(int currentPageIncrement)
        {
            _session.SelectListCurrentPage += currentPageIncrement;
            int currentPage = _session.SelectListCurrentPage;
            var items = DestinationPages.Where(x => !_closedPages.Contains(x.PageName)).Select(x => x.DisplayName).ToList();
            var noOfItems = items.Count;

            var startIndex = Math.Max(0, _selectItemsPerPage * (currentPage - 1));
            var endIndex = startIndex + _selectItemsPerPage - 1;

            var result = items.Where(x => items.IndexOf(x) >= startIndex && items.IndexOf(x) <= endIndex).ToList();

            if (noOfItems - endIndex > 1)
            {
                result.Add("Next");
            }

            if (currentPage > 1)
            {
                result.Add("Previous");
            }

            return result;
        }

        private string RedirectToSelectedPage()
        {
            if (Router._anonymousPages.Contains(_session.MenuSelectedPageName) || _session.IsUserLoaded)
            {
                _session.NextStep = 1;
                _session.NextPageName = _session.MenuSelectedPageName;
            }

            else
            {
                // refer user to registration stage
                _session.NextStep = 1;
                _session.NextPageName = "NewUser";

                // where to redirect to after creation
                _session.RedirectStepNumber = 1;
                _session.RedirectPageName = _session.MenuSelectedPageName;
            }

            return _router.LoadPage();
        }

        private List<Page> DestinationPages = new List<Page>()
        {
             new Page{ PageName = "AccountOpening", DisplayName = "Open Account",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "Balance", DisplayName = "Balance",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "CollectRechargeType", DisplayName = "Airtime",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new Page{ PageName = "Transfer", DisplayName = "Transfer",
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "DestinationAccount", DisplayName = "Destination account", RegexValidator = "^[0-9]{10}$" },
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,7}$" }
                 }
             },
             new Page{ PageName = "Cashout", DisplayName = "Dial4Cash",
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,4}000$" }
                 }
             },
             new Page{ PageName = "MobileData", DisplayName = "Data Plan",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new Page{ PageName = "Billing", DisplayName = "Bill Payments",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new Page{ PageName = "Airline", DisplayName = "Airline Tickets",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new Page{ PageName = "AccountReminderService", DisplayName = "Get Account Number/BVN",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new Page{ PageName = "GenerateOtp", DisplayName = "Generate OTP",
                 SessionVariables = new List<PageSessionVariable> {
                 }
             },
             new Page{ PageName = "Kyc", DisplayName = "Info Update",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "PinChange", DisplayName = "PIN change",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "PinReset", DisplayName = "Forgot PIN",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "ControlCard", DisplayName = "Block card",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "ContactAccountOfficer", DisplayName = "Contact Account Officer",
                 SessionVariables = new List<PageSessionVariable> { } }

             //new Page{ PageName = "CancelCardReissuance", DisplayName = "Cancel card auto-renewal",
             //    SessionVariables = new List<PageSessionVariable> { } }
        };
    }

    public class Page
    {
        public string PageName { set; get; }
        public string DisplayName { set; get; }
        public List<PageSessionVariable> SessionVariables { set; get; }
    }

    public class PageSessionVariable
    {
        public string Name { set; get; }
        public string DisplayName { set; get; }
        public string RegexValidator { set; get; }
    }
}