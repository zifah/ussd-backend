﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class RemitaOtpGenerate : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private const string _remitaApp = "Remita App";

        public RemitaOtpGenerate()
        {

        }

        public RemitaOtpGenerate(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            _session.NextStep = 2;
            return _router.DoSelectAccount(_session, _remitaApp, this);
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            _session.NextStep = 3;
            _router.SetTransactionAccount(_session);
            return XmlHelper.CollectPin(_session.SessionKey, $"You are about to generate an OTP for {_remitaApp} activation");
        }

        public string Step3()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var request = new ThirdPartyOtpRequest
            {
                is_via_direct_code = _session.IsViaDirectCode,
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount,
                reference_number = _session.SessionKey,
                password = _session.TransactionPassword,
                purpose = _remitaApp
            };

            var response = _router.DoDuisPostRequest<ThirdPartyOtpResponse>(request, "ThirdPartyOtp/Generate");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, response.response_message);
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = XmlHelper.ParseTemplate("ThirdPartyOtp\\successful.xml", new Dictionary<string, object> {
                    { "purpose", _remitaApp },
                    // { "expiration", $"{response.expiration_time:dd MMM yyyy hh:mm tt}" },
                    { "advert", HttpUtility.HtmlEncode(_router.GetAd(_session, null)) }
                });

                _router.EndSession(_session.SessionKey);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}