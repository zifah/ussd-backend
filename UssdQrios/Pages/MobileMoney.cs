﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MobileMoney : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private readonly int _selectItemsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfMenuItemsPerPage]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public MobileMoney()
        {

        }

        public MobileMoney(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        
        public string Step1()
        {
            string resultXml = string.Empty;
            var theUser = GetUser();

            if (theUser != null && theUser.response_code == Constants.DuisNotEnrolledCode)
            {
                _session.NextPageName = "MMRegister";
                resultXml = _router.LoadPage();
            }

            else if (theUser != null && theUser.response_code == Constants.DuisSuccessfulCode)
            {
                _session.UserType = UserType.MobileMoney;
                _session.IsUserLoaded = true;
                _session.DefaultAccount = GeneralHelper.GetMobileAccount(_session.Msisdn);
                resultXml = DisplayMenu(0);
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("General\\block-of-text.xml", new Dictionary<string, object>() { 
                    {"blockOfText", "Service temporarily unavailable" }
                });
            }

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            try
            {
                string enteredValue = _session.EnteredValue.Trim();
                int selectedMenuItem = Convert.ToInt32(enteredValue);

                var displayedItems = GetMenuItems(0);

                if (selectedMenuItem == 0)
                {
                    return DisplayMenu(-1);
                }

                if (displayedItems[selectedMenuItem - 1] == "Next")
                {
                    return DisplayMenu(1);
                }

                Page selectedPage = DestinationPages.
                    Single(x => x.DisplayName == displayedItems[selectedMenuItem - 1]);

                _session.MenuSelectedPageName = selectedPage.PageName;

                if (selectedPage.SessionVariables.Count == 0)
                {
                    _session.NextStep = 1;
                    _session.NextPageName = selectedPage.PageName;
                    resultXml = _router.LoadPage();
                }

                else
                {
                    _session.NextStep = 3;
                    _session.NextMenuVariableToCollect = selectedPage.SessionVariables[0].Name;
                    resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty,
                        string.Format("Enter {0}", selectedPage.SessionVariables[0].DisplayName));
                }
            }

            catch
            {
                resultXml = _router.EndSessionWithMessage("Invalid selection", _session.SessionKey);
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;

            string enteredValue = _session.EnteredValue.Trim();

            Page selectedPage = DestinationPages.Where(x => !_closedPages.Contains(x.PageName)).Single(x => x.PageName == _session.MenuSelectedPageName);


            var sessionObjectType = typeof(SessionObject);

            var sessionPropertyInfo = sessionObjectType.GetProperty(_session.NextMenuVariableToCollect);

            var sessionVariable = selectedPage.SessionVariables.Single(x => x.Name == _session.NextMenuVariableToCollect);

            var index = selectedPage.SessionVariables.IndexOf(sessionVariable);

            try
            {
                if (!Regex.IsMatch(enteredValue, sessionVariable.RegexValidator))
                {
                    throw new Exception();
                }

                sessionPropertyInfo.SetValue(_session, Convert.ChangeType(enteredValue, sessionPropertyInfo.PropertyType));
            }

            catch
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = sessionVariable.Name;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty,
                    string.Format("The {0} entered was invalid. Please try again", sessionVariable.DisplayName));

                return resultXml;
            }

            if (index == selectedPage.SessionVariables.Count - 1)
            {
                // redirect to the main page since all variables have been collected
                _session.NextStep = 1;
                _session.NextPageName = selectedPage.PageName;
                resultXml = _router.LoadPage();
            }

            else
            {
                _session.NextStep = 3;
                _session.NextMenuVariableToCollect = selectedPage.SessionVariables[index + 1].Name;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty,
                    string.Format("Enter {0}", selectedPage.SessionVariables[index + 1].DisplayName));
            }

            return resultXml;
        }

        public string DisplayMenu(int increment)
        {
            string resultXml = string.Empty;

            var templateDictionary = new Dictionary<string, object>
                    {   
                        {"itemName", "menu"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", GetMenuItems(increment)},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

            _session.NextStep = 2;

            resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);

            return resultXml;
        }

        private MMUser GetUser()
        {
            var client = Router.GetDuisClient();

            var request = new RestRequest("/mmaccounts", Method.GET);

            request.AddParameter("msisdn", _session.Msisdn);

            var response = client.Execute<MMUser>(request);
            
            return response.Data;
        }
        public List<string> GetMenuItems(int currentPageIncrement)
        {
            _session.SelectListCurrentPage += currentPageIncrement;
            int currentPage = _session.SelectListCurrentPage;
            var items = DestinationPages.Where(x => !_closedPages.Contains(x.PageName)).Select(x => x.DisplayName).ToList();
            var noOfItems = items.Count;

            var startIndex = Math.Max(0, _selectItemsPerPage * (currentPage - 1));
            var endIndex = startIndex + _selectItemsPerPage - 1;

            var result = items.Where(x => items.IndexOf(x) >= startIndex && items.IndexOf(x) <= endIndex).ToList();

            if (noOfItems - endIndex > 1)
            {
                result.Add("Next");
            }

            if (currentPage > 1)
            {
                result.Add("Previous");
            }

            return result;
        }

        private List<Page> DestinationPages = new List<Page>()
        {
             new Page{ PageName = "MMBalance", DisplayName = "Balance",
                 SessionVariables = new List<PageSessionVariable> { } },

             new Page{ PageName = "MMTransfer", DisplayName = "Transfer from Wallet",
                 SessionVariables = new List<PageSessionVariable> {                   
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,7}$" } 
                 }
             },

             new Page{ PageName = "CollectRechargeType", DisplayName = "Airtime",
                 SessionVariables = new List<PageSessionVariable> { 
                 } 
             },

             new Page{ PageName = "MMBilling", DisplayName = "Pay Bill",
                 SessionVariables = new List<PageSessionVariable> { 
                 } 
             },

             new Page{ PageName = "PinChange", DisplayName = "Change PIN",
                 SessionVariables = new List<PageSessionVariable> { 
                 } 
             },

             new Page{ PageName = "MMHistory", DisplayName = "Last three transactions",
                 SessionVariables = new List<PageSessionVariable> { 
                 } 
             },

             new Page{ PageName = "MMCashout", DisplayName = "Cashout", 
                 SessionVariables = new List<PageSessionVariable> {
                     new PageSessionVariable { Name = "Amount", DisplayName = "amount", RegexValidator = "^[1-9]{1}[0-9]{0,7}$" }  
                 } 
             }
        };
    }
}