﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MMAccountTransfer : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public MMAccountTransfer()
        {

        }

        public MMAccountTransfer(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Determine debit account and request destination bank selection
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            var resultXml = string.Empty;
            _session.NextStep = 2;

            _router.SetTransactionAccount(_session);

            // get account banks
            // put the banks into a list

            var accountBanks = Router.DoDuisGetRequest<AccountBanks>("/mmtransfer/GetAccountBanks", new Dictionary<string, object> { 
                {"account_number", _session.DestinationAccount},
                {"sender_msisdn", _session.Msisdn}
            });


            if (accountBanks != null && accountBanks.banks.Count > 0)
            {
                for (int i = 1; i <= accountBanks.banks.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.BeneficiaryBankSelectPrefix, i), accountBanks.banks[i - 1]);
                }

                if (accountBanks.banks.Count == 1)
                {
                    resultXml = Step2();
                }

                else
                {
                    var templateDictionary = new Dictionary<string, object>
                    {   
                        {"itemName", "destination bank"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", accountBanks.banks.Select(x => x.name)},                        
                        {"routerUrl", Router._defaultEndpoint},
                    };

                    resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
                }
            }

            else
            {
                resultXml = _router.EndSessionWithMessage("Destination account number is invalid", _session.SessionKey);
            }

            return resultXml;
        }

        /// <summary>
        /// Determine destination bank, validate account
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;

            object selectedBank = null;
            _session.SelectDictionary.TryGetValue(string.Format("{0}{1}", SessionConstants.BeneficiaryBankSelectPrefix, _session.EnteredValue), out selectedBank);

            _session.SelectedBank = (Bank)selectedBank;

            if (_session.SelectedBank == null)
            {
                var beneficiaryBanks = _session.SelectDictionary.Where(x => x.Key.StartsWith(SessionConstants.BeneficiaryBankSelectPrefix)).ToDictionary(x => x.Key, x => x.Value);

                if (beneficiaryBanks.Count == 1)
                {
                    _session.SelectedBank = (Bank)beneficiaryBanks.ElementAt(0).Value;
                }

                else
                {
                    return _router.EndSessionWithMessage("Your selection is invalid.", _session.SessionKey);
                }
            }

            // get account name


            try
            {
                resultXml = GeneralHelper.ValidateCollectPinTransfer(_session, 3);
            }

            catch (TimeoutException ex)
            {
                resultXml = _router.EndSessionWithMessage(ex.Message, _session.SessionKey);
            }

            return resultXml;
        }


        /// <summary>
        /// Do the transfer
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            string resultXml = GeneralHelper.DoMobileMoneyTransfer(_session, _router, 3);
            return resultXml;
        }

    }
}