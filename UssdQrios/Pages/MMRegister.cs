﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class MMRegister : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public MMRegister()
        {

        }

        public MMRegister(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            _session.NextStep = 2;

            resultXml = XmlHelper.CollectValue(_session.SessionKey,
                "Welcome to Fidelity Mobile Money<br/><br/>You need to register to get started", "Enter your first name");

            return resultXml;
        }

        /// <summary>
        /// Check account validity, eligibility and msisdn match
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            string resultXml = string.Empty;

            _session.NextStep = 3;

            _session.MMParameters.FirstName = FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.ToTitleCase(_session.EnteredValue.Trim());

            resultXml = XmlHelper.CollectValue(_session.SessionKey, null, "Enter your last name");

            return resultXml;
        }

        /// <summary>
        /// Confirm PIN
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            string resultXml = string.Empty;

            _session.NextStep = 4;

            _session.MMParameters.LastName = FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.ToTitleCase(_session.EnteredValue.Trim());
            resultXml = XmlHelper.CollectValue(_session.SessionKey, null, "Enter your date of birth in format DDMMYYYY");

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            _session.NextStep = 5;

            _session.MMParameters.DobDdMmYyyy = _session.EnteredValue.Trim();

            var birthday = FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.ToDateTime(_session.MMParameters.DobDdMmYyyy,
                FidelityBank.CoreLibraries.Utility.Engine.DateFormat.DDMMYYYY);

            if (birthday == null)
            {
                _session.NextStep = 4;
                return XmlHelper.CollectValue(_session.SessionKey, string.Format("{0} is not a valid date in DDMMYYYY format", _session.MMParameters.DobDdMmYyyy),
                    "Please retry");
            }

            string instruction = string.Format("Registration details:<br/><br/>Name: {0} {1}<br/><br/>Birthday: {2:dd MMM yyyy}<br/><br/>",
                _session.MMParameters.FirstName, _session.MMParameters.LastName, birthday);

            resultXml = XmlHelper.CollectValue(_session.SessionKey, instruction, "Enter PIN to confirm");

            return resultXml;
        }

        public string Step5()
        {
            string resultXml = string.Empty;

            string newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = IsPasswordValid(newPassword, _session.MMParameters.DobDdMmYyyy);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 6;
                _session.NewPassword = newPassword;

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Confirm your new PIN" }
            });
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Please, enter a four-digit PIN" },
                    {"pinDescription", string.Format("{0}<br/>", isPasswordValid.reason)}

                });
            }

            return resultXml;
        }

        public string Step6()
        {
            string resultXml = string.Empty;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                var response = _router.DoDuisPostRequest<MMRegisterResponse>(new
                MMRegisterRequest
                {
                    msisdn = _session.Msisdn,
                    dob_DdMmYyyy = _session.MMParameters.DobDdMmYyyy,
                    first_name = _session.MMParameters.FirstName,
                    last_name = _session.MMParameters.LastName,
                    pin = _session.NewPasswordConfirm,
                    network = _session.MsisdnNetwork
                }, "/mmaccounts");

                if (response == null)
                {
                    resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                    _router.EndSession(_session.SessionKey);
                }

                else if (response.responseCode == Constants.DuisTimeoutCode)
                {
                    resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
                }

                else if (response.responseCode == Constants.DuisSuccessfulCode)
                {
                    // redirect to the original action
                    _session.NextStep = 1;
                    _session.NextPageName = _session.RedirectPageName;
                    _session.IsUserLoaded = true;
                    _session.IsUserActive = true;
                    _session.UserType = UserType.MobileMoney;
                    resultXml = _router.LoadPage();
                }

                else if (response.responseCode == Constants.DuisWeakPinCode)
                {
                    // restart PIN collection
                    resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Invalid PIN. Please, select a 4-digit PIN" }
                });

                    _session.NextStep = 5;
                }

                else
                {
                    resultXml = XmlHelper.ComposeGenericFailure(response.responseCode, response.responseMessage);
                    _router.EndSession(_session.SessionKey);
                }
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Your PINs do not match. Please, select a PIN with 4 digits" }
                });

                _session.NextStep = 5;
            }

            return resultXml;
        }

        private PasswordValidationResponse IsPasswordValid(string password, string dobDdMmYyyy)
        {
            var response = Router.DoDuisGetRequest<PasswordValidationResponse>("/mmaccounts/ispasswordvalid", new Dictionary<string, object> { 
            { "password", password },
            { "dob_DdMmYyyy", dobDdMmYyyy }
            });

            return response;
        }
    }
}