﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// <para>Contains methods that take in object and return XML</para>
    /// <para>DEPENDENT PAGES: Airline, Mobile Data</para>
    /// </summary>
    public class Billing : IUssdPage, ITransactionalPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private const int _selectItemsPerPage = 5;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public Billing()
        {

        }

        public Billing(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        // 
        // Compose airtime Billing input
        // Ask Duis if transaction requires PIN collection
        // No, forward transaction for consummation without PIN 
        // Yes: collect PIN and forward transaction for consummation

        /// <summary>
        /// <para>Select debit account</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "debit", this);
            return resultXml;
        }

        /// <summary>
        /// Determine debit account and request biller category selection
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;
            _session.NextStep = 3;

            if (string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                _router.SetTransactionAccount(_session);
            }

            // get biller information
            _session.SelectedBiller = GeneralHelper.GetBiller(_session.BillerCode);

            if (_session.SelectedBiller == null)
            {
                if (SessionObject._allBillers == null || SessionObject._allBillers.Count == 0)
                {
                    SessionObject._allBillers = GeneralHelper.GetAllBillers();
                }

                var categories = SessionObject._allBillers.Select(x => x.category).Distinct().ToList();

                var templateDictionary = new Dictionary<string, object>
                    {
                        {"itemName", "biller category"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", categories.ToList()},
                        {"routerUrl", Router._defaultEndpoint},
                    };

                resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);

                //resultXml = _router.EndSessionWithMessage("Invalid biller code", _session.SessionKey);
            }

            else
            {
                var noOfPlans = _session.SelectedBiller.Plans.Count;

                if (noOfPlans == 1)
                {
                    // validate plan if applicable and forward customer to PIN entry stage
                    _session.SelectedBillerPlan = _session.SelectedBiller.Plans[0];

                    resultXml = CollectCustomerId();
                }

                else
                {
                    // present plan selection menu
                    _session.EnteredValue = null;
                    _session.NextStep = 3;
                    resultXml = ProcessSession();
                }

            }

            return resultXml;
        }

        /// <summary>
        ///  Plan selection menu
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            var resultXml = string.Empty;

            if (_session.SelectedBiller == null)
            {
                // did not select biller at inception
                Object selectedBillerCategory;

                _session.SelectDictionary.TryGetValue(SessionConstants.SelectedBillerCategory, out selectedBillerCategory);

                if (selectedBillerCategory == null)
                {
                    // the customer has not chosen a category
                    try
                    {
                        var selectedIndex = Convert.ToInt32(_session.EnteredValue) - 1;
                        var category = SessionObject._allBillers.Select(x => x.category).Distinct().ElementAt(selectedIndex);
                        _session.SelectDictionary[SessionConstants.SelectedBillerCategory] = category;

                        var billers = SessionObject._allBillers.Where(x => x.category.Equals(category));

                        var templateDictionary = new Dictionary<string, object>
                    {
                        {"itemName", "biller"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", billers.Select(x => x.short_name)},
                        {"routerUrl", Router._defaultEndpoint},
                    };

                        _session.NextStep = 3;

                        resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
                    }

                    catch
                    {
                        resultXml = _router.EndSessionWithMessage("Your selection was invalid!", _session.SessionKey);
                    }
                }

                else
                {
                    // category is selected, select biller
                    try
                    {
                        var selectedIndex = Convert.ToInt32(_session.EnteredValue) - 1;

                        var selectedBiller = SessionObject._allBillers.Where(x => x.category.Equals((string)selectedBillerCategory)).ElementAt(selectedIndex);
                        _session.BillerCode = selectedBiller.code;
                        _session.NextStep = 2;
                        resultXml = ProcessSession();
                    }

                    catch
                    {
                        resultXml = _router.EndSessionWithMessage("Your selection was invalid!", _session.SessionKey);
                    }
                }

                return resultXml;
            }

            else
            {
                // a biller has been selected. Go ahead to choose a plan
                var plans = _session.SelectedBiller.Plans;

                if (plans.Count == 1)
                {
                    _session.SelectedBillerPlan = plans[0];
                    resultXml = CollectCustomerId();
                    return resultXml;
                }

                else if (_session.EnteredValue == null && _session.SelectedBillerPlan == null)
                {
                    // first time, show first plan page
                    _session.SelectListCurrentPage = 1;
                }

                else
                {
                    try
                    {
                        var newPage = SetSelectedPlanOrGetNewPage();

                        if (_session.SelectedBillerPlan == null)
                        {
                            _session.SelectListCurrentPage = newPage;
                        }

                        else
                        {
                            resultXml = CollectCustomerId();
                            return resultXml;
                        }
                    }

                    catch (Exception ex)
                    {
                        resultXml = _router.EndSessionWithMessage(ex.Message, _session.SessionKey);
                        return resultXml;
                    }
                }

                var selectItems = GetPlanSelectPage();

                _session.NextStep = 3;  //return to this method

                var templateDictionary = new Dictionary<string, object>
                    {
                        {"itemName", "plan"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", selectItems},
                        {"routerUrl", Router._defaultEndpoint},
                    };

                resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
            }

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            var biller = _session.SelectedBiller;
            var plan = _session.SelectedBillerPlan;

            if (string.IsNullOrWhiteSpace(_session.BillingCustomerId))
            {
                // if BillingCustomerId was not supplied as part of the short code
                _session.BillingCustomerId = _session.EnteredValue;
            }


            if (_session.Amount >= 1 || plan.IsAmountFixed || plan.UseValidationAmount)
            {
                _session.Amount = plan.IsAmountFixed ? plan.Amount : _session.Amount;

                string validationText = biller.RequiresValidation ? null : _session.BillingCustomerId;

                // do validation if applies, display validation info and collect PIN
                if (biller.RequiresValidation)
                {
                    // get validation name and add it to PIN collection text
                    var retrievedValidator = ValidateBill();
                    decimal validationAmount = 0;
                    bool isAmountInvalid = false;

                    if (retrievedValidator != null)
                    {
                        validationAmount = retrievedValidator.validation_amount;
                        validationText = retrievedValidator.validation_text;
                        isAmountInvalid = plan.UseValidationAmount && validationAmount < 1;
                    }

                    if (string.IsNullOrWhiteSpace(validationText) || isAmountInvalid)
                    {
                        return _router.EndSessionWithMessage(
                            string.Format("Sorry. We could not validate {0}. Please, check and try again later.", HttpUtility.HtmlEncode(_session.BillingCustomerId)),
                            _session.SessionKey);
                    }


                    if (plan.UseValidationAmount)
                    {
                        _session.Amount = _session.ValidationAmount = validationAmount;
                    }
                }

                _session.ValidationName = validationText;
                _session.NextStep = 5;

                string confirmationText = string.Format("Paying N{0:N2} for {1} {2} on behalf of {3}<br/><br/>Surcharge: N{4:N2}",
                    _session.Amount,
                    HttpUtility.HtmlEncode(biller.ShortName),
                    HttpUtility.HtmlEncode(plan.Name.Split('-')[0].Trim()),
                    HttpUtility.HtmlEncode(validationText),
                    biller.Surcharge);

                resultXml = XmlHelper.CollectPin(_session.SessionKey, confirmationText);
            }

            else
            {
                // collect amount
                _session.NextStep = 45;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, "Enter amount");
            }

            return resultXml;
        }

        /// <summary>
        /// Collect amount
        /// </summary>
        /// <returns></returns>
        public string Step45()
        {
            string resultXml = string.Empty;
            try
            {
                _session.Amount = Convert.ToInt32(_session.EnteredValue);
                _session.NextStep = 4;
                resultXml = ProcessSession();
            }

            catch
            {
                _session.NextStep = 45;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, "Invalid amount entered", "Enter amount");
            }

            return resultXml;
        }

        public string Step5()
        {
            var resultXml = string.Empty;
            resultXml = _router.ProcessTransaction(_session, this);
            return resultXml;
        }

        public string DoTransaction()
        {
            string resultXml = string.Empty;

            var requestBody = GetSessionBillingRequest();

            var endpoint = _session.UserType == UserType.MobileMoney ? "MMBilling" : "Billing";

            var response = _router.DoDuisPostRequest<BillingResponse>(requestBody, endpoint);

            if (response == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string advertContent = _router.GetAd(_session, "BillPayment");

                resultXml = XmlHelper.ParseTemplate("Billing\\successful.xml", new Dictionary<string, object> {
                    { "billerName", _session.SelectedBiller.ShortName },
                    { "planName", _session.SelectedBillerPlan.Name.Split('-')[0].Trim() },
                    { "advert", HttpUtility.HtmlEncode(advertContent) }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisInsufficientFundsCode)
            {
                if (_session.SelectedBillerPlan.IsAmountFixed || _session.UserType != UserType.InstantBanking)
                {
                    resultXml = _router.EndSessionWithMessage("Insufficient funds", _session.SessionKey);
                    return resultXml;
                }

                resultXml = _router.ProcessInsufficientFunds("/Billing/GetAffordable", _session, "pay", 5);
            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (response.response_code == Constants.DuisDailyLimitExceededCode)
            {
                if (_session.SelectedBillerPlan.IsAmountFixed || _session.UserType != UserType.InstantBanking)
                {
                    resultXml = _router.EndSessionWithMessage("Sorry. You have exceeded your daily limit!", _session.SessionKey);
                    return resultXml;
                }

                resultXml = _router.ProcessLimitExceeded("/Billing/GetLimits", _session, "pay", 5);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, response.response_message);
                _session.NextStep = 5;
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private string CollectCustomerId()
        {
            var resultXml = string.Empty;
            _session.NextStep = 4;

            if (string.IsNullOrWhiteSpace(_session.BillingCustomerId))
            {
                string customerIdCollectionText = string.Format("Enter your {0} {1}", _session.SelectedBiller.Name, _session.SelectedBiller.CustomerIdDescriptor);
                resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, customerIdCollectionText);
            }

            else
            {
                resultXml = ProcessSession();
            }


            return resultXml;
        }

        private BillingResponse ValidateBill()
        {
            string result = string.Empty;

            var input = GetSessionBillingRequest();

            var response = _router.DoDuisPostRequest<BillingResponse>(input, "/Billing/ValidateBill",
                Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]));

            //if (response != null)
            //{
            //    result = response.validation_text;
            //}

            return response;
        }

        private Dictionary<string, string> GetPlanSelectList()
        {
            // at every three items, insert a null value
            var result = new Dictionary<string, string>();
            var plans = _session.SelectedBiller.Plans;

            int pageNumber = 0;

            for (int i = 0; i < plans.Count; i++)
            {
                if (i % _selectItemsPerPage == 0)
                {
                    pageNumber++;
                }

                result.Add(string.Format("{0}{1}", pageNumber, (i) % (_selectItemsPerPage) + 1), plans[i].Name);
            }

            return result;
        }

        private int SetSelectedPlanOrGetNewPage()
        {
            Plan result = null;

            int newPageNumber = 0;

            int selectedValue = 0;

            bool isInt = Int32.TryParse(_session.EnteredValue, out selectedValue);

            if (isInt)
            {
                var noOfPages = _session.GetNoOfSelectPages(_session.SelectedBiller.Plans.Count, _selectItemsPerPage);

                try
                {
                    var planSelectList = GetPlanSelectList();
                    var selectPlanName = planSelectList[string.Format("{0}{1}", _session.SelectListCurrentPage, selectedValue)];
                    result = _session.SelectedBiller.Plans.SingleOrDefault(x => x.Name == selectPlanName);
                }

                catch
                {
                    if (selectedValue - _selectItemsPerPage == 1)
                    {
                        //next page request
                        newPageNumber = _session.SelectListCurrentPage + 1;
                    }

                    else if (selectedValue == 0)
                    {
                        // previous page request
                        newPageNumber = _session.SelectListCurrentPage - 1;
                    }

                    if (result == null && (newPageNumber == 0 || newPageNumber > noOfPages))
                    {
                        throw new ArgumentException(Constants.InvalidSelectionMessage);
                    }
                }
            }

            else
            {
                throw new ArgumentException(Constants.InvalidSelectionMessage);
            }

            _session.SelectedBillerPlan = result;
            return newPageNumber;
        }

        private IList<string> GetPlanSelectPage()
        {
            int currentPage = _session.SelectListCurrentPage;

            var planSelectList = GetPlanSelectList();

            var result = planSelectList.Where(x => x.Key.StartsWith(currentPage.ToString())).Select(x => x.Value).ToList();

            if (result.Count > _selectItemsPerPage)
            {
                result = result.Take(_selectItemsPerPage).ToList();
            }

            int numberOPages = _session.GetNoOfSelectPages(planSelectList.Count, _selectItemsPerPage);

            if (currentPage < numberOPages)
            {
                result.Add("Next");
            }

            if (currentPage > 1)
            {
                result.Add("Previous");
            }

            return result;
        }

        private BillingRequest GetSessionBillingRequest()
        {
            var billingRequest = new BillingRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                biller_code = _session.SelectedBiller.Code,
                customer_id = _session.BillingCustomerId,
                msisdn = _session.Msisdn,
                validation_text = _session.ValidationName,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                selected_plan_code = _session.SelectedBillerPlan.Code,
                is_via_direct_code = _session.IsViaDirectCode,
                token = _session.TransactionToken
            };

            return billingRequest;
        }
    }
}