﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using System.Linq;

namespace UssdQrios.Pages
{
    public class ReferralInfo : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public ReferralInfo()
        {

        }

        public ReferralInfo(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            _session.NextStep = 2;

            string confirmationMessage = XmlHelper.ParseTemplate("Referral\\confirmation.txt", new Dictionary<string, object> { });

            resultXml = XmlHelper.CollectPin(_session.SessionKey, confirmationMessage);

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var serviceRequest = new ServiceRequest
            {
                account_number = _session.DefaultAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey,
                is_via_direct_code = _session.IsViaDirectCode                
            };

            var response = _router.DoDuisPostRequest<ReferralPoint>(serviceRequest, "Referral/GetPoints");

            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                string referralCode = string.Format("{0}{1}#", 
                    ConfigurationManager.AppSettings[Constants.ConfQriosUssdPrefix], 
                    _session.Msisdn);

                resultXml = XmlHelper.ParseTemplate("Referral\\successful.xml", new Dictionary<string, object> {
                    { "points", response.points },
                    { "referralCode", referralCode }
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, response.response_message);
                _session.FailedLoginAttempts++;
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}