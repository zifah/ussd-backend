﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class ContactAccountOfficer : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public ContactAccountOfficer()
        {

        }

        public ContactAccountOfficer(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// <para>Select payment account</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "an", this);
            return resultXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;

            _router.SetTransactionAccount(_session);

            var request = GetRequestPayload();

            var response = _router.DoDuisPostRequest<GetAccountOfficerResponse>(request, "/AccountOfficer");

            if (response == null || response.response_code == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                if (string.IsNullOrWhiteSpace(response.account_officer_name) ||
                    (string.IsNullOrWhiteSpace(response.account_officer_phone) && string.IsNullOrWhiteSpace(response.account_officer_email)))
                {
                    resultXml = XmlHelper.ParseTemplate("account\\account-officer-empty.xml", new Dictionary<string, object> {
                    { "customer_care", HttpUtility.HtmlEncode(ConfigurationManager.AppSettings[Constants.ConfCustomerCareName]) },
                    { "customer_care_phone", HttpUtility.HtmlEncode(ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone]) }
                    });
                }

                else
                {
                    resultXml = XmlHelper.ParseTemplate("account\\account-officer-details.xml", new Dictionary<string, object> {
                    { "name", HttpUtility.HtmlEncode(response.account_officer_name) },
                    { "phone", HttpUtility.HtmlEncode(response.account_officer_phone) },
                    { "email", HttpUtility.HtmlEncode(response.account_officer_email) }
                });
                }

                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
        
        private GetAccountOfficerRequest GetRequestPayload()
        {
            var request = new GetAccountOfficerRequest
            {
                account_number = _session.SourceAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
            };

            return request;
        }
    }
}