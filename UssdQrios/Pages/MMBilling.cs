﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using System.Linq;
using UssdQrios.Dtos;
using System.Web;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MMBilling : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public MMBilling()
        {

        }

        public MMBilling(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            string resultXml = string.Empty;

            _session.SourceAccount = GeneralHelper.GetMobileAccount(_session.Msisdn);

            _session.NextPageName = "Billing";
            _session.NextStep = 2;
            resultXml = _router.LoadPage();

            return resultXml;
        }
    }
}