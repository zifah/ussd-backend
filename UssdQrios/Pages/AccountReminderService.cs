﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using System.Linq;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Customer uses this menu to remember their account number
    /// </summary>
    public class AccountReminderService : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public AccountReminderService()
        {

        }

        public AccountReminderService(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            _session.NextStep = 2;

            decimal charge = GeneralHelper.GetServiceCharge(_session.Msisdn);

            string confirmationMessage = string.Format("Requesting for your account and BVN numbers. This service costs N{0:N2}", charge);

            resultXml = XmlHelper.CollectPin(_session.SessionKey, confirmationMessage);

            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var serviceRequest = new ServiceRequest
            {
                account_number = _session.DefaultAccount,
                msisdn = _session.Msisdn,
                password = _session.TransactionPassword,
                reference_number = _session.SessionKey
            };

            var serviceResponse = _router.DoDuisPostRequest<MsisdnAccount>(serviceRequest, "Accounts/RememberAccount");

            if (serviceResponse == null || string.IsNullOrWhiteSpace(serviceResponse.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (serviceResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = XmlHelper.ParseTemplate("Service\\account-reminder.xml", new Dictionary<string, object> {
                    { "bvn", serviceResponse.bvn },
                    //TODO: Accounts should be a dictionary of: Account number, Account type or just form the list here
                    { "accounts", serviceResponse.accounts.Select(x => string.Format("{0} ({1})", x.account_number, x.account_type)) },
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (serviceResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, serviceResponse.response_message);
                _session.FailedLoginAttempts++;
            }

            else if (serviceResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(serviceResponse.response_code, serviceResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}