﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class SelfBlock : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public SelfBlock()
        {

        }

        public SelfBlock(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;

            if (string.IsNullOrWhiteSpace(_session.TransactionPassword))
            {
                resultXml = XmlHelper.CollectPin(_session.SessionKey,
                    string.Format("Please, enter the Instant Banking PIN for {0}", _session.BillingCustomerId));
            }

            else
            {
                resultXml = ProcessSession();
            }

            return resultXml;
        }

        /// <summary>
        /// Collect birthday in DdMmYy format
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            string resultXml = string.Empty;

            _session.TransactionPassword = string.IsNullOrWhiteSpace(_session.EnteredValue) ?
                _session.TransactionPassword : _session.EnteredValue.Trim();

            var blockRequest = new SelfBlockRequest
            {
                is_via_direct_code = _session.IsViaDirectCode,
                msisdn = _session.Msisdn,
                beneficiary_msisdn = _session.BillingCustomerId,
                password = _session.TransactionPassword
            } ;

            var blockResponse = _router.DoDuisPostRequest<SelfBlockResponse>(blockRequest, "/Accounts/SelfBlock");

            if (blockResponse == null || string.IsNullOrWhiteSpace(blockResponse.response_code))
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (blockResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (blockResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = XmlHelper.ParseTemplate("Account\\successful-block.xml", new Dictionary<string, object> { 
                    { "phone", blockRequest.beneficiary_msisdn },
                    { "customer_care", ConfigurationManager.AppSettings[Constants.ConfCustomerCareName] },
                    { "customer_care_phone",  ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone] },
                });

                _router.EndSession(_session.SessionKey);
            }

            else if (blockResponse.response_code == Constants.DuisFailedAuthenticationCode && _session.FailedLoginAttempts < 4)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, blockResponse.response_message);
                _session.NextStep = 2;
                _session.FailedLoginAttempts++;
            }

            else if (blockResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(blockResponse.response_code, blockResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}