﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Collects referral code for USSD account opening feature
    /// </summary>
    public class CollectReferralCode : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public CollectReferralCode()
        {

        }

        public CollectReferralCode(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            _session.NextStep = 2;
            string resultXml = XmlHelper.CollectValue(_session.SessionKey, string.Empty, "Enter Staff Id");
            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            _session.BillingCustomerId = _session.EnteredValue.Trim();
            _session.NextPageName = typeof(AccountOpening).Name;
            _session.NextStep = 1;
            resultXml = _router.LoadPage();
            return resultXml;
        }
    }
}