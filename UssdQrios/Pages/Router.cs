﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using UssdQrios.Dtos;
using UssdQrios.Exceptions;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using FidelityCoreEngine = FidelityBank.CoreLibraries.Utility.Engine;

namespace UssdQrios.Pages
{
    public class Router
    {
        private readonly HttpContext _httpContext = HttpContext.Current;
        private readonly NameValueCollection _headers = HttpContext.Current.Request.Headers;
        private const string _qriosHeaderShortCode = "X-Connector-Description";
        private static readonly string _qriosUssdPrefix = ConfigurationManager.AppSettings[Constants.ConfQriosUssdPrefix];

        public static readonly string _defaultEndpoint = string.Format("{0}/{1}/{2}",
            HttpContext.Current.Request.ApplicationPath, Constants.ApiRoot, Constants.RouterUrl).Replace("//", "/");
        private readonly Cache _cache = HttpRuntime.Cache;
        private readonly string _shortCode;
        private readonly string _subscriber;
        private readonly string _subscriberNetwork;
        private const int _sessionValidityMinutes = 10;
        private static readonly FidelityCoreEngine.LoggingUtil _logger =
            new FidelityCoreEngine.LoggingUtil(ConfigurationManager.AppSettings[Constants.ConfLoggingRootFolder]);
        private static readonly string[] _closedPages = ConfigurationManager.AppSettings[Constants.ConfClosedPages].Split(',');
        internal static readonly string[] _anonymousPages = ConfigurationManager.AppSettings[Constants.ConfAnonymousPages].Split(',');
        private const string _backwardNavCharacter = ".";

        public Router()
        {

        }

        public Router(string subscriber, string subscriberNetwork)
        {
            _shortCode = _headers[_qriosHeaderShortCode];
            _subscriber = subscriber;
            _subscriberNetwork = subscriberNetwork;
        }

        internal string LoadInstantBankingMenu()
        {
            var resultXml = RouteRequest(null, Constants.ConstInstantBanking);
            return resultXml;
        }

        internal string LoadMobileMoneyMenu()
        {
            var resultXml = RouteRequest(null, Constants.ConstMobileMoney);
            return resultXml;
        }

        public string RouteRequest(SessionObject sessionObject, string nextPage = null)
        {
            var isNewSession = sessionObject == null || string.IsNullOrWhiteSpace(sessionObject.SessionKey);

            string generalLogFile = string.Format("General\\{0:dd-MMM-yyyy}.txt", DateTime.Today);

            if (isNewSession)
            {
                var sessionKey = string.Format("Session{0}", Guid.NewGuid().ToString());

                sessionObject = new SessionObject();

                sessionObject.SessionKey = sessionKey;
                sessionObject.ShortCode = _shortCode;
                sessionObject.Msisdn = _subscriber;
                sessionObject.MsisdnNetwork = _subscriberNetwork == null ? null : _subscriberNetwork.ToUpper();

                var endpoint = _shortCode == null ? new ShortCodeResult() : GetEndpoint(_shortCode);

                nextPage = nextPage ?? endpoint.ClassName;

                if (nextPage == Constants.ConstMobileMoney && !_closedPages.Contains(nextPage))
                {
                    sessionObject.NextPageName = nextPage;
                }

                else if (!string.IsNullOrWhiteSpace(nextPage) && !_closedPages.Contains(nextPage))
                {
                    sessionObject.IsViaDirectCode = !string.IsNullOrWhiteSpace(endpoint.ClassName);
                    var user = GetUser(_subscriber);

                    if (user == null)
                    {
                        return XmlHelper.ParseTemplate("General\\block-of-text.xml", new Dictionary<string, object>() {
                    {"blockOfText", "Service temporarily unavailable" }
                });
                    }

                    else if (user.response_code == Constants.DuisSuccessfulCode)
                    {
                        sessionObject.DefaultAccount = user.default_account.Trim();
                        sessionObject.IsUserActive = user.is_active;
                        sessionObject.UserEnrolmentTime = user.enrolment_time;
                        sessionObject.IsUserLoaded = true;
                        sessionObject.UserType = UserType.InstantBanking;

                        if (user.is_active)
                        {
                            if (user.should_reset_password)
                            {
                                sessionObject.AccountName = user.name == null ? null : user.name.Trim();
                                sessionObject.NextPageName = "ForcedPinReset";
                                sessionObject.RedirectPageName = nextPage;
                            }

                            else
                            {
                                sessionObject.NextPageName = nextPage;
                            }
                        }

                        else if (_anonymousPages.Contains(nextPage))
                        {
                            sessionObject.NextPageName = nextPage;
                        }

                        else
                        {
                            sessionObject.NextPageName = "UserInactive";
                        }
                    }

                    else if (_anonymousPages.Contains(nextPage))
                    {
                        sessionObject.NextPageName = nextPage;
                    }

                    else if (user.response_code == Constants.DuisNotEnrolledCode)
                    {
                        // allow unregistered users to proceed to Instant banking menu
                        if (nextPage == Constants.ConstInstantBanking)
                        {
                            sessionObject.NextPageName = nextPage;
                        }

                        else
                        {
                            sessionObject.NextPageName = "NewUser";
                            sessionObject.RedirectPageName = nextPage;
                        }
                    }

                    else
                    {
                        return XmlHelper.ParseTemplate("General\\block-of-text.xml", new Dictionary<string, object>() {
                    {"blockOfText", "Service temporarily unavailable" }

                });
                    }

                    foreach (var pair in endpoint.KeyValuePairs)
                    {
                        var property = typeof(SessionObject).GetProperty(pair.Key);

                        if (property != null)
                        {
                            var value = Convert.ChangeType(pair.Value, property.PropertyType);
                            property.SetValue(sessionObject, value);
                        }
                    }
                }

                else
                {
                    sessionObject.NextPageName = _closedPages.Contains(nextPage) ? "PageClosed" : "Index";
                }

                var sessionList = new List<SessionObject>() { };
                _cache.Insert(sessionKey, sessionList, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(_sessionValidityMinutes));
            }

            else
            {
                var enteredValue = sessionObject.EnteredValue;
                var suppliedSessionKey = sessionObject.SessionKey;
                SetSessionKey(suppliedSessionKey);

                var sessionList = (List<SessionObject>)_cache[sessionObject.SessionKey];

                var last = sessionList.Last();

                if (enteredValue.Equals(_backwardNavCharacter) && sessionList.Count > 1)
                {
                    // Navigate backwards
                    // Track: Backward navigation by calling a backend API
                    sessionList.Remove(last);
                    sessionObject = sessionList.Last();
                    string logFile = string.Format("Sessions\\{0:dd-MMM-yyyy}\\{1}\\{2}.txt", DateTime.Today, sessionObject.Msisdn, sessionObject.SessionKey);
                    _logger.LogMessage($"Replaying previous session step with response: \r\n {sessionObject.Response}", logFile);
                    return sessionObject.Response;
                }

                else
                {
                    sessionObject = last;
                }

                if (sessionObject == null)
                {
                    _logger.LogMessage(HttpContext.Current.Request.RawUrl, generalLogFile);
                    return EndSessionWithMessage("Session ended abruptly!", suppliedSessionKey);
                }

                else
                {
                    sessionObject.EnteredValue = enteredValue;
                }
            }

            _httpContext.Items["SessionKey"] = sessionObject.SessionKey;

            string sessionLogFile = string.Format("Sessions\\{0:dd-MMM-yyyy}\\{1}\\{2}.txt", DateTime.Today, sessionObject.Msisdn, sessionObject.SessionKey);

            try
            {
                string urlWithoutEnteredValue = FidelityCoreEngine
                    .StringManipulation
                    .RemoveQueryStringByKey(_httpContext.Request.Url.AbsoluteUri, "enteredValue");
                _logger.LogMessage(string.Format("Starting session for request: {0}", urlWithoutEnteredValue), sessionLogFile);

                // freeze the latest session object at session start 
                var sessionList = (List<SessionObject>)_cache[sessionObject.SessionKey];
                var newObject = sessionObject.Copy();  //JsonConvert.DeserializeObject<SessionObject>(serialized);
                sessionList.Add(newObject);

                var cleanXml = LoadPage();

                _logger.LogMessage(string.Format("Ending session with response: \r\n {0}", cleanXml), sessionLogFile);

                return cleanXml;
            }            

            catch (Exception ex)
            {
                if (ex.InnerException is UssdException)
                {
                    UssdException ussdException = (UssdException)ex.InnerException;
                    return EndSessionWithMessage(string.Format("{0}{1}",
                        ussdException.ErrorMessage, ussdException.ErrorCode == null ? null : $" ({ussdException.ErrorCode})"),
                    sessionObject.SessionKey);
                }

                _logger.LogException(ex, sessionLogFile);
                return EndSessionWithMessage("Oops; an unexpected error occured!", sessionObject.SessionKey);
            }
        }

        /// <summary>
        /// Load the page indicated by Session.NextPageName
        /// </summary>
        /// <returns></returns>
        public string LoadPage()
        {
            var sessionObject = GetSessionObject();
            var pageType = Activator.CreateInstance(null, string.Format("UssdQrios.Pages.{0}", GetSessionObject().NextPageName)).Unwrap().GetType();
            var pageInstance = (IUssdPage)Activator.CreateInstance(pageType, this);
            // Call the handler method
            var result = pageInstance.ProcessSession();

            // return the output string
            sessionObject.Response = result;
            return result;
        }

        public SessionObject GetSessionObject(string sessionKey = null)
        {
            if (sessionKey == null)
                sessionKey = GetSessionKey();

            var theList = (List<SessionObject>)HttpContext.Current.Cache.Get(sessionKey);

            if (theList != null && theList.Count > 0)
            {
                return theList.Last();
            }
            return null;
        }

        internal static string GetSessionKey()
        {
            return Convert.ToString(HttpContext.Current.Items[Constants.ConstSessionKey]);
        }

        internal void EndSession(string sessionKey)
        {
            // SAVE THE SESSION STATE TO FILE (remove entered value)
            _cache.Remove(sessionKey);
        }

        public static ShortCodeResult GetEndpoint(string shortCode)
        {
            // *770*5050114937*1000# - funds transfer: ten_numeric_characters*number between 1 and 99 999 999 (no leading zeros)# ^[0-9]{10}*[1-9]{1}[0-9]{0,7}#$
            // *770*08033209433*1000# - recharge: eleven_numeric_characters*number between 1 and 99 999 999 (no leading zeros)#  ^[0-9]{11}*[1-9]{1}[0-9]{0,7}#$
            // *770*1000# - recharge: number between 1 and 99 999 999 (no leading zeros)# ^[1-9]{1}[0-9]{0,7}#$
            // *770*8*1000# - cash out: 8*number between 1 and 99 999 999 (no leading zeros)# ^8*[1-9]{1}[0-9]{0,7}#$
            // *770*00# - pin change: 00# ^00#$
            // *770*01# - pin reset: 01# ^01#$
            // *770*02# - Info update: 02# ^02#$
            // *770*0#  - balance enquiry: 0# ^0#$
            // *770*1946*1000# - bill payment: number between 1000 and 9999*number between 1 and 99 999 999 (no leading zeros)# ^[1-9]{1}[0-9]{3}*[1-9]{1}[0-9]{0,7}#$
            // *770*03*1# or *770*03*2# - alert subscription: 1 is subscription, 2 is subscription
            // *770*33*12345678# - alert subscription: 1 is subscription, 2 is subscription

            var regexInput = shortCode.Replace(_qriosUssdPrefix, string.Empty);

            var result = new ShortCodeResult
            {
                ShortCode = shortCode,
            };

            // do not rearrange indiscriminately
            IDictionary<string, string> shortCodes = new Dictionary<string, string>() {
                {"^(?!DestinationAccount*Amount#)[0-9]{10}\\*[1-9]{1}[0-9]{0,7}#$", "Transfer"},
                {"^(?!DestinationAccount*Amount*CollectNarration#)[0-9]{10}\\*[1-9]{1}[0-9]{0,7}\\*[0,1]{1}#$", "Transfer"},
                {"^(?!BillingCustomerId#)[0-9]{7}([0-9]{5})?#$", "RemitaPayment"},
                {"^(?!22*BillerCode*Amount#)22\\*[0-9]{8}\\*[1-9]{1}[0-9]{0,7}#$", "MicroPay"},
                {"^911#$", "ControlCard"}, //must be positioned over recharge due to precedence
                {"^0#$", "Balance"},
                {"^1#$", "InstantBanking"},
                {"^2#$", "AlertOptOut"}, //must be positioned over recharge due to precedence
                {"^3#$", Constants.ConstMobileMoney}, //must be positioned over recharge due to precedence
                {"^5#$", "AccountReminderService"}, //must be positioned over recharge due to precedence
                {"^6#$", "CancelCardReissuance"}, //must be positioned over recharge due to precedence
                {"^7#$", "ReferralInfo" }, //must be positioned over recharge due to precedence
                {"^(?!8*Amount#)8\\*[1-9]{1}[0-9]{0,4}000#$", "Cashout"},
                {"^(?!8*Amount#)8\\*[1-9]{1}[0-9]{0,4}000\\*1#$", "VervePaycode"},
                {"^911\\*9#$", "SelfUnlock"},
                {"^(?!911*BillingCustomerId#)911\\*[0-9]{11}#$", "SelfBlock"},
                {"^(?!911*BillingCustomerId*TransactionPassword#)911\\*[0-9]{11}\\*[0-9]{4,10}#$", "SelfBlock"},
                {"^(?!BillingCustomerId*Amount#)[0-9]{11}\\*[1-9]{1}[0-9]{0,7}#$", "Recharge"},
                {"^(?!BillingCustomerId*Amount*NotifyBeneficiary#)[0-9]{11}\\*[1-9]{1}[0-9]{0,7}\\*1#$", "RechargeWithNotify"},
                {"^(?!Amount#)[1-9]{1}[0-9]{0,7}#$", "Recharge"},
                {"^00#$", "PinChange"},
                {"^01#$", "PinReset"},
                {"^02#$", "Kyc"},
                {"^03#$", "ContactAccountOfficer"},
                {"^04#$", "RemitaOtpGenerate"},
                {"^(?!BillerCode*Amount#)[1-9]{1}[0-9]{3}\\*[1-9]{1}[0-9]{0,7}#$", "Billing"},
                {"^(?!BillerCode*BillingCustomerId*Amount#)[1-9]{1}[0-9]{3}\\*[0-9]+\\*[1-9]{1}[0-9]{0,7}#$", "Billing"},
                {"^(?!ReferrerPhone#)[0-9]{11}#$", "NewUser"},
                {"^000#$", "AccountOpening"},
                // BillerCode is the agent's SolId (or branch code)
                {"^(?!000*BillerCode#)000\\*[0-9]{3}#$", "CollectReferralCode"}
            };


            var match = shortCodes.FirstOrDefault(x => Regex.IsMatch(regexInput, x.Key));

            var doesMatchExist = !match.Equals(new KeyValuePair<string, string>());

            result.ClassName = doesMatchExist ? match.Value : string.Empty;

            if (doesMatchExist)
            {
                var kvpDictionary = new Dictionary<string, string>();
                var sections = match.Key.Split(new[] { "(?!", "#)" }, StringSplitOptions.RemoveEmptyEntries);

                if (sections.Count() == 3)
                {
                    var format = sections[1];
                    var value = regexInput.Replace("#", string.Empty);

                    var formatSplit = format.Split('*');
                    var valueSplit = value.Split('*');

                    for (int i = 0; i < formatSplit.Length; i++)
                    {
                        if (formatSplit[i] != valueSplit[i])
                        {
                            kvpDictionary.Add(formatSplit[i], valueSplit[i]);
                        }
                    }
                }
                result.KeyValuePairs = kvpDictionary;
            }

            return result;
        }

        internal static RestSharp.RestClient GetDuisClient()
        {
            //ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
            var duisRootUrl = ConfigurationManager.AppSettings["DuisRootUrl"];
            RestClient client = new RestClient(duisRootUrl);
            return client;
        }

        /// <summary>
        /// <para>Assumes that the phone number supplied is in 234... format. This is the current format which Qrios provides</para>
        /// </summary>
        /// <param name="msisdn234"></param>
        /// <returns></returns>
        internal static string ConvertToElevenDigitFormat(string msisdn234)
        {
            var result = msisdn234;

            if (msisdn234.StartsWith("234") && msisdn234.Length == 13)
            {
                result = string.Format("0{0}", msisdn234.Remove(0, 3));
            }

            return result;
        }

        /// <summary>
        /// <para>The response object should not be null even if the msisdn does not match a user</para>
        /// <para>If response object is null, there is a system error at the server</para>
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        internal static UssdUser GetUser(string msisdn)
        {
            var client = GetDuisClient();

            var request = new RestRequest("/accounts/GetUser", Method.GET);

            request.AddParameter("msisdn", msisdn);

            var response = client.Execute<UssdUser>(request);

            string generalLogFile = string.Format("General\\{0:dd-MMM-yyyy}.txt", DateTime.Today);
            _logger.LogMessage(string.Format("{0} {1} {2} {3}",
                response.StatusCode, response.StatusDescription, response.ErrorException, response.ErrorMessage), generalLogFile);

            return response.Data;
        }

        internal static T DoDuisGetRequest<T>(string endpoint, Dictionary<string, object> queryParameters, int timeoutSeconds = 0) where T : new()
        {
            var client = GetDuisClient();

            var request = new RestRequest(endpoint, Method.GET);

            foreach (var param in queryParameters)
            {
                request.AddParameter(param.Key, param.Value);
            }

            if (timeoutSeconds > 0)
            {
                request.Timeout = timeoutSeconds * 1000;
            }

            var response = client.Execute<T>(request);

            return response.Data;
        }

        internal IRestResponse DoDuisGetRequest(string endpoint, Dictionary<string, object> queryParameters)
        {
            var client = GetDuisClient();

            var request = new RestRequest(endpoint, Method.GET);

            foreach (var param in queryParameters)
            {
                request.AddParameter(param.Key, param.Value);
            }

            var response = client.Execute(request);
            return response;
        }

        internal T DoDuisPostRequest<T>(dynamic requestBody, string endpoint, int timeoutSeconds = 0) where T : new()
        {
            var duisClient = GetDuisClient();

            var request = new RestRequest(endpoint, Method.POST);

            request.AddJsonBody(requestBody);

            if (timeoutSeconds > 0)
            {
                request.Timeout = timeoutSeconds * 1000;
            }

            var response = duisClient.Execute<T>(request);

            return response.Data;
        }

        internal PasswordValidationResponse IsPasswordValid(string password, string account, UserType? userType = UserType.InstantBanking)
        {
            var endpoint = userType == UserType.MobileMoney ? "/mmaccounts/ispasswordvalid" : "/accounts/ispasswordvalid";

            var response = DoDuisGetRequest<PasswordValidationResponse>(endpoint, new Dictionary<string, object> {
            { "password", password },
            { "account", account }
            });

            return response;
        }

        internal IRestResponse DoDuisPostRequest(dynamic requestBody, string endpoint)
        {
            var duisClient = GetDuisClient();

            var request = new RestRequest(endpoint, Method.POST);

            request.AddJsonBody(requestBody);

            var response = duisClient.Execute(request);

            return response;
        }

        /// <summary>
        /// Ends the session and display the entered message on customer's screen
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        internal string EndSessionWithMessage(string message, string sessionKey)
        {
            var xmlString = XmlHelper.ParseTemplate("General\\block-of-text.xml", new Dictionary<string, object>() {
                    {"blockOfText", message }

                });

            EndSession(sessionKey);
            return xmlString;
        }

        internal string EndSessionWithXml(string xmlMessage, string sessionKey)
        {
            EndSession(sessionKey);
            return xmlMessage;
        }

        /// <summary>
        /// Ends the session and display the entered message on customer's screen
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        internal string EndSessionWithGenericFailure(string responseCode, string responseMessage, string sessionKey)
        {
            var xmlString = XmlHelper.ComposeGenericFailure(responseCode, responseMessage);
            EndSession(sessionKey);
            return xmlString;
        }

        /// <summary>
        /// </summary>
        /// <param name="_session"></param>
        /// <param name="accountApplication">This will be used by default to compose the prompt as follows: "Select your <accountApplication> account"</param>
        /// <param name="caller"></param>
        /// <param name="fullAccountDescription">If supplied, it will be used to prompt the customer's selection i.e. "Select <fullAccountDescription>"</param>
        /// <returns></returns>
        internal string DoSelectAccount(SessionObject _session, string accountApplication, IUssdPage caller, string fullAccountDescription = null)
        {
            string resultXml = string.Empty;

            var request = new RestRequest("/accounts/GetPhoneAccounts", Method.GET);

            request.AddParameter("phone", _session.Msisdn);

            var duisClient = GetDuisClient();

            var response = duisClient.Execute<MsisdnAccount>(request);

            var msisdnAccount = response.Data;

            if (msisdnAccount != null)
            {
                for (int i = 1; i <= msisdnAccount.accounts.Count; i++)
                {
                    _session.SelectDictionary.Add(string.Format("{0}{1}", SessionConstants.AccountSelectPrefix, i), msisdnAccount.accounts[i - 1]);
                }

                if (msisdnAccount.accounts.Count > 1)
                {
                    var templateDictionary = new Dictionary<string, object>
                    {
                        {"itemName", fullAccountDescription ?? accountApplication + " account"},
                        {"sessionKey", _session.SessionKey},
                        {"selectList", msisdnAccount.accounts.Select(x => x.masked_account_number)},
                        {"routerUrl", Router._defaultEndpoint},
                    };

                    resultXml = XmlHelper.ParseTemplate("General\\select-item.xml", templateDictionary);
                }

                else
                {
                    resultXml = caller.ProcessSession();
                }
            }

            else
            {
                resultXml = caller.ProcessSession();
            }

            return resultXml;
        }

        /// <summary>
        /// If account selection is invalid, use the default profile account
        /// </summary>
        /// <param name="_session"></param>
        internal void SetTransactionAccount(SessionObject _session)
        {
            string txnAccount = _session.DefaultAccount;

            if (!string.IsNullOrWhiteSpace(_session.EnteredValue))
            {
                object selectedAccount = null;
                _session.SelectDictionary.TryGetValue(string.Format("{0}{1}", SessionConstants.AccountSelectPrefix, _session.EnteredValue.Trim()), out selectedAccount);
                txnAccount = (Account)selectedAccount == null ? txnAccount : ((Account)selectedAccount).account_number;
            }

            _session.SourceAccount = txnAccount;
        }

        internal string GetBadPinMessage(string loginTriesLeftString)
        {
            int loginTriesLeft = Convert.ToInt32(loginTriesLeftString);

            string result = string.Empty;

            if (loginTriesLeft <= 2)
            {
                result = string.Format("Incorrect PIN<br/><br/>You have only {0} tr{1} left until the system locks you out. Consider choosing a new PIN using the 'Forgot PIN' menu<br/>",
                    loginTriesLeft, loginTriesLeft == 1 ? "y" : "ies");
            }

            else
            {
                result = "Incorrect PIN<br/>";
            }

            return result;
        }

        internal string ProcessInsufficientFunds(string endpoint, SessionObject session, string transactionAction, int nextStep)
        {
            string resultXml = string.Empty;

            var affordable = DoDuisGetRequest<AffordableResponse>(endpoint, new Dictionary<string, object>
                    {
                        { "accountNumber", session.SourceAccount }
                    });

            if (affordable == null || affordable.max_amount < 1)
            {
                resultXml = EndSessionWithMessage(string.Format("Insufficient funds"), session.SessionKey);
            }

            else
            {
                session.NextStep = nextStep;
                session.MinAmount = affordable.min_amount;
                session.MaxAmount = affordable.max_amount;

                string displayText = string.Format("Your account balance is insufficient for transaction. But you can still {0} between N{1:N2} and  N{2:N2}<br/>",
                    transactionAction, session.MinAmount, session.MaxAmount);

                resultXml = XmlHelper.CollectValue(session.SessionKey, displayText, "Enter amount");
                session.RetryCount += 1;
                session.TransactionRetryType = RetryType.AmountRetry;
            }

            return resultXml;
        }

        internal string ProcessLimitExceeded(string endpoint, SessionObject session, string transactionAction, int nextStep)
        {
            string resultXml = string.Empty;

            var limit = DoDuisGetRequest<LimitResponse>(endpoint, new Dictionary<string, object>
                    {
                        { "msisdn", session.Msisdn },
                        { "amount", session.Amount }
                    });

            if (limit == null || limit.max_amount < 1 || limit.count_left < 1)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = XmlHelper.ParseTemplate("General\\limit-exceeded.txt", new Dictionary<string, object> {
                    { "trueserve_phone", trueservePhone }
                });
                resultXml = EndSessionWithMessage(message, session.SessionKey);
            }

            else
            {
                session.MinAmount = limit.min_amount;
                session.MaxAmount = limit.max_amount;
                session.MaxBeforeTokenAmount = limit.max_before_token_amount;

                if (limit.is_limit_exceeded)
                {
                    session.NextStep = nextStep;
                    session.RetryCount += 1;

                    string displayText = string.Format("Amount is too {0}. You can {1} between N{2:N2} and  N{3:N2}<br/>",
                        limit.is_exceeded_limit_minimum ? "low" : "high", transactionAction, session.MinAmount, session.MaxAmount);

                    resultXml = XmlHelper.CollectValue(session.SessionKey, displayText, "Enter amount");
                    session.TransactionRetryType = RetryType.AmountRetry;
                }

                else if (limit.is_token_limit_exceeded)
                {
                    session.RedirectPageName = session.NextPageName;
                    session.RedirectStepNumber = session.NextStep;
                    session.NextStep = 1;
                    session.NextPageName = "TokenFlow";
                    resultXml = LoadPage();
                }
            }

            return resultXml;
        }

        internal string ProcessTransaction(SessionObject session, ITransactionalPage caller)
        {
            var resultXml = string.Empty;

            if (session.RetryCount > 0 && session.IsAmountRetry)
            {
                decimal amount = 0;

                Decimal.TryParse(session.EnteredValue, out amount);

                if (amount >= session.MinAmount && amount <= session.MaxAmount)
                {
                    session.Amount = amount;
                    resultXml = caller.DoTransaction();
                }

                else
                {
                    string displayText = string.Format("Please, enter an amount between N{0:N2} and  N{1:N2}<br/>",
                        session.MinAmount, session.MaxAmount);

                    resultXml = XmlHelper.CollectValue(session.SessionKey, displayText, "Enter amount");
                }
            }

            else if (session.RetryCount > 0 && session.IsTokenRetry)
            {
                session.TransactionToken = session.EnteredValue == null ? null : session.EnteredValue.Trim();
                resultXml = caller.DoTransaction();
            }

            else
            {
                session.TransactionPassword = session.EnteredValue == null ? null : session.EnteredValue.Trim();
                resultXml = caller.DoTransaction();
            }

            return resultXml;
        }

        /// <summary>
        /// Returns the ad content or null if we could not get the ad
        /// </summary>
        /// <param name="session"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        internal string GetAd(SessionObject session, string module)
        {
            if (session.UserType == UserType.MobileMoney)
            {
                return null;
            }

            var advert = DoDuisGetRequest<AdvertResponse>("/Adverts", new Dictionary<string, object> {
                { "msisdn", session.Msisdn },
                { "module", module }
            });

            string content = advert == null || advert.response_code != Constants.DuisSuccessfulCode ? null : advert.content;

            return content;
        }

        private static void SetSessionKey(string sessionKey)
        {
            HttpContext.Current.Items[Constants.ConstSessionKey] = sessionKey;
        }
    }

    public class ShortCodeResult
    {
        public string ShortCode { set; get; }
        public string ClassName { set; get; }
        public Dictionary<string, string> KeyValuePairs { set; get; }

        public ShortCodeResult()
        {
            KeyValuePairs = new Dictionary<string, string>();

        }
    }
}