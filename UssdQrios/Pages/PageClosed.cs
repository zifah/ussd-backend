﻿using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class PageClosed : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public PageClosed()
        {

        }

        public PageClosed(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }
        
        public string Step1()
        {
            return _router.EndSessionWithMessage(XmlHelper.ParseTemplate("General\\page-temporarily-closed.txt",
                new Dictionary<string, object>() { }), _session.SessionKey);
        }
    }
}