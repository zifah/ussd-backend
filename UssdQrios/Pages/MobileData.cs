﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MobileData : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;
        private const int _selectItemsPerPage = 3;
        private const string _categoryName = "Mobile Data";

        private readonly List<string> _rechargeTypes = new List<string>()
        {
            Constants.ConfConstantThisPhone,
            Constants.ConfConstantAnotherPhone
        };

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public MobileData()
        {

        }

        public MobileData(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            _session.NextStep = 2;
            string resultXml = XmlHelper.DoSelectItem("phone to renew", _session.SessionKey, _rechargeTypes);
            return resultXml;
        }


        public string Step2()
        {
            string resultXml = string.Empty;

            string selectedItem = GeneralHelper.GetSelectedItem(_rechargeTypes, _session);

            if (selectedItem == Constants.ConfConstantThisPhone)
            {
                _session.BillingCustomerId = _session.Msisdn;
                resultXml = SetBillerByNetwork(_session.MsisdnNetwork, 5);
            }

            else if (selectedItem == Constants.ConfConstantAnotherPhone)
            {
                _session.NextStep = 3;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, null, "Enter phone to renew");
            }

            else
            {
                // return to Step 1
                _session.NextStep = 1;
                resultXml = ProcessSession();
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;

            try
            {
                if (string.IsNullOrWhiteSpace(_session.BillingCustomerId))
                {
                    _session.BillingCustomerId = _session.EnteredValue.Trim();
                }
                resultXml = XmlHelper.DoSelectItem("network", _session.SessionKey, Constants.Networks);
                _session.NextStep = 4;
            }

            catch
            {
                _session.NextStep = 3;
                resultXml = XmlHelper.CollectValue(_session.SessionKey, "Incorrect phone number", "Enter phone to renew");
            }

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            try
            {
                string network = GeneralHelper.GetSelectedItem(Constants.Networks, _session);
                resultXml = SetBillerByNetwork(network, 5);
            }

            catch
            {
                resultXml = XmlHelper.DoSelectItem("network", _session.SessionKey, Constants.Networks);
            }

            return resultXml;
        }

        public string Step5()
        {
            string resultXml = string.Empty;
            _session.NextStep = 6;
            resultXml = _router.DoSelectAccount(_session, "debit", this);
            return resultXml;
        }


        public string Step6()
        {
            string resultXml = string.Empty;

            if (string.IsNullOrWhiteSpace(_session.SourceAccount))
            {
                _router.SetTransactionAccount(_session);
            }

            resultXml = RedirectToBilling();

            return resultXml;
        }


        public string SetBillerByNetwork(string networkName, int nextStep)
        {
            string resultXml = string.Empty;

            if (SessionObject._allBillers == null || SessionObject._allBillers.Count == 0)
            {
                SessionObject._allBillers = GeneralHelper.GetAllBillers();
            }

            try
            {
                _session.BillerCode = SessionObject
                    ._allBillers
                    .Single(x => x.category.Equals(_categoryName) && x.name.ToLower().StartsWith(networkName.ToLower()))
                    .code;

                _session.SelectedBiller = GeneralHelper.GetBiller(_session.BillerCode);

                _session.NextStep = nextStep;

                resultXml = ProcessSession();
            }

            catch
            {
                string message = string.Format("{0} data is currently not available. Please bear with us", networkName);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            return resultXml;
        }

        public string RedirectToBilling()
        {
            _session.NextPageName = "Billing";
            _session.EnteredValue = null;

            _session.NextStep = 3;

            string resultXml = _router.LoadPage();
            return resultXml;
        }        
    }
}