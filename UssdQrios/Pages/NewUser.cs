﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    public class NewUser : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        public NewUser()
        {

        }

        public NewUser(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// Collect account number
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;

            if (_session.IsUserLoaded)
            {
                //Not eligible to access this menu
                string message = XmlHelper.ParseTemplate("Account\\user-already-enrolled.xml", new Dictionary<string, object>(){
                    { "mainMenuCode", ConfigurationManager.AppSettings[Constants.ConfMainMenuCode] }
                });

                return _router.EndSessionWithXml(message, _session.SessionKey);
            }

            if(_session.ReferrerPhone != null)
            {
                _session.NextStep = 121;
                resultXml = GeneralHelper.DoProceedOrNot(_session.SessionKey, string.Format("Confirm referrer phone as {0}", _session.ReferrerPhone));
                return resultXml;
            }

            _session.NextStep = 2;

            resultXml = XmlHelper.ParseTemplate("Account\\start-enrolment.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint }
                });

            return resultXml;
        }

        public string Step121()
        {
            string resultXml = string.Empty;
            bool proceed = GeneralHelper.IsResponsePositive(_session);

            if(proceed)
            {
                _session.NextStep = 2;
                resultXml = XmlHelper.ParseTemplate("Account\\start-enrolment.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint }
                });
            }

            else
            {
                resultXml = XmlHelper.CollectValue(_session.SessionKey, null, "Enter referrer phone number");
                _session.NextStep = 122;
            }

            return resultXml;
        }

        public string Step122()
        {
            _session.ReferrerPhone = _session.EnteredValue.Trim();
            _session.NextStep = 1;
            string resultXml = _router.LoadPage();
            return resultXml;
        }

        /// <summary>
        /// Check account validity, eligibility and msisdn match
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            string resultXml = string.Empty;

            _session.NextStep = 3;

            _session.SourceAccount = _session.EnteredValue.Trim();

            var requestBody = new
            {
                msisdn = _session.Msisdn,
                account_number = _session.SourceAccount
            };

            var request = new RestRequest("/accounts/CanEnrol", Method.POST);

            request.AddJsonBody(requestBody);

            var response = _duisClient.Execute<UssdUser>(request);

            var canEnrolResponse = response.Data;

            if (canEnrolResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Select a PIN with 4 or more numbers or letters"}
                });
            }

            else if (canEnrolResponse.response_code == Constants.DuisEnrolmentClosedCode)
            {
                var enrolClosedMessage = "System is temporarily under maintenance<br/><br/>Please, retry after 15 minutes";
                resultXml = _router.EndSessionWithMessage(enrolClosedMessage, _session.SessionKey);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(canEnrolResponse.response_code, canEnrolResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        /// <summary>
        /// Confirm PIN
        /// </summary>
        /// <returns></returns>
        public string Step3()
        {
            string resultXml = string.Empty;

            var newPassword = _session.EnteredValue.Trim();

            var isPasswordValid = _router.IsPasswordValid(newPassword, _session.SourceAccount);

            if (isPasswordValid.is_valid)
            {
                _session.NextStep = 4;
                _session.NewPassword = newPassword;

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Confirm your new PIN" }
            });
            }

            else
            {
                _session.NextStep = 3;

                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Please, enter a PIN with 4 or more numbers or letters" },
                    {"pinDescription", string.Format("{0}<br/>", isPasswordValid.reason)}

                });
            }

            return resultXml;
        }        

        public string Step4()
        {
            string resultXml = string.Empty;

            _session.NextStep = 4;

            _session.NewPasswordConfirm = _session.EnteredValue.Trim();

            if (_session.NewPassword == _session.NewPasswordConfirm)
            {
                var response = _router.DoDuisPostRequest<UssdUser>(new
                {
                    msisdn = _session.Msisdn,
                    password = _session.NewPasswordConfirm,
                    account_number = _session.SourceAccount,
                    msisdn_network = _session.MsisdnNetwork,
                    is_via_direct_code = _session.IsViaDirectCode,
                    referrer_phone = _session.ReferrerPhone,
                    reference_number = _session.SessionKey
                }, "/accounts");

                if (response == null)
                {
                    resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                    _router.EndSession(_session.SessionKey);
                }

                else if (response.response_code == Constants.DuisTimeoutCode)
                {
                    resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
                }

                else if (response.response_code == Constants.DuisSuccessfulCode)
                {
                    _session.UserEnrolmentTime = DateTime.Now;

                    // redirect to the original action
                    if (_session.RedirectPageName == this.GetType().Name)
                    {
                        _session.RedirectPageName = Constants.ConstInstantBanking;
                    }

                    _session.NextStep = 1;
                    _session.DefaultAccount = _session.SourceAccount;
                    _session.NextPageName = _session.RedirectPageName;
                    resultXml = _router.LoadPage();
                }

                else if (response.response_code == Constants.DuisWeakPinCode)
                {
                    // restart PIN collection
                    resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Invalid PIN. Please, select a PIN with 4 or more numbers or letters" }
                });

                    _session.NextStep = 3;
                }

                else
                {
                    resultXml = XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                    _router.EndSession(_session.SessionKey);
                }
            }

            else
            {
                resultXml = XmlHelper.ParseTemplate("General\\enter-pin.xml", new Dictionary<string, object>() { 
                    {"sessionKey", _session.SessionKey },
                    {"routerUrl", Router._defaultEndpoint },
                    {"pinLabel", "Your PINs do not match. Please, select a PIN with 4 or more numbers or letters" }
                });

                _session.NextStep = 3;
            }

            return resultXml;
        }
    }
}