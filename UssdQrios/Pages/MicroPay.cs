﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using UssdQrios.Dtos;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;

namespace UssdQrios.Pages
{
    /// <summary>
    /// Contains methods that take in object and return XML
    /// </summary>
    public class MicroPay : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        /// <summary>
        /// <para>I leave this constructor here because at some point, the class is instantiated with reflection and I'm not sure how to pass in method parameters inline</para>
        /// <para>As soon as I figure out how to do that, this constructor can go to where all redundant blocks of code go</para>
        /// </summary>
        public MicroPay()
        {

        }

        public MicroPay(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        /// <summary>
        /// <para>Select payment account</para>
        /// </summary>
        /// <returns></returns>
        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = _router.DoSelectAccount(_session, "payment", this);
            return resultXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Step2()
        {
            var resultXml = string.Empty;

            _session.NextStep = 3;

            _router.SetTransactionAccount(_session);

            var validationRequest = GetRequestPayload();

            var validateResponse = _router.DoDuisPostRequest<MicroPayResponse>(validationRequest, "/MicroPay/Validate",
                Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]));

            if (validateResponse != null && validateResponse.response_code == Constants.DuisSuccessfulCode)
            {
                string productName = string.Format("{0} {1}",
                    validateResponse.biller_name,
                    validateResponse.payment_item_name);

                _session.ValidationName = validateResponse.biller_name;
                _session.Amount = validateResponse.payment_amount;
                _session.ValidationFee = validateResponse.surcharge_amount;

                string surchargeText = validateResponse.surcharge_amount > 0 ? $"<br/><br/>Surcharge: {validateResponse.surcharge_amount:N2}" : string.Empty;

                string displayMessage = $"Pay N{_session.Amount:N2} to {HttpUtility.HtmlEncode(_session.ValidationName)}{surchargeText}";
                resultXml = XmlHelper.CollectPin(_session.SessionKey, string.Format("{0}<br/>", displayMessage));
            }

            else
            {
                string validationError = validateResponse != null && !string.IsNullOrWhiteSpace(validateResponse.response_message) ?
                    validateResponse.response_message :
                    string.Format("We could not verify merchant #{0} at this time; please check and retry shortly", _session.BillerCode);

                resultXml = _router.EndSessionWithMessage(validationError, _session.SessionKey);
            }

            return resultXml;
        }

        public string Step3()
        {
            var resultXml = string.Empty;

            _session.TransactionPassword = _session.EnteredValue.Trim();

            var paymentRequest = GetRequestPayload();

            var microPayResponse = _router.DoDuisPostRequest<MicroPayResponse>(paymentRequest, "/MicroPay/Pay");

            if (microPayResponse == null || microPayResponse.response_code == null)
            {
                resultXml = XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Transaction is being processed; you will be notified shortly", _session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisDailyLimitExceededCode)
            {
                string trueservePhone = ConfigurationManager.AppSettings[Constants.ConfCustomerCarePhone];
                string message = string.Format(@"Limit over 24 hours exceeded. You can increase your transaction limits at any of our branches or call True Serve on {0}", trueservePhone);
                resultXml = _router.EndSessionWithMessage(message, _session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = _router.EndSessionWithMessage(
                    string.Format(
                    "Your payment of N{0:N2} to {1} was successful.", _session.Amount, HttpUtility.HtmlEncode(paymentRequest.merchant_name), _session.BillingCustomerId),
                    _session.SessionKey);
            }

            else if (microPayResponse.response_code == Constants.DuisFailedAuthenticationCode)
            {
                // allow PIN re-entry
                resultXml = XmlHelper.ShowReenterPin(_session.SessionKey, _router, microPayResponse.response_message);
                _session.FailedLoginAttempts++;
            }

            else if (microPayResponse.response_code == Constants.DuisAccountBlockedCode)
            {
                resultXml = GeneralHelper.RedirectToBlocked(_session, _router);
            }

            else
            {
                resultXml = XmlHelper.ComposeGenericFailure(microPayResponse.response_code, microPayResponse.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }

        private MicroPayRequest GetRequestPayload()
        {
            var request = new MicroPayRequest
            {
                account_number = _session.SourceAccount,
                amount = _session.Amount,
                surcharge = _session.ValidationFee,
                msisdn = _session.Msisdn,
                merchant_name = _session.ValidationName,
                merchant_code = _session.BillerCode,
                msisdn_network = _session.MsisdnNetwork,
                password = _session.TransactionPassword,
                reference_number = _session.RetryCount == 0 ?
                _session.SessionKey : string.Format("{0}R{1}", _session.SessionKey, _session.RetryCount),
                is_via_direct_code = _session.IsViaDirectCode,
                token = _session.TransactionToken
            };

            return request;
        }
    }
}