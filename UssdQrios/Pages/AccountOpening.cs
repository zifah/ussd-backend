﻿using RestSharp;
using System.Collections.Generic;
using UssdQrios.Interfaces;
using UssdQrios.Models;
using UssdQrios.Utilities;
using System;
using UssdQrios.Dtos;
using System.Configuration;
using FidelityBank.CoreLibraries.Utility.Engine;

namespace UssdQrios.Pages
{
    public class AccountOpening : IUssdPage
    {
        private SessionObject _session;
        private RestClient _duisClient;
        private Router _router;

        #region Constants
        private const string _constFirstName = "AccountOpening_FirstName";
        private const string _constLastName = "AccountOpening_LastName";
        private const string _constBirthday = "AccountOpening_Birthday";
        private const string _constBvn = "AccountOpening_Bvn";
        private const string _constEmail = "AccountOpening_Email";
        #endregion

        public AccountOpening()
        {

        }

        public AccountOpening(Router router)
        {
            _router = router;
            _session = router.GetSessionObject();
            _duisClient = Router.GetDuisClient();
        }

        public string ProcessSession()
        {
            var methodName = string.Format("Step{0}", _session.NextStep);
            var method = this.GetType().GetMethod(methodName);
            var result = (string)method.Invoke(this, null);
            return result;
        }

        public string Step1()
        {
            string resultXml = string.Empty;
            _session.NextStep = 2;
            resultXml = GeneralHelper.DoProceedOrNot(_session.SessionKey, "Do you have a Fidelity Bank account?");
            return resultXml;
        }

        public string Step2()
        {
            string resultXml = string.Empty;
            bool isResponsePositive = GeneralHelper.IsResponsePositive(_session) || _session.IsUserLoaded;

            if (isResponsePositive)
            {
                string xmlMessage = Utilities.XmlHelper.ParseTemplate("AccountOpening\\existing-customer.xml", new Dictionary<string, object>());
                resultXml = _router.EndSessionWithXml(xmlMessage, _session.SessionKey);
            }

            else
            {
                _session.NextStep = 3;
                resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, null, "First name");
            }

            return resultXml;
        }

        public string Step3()
        {
            string resultXml = string.Empty;

            _session.SelectDictionary.Add(_constFirstName, _session.EnteredValue.Trim().ToTitleCase());

            _session.NextStep = 4;
            resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, null, "Last name");

            return resultXml;
        }

        public string Step4()
        {
            string resultXml = string.Empty;

            _session.SelectDictionary.Add(_constLastName, _session.EnteredValue.Trim().ToTitleCase());

            _session.NextStep = 5;
            resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, null, "Enter your email or 0 if none");

            return resultXml;
        }

        public string Step5()
        {
            string resultXml = string.Empty;

            var theEmail = _session.EnteredValue.Trim().ToLower();

            bool noEmail = theEmail == "0";
            if (noEmail || ValidationUtil.IsValidEmail(theEmail))
            {
                _session.SelectDictionary.Add(_constEmail, noEmail ? null : theEmail);
                _session.NextStep = 6;
                resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, null, "DOB (DD-MM-YYYY)");
            }

            else
            {
                resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, null, "Invalid email. Enter your email or 0 if none");
            }

            return resultXml;
        }

        public string Step6()
        {
            string resultXml = string.Empty;

            var dobSplit = _session.EnteredValue.Trim().Split('-');
            DateTime? birthday = null;

            try
            {
                birthday = new DateTime(Convert.ToInt32(dobSplit[2]), Convert.ToInt32(dobSplit[1]), Convert.ToInt32(dobSplit[0]));
                _session.SelectDictionary.Add(_constBirthday, birthday);
            }

            catch
            {
                resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, "Incorrect date format. Please retry with specified format", "DOB (DD-MM-YYYY)");
                return resultXml;
            }


            _session.NextStep = 7;

            string firstName = (string)_session.SelectDictionary[_constFirstName];
            string lastName = (string)_session.SelectDictionary[_constLastName];

            string instruction = $"Name: {firstName} {lastName}<br/>DOB: {birthday:dd MMM yyyy}";

            resultXml = Utilities.XmlHelper.CollectValue(_session.SessionKey, instruction, "Enter your BVN to confirm or 0 if none");

            return resultXml;
        }

        public string Step7()
        {
            string resultXml = string.Empty;

            string bvn = _session.EnteredValue.Trim();
            bvn = bvn == "0" ? null : bvn;

            _session.SelectDictionary.Add(_constBvn, bvn);

            string firstName = (string)_session.SelectDictionary[_constFirstName];
            string lastName = (string)_session.SelectDictionary[_constLastName];
            string email = (string)_session.SelectDictionary[_constEmail];

            DateTime birthday = (DateTime)_session.SelectDictionary[_constBirthday];

            if(bvn == null)
            {
                return DoAccountCreation(firstName, lastName, birthday, bvn, email);
            }

            var timeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ConfGeneralValidationTimeoutSeconds]);

            var bvnResponse = Router.DoDuisGetRequest<BvnResponse>("Bvn", new Dictionary<string, object> {
                { "bvn", bvn },
                { "msisdn", _session.Msisdn },
                { "reference_number", _session.SessionKey },
            }, timeoutSeconds);

            // Compare BVN response to name and determine verdict

            if (bvnResponse == null || bvnResponse.response_code != Constants.DuisSuccessfulCode)
            {
                return _router.EndSessionWithMessage("Sorry. We could not validate your BVN. Please, try again.", _session.SessionKey);
            }

            else if (bvnResponse == null || bvnResponse.first_name == null || bvnResponse.last_name == null ||
                bvnResponse.first_name.Trim().ToTitleCase() != firstName ||
                bvnResponse.last_name.Trim().ToTitleCase() != lastName ||
                bvnResponse.birthday != birthday ||
                bvnResponse.phone != _session.Msisdn)
            {
                return _router.EndSessionWithMessage("The details supplied do not match the BVN. Please, try again.", _session.SessionKey);
            }

            return DoAccountCreation(firstName, lastName, birthday, bvn, email);
        }

        private string DoAccountCreation(string firstName, string lastName, DateTime birthday, string bvn, string email)
        {
            string resultXml;

            // Do Account Creation
            var response = _router.DoDuisPostRequest<OpenAccountResponse>(new OpenAccountRequest
            {
                birthday = birthday.ToString("dd MMM yyyy"),
                bvn = bvn,
                first_name = firstName,
                last_name = lastName,
                msisdn = _session.Msisdn,
                reference_number = _session.SessionKey,
                referrer_code = _session.BillingCustomerId,
                branch_code = _session.BillerCode,
                email = email,
            }, "BankAccount");


            // display result to customer (allow re-entry of PIN)
            if (response == null || string.IsNullOrWhiteSpace(response.response_code))
            {
                resultXml = Utilities.XmlHelper.ParseTemplate("General\\system-error.xml", new Dictionary<string, object>());
                _router.EndSession(_session.SessionKey);

            }

            else if (response.response_code == Constants.DuisTimeoutCode)
            {
                resultXml = _router.EndSessionWithMessage("Your request is being processed. We will notify you shortly.", _session.SessionKey);
                _router.EndSession(_session.SessionKey);
            }

            else if (response.response_code == Constants.DuisSuccessfulCode)
            {
                resultXml = Utilities.XmlHelper.ParseTemplate("AccountOpening\\successful.xml", new Dictionary<string, object> {
                    { "accountNumber", response.account_number }
                });
            }

            else
            {
                resultXml = Utilities.XmlHelper.ComposeGenericFailure(response.response_code, response.response_message);
                _router.EndSession(_session.SessionKey);
            }

            return resultXml;
        }
    }
}