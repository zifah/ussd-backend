﻿

namespace UssdQrios.Models
{
    public class Plan
    {
        public string Name { set; get; }
        public string Code { set; get; }
        public decimal Amount { set; get; }
        public bool IsAmountFixed { set; get; }
        public bool UseValidationAmount { set; get; }
    }
}