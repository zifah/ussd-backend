﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using UssdQrios.Models;
using UssdQrios.Pages;
using UssdQrios.Utilities;

namespace UssdQrios.Controllers
{

    public class MasterController : ApiController
    {
        //// POST api/values
        //public HttpResponseMessage Post([FromBody]SessionObject value)
        //{
        //    var router = new Router();
        //    var result = router.RouteRequest(value);
        //    return XmlHelper.GetXmlStringHttpResponse(result);
        //}

        public HttpResponseMessage Get([FromUri]QriosInput value)
        {
            var result = string.Empty;

            if (value != null && value.protocol != null && value.protocol.ToLower() == "ussd")
            {
                // new session
                if (string.IsNullOrWhiteSpace(value.sessionKey))
                {
                    var cleanSubscriber = Router.ConvertToElevenDigitFormat(value.subscriber ?? value.abonent);
                    var router = new Router(cleanSubscriber, value._operator);
                    result = router.RouteRequest(null);
                }

                else
                {
                    var router = new Router();

                    var enteredValue = HttpContext.Current.Request.Headers[Constants.ConfQriosEnteredValueHeader];
                    result = router.RouteRequest(new SessionObject { SessionKey = value.sessionKey, EnteredValue = enteredValue });
                }
            }

            return XmlHelper.GetXmlStringHttpResponse(result);
        }

        [HttpGet]
        public HttpResponseMessage Menu([FromUri]QriosInput value)
        {
            var result = string.Empty;

            if (value != null && value.protocol != null && value.protocol.ToLower() == "ussd")
            {
                var cleanSubscriber = Router.ConvertToElevenDigitFormat(value.subscriber ?? value.abonent);
                var router = new Router(cleanSubscriber, value._operator);
                result = router.LoadInstantBankingMenu();
            }

            return XmlHelper.GetXmlStringHttpResponse(result);
        }

        /// <summary>
        /// MMMenu means MobileMoneyMenu
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage MMMenu([FromUri]QriosInput value)
        {
            var result = string.Empty;

            if (value != null && value.protocol != null && value.protocol.ToLower() == "ussd")
            {
                var cleanSubscriber = Router.ConvertToElevenDigitFormat(value.subscriber ?? value.abonent);
                var router = new Router(cleanSubscriber, value._operator);
                result = router.LoadMobileMoneyMenu();
            }

            return XmlHelper.GetXmlStringHttpResponse(result);
        }

    }

    public class QriosInput
    {
        public string subscriber { set; get; }
        public string abonent { set; get; }
        public string protocol { set; get; }
        public string _operator { set; get; }
        public string sessionKey { set; get; }
        public string enteredValue { set; get; }
        public string page { set; get; }
    }
}
