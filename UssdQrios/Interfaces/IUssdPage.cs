﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UssdQrios.Pages;

namespace UssdQrios.Interfaces
{
    public interface IUssdPage
    {
        string ProcessSession();
    }
}