﻿using System;

namespace UssdQrios.Exceptions
{
    /// <summary>
    /// <para>Custom exceptions for the Duis framework</para>
    /// <para>Message passed in via constructor should be in the format: ErrorCode~ErrorMessage</para>
    /// </summary>
    public class UssdException : Exception
    {
        public UssdException(string message)
        {
            ErrorMessage = message;
        }

        public UssdException(string code, string message)
        {
            ErrorCode = code;
            ErrorMessage = message;
        }

        public UssdException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected UssdException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }

        /// <summary>
        /// ErrorCode
        /// </summary>
        public virtual string ErrorCode
        {
            set; get;
        }

        /// <summary>
        /// Error Message
        /// </summary>
        public virtual string ErrorMessage
        {
            set; get;
        }
    }
}