﻿using FidelityBank.MTNOnDemand.WCFAPI.WireObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCFAPI
{
    public class APISystem
    {
        public GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input)
        {
            var response = new GetAccountsByMSISDNOutput { ErrorCode = APIConstants.FailureCode, ErrorDetails = "Incomplete information provided." };

            if(input != null && ValidateMSISDN(input.MSISDN))
            {
                //TODO: Query Fincore for linked accounts
                //Package results into output format
                var msisdn = input.MSISDN;

                var dummyAccountOne = new Account
                {
                     Balance = Convert.ToDecimal(msisdn.Substring(8)), 
                     Name = "Judy Garland", 
                     Number = msisdn.Substring(1)
                };

                var dummyAccountTwo = new Account
                {
                    Balance = Convert.ToDecimal(msisdn.Substring(5, 3)),
                    Name = "Don Moen",
                    Number = msisdn.Substring(0, 10)
                };

                response.Accounts.Add(dummyAccountOne);
                response.Accounts.Add(dummyAccountTwo);

                response.ErrorCode = APIConstants.SuccessCode;
                response.ErrorDetails = APIConstants.SuccessMessage;
            }

            return response;
        }

        private bool ValidateMSISDN(string msisdn)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            return msisdn != null && msisdn.Length == 11 && regex.IsMatch(msisdn);
        }
    }
}