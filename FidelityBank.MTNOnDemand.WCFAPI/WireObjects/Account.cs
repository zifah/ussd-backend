﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCFAPI
{
    [DataContract]
    public class Account
    {
        [DataMember]
        public string Number { set; get; }

        [DataMember]
        public string Name { set; get; }

        [DataMember]
        public decimal Balance { set; get; }
    }
}