﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCFAPI
{
    public abstract class IAPIOutput
    {
        string ErrorCode { set; get; }
        string ErrorDetails { set; get; }
    }
}