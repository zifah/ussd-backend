﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCFAPI.WireObjects
{
    public class APIConstants
    {
        public const string SuccessCode = "00";
        public const string FailureCode = "01";        
        // For inconclusive scenarios, HTTP error response will be returned (exception, aah)
        public const string SuccessMessage = "Successful";
    }
}