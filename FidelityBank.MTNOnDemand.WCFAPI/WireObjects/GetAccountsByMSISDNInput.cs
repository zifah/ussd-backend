﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCFAPI
{
    [DataContract]
    public class GetAccountsByMSISDNInput
    {
        [DataMember]
        /// <summary>
        /// Format is: 08012345678
        /// </summary>
        public string MSISDN { set; get; }
    }
}
