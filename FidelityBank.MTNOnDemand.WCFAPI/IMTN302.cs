﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FidelityBank.MTNOnDemand.WCFAPI;

namespace FidelityBank.MTNOnDemand.WCFAPI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMTN302" in both code and config file together.
    [ServiceContract]
    public interface IMTN302
    {
        [OperationContract]
        GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input);
    }
}
