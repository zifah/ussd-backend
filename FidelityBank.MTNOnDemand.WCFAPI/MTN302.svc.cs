﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FidelityBank.MTNOnDemand.WCFAPI
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MTN302" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MTN302.svc or MTN302.svc.cs at the Solution Explorer and start debugging.
    public class MTN302 : IMTN302
    {
        APISystem engine = new APISystem();

        public GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input)
        {
            var response = engine.GetAccountsByMSISDN(input);
            return response;
        }
    }
}
