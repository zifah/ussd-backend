﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Fbp.Net.Integrations.ConsoleApp
{
    public class NipSsm
    {      
        private Logger logger = null;
        
        public void StartLogger()
        {
            if (logger == null)
                logger = new Logger();
        }

        public void SetLogger(Logger m_logger)
        {
            if (logger != null)
                m_logger.Log(logger.ToString());                
            
            logger = m_logger;
        }

        public NipSsm()
        {
            StartLogger();
        }

        public string GenerateKeyPairs(string user_name,string pwd)
        {
            logger.Log("NipSSM.GenerateKeyPairs():");
            string gen_msg = "GEN" + user_name + "#" + pwd;
            return SendMsgToNfpSSM(gen_msg);
        }

        public string EncryptMsg(string msg,string channel_node)
        {
            string ENCRYPT_ON = ConfigurationManager.AppSettings["ENCRYPT_ON"];
            if (ENCRYPT_ON.ToUpper() == "Y")
            {
                if (channel_node == "NIBSS")
                    msg = EncryptNfpChannel(msg);
                else
                    msg = EncryptLocalChannel(msg, channel_node);
            }
            return msg;
        }

        public string DecryptMsg(string msg,string channel_node)
        {
            string encrypt_on = ConfigurationManager.AppSettings["ENCRYPT_ON"];
            if (encrypt_on.ToUpper() == "Y")
            {
                if (channel_node == "NIBBS")
                    msg = DecryptNfpChannel(msg);
                else
                    msg = DecryptLocalChannel(msg,channel_node);
            }
            logger.Log("DecryptMsg:" + msg);
            return msg;
        }

        public string SendMsgToNfpSSM(string msg, string ssm_ip, string ssm_port)
        {
            string rsp_msg = null;
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IAsyncResult result = socket.BeginConnect(ssm_ip, Convert.ToInt16(ssm_port), null, null);
            result.AsyncWaitHandle.WaitOne(1000, true);
            socket.SendTimeout = 1000;
            socket.ReceiveTimeout = 1000;
            try
            {
                byte[] iso_req = Encoding.ASCII.GetBytes(msg);
                socket.Send(iso_req, 0, iso_req.Length, SocketFlags.None);
                Thread.Sleep(200);
                byte[] iso_rcv = null;
                if (socket.Available > 0)
                {
                    iso_rcv = new byte[socket.Available];
                    socket.Receive(iso_rcv);
                    rsp_msg = Encoding.ASCII.GetString(iso_rcv);
                }
                socket.Close();
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
            return rsp_msg;
        }

        private string SendMsgToNfpSSM(string msg)
        {
            const int MAX_RETRY = 5;
            string rsp_msg = null;
            string ssm_ip = ConfigurationManager.AppSettings["NFP_SSM_IP"];
            string ssm_port = ConfigurationManager.AppSettings["NFP_SSM_PORT"];

            int i = 1;
            while (rsp_msg == null && i <= MAX_RETRY)
            {
                logger.Log("Encrypt<=>Decrypt Attempt " + i.ToString());
                rsp_msg = SendMsgToNfpSSM(msg, ssm_ip, ssm_port);
                if (rsp_msg == null) 
                    Thread.Sleep(500);
                i++;
            }
            if (rsp_msg == null)
            {
                logger.Log("Primary SSM failed or too busy..Switching to Secondary SSM..");
                ssm_ip = ConfigurationManager.AppSettings["NFP_SSM_IP2"];
                
                while (rsp_msg == null && i <= MAX_RETRY*2)
                {
                    logger.Log("Encrypt<=>Decrypt Attempt " + i.ToString());
                    rsp_msg = SendMsgToNfpSSM(msg, ssm_ip, ssm_port);
                    if (rsp_msg == null)
                        Thread.Sleep(500);
                    i++;
                }
            }
            return rsp_msg;
        }

        private string EncryptNfpChannel(string msg)
        {
            string encr_msg = null;
            try
            {
                encr_msg =  SendMsgToNfpSSM("ENC" + msg);
                if ((encr_msg != null) && (encr_msg.IndexOf("ENC") == 0))
                    encr_msg = encr_msg.Substring(3);
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
            logger.Log("In NipSSM.EncryptNfpChannel(),EncrMsg:" + encr_msg);
            return encr_msg;
        }

        private string DecryptNfpChannel(string msg)
        {
            string decr_msg = null;
            try
            {
                logger.Log("NipSSM.DecryptNfpChannel()");
                logger.Log(msg);
                string SSM_PWD = ConfigurationManager.AppSettings["NFP_SSM_PWD"];
                decr_msg = SendMsgToNfpSSM("DEC" + SSM_PWD + "#" + msg);
                if ((decr_msg != null) && (decr_msg.IndexOf("DEC") == 0))
                    decr_msg = decr_msg.Substring(3);

                logger.Log("In NipSSM.DecryptNfpChannel(); decr_msg:" + decr_msg);
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
            return decr_msg;
        }

        private string EncryptLocalChannel(string msg,string keyname)
        {
            //encrypt msg using public key stored in pk_filepath for this 
            string pk_filepath = ConfigurationManager.AppSettings[keyname];
            //LogMsg("In NIPSSM.EncryptChannel() pk_filepath=>" + pk_filepath,false);
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

                logger.Log("pk_filepath for :" + keyname + ":" + pk_filepath);
                rsa.FromXmlString(File.ReadAllText(pk_filepath));
                byte[] msg_bytes = ASCIIEncoding.ASCII.GetBytes(msg);
                msg_bytes = rsa.Encrypt(msg_bytes, true);
                msg = ASCIIEncoding.ASCII.GetString(msg_bytes);
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
            //LogMsg("Leaving NIPSSM.EncryptChannel()", true);
            return msg;
        }

        private string DecryptLocalChannel(string msg,string keyname)
        {
            //decrypt msg using keyname as the containername
            //LogMsg("In NIPSSM.DecryptLocalChannel() keyname=>" + keyname,false);
            try
            {
                CspParameters cp = new CspParameters();
                cp.KeyContainerName = keyname;
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp);
                byte[] msg_bytes = ASCIIEncoding.ASCII.GetBytes(msg);
                msg_bytes = rsa.Decrypt(msg_bytes, true);
                msg = ASCIIEncoding.ASCII.GetString(msg_bytes);
            }
           catch (Exception ex)
            {
                logger.Log(ex);
            }
            //LogMsg("Leaving NIPSSM.DecryptLocalChannel()", true);
            return msg;
        }
    }
}
