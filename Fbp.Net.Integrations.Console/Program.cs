﻿using FidelityBank.CoreLibraries.Utility.Engine;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IronCladIntegration.VerifyUsername("tsodolamu");

            SecretUpdater();
            var converted = Convert.ChangeType("1", typeof(bool));

            var crypto = TripleDesEncrypt(Encoding.UTF8.GetBytes("2580;5145851169478915"), Encoding.UTF8.GetBytes("523DTD66658"));
            Console.WriteLine(crypto);

            var decrypto = TripleDesDecrypt(Convert.FromBase64String(crypto), Encoding.UTF8.GetBytes("523DTD66658"));

            var sha256 = Encoding.UTF8.GetBytes("523DTD6699");
            var result = Convert.ToBase64String((AES_Encrypt(Encoding.UTF8.GetBytes("1234"), sha256)));

            var output = Encoding.UTF8.GetString(AES_Decrypt(Convert.FromBase64String(result), SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes("523DTD66658"))));

            var sharers = new Dictionary<string, decimal>() {
                {"Qrios", 0.2M},
                {"MTN", 98.3M},
                {"Fidelity", 1.5M}
            };

            decimal bulkAmount = 309384.00M;

            var entries = RatioSplitter(bulkAmount, sharers);

            //TestKeyPairGeneration();
            TestSSMEncryption();
        }

        private static void SecretUpdater()
        {
            string connectionString = ""; //ConfigurationManager.ConnectionStrings[InfopoolCustom].ConnectionString;

            // Provide the query string with a parameter placeholder.
            string queryString = @"update BillPaymentConfigs
set JsonPlans = ''";


            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    using (command)
                    {
                        connection.Open();
                        var rowsAffected = command.ExecuteNonQuery();
                        Console.WriteLine("Success: {0} rows were updated", rowsAffected);
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error: {0}", ex.Message);
                    throw ex;
                }
            }

            Console.ReadLine();
        }

        static void TestKeyPairGeneration()
        {
            string username = "hafiz.adewuyi";
            string password = "hafiz@Nibss.co.uk";
            var nipSsm = new NipSsm();
            var keyPairs = nipSsm.GenerateKeyPairs(username, password);
        }

        static void TestSSMEncryption()
        {
            string channelNode = "NIBSS";
            string message = "Today is the day that the lord has made and I will rejoice in it.";

            var nipSsm = new NipSsm();
            var cipher = nipSsm.EncryptMsg(message, channelNode);
        }

        static void TestAuthentication()
        {
            //1000.00MTNRECHARGE0803493290010701455550779696317|Feb|2016/00:00:006030018131
            //var request = new ChargeCustomerInput
            //{
            //    Amount = 1000,
            //    Description = "MTNRECHARGE",
            //    MSISDN = "08034932900",
            //    RefCode = "107014555507796963",
            //    SourceAccount = "6030018131",
            //    RequestTime = DateTime.Today
            //};

            //var secretKey = "3cnRg7Z6sTKb5sbxij5b9462m998dNtL";

            //var auth = new AuthenticationSystem().ComputeHash(request, secretKey);
        }

        private static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        private static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        static Dictionary<string, decimal> RatioSplitter(decimal bulkAmount, Dictionary<string, decimal> personRatio)
        {
            var result = new Dictionary<string, decimal>();

            var numberOfEntries = personRatio.Count;

            // generate the create request inputs
            for (int i = 0; i < numberOfEntries; i++)
            {
                decimal amount = 0;
                var entry = personRatio.ElementAt(i);
                amount = decimal.Round(entry.Value * bulkAmount / 100, 3);

                result.Add(entry.Key, amount);
            }

            var rankings = new Dictionary<string, int>();

            foreach (var entry in result)
            {
                rankings.Add(entry.Key, Convert.ToInt32(entry.Value.ToString().Last().ToString()));
            }

            rankings.OrderByDescending(x => x.Value);

            decimal totalSplit = 0;

            for (int i = 0; i < numberOfEntries; i++)
            {
                decimal amount = 0;
                var matchAmount = result[rankings.ElementAt(i).Key];

                if (i == numberOfEntries - 1)
                {
                    amount = decimal.Round(bulkAmount - totalSplit, 2);
                }

                else
                {
                    amount = decimal.Round(matchAmount, 2);
                }

                result[rankings.ElementAt(i).Key] = amount;
                totalSplit += amount;
            }

            return result;
        }

        private static string TripleDesEncrypt(byte[] toEncrypt, byte[] key)
        {
            TripleDESCryptoServiceProvider desCryptoProvider = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

            byte[] byteHash;
            byte[] bytesToHash = toEncrypt;

            byteHash = hashMD5Provider.ComputeHash(key);
            desCryptoProvider.Key = byteHash;
            desCryptoProvider.Mode = CipherMode.ECB; //CBC, CFB

            string encoded =
                Convert.ToBase64String(desCryptoProvider.CreateEncryptor().TransformFinalBlock(bytesToHash, 0, toEncrypt.Length));
            return encoded;
        }

        private static string TripleDesDecrypt(byte[] toDecrypt, byte[] key)
        {
            TripleDESCryptoServiceProvider desCryptoProvider = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

            byte[] byteHash;
            byte[] bytesToHash = toDecrypt;

            byteHash = hashMD5Provider.ComputeHash(key);
            desCryptoProvider.Key = byteHash;
            desCryptoProvider.Mode = CipherMode.ECB; //CBC, CFB

            string decoded =
                Encoding.UTF8.GetString(desCryptoProvider.CreateDecryptor().TransformFinalBlock(bytesToHash, 0, toDecrypt.Length));
            return decoded;
        }
    }
}
