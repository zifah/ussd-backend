﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.ConsoleApp
{
    public class Logger
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }

        public  void Log(Exception ex)
        {
            if (ex != null)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}
