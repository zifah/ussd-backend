﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class AffordableOutput : IApiOutput
    {
        public AffordableOutput()
        {

        }

        public decimal min_amount { set; get; }
        public decimal max_amount { set; get; }
        public string account_number { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}