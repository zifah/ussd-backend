﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class TokenStatus
    {
        public bool IsValid { set; get; }
        public string Token { set; get; }
        public string ResponseCode { set; get; }
        public string ResponseDescription { set; get; }
        public bool IsInvalid { get { return !IsValid; } }
    }
}