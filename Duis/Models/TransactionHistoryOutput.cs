﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class TransactionHistoryOutput : IApiOutput
    {
        public List<TransactionDetails> transactions { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}