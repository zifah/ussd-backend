﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class AccountEnquiryOutput : IApiOutput
    {
        public decimal amount { set; get; }
        public Account account_details {set; get;}
        public bool is_blacklisted { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string account_name { set; get; }
    }
}