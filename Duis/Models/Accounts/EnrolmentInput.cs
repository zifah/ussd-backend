﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class EnrolmentInput : IApiInput
    {
        public string msisdn { set; get; }
        public string msisdn_network { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        /// <summary>
        /// For password change/reset
        /// </summary>
        public string new_password { set; get; }
        /// <summary>
        /// For password reset validation
        /// </summary>
        public string birthday_DdMmYy { set; get; }
        public string reference_number { set; get; }

        public string referrer_phone { set; get; }
    }
}