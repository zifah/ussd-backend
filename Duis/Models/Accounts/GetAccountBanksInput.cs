﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.Accounts
{
    public class GetAccountBanksInput
    {
        public string account_number { set; get; }
        public string sender_msisdn { set; get; }
    }
}