﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class LockoutInput : IApiInput
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string reason { set; get; }
        public string reference_number { set; get; }
    }
}