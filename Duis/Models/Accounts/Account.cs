﻿using Duis.Interfaces;
using FidelityBank.CoreLibraries.Infopool;
using System.Collections.Generic;
using System.Configuration;

namespace Duis.Models.Accounts
{

    public class MsisdnAccounts : IApiOutput
    {
        public string response_code { set; get; }

        public string response_message { set; get; }

        public string msisdn { set; get; }

        public IList<Account> accounts { set; get; }

        public string bvn { set; get; }

        public MsisdnAccounts()
        {
            accounts = new List<Account>();
        }
    }

    public class Account
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private static InfopoolSystem _infopoolSystem = new InfopoolSystem(_infopoolReader);

        public string account_number { set; get; }
        public string account_name { set; get; }
        public string account_type
        {
            set; get;
        }

        /// <summary>
        /// Masked account number; actually
        /// </summary>
        public string masked_account_number
        {
            get
            {
                var maskedAccount = _infopoolSystem.GetMaskedAccount(account_number);
                return maskedAccount;
            }
        }

        public string account_balance { set; get; }
        public string cleared_balance { set; get; }
        public string uncleared_balance { set; get; }
        public string lien { set; get; }
        public string minimum_balance { set; get; }
        public string birthday_ddMmYy { set; get; }
    }
}