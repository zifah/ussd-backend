﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class PasswordValidationInput : IApiInput
    {
        public string reference_number { set; get; }
        public string account { set; get; }
        public string password { set; get; }
        public string dob_DdMmYyyy { set; get; }
    }
}