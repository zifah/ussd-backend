﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class AccountEnquiryInput : IApiInput
    {
        public string msisdn { set; get; }
        public string msisdn_network { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public string reference_number { set; get; }
        public string transaction_type { set; get; }
    }
}