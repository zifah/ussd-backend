﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class SelfBlockInput : IApiInput
    {
        public string msisdn { set; get; }
        public string beneficiary_msisdn { set; get; }
        public string password { set; get; }
        public string reference_number { set; get; }
    }
}