﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class PasswordValidationOutput
    {
        public bool is_valid { set; get; }
        /// <summary>
        /// reason why the password is not valid
        /// </summary>
        public string reason { set; get; }
    }
}