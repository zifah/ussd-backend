﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    /// <summary>
    /// <para>List of banks is automatically initialized</para>
    /// </summary>
    public class GetAccountBanksOutput : IApiOutput
    {
        /// <summary>
        /// Initializes banks with an empty list
        /// </summary>
        public GetAccountBanksOutput()
        {
            banks = new List<Bank>();
        }

        public string account_number { set; get; }
        /// <summary>
        /// Initialized to an empty list by the default constructor
        /// </summary>
        public List<Bank> banks { set; get; }        
        public string response_code { set; get; }
        public string response_message { set; get; }

    }
}