﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class SelfUnlockInput : IApiInput
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string pan_last_four_digits { set; get; }
        public string birthday_DdMmYy { set; get; }
        public bool is_via_direct_code { get; set; }
        public string reference_number { set; get; }
    }
}