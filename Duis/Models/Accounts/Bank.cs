﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.Accounts
{
    public class Bank
    {
        public string code_v1 { set; get; }
        public string code_v2 { set; get; }
        public string name { set; get; }
        public string name_short { set; get; }
        public decimal transfer_charge { set; get; }
    }
}