﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class InfoUpdateInput : IApiInput
    {
        public string msisdn { set; get; }
        public string password { set; get; }
        /// <summary>
        /// possible values: email, bvn, address
        /// </summary>
        public string info_type { set; get; }
        public string new_value { set; get; }
        public string reason { set; get; }
        public string reference_number { set; get; }
    }

}