﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class InfoUpdateOutput : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public int login_attempts_left { set; get; }
    }
}