﻿using System;
using Duis.Interfaces;

namespace Duis.Models.BankAccounts
{
    public class CreateAccountOutput : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string account_number { set; get; }
        public string customer_id { set; get; }
    }
}