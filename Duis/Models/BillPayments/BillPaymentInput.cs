﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class BillPaymentInput : IApiInput
    {
        public BillPaymentInput()
        {
            require_password = true;
        }

        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public string biller_code { set; get; }
        public string product_name { set; get; }
        public decimal amount { set; get; }
        /// <summary>
        /// This field was added because of MicroPay where the client passes in the surcharge dynamically
        /// </summary>
        public decimal? surcharge { set; get; }
        public string selected_plan_code { set; get; }
        public string customer_id { set; get; }
        public string validation_text { get; set; }
        public string reference_number { set; get; }
        internal bool require_password { set; get; }
        public bool is_via_direct_code { get; set; }
        public string token { get; set; }
        public string network { set; get; }
    }

}