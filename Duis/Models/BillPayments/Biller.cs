﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.BillPayments
{
    public class Biller
    {
        public string name { set; get; }
        public string short_name { set; get; }
        public string code { set; get; }
        public string category { set; get; }
    }
}