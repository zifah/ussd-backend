﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class CashoutOutput : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string paycode { set; get; }
        public string payment_reference { set; get; }
        public DateTime? expiration_date { set; get; }
    }
}