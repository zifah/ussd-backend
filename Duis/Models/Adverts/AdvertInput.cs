﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class AdvertInput
    {
        public string msisdn { set; get; }
        public string module { set; get; }
    }

}