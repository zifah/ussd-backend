﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class ReferralOutput : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public double points { set; get; }
        public DateTime expiration_date { set; get; }
        public string expiration_day { set; get; }
    }
}