﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class MarkForReversalInput : IApiInput
    {
        public long transaction_id { set; get; }
        /// <summary>
        /// <para>Set to true if to reverse transaction</para>
        /// <para>Set to false if to not reverse transaction</para>
        /// </summary>
        public bool reverse_flag { set; get; }
        public string reference_number { set; get; }
    }

    public class MarkForReversalOutput :  IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}