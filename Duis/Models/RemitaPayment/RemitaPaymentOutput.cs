﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class RemitaPaymentOutput : IApiOutput
    {
        public RemitaPaymentOutput()
        {

        }

        public string response_code { set; get; }
        /// <summary>
        /// <para>For validation, the content of this field should be displayed except it is null</para>
        /// <para>In that case, a generic validation failure message should be displayed</para>
        /// </summary>
        public string response_message { set; get; }
        public string payment_reference { set; get; }
        public decimal payment_amount { set; get; }
        public string plan_code { set; get; }
        public string payer_name { set; get; }
        public string biller_name { set; get; }
        public string payment_item_name { set; get; }
    } 
}