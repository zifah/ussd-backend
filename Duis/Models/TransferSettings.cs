﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class TransferSettings
    {
        public string ProviderName { set; get; }
        public string ProviderClassFqn { set; get; }
        public string ProviderAssemblyName { set; get; }

        public string ProcessingCodePrefix { get; set; }
        public string ProcessingCodeColumn { get; set; }

        public string ProviderParametersJson { set; get; }
        public string InsufficientFundsCode { get; set; }
    }
}