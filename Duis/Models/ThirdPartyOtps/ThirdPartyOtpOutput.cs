﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class ThirdPartyOtpOutput : IApiOutput
    {
        public string otp { set; get; }
        public DateTime? expiration_time { set; get; }
        public decimal convenience_fee { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}