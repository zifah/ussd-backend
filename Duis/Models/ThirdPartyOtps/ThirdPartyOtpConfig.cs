﻿namespace Duis.Models
{
    public class ThirdPartyOtpConfiguration
    {
        public string Name { set; get; }
        public int ValidSeconds { set; get; }
        public bool IsActive { set; get; }
        public decimal CostNaira { set; get; }
        public int Length { set; get; }
        // Phone, Account, Phone and Account
        public string[] LookupKeys { set; get; }
        public int TransactionsDone { set; get; }
        public int DaysActive { set; get; }

    }
}