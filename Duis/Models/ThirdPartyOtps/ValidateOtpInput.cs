﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class ValidateOtpInput : IApiInput
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string purpose { set; get; }
        public string otp { set; get; }
        public string reference_number { set; get; }
        public bool is_via_direct_code { set; get; }
    }

}
