﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class TransactionDetails
    {
        // Ref DateTime Type BeneficiaryId Amount Status
        public string reference { set; get; }
        public DateTime date { set; get; }
        public string type { set; get; }
        public string beneficiary { set; get; }
        public decimal amount { set; get; }
        public string status { set; get; }
    }
}