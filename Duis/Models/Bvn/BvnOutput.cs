﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.Bvn
{
    public class BvnOutput : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public DateTime birthday { set; get; }
        public string bvn { set; get; }
        public string phone { set; get; }
        public string email { set; get; }
        public bool is_watchlisted { set; get; }
        /// <summary>
        /// Male or Female
        /// </summary>
        public string gender { set; get; }
        public string enrolment_bank_code { set; get; }
        public DateTime enrolment_date { set; get; }
    }
}