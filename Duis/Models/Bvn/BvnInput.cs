﻿using System;
using Duis.Interfaces;

namespace Duis.Models.Bvn
{
    public class BvnInput : IApiInput
    {
        public string bvn { set; get; }
        public string msisdn { set; get; }
        public string reference_number { set; get; }
    }
}