﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.Bvn
{
    public class BvnValidationParameters
    {
        public string RootUrl { set; get; }
        public string ClientId { set; get; }
        public string ClientKey { set; get; }
    }
}