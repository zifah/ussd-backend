﻿using Duis.Interfaces;
using Newtonsoft.Json;
using System.Net;

namespace Duis.Models.Clickatell
{
    public class ClickatellRechargeOutput : IApiOutput
    {
        /// <summary>
        /// <para>The response code used on the reserveFunds Response indicates the result of attempting to reserve funds on a buyer’s account.</para>
        /// </summary>
        public string responseCode { set; get; }
        [JsonIgnore]
        public string responseMessage { set; get; }
        public string reserveFundsTxnRef { set; get; }
        
        /// <summary>
        /// Status Code for Transact Result response
        /// </summary>
        [JsonIgnore]
        public HttpStatusCode StatusCode { set; get; }


        #region Exists simply to make this class conform to IApiOutput, hence the JSON ignore. No one uses it
        [JsonIgnore]
        public string response_code { set; get; }
        [JsonIgnore]
        public string response_message { set; get; }
        #endregion
    }
}