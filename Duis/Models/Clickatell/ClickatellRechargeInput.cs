﻿using Duis.CustomAttributes;
using Duis.Interfaces;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Duis.Models.Clickatell
{
    public class ClickatellRechargeInput : IApiInput
    {
        /// <summary>
        /// Unique Bank reference to the transaction
        /// </summary>
        public string clientTxnRef { get; set; }

        /// <summary>
        /// Unique payd reference to the transaction 
        /// </summary>
        public string raasTxnRef { get; set; }

        /// <summary>
        /// Unique reference to the product that was purchased
        /// </summary>
        public int productId { get; set; }

        /// <summary>
        /// Unique reference to the product type (classification, group or line) that was purchased, for instance Pre-purchased Airtime or Data
        /// </summary>
        public int productType { get; set; }

        /// <summary>
        /// The TPV (Total Purchase Value) or price of the product that the buyer bought, in the lowest denomination (e.g. cents or pennies), for instance $250.00 would equate to the value 25000
        /// </summary>
        public int amount { get; set; }

        public decimal amountNaira
        {
            get
            {
                return amount / 100;
            }
        }

        /// <summary>
        /// <para>msisdn which starts with 234</para>
        /// <para>The unique identifier for the buyer, as recognised by the bank and used to look up the buyer’s bank account from where the payment must be made. This is typically the buyer’s mobile phone number (MSISDN) but can be another unique identifier recognised by both payd and the bank</para>
        /// </summary>
        public string sourceIdentifier { get; set; }

        /// <summary>
        /// <para>msisdn which starts with 234</para>
        /// <para>The unique identifier for the intended recipient (or target/destination) of the product being purchased. This may be the buyer (“Self”) or someone else (a so-called “3rd Party purchase”)</para>
        /// </summary>
        public string targetIdentifier { get; set; }

        /// <summary>
        /// The channel is the user interface, platform or service that the buyer used to initiate a purchase from.
        /// </summary>
        public int channelId { get; set; }

        /// <summary>
        /// This is the specific name of the channel
        /// </summary>
        public string channelName { get; set; }

        /// <summary>
        /// This is a unique reference to the channel-specific engagement when the purchase was initiated (for instance the USSD session reference, if the purchase was over USSD)
        /// </summary>
        public string channelSessionId { get; set; }

        /// <summary>
        /// An authentication code provided by the buyer which the bank can use to verify the buyer’s identity
        /// </summary>
        [JsonProperty("authCode")]
        [ApiAuthIgnore]
        public string AuthCode { get; set; }
        
        /// <summary>
        /// This field is only used to persist legacy interfaces between payd and certain banks and should not be used unless under specific arrangement with payd
        /// </summary>
        public string accountIdentifier { get; set; }

        /// <summary>
        /// This is the universally unique identifier that the bank must generate when it reserves funds on a buyer’s account
        /// </summary>
        public string reserveFundsTxnRef { set; get; }
        /// <summary>
        /// <para>The response code used on the transactResult Request indicates the outcome of the digital dispensing of the product by the supplier.</para>
        /// </summary>
        public string responseCode { set; get; }
        /// <summary>
        /// <para>The timestamp of when an API request or response was sent, in iso8601 format (see https://en.wikipedia.org/wiki/ISO_8601) Format: yyyy-MM-dd HH:mm:ss.SSSXXX</para>
        /// </summary>
        public DateTime timestamp { set; get; }

        [JsonIgnore]
        public string reference_number
        {
            get
            {
                return raasTxnRef;
            }

            set
            {

            }
        }

        public bool ShouldSerializeAuthCode()
        {
            return false;
        }


        public bool ShouldSerializeOneTimePassword()
        {
            return false;
        }
    }

}