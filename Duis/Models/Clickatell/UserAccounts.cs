﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.Clickatell
{
    public class TransactionVerifyResponse
    {
        public string responseCode { set; get; }
        public List<AccountHolder> accounts { set; get; }
        public bool requiresToken { set; get; }
        /// <summary>
        /// This should be in the minor currency (kobo)
        /// </summary>
        public int minimumAmount { set; get; }
        /// <summary>
        /// This should be in the minor currency (kobo)
        /// </summary>
        public int maximumAmount { set; get; }
    }

    public class AccountHolder
    {
        public string account { set; get; }
        public int balance { set; get; }
    }
}