﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class TokenInput : IApiInput
    {
        public string msisdn { set; get; }
        public string token_serial { set; get; }
        public string account_number { set; get; }
        /// <summary>
        /// This should be null except the token serial is already assigned to a username (whose value will be here)
        /// </summary>
        public string current_username { set; get; }
        public string comment { set; get; }
        public string reference_number { set; get; }
        public string token_response { set; get; }
        public string issuing_sol_id { set; get; }
    }

}