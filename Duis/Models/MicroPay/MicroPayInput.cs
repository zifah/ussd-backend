﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class MicroPayInput : IApiInput
    {
        public string msisdn { set; get; }
        public string msisdn_network { set; get; }
        public string password { set; get; }
        public string account_number { set; get; }
        public decimal amount { set; get; }
        public decimal surcharge { set; get; }
        public string reference_number { set; get; }
        public string merchant_code { set; get; }
        public string merchant_name { set; get; }
        public bool is_via_direct_code { get; set; }
        public string token { set; get; }
    }

}