﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.Cards
{
    public class AccountCards
    {
        public string account_number { set; get; }
        public List<Card> cards { set; get; }
    }
}