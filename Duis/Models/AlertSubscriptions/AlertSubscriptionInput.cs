﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.AlertSubscriptions
{
    public class AlertSubscriptionInput : IApiInput
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string password { set; get; }
        public string product_id { set; get; }
        public bool subscribe { set; get; }
        public string reference_number { get; set; }
        public bool is_via_direct_code { get; set; }
    }
}