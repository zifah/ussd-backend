﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models.AlertSubscriptions
{
    public class AlertSubscriptionOutput : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}