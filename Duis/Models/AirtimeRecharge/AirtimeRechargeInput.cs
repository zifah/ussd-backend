﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class AirtimeRechargeInput : IApiInput
    {
        public string msisdn { set; get; }
        public string account_number { set; get; }
        public string msisdn_network { set; get; }
        public string password { set; get; }
        internal bool is_self_recharge
        {
            get
            {
                return msisdn.Equals(beneficiary_msisdn);
            }
        }
        public decimal amount { set; get; }
        public string beneficiary_msisdn { set; get; }
        public string reference_number { set; get; }

        public bool is_via_direct_code { get; set; }
        public string token { set; get; }
        public bool send_beneficiary_sms { set; get; }

        public bool requires_authentication { get; set; }
        public string sender_name { set; get; }
    }

}