﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class AirtimeRechargeOutput : IApiOutput
    {
        public AirtimeRechargeOutput()
        {

        }

        public string response_code { set; get; }
        public string response_message { set; get; }
        public string network { set; get; }
        public string payment_reference { set; get; }
    }
}