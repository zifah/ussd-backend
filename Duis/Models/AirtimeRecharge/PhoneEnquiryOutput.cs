﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{

    public class PhoneEnquiryOutput : IApiOutput
    {
        public string msisdn { set; get; }
        public string network { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }

    }
}