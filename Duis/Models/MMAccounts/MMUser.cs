﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class MMUser : IApiOutput
    {
        public string msisdn { set; get; }
        public string customer_id { set; get; }
        public bool is_active { set; get; }
        public bool is_valid { set; get; }
        public string response_code { set; get; }
        public string response_message { set; get; }
        public string name { set; get; }
    }
}