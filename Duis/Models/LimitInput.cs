﻿using Duis.Interfaces;
using Duis.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Models
{
    public class LimitInput
    {
        public LimitInput()
        {

        }

        public decimal? amount { set; get; }
        public string msisdn { set; get; }
        public string password { set; get; }

    }
}