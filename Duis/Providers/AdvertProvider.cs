﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Adverts;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
namespace Duis.Providers
{
    public class AdvertProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly IAdvertAppService _advertService;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;

        public AdvertProvider(EnrolmentProvider enrolmentProvider, ConfigurationProvider configProvider, IAdvertAppService advertService)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _advertService = advertService;
            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }

        public AdvertOutput GetAdvert(AdvertInput input)
        {
            var result = new AdvertOutput();
            var user = _enrolmentProvider.GetByMsisdn(input.msisdn);

            if(user == null)
            {
                DuisProvider.AssignResponse(result, DuisResponses._notEnrolled);
                return result;
            }

            var ads = _advertService.GetAdverts(input.module, user);

            if (ads.Count == 0)
            {
                DuisProvider.AssignResponse(result, DuisResponses._emptySetError);
            }

            else
            {
                DuisProvider.AssignResponse(result, DuisResponses._successful);
                result.content = ads[new Random().Next(0, ads.Count)].Content;
            }

            return result;
        }
    }
}