﻿using Abp;
using Duis.Constants;
using Duis.Exceptions;
using Duis.Interfaces;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Models.AlertSubscriptions;
using Duis.Models.BillPayments;
using Duis.Models.Cards;
using Duis.Utilities;
using Fbp.Net.Integrations;
using FidelityBank.CoreLibraries.Infopool;
using FidelityBank.CoreLibraries.Infopool.Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using Duis.Models.Bvn;
using Duis.Models.BankAccounts;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Duis.Models.Clickatell;
using System.Net;

namespace Duis.Providers
{
    public class DuisProvider : ICentralProvider
    {
        private static readonly AbpBootstrapper _bootstrapper = AbpDIManager.AbpBootstraper;

        private readonly EnrolmentProvider _enrolmentProvider = null;

        private readonly AccountEnquiryProvider _accountEnquiryProvider = null;
        private readonly FundsTransferProvider _fundsTransferProvider = null;


        private readonly InfoUpdateProvider _infoUpdateProvider = null;
        private readonly BillPaymentProvider _billingProvider = null;
        private readonly AirtimeRechargeProvider _airtimeRechargeProvider = null;
        private readonly RemitaPaymentProvider _remitaPayProvider = null;
        private readonly MicroPayProvider _microPayProvider = null;
        private readonly CashoutProvider _cashoutProvider = null;
        private readonly CardControlProvider _cardControlProvider = null;
        private readonly AlertSubscriptionProvider _alertSubsProvider = null;
        private readonly TransactionProvider _transactionProvider = null;
        private readonly UserTokenProvider _tokenProvider = null;
        private readonly RequestLogProvider _requestLogProvider = null;
        private readonly OtpProvider _otpProvider = null;
        private readonly CentralOperations _centralOperations = null;

        internal List<string> GetCashoutChannels()
        {
            return _cashoutProvider.GetChannels();
        }

        private readonly AdvertProvider _advertProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;
        internal static readonly string _transactionSmsSubject = ConfigurationManager.AppSettings[DuisConstants.ConfTransactionSmsSubject];

        public static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        public static readonly string _infopoolWriter = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolWriter].ConnectionString;

        #region Background process management variables
        public bool IsTimeoutElapsed { set; get; } //= false;
        public bool IsBackgroundJobCompleted { set; get; } //= false;
        public bool ReturnedTimeoutResponse { set; get; } //= false;
        #endregion

        public DuisProvider()
        {
            _enrolmentProvider = _bootstrapper.IocManager.Resolve<EnrolmentProvider>();
            _accountEnquiryProvider = _bootstrapper.IocManager.Resolve<AccountEnquiryProvider>();
            _fundsTransferProvider = _bootstrapper.IocManager.Resolve<FundsTransferProvider>();
            _infoUpdateProvider = _bootstrapper.IocManager.Resolve<InfoUpdateProvider>();
            _billingProvider = _bootstrapper.IocManager.Resolve<BillPaymentProvider>();
            _airtimeRechargeProvider = _bootstrapper.IocManager.Resolve<AirtimeRechargeProvider>();
            _cashoutProvider = _bootstrapper.IocManager.Resolve<CashoutProvider>();
            _alertSubsProvider = _bootstrapper.IocManager.Resolve<AlertSubscriptionProvider>();
            _remitaPayProvider = _bootstrapper.IocManager.Resolve<RemitaPaymentProvider>();
            _microPayProvider = _bootstrapper.IocManager.Resolve<MicroPayProvider>();
            _transactionProvider = _bootstrapper.IocManager.Resolve<TransactionProvider>();
            _tokenProvider = _bootstrapper.IocManager.Resolve<UserTokenProvider>();
            _otpProvider = _bootstrapper.IocManager.Resolve<OtpProvider>();
            _cardControlProvider = _bootstrapper.IocManager.Resolve<CardControlProvider>();
            _advertProvider = _bootstrapper.IocManager.Resolve<AdvertProvider>();
            _requestLogProvider = _bootstrapper.IocManager.Resolve<RequestLogProvider>();
            _centralOperations = new CentralOperations(this);

            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        internal EnrolmentOutput CanEnrol(EnrolmentInput input)
        {
            EnrolmentOutput result = null;

            if (input != null)
            {
                string msisdn = input.msisdn;
                string accountNumber = input.account_number;

                InfopoolAccount accountDetails = null;

                DuisResponse response = _enrolmentProvider.CheckCanEnrol(msisdn, accountNumber, out accountDetails);

                result = new EnrolmentOutput
                {
                    account_number = accountNumber,
                    msisdn = msisdn,
                    response_code = response.response_code,
                    response_message = response.response_message
                };

                _requestLogProvider.LogRequest(msisdn, accountNumber, null, response.response_code, response.response_message, null);
            }


            return result;
        }

        internal EnrolmentOutput Enrol(EnrolmentInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _enrolmentProvider.Enrol);
            return output;
        }

        internal MsisdnAccounts GetAccountsByPhone(string msisdn)
        {
            var result = _enrolmentProvider.GetMsisdnAccounts(msisdn);
            return result;
        }

        internal EnrolmentOutput ResetPassword(EnrolmentInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _enrolmentProvider.ResetPassword);

            return output;
        }

        internal SelfUnlockOutput SelfUnlock(SelfUnlockInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _enrolmentProvider.SelfUnlock);

            return output;
        }

        internal EnrolmentOutput ChangePassword(EnrolmentInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _enrolmentProvider.ChangePassword);

            return output;
        }

        internal MsisdnAccounts RememberAccounts(AccountEnquiryInput input)
        {
            var output = _centralOperations.Process<AccountEnquiryInput, MsisdnAccounts>
                (input, 0, input.msisdn, input.account_number, _accountEnquiryProvider.GetAccountService);

            return output;
        }

        internal AccountEnquiryOutput GetAccountBalance(AccountEnquiryInput input)
        {
            var output = _centralOperations.Process<AccountEnquiryInput, AccountEnquiryOutput>
                (input, 0, input.msisdn, input.account_number, _accountEnquiryProvider.GetAccountBalance);

            return output;
        }


        internal GetAccountBanksOutput GetAccountBanks(GetAccountBanksInput input)
        {
            var result = _accountEnquiryProvider.GetAccountBanks(input);
            return result;
        }

        internal FundsTransferOutput DoFundsTransfer(FundsTransferInput input)
        {
            var output = _centralOperations.Process<FundsTransferInput, FundsTransferOutput>
                (input, input.amount, input.msisdn, input.source_nuban, _fundsTransferProvider.DoFundsTransfer);

            return output;
        }

        internal InfoUpdateOutput DoInfoUpdate(InfoUpdateInput input)
        {
            var result = _centralOperations.ProcessSync(input, null, input.msisdn, null, _infoUpdateProvider.DoUpdate);
            return result;
        }

        internal BillPaymentOutput DoBillPayment(BillPaymentInput input)
        {
            var output = _centralOperations.Process<BillPaymentInput, BillPaymentOutput>
                (input, input.amount, input.msisdn, input.account_number, _billingProvider.DoBillPaymentAsync);

            return output;
        }

        internal BillPaymentOutput ValidateBillPayment(BillPaymentInput input)
        {
            var result = _billingProvider.ValidateBillPayment(input);
            _requestLogProvider.LogRequest(input.msisdn, input.account_number, input.amount, result.response_code, result.validation_text, input.reference_number);
            return result;
        }

        internal BillPaymentOutput GetBillerDetails(string billerCode)
        {
            var result = _billingProvider.GetBillerDetails(billerCode);
            return result;
        }

        internal AirtimeRechargeOutput DoAirtimeRecharge(AirtimeRechargeInput input)
        {
            var output = _centralOperations.Process<AirtimeRechargeInput, AirtimeRechargeOutput>
                (input, input.amount, input.msisdn, input.account_number, _airtimeRechargeProvider.DoRecharge);

            return output;
        }

        /// <summary>
        /// Will deserialize the supplied serialResponse and assign the code and message it to the supplied response object
        /// </summary>
        /// <param name="response"></param>
        /// <param name="serialResponse"></param>
        internal static void AssignResponse(IApiOutput response, string serialResponse)
        {
            var deserializedResponse = Constants.DuisResponses.GetDuisResponse(serialResponse);
            response.response_code = deserializedResponse.response_code;
            response.response_message = deserializedResponse.response_message;
        }

        internal string GetAccountName(string accountNumber, string bankCodeNipV2)
        {
            var accountName = _accountEnquiryProvider.GetAccountName(accountNumber, bankCodeNipV2);
            return accountName;
        }

        internal GetUserOutput GetUser(string msisdn)
        {
            var userDetails = _enrolmentProvider.GetUser(msisdn);
            return userDetails;
        }

        internal bool RequiresAuthentication(AirtimeRechargeInput input)
        {
            var result = _airtimeRechargeProvider.RequiresAuthentication(input);

            _requestLogProvider.LogRequest(input.msisdn, input.account_number, null, null, result.ToString(), input.reference_number);

            return result;
        }

        internal string[] GetUpdatableInformation()
        {
            string[] result = _infoUpdateProvider.GetUpdatableInformation();
            return result;
        }

        internal PasswordValidationOutput IsPasswordValid(string password, string account)
        {
            var result = _enrolmentProvider.IsPasswordValid(password, account);
            return result;
        }

        /// <summary>
        /// Return null if no result
        /// </summary>
        /// <returns></returns>
        internal static string GetAccountName(string accountNumber)
        {
            string result = null;

            try
            {
                result = new InfopoolSystem(_infopoolReader).GetAccount(accountNumber).AccountName;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Could not get name for account number: {0}", accountNumber));
            }

            return result;
        }

        internal EnrolmentOutput Authenticate(EnrolmentInput input)
        {
            var _authenticationOutput = new EnrolmentOutput()
            {
                response_code = _defaultResponse.response_code,
                response_message = _defaultResponse.response_message
            };

            if (input != null)
            {
                try
                {
                    var user = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                    if (user != null)
                    {
                        DuisProvider.AssignResponse(_authenticationOutput, DuisResponses._successful);
                    }

                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    _authenticationOutput.response_code = ex.ErrorCode;
                    _authenticationOutput.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(_authenticationOutput, DuisResponses._systemError);
                }

            }

            _requestLogProvider.LogRequest(input.msisdn, input.account_number, null, _authenticationOutput.response_code,
                _authenticationOutput.response_message, input.reference_number);

            return _authenticationOutput;
        }

        internal AlertSubscriptionOutput ManageAlertSubscription(AlertSubscriptionInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _alertSubsProvider.ManageSubscription);

            return output;
        }

        internal Dictionary<string, string> GetAlertProducts()
        {
            var result = _alertSubsProvider.GetAlertProducts();
            return result;
        }

        internal DuisResponse LockoutUser(LockoutInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _enrolmentProvider.LockoutUser);
            return output;
        }

        internal Account GetAccountDetails(string accountNumber)
        {
            InfopoolAccount account = _enrolmentProvider.GetAccountDetails(accountNumber);

            var result = new Account
            {
                account_number = accountNumber
            };

            if (account != null)
            {
                result.birthday_ddMmYy = account.BirthdayDdMmYy;
            }

            return result;
        }

        internal List<Biller> GetVisibleBillers()
        {
            List<Biller> result = _billingProvider.GetVisibleBillers();
            return result;
        }

        internal AccountEnquiryOutput GetBalanceEnquiryCharge(string phone)
        {
            return _accountEnquiryProvider.GetBalanceEnquiryCharge(phone);
        }

        internal RemitaPaymentOutput ValidateRemitaPayment(RemitaPaymentInput input)
        {
            var output = _centralOperations.ProcessSync(input, input.amount, input.msisdn, input.account_number, _remitaPayProvider.Validate);
            return output;
        }

        internal RemitaPaymentOutput DoRemitaPayment(RemitaPaymentInput input)
        {

            var output = _centralOperations.Process<RemitaPaymentInput, RemitaPaymentOutput>
                (input, input.amount, input.msisdn, input.account_number, _remitaPayProvider.Pay);

            return output;
        }

        internal MicroPayOutput ValidateMicroPay(MicroPayInput input)
        {
            var result = _microPayProvider.Validate(input);

            _requestLogProvider.LogRequest(input.msisdn, input.account_number, input.amount,
                result.response_code, result.response_message, input.reference_number);

            return result;
        }

        internal MicroPayOutput DoMicroPay(MicroPayInput input)
        {
            var output = _centralOperations.Process<MicroPayInput, MicroPayOutput>
                (input, input.amount, input.msisdn, input.account_number, _microPayProvider.Pay);

            return output;
        }

        internal CashoutOutput DoCashout(CashoutInput input)
        {
            var output = _centralOperations.Process<CashoutInput, CashoutOutput>
                (input, input.amount, input.msisdn, input.account_number, _cashoutProvider.DoCashout);

            return output;
        }

        internal PhoneEnquiryOutput GetNetwork(string msisdn)
        {
            var result = _airtimeRechargeProvider.GetNetwork(msisdn);
            return result;
        }

        internal AccountEnquiryOutput CheckBlacklist(AccountEnquiryInput input)
        {
            AccountEnquiryOutput isBlacklisted = _enrolmentProvider.IsBlacklisted(input.msisdn, input.account_number);
            return isBlacklisted;
        }

        internal AffordableOutput GetAffordable(string accountNumber, string transactionType, EnrolmentDto theUser = null)
        {
            return _transactionProvider.GetAffordable(accountNumber, transactionType, theUser);
        }

        internal LimitOutput GetLimits(string msisdn, string transactionType, decimal? amount)
        {
            return _transactionProvider.GetLimit(amount ?? 100, msisdn, transactionType);
        }

        internal AccountCards GetCards(AccountEnquiryInput input)
        {
            var result = _cardControlProvider.GetValidCards(input);
            return result;
        }

        internal CardControlOutput ControlCard(CardControlInput input)
        {
            return _centralOperations.ProcessSync(input, null, input.msisdn, input.account_number, _cardControlProvider.ManageCard);
        }

        internal CardControlOutput CancelReissuance(CardControlInput input)
        {
            return _centralOperations.Process<CardControlInput, CardControlOutput>(input, null, input.msisdn, input.account_number,
                _cardControlProvider.CancelReissuance);
        }

        internal TokenOutput LinkToken(TokenInput input)
        {

            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _tokenProvider.LinkToken);
            return output;
        }

        internal TokenOutput UnlinkToken(TokenInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _tokenProvider.RemoveToken);
            return output;
        }

        internal TokenOutput ResyncToken(TokenInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _tokenProvider.ResyncToken);
            return output;
        }

        internal SelfBlockOutput SelfBlock(SelfBlockInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, null, _enrolmentProvider.SelfBlock);
            return output;
        }

        internal MarkForReversalOutput MarkForReversal(MarkForReversalInput input)
        {
            var response = new MarkForReversalOutput();

            try
            {
                _transactionProvider.MarkForReversal(input);
                AssignResponse(response, DuisResponses._successful);
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Error at MarkForReversal operation for transaction #{0}",
                    input != null ? input.transaction_id : 0));
                AssignResponse(response, DuisResponses._systemError);
            }

            return response;
        }

        internal AdvertOutput GetAdverts(AdvertInput input)
        {
            var result = _advertProvider.GetAdvert(input);
            return result;
        }

        internal OtpOutput GetOtpCharge(OtpInput input)
        {
            var result = _otpProvider.GetCharge(input);
            return result;
        }

        internal AccountEnquiryOutput GetServiceCharge(AccountEnquiryInput input)
        {
            AccountEnquiryOutput output = new AccountEnquiryOutput();

            try
            {
                var result = _transactionProvider.GetSurcharge(input.msisdn, ApplicationConstants.Service);
                output.amount = result;
                DuisProvider.AssignResponse(output, DuisResponses._successful);
            }

            catch (Exception ex)
            {
                DuisProvider.AssignResponse(output, DuisResponses._systemError);
                LoggingSystem.LogException(ex);
            }

            return output;
        }

        internal OtpOutput GetOtpUsername(OtpInput input)
        {
            var result = _otpProvider.GetUsername(input);
            return result;
        }

        internal OtpOutput GenerateOtp(OtpInput input)
        {
            var output = _centralOperations.Process<OtpInput, OtpOutput>
                (input, null, input.msisdn, null, _otpProvider.GetOtp);

            return output;
        }

        internal ReferralOutput GetReferralPoints(AccountEnquiryInput input)
        {
            ReferralProvider referralProvider = _bootstrapper.IocManager.Resolve<ReferralProvider>();
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, null, referralProvider.GetPoints);
            return output;
        }

        internal GetAccountOfficerOutput GetAccountOfficer(GetAccountOfficerInput input)
        {
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, input.account_number, _accountEnquiryProvider.GetAccountOfficer);
            return output;
        }


        internal BvnOutput GetBvn(BvnInput input)
        {
            var bvnProvider = _bootstrapper.IocManager.Resolve<BvnProvider>();
            var output = _centralOperations.ProcessSync(input, 0, input.msisdn, null, bvnProvider.GetDetails);
            return output;
        }

        internal CreateAccountOutput CreateBankAccount(CreateAccountInput input)
        {
            var bankAccountProvider = _bootstrapper.IocManager.Resolve<BankAccountProvider>();
            var output = _centralOperations.Process<CreateAccountInput, CreateAccountOutput>
                (input, null, input.msisdn, input.account_number, bankAccountProvider.CreateAccount);

            return output;
        }

        internal ClickatellRechargeOutput ClickatellReserveFunds(ClickatellRechargeInput input)
        {
            var clickatellProvider = _bootstrapper.IocManager.Resolve<ClickatellProvider>();            
            var output = _centralOperations.ProcessSync(input, input.amountNaira, input.sourceIdentifier, input.accountIdentifier, clickatellProvider.ReserveFunds);
            return output;
        }

        internal HttpStatusCode ClickatellTransactResult(ClickatellRechargeInput input)
        {
            var clickatellProvider = _bootstrapper.IocManager.Resolve<ClickatellProvider>();
            var output = _centralOperations.ProcessSync(input, input.amountNaira, input.sourceIdentifier, input.accountIdentifier, clickatellProvider.TransactResult);
            return output.StatusCode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <exception cref="KeyNotFoundException"></exception>
        /// <returns></returns>
        internal TransactionVerifyResponse ClickatellVerifyRecharge(string phone, int amount)
        {
            var clickatellProvider = _bootstrapper.IocManager.Resolve<ClickatellProvider>();
            return clickatellProvider.VerifyRecharge(phone, amount);
        }       
        
        internal ThirdPartyOtpOutput GenerateThirdPartyOtp(ThirdPartyOtpInput input)
        {
            var provider = _bootstrapper.IocManager.Resolve<ThirdPartyOtpProvider>();
            var output = _centralOperations.Process<ThirdPartyOtpInput, ThirdPartyOtpOutput>
                (input, null, input.msisdn, input.account_number, provider.SendThirdPartyOtp);
            return output;
        }

        internal ValidateOtpOutput ValidateThirdPartyOtp(ValidateOtpInput input)
        {
            var provider = _bootstrapper.IocManager.Resolve<ThirdPartyOtpProvider>();
            var output = _centralOperations.ProcessSync(input, null, input.msisdn, input.account_number, provider.ValidateOtp);
            return output;
        }
    }
}
