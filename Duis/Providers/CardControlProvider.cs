﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using FidelityBank.CoreLibraries.AlertSubscriptions;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Newtonsoft.Json;
using Duis.Models.AlertSubscriptions;
using Fbp.Net.Integrations.InfoUpdates;
using Duis.Models.Cards;
using FidelityBank.CoreLibraries.Profectus;
using FidelityBank.CoreLibraries.Viacard;

namespace Duis.Providers
{
    public class CardControlProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly IInfoUpdateAppService _customerInfoChangeService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;

        public CardControlProvider(EnrolmentProvider enrolmentProvider,
            IInfoUpdateAppService customerInfoChangeService,
            ConfigurationProvider configProvider,
            TransactionProvider transactionProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _customerInfoChangeService = customerInfoChangeService;
            _transactionProvider = transactionProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }

        public AccountCards GetValidCards(AccountEnquiryInput userInfo)
        {
            var theUser = _enrolmentProvider.GetByAccountMsisdn(userInfo.msisdn, userInfo.account_number);
            var result = new AccountCards
            {
                account_number = userInfo.account_number,
                cards = new List<Card>()
            };

            if (theUser != null)
            {
                var infopoolSystem = _enrolmentProvider.GetInfopoolSystem();
                var customerCards = infopoolSystem.GetHotlistableCards(theUser.BankCustomerId);
                result.cards = customerCards.Select(x => new Card
                {
                    card_network = x.CardNetwork,
                    display_name = x.DisplayName,
                    masked_pan = x.MaskedPAN,
                    name_on_card = x.NameOnCard,
                    account_number = x.AccountNumber
                }).ToList();
            }

            return result;
        }

        public CardControlOutput ManageCard(CardControlInput input)
        {
            CardControlOutput response = new CardControlOutput();

            if (input != null)
            {
                // check if number is enrolled
                try
                {
                    var enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                    var profectusRootUrl = _configProvider.GetByName<string>(DuisConstants.ConfProfectusRootUrl);
                    var profectusTokenUrl = _configProvider.GetByName<string>(DuisConstants.ConfProfectusTokenUrl);
                    var profectusCardControlEndpoint = _configProvider.GetByName<string>(DuisConstants.ConfProfectusCardControlEndpoint);
                    var profectusClientId = _configProvider.GetByName<string>(DuisConstants.ConfProfectusClientId);
                    var profectusSecretKey = _configProvider.GetByName<string>(DuisConstants.ConfProfectusSecretKey);

                    // subscribe the customer
                    var cardControlSystem = new OperationUtil(profectusRootUrl, profectusTokenUrl, profectusClientId, profectusSecretKey);

                    CardManagementOutput cardControlResult = null;

                    var procReqTime = DateTime.Now;

                    if (input.is_unblock)
                    {
                        LoggingSystem.LogMessage(string.Format("Hotlisting {0}", input.masked_pan));
                        cardControlResult = cardControlSystem.OpenCard(input.account_number, null, input.masked_pan, profectusCardControlEndpoint);
                    }

                    else
                    {
                        LoggingSystem.LogMessage(string.Format("Dehotlisting {0}", input.masked_pan));
                        cardControlResult = cardControlSystem.HotlistCard(input.account_number, null, input.masked_pan, profectusCardControlEndpoint);
                    }

                    var transactionCreator = new CreateTransactionInput
                    {
                        ProcessorReqTime = procReqTime,
                        ProcessorRespTime = DateTime.Now,
                        ProcessorName = DuisConstants.ValueProfectus,
                        ProcessorUserId = input.masked_pan,
                        ProcessorOptionCode = input.is_unblock ? "1" : "0",
                        UserId = enrolment.Id,
                        UserMsisdn = enrolment.Msisdn,
                        Type = ApplicationConstants.CardControl,
                        ClientRefId = input.reference_number,
                        DrSource = input.account_number,
                        IsViaDirectCode = input.is_via_direct_code
                    };

                    if (cardControlResult != null)
                    {
                        // determine status based on response
                        if (cardControlResult.success)
                        {
                            DuisProvider.AssignResponse(response, DuisResponses._successful);
                        }

                        else
                        {
                            DuisProvider.AssignResponse(response, DuisResponses._failedAtProcessor);
                        }

                        // create a transaction that stores the outcome of the transaction
                        //transactionCreator.Success = cardControlResult.success;
                        transactionCreator.ProcessorResponseBody = JsonConvert.SerializeObject(cardControlResult);
                    }

                    else
                    {
                        DuisProvider.AssignResponse(response, DuisResponses._systemError);
                    }

                    transactionCreator.StatusCode = response.response_code;
                    transactionCreator.Status = response.response_message;

                    _transactionProvider.Create(transactionCreator);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    response.response_code = ex.ErrorCode;
                    response.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
                }
            }

            return response;
        }

        /// <summary>
        /// <para>Remove the lien placed on supplied account number by Viacard for Card Reissuance</para>
        /// <para>Response Message in output will contain message to provide adequate information to the customer on outcome of request</para>
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        internal void CancelReissuance(CardControlInput input, CardControlOutput output, ICentralProvider coreProvider)
        {
            // check if number is enrolled
            try
            {
                var enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);
                var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);
                var cardReissuanceSecret = _configProvider.GetByName<string>(DuisConstants.ConfCardReissuanceSecret);
                var cardReissuanceSystem = new CardReissuanceSystem(cardReissuanceSecret);
                var providerResponse = cardReissuanceSystem.RemoveLien(new LienAdminRequest { AccountNumber = input.account_number });

                if (providerResponse != null)
                {
                    // determine status based on response
                    if (providerResponse.IsSuccessful)
                    {
                        DuisProvider.AssignResponse(output, DuisResponses._successful);
                        TakeReissuanceServiceCharge(input, enrolment, accountDetails.SolId);
                    }

                    else
                    {
                        DuisProvider.AssignResponse(output, DuisResponses._failedAtProcessor);
                        output.response_message = providerResponse.ResponseMessage;
                    }

                    LoggingSystem.LogMessage(string.Format("Cancel Reissuance Response for {0}: {1}", input.account_number, JsonConvert.SerializeObject(providerResponse)));
                }

                else
                {
                    LoggingSystem.LogMessage("Cancel Reissuance Response was null");
                    DuisProvider.AssignResponse(output, DuisResponses._systemError);
                }
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendCompletionSms(input, output);
            }
        }

        private void TakeReissuanceServiceCharge(CardControlInput input, EnrolmentDto user, string solId)
        {
            var serviceCharge = _configProvider.GetByName<decimal>(DuisConstants.ConfReissuanceLienRemovalCharge);

            if (serviceCharge > 0)
            {
                var serviceChargeAccount = _configProvider.GetByName<string>(DuisConstants.ConfServiceChargeAccount).Replace("SOL", solId);
                string narration = string.Format("770 Service Charge: {0}", DuisConstants.AppReissuanceCancellation);
                _transactionProvider.DoSystemInitiatedTransfer(user, input.account_number, serviceChargeAccount, narration, serviceCharge, 0,
                    ApplicationConstants.Service, input.reference_number, DuisConstants.AppViacard);
            }
        }

        private void SendCompletionSms(CardControlInput input, CardControlOutput output)
        {
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            if (isSuccessful)
            {
                var subject = DuisConstants.ApplicationName;
                var sms = string.Format("Debit card auto-renewal has been cancelled for your account ******{0}", StringManipulation.LastCharacters(input.account_number, 4));
                var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                smsSender.SendSMS(input.msisdn, subject, sms);
            }

            else
            {
                LoggingSystem.LogMessage(string.Format("DONOTSMS: Card reissuance request failed with response {0}", JsonConvert.SerializeObject(output)));
            }
        }
    }
}