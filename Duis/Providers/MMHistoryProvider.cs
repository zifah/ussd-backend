﻿

using Abp.Dependency;
using Duis.Exceptions;
using Duis.Models;
using Fbp.Net.Integrations;
using System;
namespace Duis.Providers
{
    public class MMHistoryProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;


        public MMHistoryProvider(EnrolmentProvider enrolmentProvider, TransactionProvider transactionProvider)
        {
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;
        }

        public TransactionHistoryOutput GetLastThreeTransactions(AccountEnquiryInput input)
        {
            var response = new TransactionHistoryOutput { };

            try
            {
                var user = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, true, UserType.MobileMoney);
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);

                response = _transactionProvider.GetHistory(user.Id, 3);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
            }

            return response;
        }
    }
}