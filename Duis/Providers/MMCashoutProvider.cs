﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Fincore;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Newtonsoft.Json;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Utilities;

namespace Duis.Providers
{
    public class MMCashoutProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly InfopoolSystem _infopoolSystem;
        private readonly CashoutProvider _cashoutProvider = null;


        public MMCashoutProvider(EnrolmentProvider enrolmentProvider, ConfigurationProvider configProvider, CashoutProvider cashoutProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _cashoutProvider = cashoutProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];

            _infopoolSystem = new InfopoolSystem(_infopoolReader);
        }
        
        public void DoCashout(CashoutInput input, CashoutOutput output, ICentralProvider coreProvider)
        {
            try
            {
                var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, true, UserType.MobileMoney);
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);

                input.account_number = theUser.AccountNumber;   // to prevent manipulation of debit account at API level
                _cashoutProvider.DoCashoutTransaction(input, output, theUser, ApplicationConstants.MobileMoneyCashout);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                _cashoutProvider.SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }
    }
}