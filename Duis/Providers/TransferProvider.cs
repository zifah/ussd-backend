﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Configuration;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using FidelityBank.CoreLibraries.Fincore;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Newtonsoft.Json;

namespace Duis.Providers
{
    public class TransferProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly ITransactionAppService _transactionAppService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly FincoreSystem _fincore = null;
        private readonly TransactionProvider _transactionProvider;
        private readonly InfopoolSystem _infopoolSystem;

        //configs
        private readonly string _fincoreSuccessCode = null;
        private readonly string _fincoreDuplicateCode = null;
        private readonly string _ownBankAlphaCode = null;

        public TransferProvider(EnrolmentProvider enrolmentProvider,
            ITransactionAppService transactionAppService, TransactionProvider transactionProvider,
            IEnrolmentAppService enrolmentAppService, ConfigurationProvider configProvider)
        {
            _configProvider = configProvider;
            _enrolmentAppService = enrolmentAppService;
            _enrolmentProvider = enrolmentProvider;
            _transactionAppService = transactionAppService;
            _transactionProvider = transactionProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];
            _fincore = new FincoreSystem(fincoreResponsesPath);

            _fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
            _fincoreDuplicateCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreDuplicateCode);
            _ownBankAlphaCode = _configProvider.GetByName<string>(DuisConstants.ConfOwnBankAlphaCode);
            _infopoolSystem = new InfopoolSystem(_infopoolReader);
        }

        internal void ProcessFundsTransfer(EnrolmentDto enrolment, FundsTransferInput input, FundsTransferOutput output,  bool isIntrabank)
        {
            output.response_code = _badRequestResponse.response_code;
            output.response_message = _badRequestResponse.response_message;

            if (input != null)
            {
                output.payment_reference = input.reference_number;
                // check if number is enrolled
                                
                // do the funds transfer   
                decimal charge = _transactionProvider.GetSurcharge(enrolment, input.TransactionType);

                string narration = input.Narration;

                var transactionSaved = CreateTransaction(enrolment, charge, narration, input, input.TransactionType);

                var ftRequest = new FtRequest
                {
                    Amount = input.amount,
                    ChargeAmount = charge,
                    VatInclusive = false,
                    FromAcct = input.source_nuban ?? input.source_account,
                    Narration = narration,
                    ToAcct = input.dest_nuban ?? input.dest_account,
                    ToBankCode = input.dest_bank_alpha_code,
                    ToAcctName = input.dest_account_name,
                    TransactionTime = DateTime.Now,
                    RetrievalRefNum = transactionSaved.DrRefIdNumeric
                };

                if (isIntrabank)
                {
                    try
                    {
                        var requestTime = DateTime.Now;
                        var ftResponse = _fincore.DoFundsTransfer(ftRequest);

                        var responseCode = ftResponse.ResponseCode;
                        var responseDesc = ftResponse.ResponseDescription;

                        if (responseCode.Equals(_fincoreSuccessCode))
                        {
                            DuisProvider.AssignResponse(output, DuisResponses._successful);
                        }

                        else
                        {
                            var insufficientFundsCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreInsufficientFundsCode);

                            var responseString = responseCode == insufficientFundsCode ?
                                Constants.DuisResponses._insufficientFunds : Constants.DuisResponses._chargeCustomerFailed;

                            DuisProvider.AssignResponse(output, responseString);
                        }

                        transactionSaved = _transactionAppService.Update(new UpdateTransactionInput
                        {
                            Id = transactionSaved.Id,
                            DrResponseCode = responseCode,
                            DrResponseDesc = responseDesc,
                            Status = output.response_message,
                            StatusCode = output.response_code,
                            DrReqTime = requestTime,
                            DrRespTime = DateTime.Now,
                            Success = output.response_code == _successResponse.response_code
                        });
                    }

                    catch(Exception ex)
                    {
                        LoggingSystem.LogException(ex, "Exception during Intrabank transfer");
                        DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);
                    }
                }

                else
                {
                    var transferSettings = JsonConvert.DeserializeObject<TransferSettings>(
                        _configProvider.GetByName<string>(DuisConstants.ConfTransferSettings));
                    var processingCode = _infopoolSystem.GetBankProcessingCode(input.dest_bank_alpha_code, transferSettings.ProcessingCodeColumn);

                    //MODIFICATION BY HAFIZ: Added optional prefix to cater for test code situation on 9th February 2017
                    //processingCode = $"{transferSettings.ProcessingCodePrefix}{processingCode}";

                    var paymentRef = GetInterbankPaymentRef(transactionSaved.DrRefIdNumeric, processingCode);

                    var processingStart = DateTime.Now;

                    transactionSaved = _transactionAppService.Update(
                        new UpdateTransactionInput
                        {
                            Id = transactionSaved.Id,
                            RefId = paymentRef,
                            ProcessorName = transferSettings.ProviderName,
                            ProcessorReqTime = processingStart,
                        });


                    var processor = ProcessorProvider.GetTransferProcessor(transferSettings.ProviderClassFqn, transferSettings.ProviderAssemblyName);

                    var request = new FidelityBank.CoreLibraries.Interfaces.Transfer.TransferIn
                    {
                        Amount = input.amount,
                        ChargeAmount = charge,
                        IncludesVat = false,
                        DebitAccount = input.source_nuban ?? input.source_account,
                        Narration = narration,
                        CreditAccount = input.dest_nuban ?? input.dest_account,
                        CreditBankCode = processingCode,
                        BeneficiaryName = input.dest_account_name,
                        TransactionTime = processingStart,
                        DebitReference = transactionSaved.DrRefIdNumeric,
                        PaymentReference = paymentRef,
                        ProviderParametersJson = transferSettings.ProviderParametersJson
                    };

                    LoggingSystem.LogMessage(JsonConvert.SerializeObject(request));

                    var ftResponse = processor.DoInterbankTransfer(request);


                    var responseCode = ftResponse.ResponseCode;
                    var responseDesc = ftResponse.ResponseDescription;

                    if (ftResponse.IsSuccessful)
                    {
                        DuisProvider.AssignResponse(output, DuisResponses._successful);
                    }

                    else
                    {
                        var responseString = responseCode == transferSettings.InsufficientFundsCode ?
                            Constants.DuisResponses._insufficientFunds : Constants.DuisResponses._failedAtProcessor;

                        DuisProvider.AssignResponse(output, responseString);
                    }

                    transactionSaved = _transactionAppService.Update(
                        new UpdateTransactionInput
                        {
                            Id = transactionSaved.Id,
                            ProcessorRespCode = responseCode,
                            ProcessorRespDesc = responseDesc,
                            Status = output.response_message,
                            StatusCode = output.response_code,
                            ProcessorRespTime = DateTime.Now,
                            ProcessorRefId = ftResponse.ProviderReference,
                            ProcessorResponseBody = JsonConvert.SerializeObject(ftResponse),
                            Success = output.response_code == _successResponse.response_code
                        });
                }

                output.payment_reference = transactionSaved.ProcessorRefId;
            }
        }

        private TransactionDto CreateTransaction(EnrolmentDto user, decimal charge, string narration, FundsTransferInput input, string transactionType)
        {
            var databaseRecord = new CreateTransactionInput
            {
                Amount = input.amount,
                ChargeAmount = charge,
                ClientRefId = input.reference_number,
                DrAmount = input.amount,
                DrSource = input.source_nuban ?? input.source_account,
                DrDestination = input.dest_nuban ?? input.dest_account,
                DrDestinationCode = input.dest_bank_alpha_code,
                DrNarration = narration,
                UserId = user.Id,
                UserMsisdn = user.Msisdn,
                ProcessorValidationOutput = input.dest_account_name,
                Type = transactionType,
                IsViaDirectCode = input.is_via_direct_code
            };

            var record = _transactionAppService.Create(databaseRecord);

            return record;
        }

        /// <summary>
        /// <para>1. Pick the RRN</para>
        /// <para>2. Generate PaymentRef as 'I'+<dest_bank_code>+<RRN></para>
        /// </summary>
        /// <param name="ftRequest"></param>
        private string GetInterbankPaymentRef(int rrn, string bankCode)
        {
            string payRef = string.Format("M{0}{1}", bankCode, rrn.ToString().PadLeft(10, '0'));
            return payRef;
        }
    }
}