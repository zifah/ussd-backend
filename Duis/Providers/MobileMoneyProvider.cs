﻿using Abp;
using Duis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Duis.Dto.MobileMoney;
using FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Utilities;

namespace Duis.Providers
{
    public class MobileMoneyProvider : ICentralProvider
    {
        private readonly MMAccountProvider _mmAccountProvider;
        private readonly MMTransferProvider _mmTransferProvider;
        private readonly MMBillingProvider _mmBillingProvider;
        private readonly MMRechargeProvider _mmRechargeProvider;
        private readonly MMCashoutProvider _mmCashoutProvider;
        private readonly MMHistoryProvider _mmHistoryProvider;
        private readonly RequestLogProvider _requestLogProvider = null;
        private readonly CentralOperations _centralOperations = null;

        private readonly AbpBootstrapper _bootstrapper = AbpDIManager.AbpBootstraper;


        #region Background process management variables
        public bool IsTimeoutElapsed { set; get; }
        public bool IsBackgroundJobCompleted { set; get; }
        public bool ReturnedTimeoutResponse { set; get; }
        #endregion

        public MobileMoneyProvider()
        {
            _mmAccountProvider = _bootstrapper.IocManager.Resolve<MMAccountProvider>();
            _mmTransferProvider = _bootstrapper.IocManager.Resolve<MMTransferProvider>();
            _mmBillingProvider = _bootstrapper.IocManager.Resolve<MMBillingProvider>();
            _mmRechargeProvider = _bootstrapper.IocManager.Resolve<MMRechargeProvider>();
            _mmCashoutProvider = _bootstrapper.IocManager.Resolve<MMCashoutProvider>();
            _mmHistoryProvider = _bootstrapper.IocManager.Resolve<MMHistoryProvider>();
            _requestLogProvider = _bootstrapper.IocManager.Resolve<RequestLogProvider>();
            _centralOperations = new CentralOperations(this);
        }

        public MMUser GetUser(string msisdn)
        {
            var result = _mmAccountProvider.GetUser(msisdn);
            return result;
        }

        internal UserCreateResponse CreateUser(UserCreateRequest value)
        {
            UserCreateResponse result = _mmAccountProvider.CreateUser(value);
            return result;
        }

        internal PasswordValidationOutput IsPasswordValid(PasswordValidationInput input)
        {
            PasswordValidationOutput result = _mmAccountProvider.IsPasswordValid(input);
            return result;
        }

        internal object DoFundsTransfer(FundsTransferInput input)
        {
            var output = _centralOperations.Process<FundsTransferInput, FundsTransferOutput>
                (input, input.amount, input.msisdn, input.source_account, _mmTransferProvider.DoTransfer);

            return output;
        }

        internal GetAccountBanksOutput GetOwnBank(Models.Accounts.GetAccountBanksInput input)
        {
            GetAccountBanksOutput result = _mmTransferProvider.GetOwnBank(input);
            return result;
        }

        internal GetAccountBanksOutput GetMmos(Models.Accounts.GetAccountBanksInput input)
        {
            GetAccountBanksOutput mmos = _mmTransferProvider.GetOtherMmos(input);
            return mmos;
        }

        internal GetAccountBanksOutput GetAccountBanks(Models.Accounts.GetAccountBanksInput input)
        {
            GetAccountBanksOutput mmos = _mmTransferProvider.GetAccountBanks(input);
            return mmos;
        }

        internal BillPaymentOutput DoBillPayment(BillPaymentInput input)
        {
            var output = _centralOperations.Process<BillPaymentInput, BillPaymentOutput>
                (input, input.amount, input.msisdn, input.account_number, _mmBillingProvider.DoBillPayment);

            return output;
        }

        internal AirtimeRechargeOutput DoAirtimeRecharge(AirtimeRechargeInput input)
        {
            var output = _centralOperations.Process<AirtimeRechargeInput, AirtimeRechargeOutput>
                   (input, input.amount, input.msisdn, input.account_number, _mmRechargeProvider.DoRecharge);

            return output;
        }

        internal bool RequiresAuthentication(AirtimeRechargeInput input)
        {
            var result = _mmRechargeProvider.RequiresAuthentication(input);

            _requestLogProvider.LogRequest(input.msisdn, input.account_number, input.amount, null, result.ToString(), input.reference_number);

            return result;
        }

        internal AccountEnquiryOutput GetAccountBalance(AccountEnquiryInput input)
        {
            var output = _centralOperations.Process<AccountEnquiryInput, AccountEnquiryOutput>
                      (input, null, input.msisdn, input.account_number, _mmAccountProvider.GetBalance);

            return output;
        }

        internal EnrolmentOutput ChangePassword(EnrolmentInput input)
        {
            var output = _centralOperations.
                ProcessSync<EnrolmentInput, EnrolmentOutput>(input, null, input.msisdn, input.account_number, _mmAccountProvider.ChangePassword);
            
            return output;
        }

        internal CashoutOutput DoCashout(CashoutInput input)
        {
            var output = _centralOperations.Process<CashoutInput, CashoutOutput>
                      (input, input.amount, input.msisdn, input.account_number, _mmCashoutProvider.DoCashout);

            return output;
        }

        internal TransactionHistoryOutput GetLastThreeTransactions(AccountEnquiryInput input)
        {
            var output = _centralOperations.
                ProcessSync<AccountEnquiryInput, TransactionHistoryOutput>(input, null, input.msisdn, input.account_number, _mmHistoryProvider.GetLastThreeTransactions);

            return output;
        }
    }
}