﻿using Abp.Dependency;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.RequestLogs;
using Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos;
using System.Web;

namespace Duis.Providers
{
    public class RequestLogProvider : ITransientDependency
    {
        IRequestLogAppService _requestLogAppService = null;

        public RequestLogProvider(IRequestLogAppService requestLogAppService)
        {
            _requestLogAppService = requestLogAppService;
        }


        public RequestLogDto LogRequest(string msisdn, string account, decimal? amount, string responseCode,
            string responseMessage, string clientReference)
        {
            var request = HttpContext.Current.Request;
            var clientIp = request.Headers["X-Forwarded-For"] ?? request.UserHostAddress;

            var requestLog = new CreateRequestLogInput
            {
                Msisdn = msisdn,
                AccountNumber = account,
                Amount = amount,
                ResponseCode = responseCode,
                ResponseMessage = responseMessage,
                ClientReference = clientReference,
                Url = request.Url.AbsolutePath,
                HttpMethod = request.HttpMethod,
                OriginatingAddress = clientIp
            };

            LoggingSystem.LogMessage("About to log request");

            var theLog = _requestLogAppService.Create(requestLog);

            return theLog;
        }

    }
}