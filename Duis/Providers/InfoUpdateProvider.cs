﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.InfoUpdates;
using Fbp.Net.Integrations.InfoUpdates.Dtos;

namespace Duis.Providers
{
    public class InfoUpdateProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentAppService;
        private readonly IInfoUpdateAppService _infoUpdateService;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;

        public InfoUpdateProvider(EnrolmentProvider enrolmentProvider,
            IInfoUpdateAppService infoUpdateService)
        {
            _enrolmentAppService = enrolmentProvider;
            _infoUpdateService = infoUpdateService;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }

        public InfoUpdateOutput DoUpdate(InfoUpdateInput input)
        {
            InfoUpdateOutput response = new InfoUpdateOutput
            {
                response_code = _badRequestResponse.response_code,
                response_message = _badRequestResponse.response_message
            };

            if (input != null)
            {
                // check if number is enrolled
                try
                {
                    var enrolment = _enrolmentAppService.AuthenticateUser(input.msisdn, input.password);

                    CustomerInfoKey infoType = GetInfoType(input.info_type);

                    var saved = _infoUpdateService.Create(new CreateInfoUpdateInput
                    {
                        AccountNumber = enrolment.AccountNumber,
                        Reason = input.reason,
                        NewValue = input.new_value,
                        InfoType = infoType,
                        Msisdn = enrolment.Msisdn,
                        UserId = enrolment.Id,
                        ClientReference = input.reference_number
                    });

                    DuisProvider.AssignResponse(response, Constants.DuisResponses._successful);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    response.response_code = ex.ErrorCode;
                    response.response_message = ex.ErrorMessage;
                }
            }

            return response;
        }

        /// <summary>
        /// Valid info types: email, address, bvn
        /// </summary>
        /// <param name="infoType"></param>
        /// <returns></returns>
        private CustomerInfoKey GetInfoType(string infoType)
        {
            switch (infoType.ToLower())
            {
                case "email":
                    return CustomerInfoKey.Email;

                case "bvn":
                    return CustomerInfoKey.BVN;

                default:
                    string message = "Supplied info_type is not valid";
                    var errorResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
                    throw new CustomException(DuisResponses._inputInvalid, false, errorResponse.response_code, message);
            }
        }

        internal string[] GetUpdatableInformation()
        {
            string[] result = Enum.GetNames(typeof(CustomerInfoKey));

            return result;
        }
    }
}