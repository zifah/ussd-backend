﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using FidelityBank.CoreLibraries.AlertSubscriptions;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Newtonsoft.Json;
using Duis.Models.AlertSubscriptions;
using Fbp.Net.Integrations.InfoUpdates;

namespace Duis.Providers
{
    public class AlertSubscriptionProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly IInfoUpdateAppService _customerInfoChangeService;
        private readonly IConfigurationAppService _configAppService;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;

        private readonly string _alertApiRootUrl = null;
        private readonly string _alertApiClientId = null;
        private readonly string _alertApiAccessKey = null;
        private readonly string _alertApiSuccessCode = null;

        public AlertSubscriptionProvider(EnrolmentProvider enrolmentProvider,
            IInfoUpdateAppService customerInfoChangeService,
            IConfigurationAppService configAppService,
            TransactionProvider transactionProvider)
        {
            _configAppService = configAppService;
            _enrolmentProvider = enrolmentProvider;
            _customerInfoChangeService = customerInfoChangeService;
            _transactionProvider = transactionProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            _alertApiRootUrl = ConfigurationManager.AppSettings[DuisConstants.ConfAlertApiRootUrl];
            _alertApiClientId = ConfigurationManager.AppSettings[DuisConstants.ConfAlertApiClientId];
            _alertApiAccessKey = ConfigurationManager.AppSettings[DuisConstants.ConfAlertApiAccessKey];
            _alertApiSuccessCode = _configAppService.GetByName<string>(DuisConstants.ConfAlertApiSuccessCode);
        }

        public AlertSubscriptionOutput ManageSubscription(AlertSubscriptionInput input)
        {
            AlertSubscriptionOutput response = new AlertSubscriptionOutput();

            if (input != null)
            {
                // check if number is enrolled
                try
                {
                    var enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                    // subscribe the customer
                    var alertSubsSystem = new SubscriptionSystem(_alertApiRootUrl, _alertApiClientId, _alertApiAccessKey);
                    string ipAddress = WebUtil.GetIPAddress();
                    SubscribeResponse subsResponse = null;

                    var procReqTime = DateTime.Now;

                    if (input.subscribe)
                    {
                        LoggingSystem.LogMessage(string.Format("Subscribing {0} {1} {2} {3}", enrolment.BankCustomerId, input.product_id, ipAddress, "Duis"));
                        subsResponse = alertSubsSystem.Subscribe(input.account_number, input.product_id, ipAddress, "Duis");
                    }

                    else
                    {
                        LoggingSystem.LogMessage(string.Format("Unsubscribing {0} {1} {2} {3}", enrolment.BankCustomerId, input.product_id, ipAddress, "Duis"));
                        subsResponse = alertSubsSystem.Unsubscribe(input.account_number, input.product_id, ipAddress, "Duis");
                    }

                    var transactionCreator = new CreateTransactionInput
                        {
                            ProcessorReqTime = procReqTime,
                            ProcessorRespTime = DateTime.Now,
                            ProcessorName = DuisConstants.ValueAlertSubscriptionsApi,
                            ProcessorCode = input.product_id,
                            ProcessorOptionCode = input.subscribe ? "1" : "0",
                            UserId = enrolment.Id,
                            UserMsisdn = enrolment.Msisdn,
                            Type = ApplicationConstants.AlertSubscription,
                            ClientRefId = input.reference_number,
                            DrSource = input.account_number,
                            IsViaDirectCode = input.is_via_direct_code
                        };

                    if (subsResponse != null)
                    {
                        bool success = subsResponse.StatusCode == _alertApiSuccessCode;

                        // determine status based on response
                        if (success)
                        {
                            DuisProvider.AssignResponse(response, DuisResponses._successful);
                        }

                        else
                        {
                            DuisProvider.AssignResponse(response, DuisResponses._failedAtProcessor);
                        }
                        
                        // create a transaction that stores the outcome of the transaction
                        transactionCreator.Success = success;
                        transactionCreator.ProcessorRespCode = subsResponse.StatusCode;
                        transactionCreator.ProcessorRespDesc = subsResponse.StatusMessage;
                        transactionCreator.ProcessorResponseBody = JsonConvert.SerializeObject(subsResponse);                        
                    }

                    else
                    {
                        DuisProvider.AssignResponse(response, DuisResponses._systemError);
                    }

                    transactionCreator.StatusCode = response.response_code;
                    transactionCreator.Status = response.response_message;

                    _transactionProvider.Create(transactionCreator);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    response.response_code = ex.ErrorCode;
                    response.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
                }
            }

            return response;
        }

        /// <summary>
        /// Returns dictionary with product id as key and product name as value
        /// </summary>
        /// <returns></returns>
        internal Dictionary<string, string> GetAlertProducts()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            var alertSubsSystem = new SubscriptionSystem(_alertApiRootUrl, _alertApiClientId, _alertApiAccessKey);

            //var products = alertSubsSystem.GetSubscribables();
            var products = JsonConvert.DeserializeObject<AlertProduct[]>(ConfigurationManager.AppSettings[DuisConstants.AppAlertProductsListJson]);
            result = products.ToDictionary(x => x.Id, x => x.Name);

            return result;
        }
    }
}