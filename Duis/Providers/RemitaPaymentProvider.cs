﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using FidelityBank.CoreLibraries.Interfaces.BillPayment;
using Fbp.Net.Integrations.DuisApp.Transactions;
using System.Web.Helpers;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using FidelityBank.CoreLibraries.Interfaces;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos;
using Newtonsoft.Json;

namespace Duis.Providers
{
    public class RemitaPaymentProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly BillPaymentProvider _billingProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly IBillPaymentConfigAppService _billerAppService;

        public RemitaPaymentProvider(EnrolmentProvider enrolmentProvider,
            TransactionProvider transactionProvider,
            BillPaymentProvider billingProvider,
            IBillPaymentConfigAppService billerAppService,
            ConfigurationProvider configProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;
            _billingProvider = billingProvider;
            _billerAppService = billerAppService;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }


        public RemitaPaymentOutput Validate(RemitaPaymentInput input)
        {
            var output = new RemitaPaymentOutput();

            DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    // load the remita biller dll
                    var remitaPayBillerName = _configProvider.GetByName<string>(DuisConstants.ConfRemitaPaymentBillerName);
                    BillPaymentConfigDto biller = _billerAppService.GetByName(remitaPayBillerName);
                    var remitaProvider = ProcessorProvider.GetBillingProcessor(biller.ProviderClassFqn, biller.ProviderAssembly);


                    // compose validation input
                    var validationInput = new BillPaymentIn
                    {
                        DestCustomerCode = input.rrr,
                        Amount = input.amount,
                        ProviderParameters = Json.Decode(biller.PaymentParametersJson)
                    };

                    // fire a validate request
                    var validationResponse = remitaProvider.ValidateBillPayment(validationInput);

                    LoggingSystem.LogMessage(JsonConvert.SerializeObject(validationResponse));

                    if (validationResponse.IsSuccessful)
                    {
                        output.biller_name = validationResponse.ValidationBillerName;
                        output.payer_name = StringManipulation.Trim(validationResponse.ValidationText);
                        output.payment_amount = validationResponse.ValidationAmount;
                        output.payment_item_name = validationResponse.ValidationBillDescription;
                        output.plan_code = validationResponse.ValidationBillerPlanCode;
                        DuisProvider.AssignResponse(output, DuisResponses._successful);
                    }

                    else
                    {
                        DuisProvider.AssignResponse(output, DuisResponses._validationFailed);
                        output.response_message = !string.IsNullOrWhiteSpace(validationResponse.ResponseDescription) ? 
                            validationResponse.ResponseDescription : null; // null so the client can know to display what they want
                    }

                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }

            return output;
        }

        public void Pay(RemitaPaymentInput input, RemitaPaymentOutput output, ICentralProvider coreProvider)
        {
            DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    // load the remita biller dll
                    var remitaPayBillerName = _configProvider.GetByName<string>(DuisConstants.ConfRemitaPaymentBillerName);
                    BillPaymentConfigDto biller = _billerAppService.GetByName(remitaPayBillerName);

                    // compose validation input
                    var billingInput = new BillPaymentInput
                    {
                        account_number = input.account_number,
                        msisdn = input.msisdn,
                        password = input.password,
                        reference_number = input.reference_number,
                        amount = input.amount,
                        biller_code = biller.BillerCode,
                        customer_id = input.rrr,
                        selected_plan_code = input.plan_code,
                        is_via_direct_code = input.is_via_direct_code,
                        product_name = input.product_name,
                        token = input.token
                    };

                    // fire a validate request
                    var billingOutput = _billingProvider.DoBillPayment(billingInput);

                    LoggingSystem.LogMessage(JsonConvert.SerializeObject(billingOutput));

                    // compose a response with the feedback
                    output.response_code = billingOutput.response_code;
                    output.response_message = billingOutput.response_message;
                    output.payment_reference = billingOutput.payment_reference;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        private void SendCompletionSms(RemitaPaymentInput input, RemitaPaymentOutput output)
        {
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            var sms = isSuccessful ?
                string.Format("Your payment of {0:N2} with ref. #{1} was successful", input.amount, input.rrr) :
                string.Format(string.Format("Sorry, your remita payment transaction of {0:N2} ref. #{1} was not successful", input.amount, input.rrr));

            var subject = DuisConstants.ApplicationName;

            var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
            smsSender.SendSMS(input.msisdn, subject, sms);
        }
    }
}