﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using FidelityBank.CoreLibraries.AlertSubscriptions;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Newtonsoft.Json;
using Duis.Models.AlertSubscriptions;
using Fbp.Net.Integrations.InfoUpdates;
using Duis.Models.Cards;
using FidelityBank.CoreLibraries.Profectus;
using Duis.Interfaces;

namespace Duis.Providers
{
    public class UserTokenProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly string _tokenLinkageComment = null;
        private readonly string _entrustParameters = null;
        private readonly string _entrustGroupName = null;

        public UserTokenProvider(EnrolmentProvider enrolmentProvider,
            ConfigurationProvider configProvider,
            TransactionProvider transactionProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            _entrustParameters = _configProvider.GetByName<string>(DuisConstants.ConfEntrustParamsJson);
            _entrustGroupName = _configProvider.GetByName<string>(DuisConstants.ConfEntrustGroupName);
            _tokenLinkageComment = _configProvider.GetByName<string>(DuisConstants.ConfTokenLinkageComment);
        }

        public TokenOutput LinkToken(TokenInput input)
        {
            TokenOutput response = new TokenOutput();

            DuisProvider.AssignResponse(response, Constants.DuisResponses._invalidRequestError);

            if (input == null)
            {
                return response;
            }

            try
            {
                var theUser = _enrolmentProvider.GetByAccountMsisdn(input.msisdn, input.account_number);

                if (theUser != null && theUser.Status == EnrolStatus.Activated)
                {
                    var customerId = theUser.BankCustomerId;

                    LoggingSystem.LogMessage(string.Format("Linking token #{0} to User #{1} CustomerId: {2}",
                        input.token_serial, theUser.Id, theUser.BankCustomerId));

                    var entrustUtil = new FidelityBank.CoreLibraries.Entrust.EntrustUtil(_entrustParameters);

                    if (!string.IsNullOrWhiteSpace(input.current_username))
                    {
                        // add as alias
                        var linkResponse = entrustUtil.IsLinked(input.current_username, _entrustGroupName, input.token_serial);

                        if (linkResponse.Success)
                        {
                            var addAliasResponse = entrustUtil.AddAlias(input.current_username, _entrustGroupName, customerId, _tokenLinkageComment);

                            if (addAliasResponse.Success)
                            {
                                // compose response
                                DuisProvider.AssignResponse(response, DuisResponses._successful);
                            }

                            else
                            {
                                FormResponse(response, addAliasResponse, DuisResponses._tokenError, null, DuisResponses._tokenError);
                            }
                        }

                        else
                        {
                            // compose token is not linked to user message based on if response message is there
                            FormResponse(response, linkResponse, DuisResponses._tokenError,
                                string.Format("The token {0} does not exist or is not assigned to user {1}", input.token_serial,
                                    input.current_username), DuisResponses._tokenError);
                        }
                    }

                    else
                    {
                        // confirm if token is free for assignment
                        var tokenFreeResponse = entrustUtil.IsTokenUnassigned(input.token_serial);

                        decimal tokenCharge = _configProvider.GetByName<decimal>(DuisConstants.ConfTokenCharge);
                        decimal tokenSurcharge = _configProvider.GetByName<decimal>(DuisConstants.ConfTokenSurcharge);
                        //decimal tokenCost = _configProvider.GetByName<decimal>(DuisConstants.ConfTokenCost);

                        var tokenIncomeAccount = _configProvider.GetByName<string>(DuisConstants.ConfTokenIncomeAccount);

                        //string solId = string.IsNullOrWhiteSpace(input.issuing_sol_id) ? accountDetails.SolId.Trim() : input.issuing_sol_id.Trim();

                        tokenIncomeAccount = tokenIncomeAccount.Replace("SOL", input.issuing_sol_id);
                        //var tokenVatAccount = _configProvider.GetByName<string>(DuisConstants.ConfTokenVatAccount);
                        //var tokenCostDebitAccount = _configProvider.GetByName<string>(DuisConstants.ConfTokenCostDebitAccount);
                        //var tokenCostCreditAccount = _configProvider.GetByName<string>(DuisConstants.ConfTokenCostCreditAccount);

                        if (tokenFreeResponse.Success)
                        {
                            string chargeNarration = GetTokenNarrationChargeLeg(customerId, input.token_serial);
                            //string costAdjustmentNarration = GetTokenNarrationCostLeg(customerId, input.token_serial);

                            string[] successResponseCodes = new string[] { "00", "94" };

                            // Do account to income account transfer
                            var chargeTransaction = _transactionProvider.DoSystemInitiatedTransfer
                                (theUser, theUser.AccountNumber, tokenIncomeAccount, chargeNarration, tokenCharge, tokenSurcharge, ApplicationConstants.TokenIssuance,
                                input.reference_number);

                            // Do cost to stock transfer
                            if (successResponseCodes.Contains(chargeTransaction.DrResponseCode))
                            {
                                // try linkage
                                var linkResponse = entrustUtil.AssignToken(customerId, _entrustGroupName, input.token_serial, _tokenLinkageComment);

                                if (!linkResponse.Success && linkResponse.Code == FidelityBank.CoreLibraries.Entrust.EntrustResponseCodes.USER_NOT_EXIST.ToString())
                                {
                                    var accountDetails = _enrolmentProvider.GetAccountDetails(input.account_number);
                                    var userCreateResponse = entrustUtil.CreateUser(customerId, accountDetails.AccountName, _tokenLinkageComment, _entrustGroupName);

                                    linkResponse = entrustUtil.AssignToken(customerId, _entrustGroupName, input.token_serial, _tokenLinkageComment);

                                }

                                if (linkResponse.Success)
                                {
                                    DuisProvider.AssignResponse(response, DuisResponses._successful);

                                    //var costTransaction = _transactionProvider.DoSystemInitiatedTransfer
                                    //(theUser, tokenCostDebitAccount, tokenCostCreditAccount, costAdjustmentNarration, tokenCost, 0,
                                    //ApplicationConstants.TokenIssuance, string.Format("{0}T1", input.reference_number));
                                }

                                else
                                {
                                    // if not successfully linked, reverse the debit
                                    FormResponse(response, linkResponse, DuisResponses._systemError, null, DuisResponses._tokenError);
                                    _transactionProvider.ReverseTransaction(chargeTransaction);
                                }

                            }

                            else
                            {
                                DuisProvider.AssignResponse(response, DuisResponses._systemError);
                                response.response_message = string.Format("Unable to take token charge due to {0}", chargeTransaction.DrResponseDesc);
                            }

                            _transactionProvider.Update(new UpdateTransactionInput
                            {
                                Id = chargeTransaction.Id,
                                Status = response.response_message,
                                StatusCode = response.response_code
                            });
                        }

                        else
                        {
                            FormResponse(response, tokenFreeResponse, DuisResponses._systemError, null, DuisResponses._tokenError);
                        }

                    }
                }

                else
                {
                    DuisProvider.AssignResponse(response, Constants.DuisResponses._accountInvalidOrIneligible);
                }

            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
                response.response_message = ex.Message;
            }

            return response;
        }

        public TokenOutput RemoveToken(TokenInput input)
        {
            TokenOutput response = new TokenOutput();

            DuisProvider.AssignResponse(response, Constants.DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    var entrustUtil = new FidelityBank.CoreLibraries.Entrust.EntrustUtil(_entrustParameters);
                    var theUser = _enrolmentProvider.GetByAccountMsisdn(input.msisdn, input.account_number);

                    if (theUser != null)
                    {
                        string comment = string.Format("{0}; {1}", _tokenLinkageComment, input.comment);

                        var unlinkResponse = entrustUtil.DoesUserExist(theUser.BankCustomerId, _entrustGroupName);

                        if (unlinkResponse.Success)
                        {
                            if (unlinkResponse.Username.Contains(theUser.BankCustomerId))
                            {
                                unlinkResponse = entrustUtil.UnassignToken(theUser.BankCustomerId, _entrustGroupName, input.token_serial, comment);
                            }

                            else
                            {
                                unlinkResponse = entrustUtil.RemoveAlias(theUser.BankCustomerId, _entrustGroupName, theUser.BankCustomerId, comment);
                            }
                        }

                        if (unlinkResponse.Success)
                        {
                            // compose response
                            DuisProvider.AssignResponse(response, DuisResponses._successful);
                        }

                        else
                        {
                            FormResponse(response, unlinkResponse, DuisResponses._tokenError, null, DuisResponses._tokenError);
                        }
                    }

                    else
                    {
                        DuisProvider.AssignResponse(response, Constants.DuisResponses._accountInvalidOrIneligible);
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
                    response.response_message = ex.Message;
                }
            }

            return response;
        }

        private void FormResponse(IApiOutput response, FidelityBank.CoreLibraries.Entrust.EntrustResponse entrustResponse, string messageAbsentResponse,
            string messageAbsentMessage, string messagePresentResponse)
        {
            if (string.IsNullOrWhiteSpace(entrustResponse.Message))
            {
                DuisProvider.AssignResponse(response, messageAbsentResponse);
                response.response_message = messageAbsentMessage ?? response.response_message;
            }

            else
            {
                DuisProvider.AssignResponse(response, messagePresentResponse);
                response.response_message = entrustResponse.Message;
            }
        }

        public string GetTokenNarrationChargeLeg(string customerId, string tokenSerial)
        {
            var result = string.Empty;
            // formulate the narration
            result = string.Format("770 HARD TOKEN CHARGE UZ{0} TK{1}", customerId, tokenSerial);
            return result;
        }

        public string GetTokenNarrationCostLeg(string customerId, string tokenSerial)
        {
            var result = string.Empty;
            // formulate the narration
            result = string.Format("770 HARD TOKEN COST UZ{0} TK{1}", customerId, tokenSerial);
            return result;
        }

        internal TokenOutput ResyncToken(TokenInput input)
        {
            TokenOutput response = new TokenOutput();

            DuisProvider.AssignResponse(response, Constants.DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    var entrustUtil = new FidelityBank.CoreLibraries.Entrust.EntrustUtil(_entrustParameters);
                    var theUser = _enrolmentProvider.GetByAccountMsisdn(input.msisdn, input.account_number);

                    if (theUser != null)
                    {
                        var resyncResponse = entrustUtil.ResyncToken(theUser.BankCustomerId, _entrustGroupName, input.token_response, input.token_serial);

                        LoggingSystem.LogMessage(string.Format("Resync response: {0}", JsonConvert.SerializeObject(resyncResponse)));

                        if (resyncResponse.Success)
                        {
                            // compose response
                            DuisProvider.AssignResponse(response, DuisResponses._successful);
                        }

                        else
                        {
                            FormResponse(response, resyncResponse, DuisResponses._tokenError, null, DuisResponses._tokenError);
                        }
                    }

                    else
                    {
                        DuisProvider.AssignResponse(response, Constants.DuisResponses._accountInvalidOrIneligible);
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
                    response.response_message = ex.Message;
                }
            }

            return response;
        }

        internal OtpOutput GenerateOtp(string username, long? lifeTimeMinutes)
        {
            var entrustUtil = new FidelityBank.CoreLibraries.Entrust.EntrustUtil(_entrustParameters);
            var result = new OtpOutput();

            try
            {
                var deletedCount = entrustUtil.DeleteOtp(username, _entrustGroupName);
                var otpResponse = entrustUtil.CreateOtp(username, _entrustGroupName, lifeTimeMinutes * 60 * 1000);

                if (otpResponse.Success)
                {
                    DuisProvider.AssignResponse(result, DuisResponses._successful);
                    result.otp = otpResponse.Otp;
                    result.expiration_time = otpResponse.OtpExpirationTime;
                }

                else
                {
                    LoggingSystem.LogMessage(JsonConvert.SerializeObject(otpResponse));
                    DuisProvider.AssignResponse(result, DuisResponses._systemError);
                }
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(result, Constants.DuisResponses._systemError);
            }

            return result;

        }
    }
}