﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Bvn;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.Adverts;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using FidelityBank.CoreLibraries.BvnNibss;
using FidelityBank.CoreLibraries.Coligo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
namespace Duis.Providers
{
    public class BvnProvider : ITransientDependency
    {
        private readonly ConfigurationProvider _configProvider;

        public BvnProvider(ConfigurationProvider configProvider)
        {
            _configProvider = configProvider;
        }

        public BvnOutput GetDetails(BvnInput input)
        {
            BvnOutput result;

            var bvnValidationSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfBvnValidationSuccessCode);

            var coligoParameters = _configProvider.GetByName<string>(DuisConstants.ConfColigoParameters);
            var bvnApi = new ColigoEngine(coligoParameters);
            var bvnResponse = bvnApi.ValidateBvn(input.bvn);

            LoggingSystem.LogMessage($"BVN response: {JsonConvert.SerializeObject(bvnResponse)}");

            FidelityBank.CoreLibraries.Coligo.Model.BvnDetails theBvnDetails;

            if (bvnResponse == null || bvnResponse.ResultStatus != bvnValidationSuccessCode)
            {
                result = new BvnOutput();
                DuisProvider.AssignResponse(result, DuisResponses._failedAtProcessor);
                result.response_message = $"Unable to validate BVN {input.bvn}";
                return result;
            }

            theBvnDetails = bvnResponse.BvnSearchResult;

            var fakePhoneNumber = Convert.ToBoolean(ConfigurationManager.AppSettings[DuisConstants.AppFakeBvnPhone]);

            result = new BvnOutput
            {
                first_name = theBvnDetails.FirstName,
                last_name = theBvnDetails.LastName,
                birthday = theBvnDetails.DateOfBirthConv.Value,
                email = theBvnDetails.Email,
                bvn = theBvnDetails.Bvn,
                enrolment_bank_code = theBvnDetails.EnrollmentBank,
                enrolment_date = theBvnDetails.RegistrationDateConv.Value,
                phone = fakePhoneNumber ? input.msisdn : theBvnDetails.PhoneNumber
            };

            DuisProvider.AssignResponse(result, DuisResponses._successful);
            LoggingSystem.LogMessage($"Own response: {JsonConvert.SerializeObject(result)}");
            return result;
        }
    }
}