﻿using Abp.Dependency;
using Duis.Interfaces;
using Fbp.Net.Integrations.DuisApp.ApiClients;
using Fbp.Net.Integrations.DuisApp.ApiClients.Dtos;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using System.Linq;
using Fbp.Net.Integrations;
using Duis.CustomAttributes;

namespace Duis.Providers
{
    public class AuthenticationProvider : ITransientDependency
    {
        private readonly IApiClientAppService _apiClientAppService;
        private readonly ConfigurationProvider _configProvider;

        public AuthenticationProvider(IApiClientAppService apiClientAppService, ConfigurationProvider configProvider)
        {
            _apiClientAppService = apiClientAppService;
            _configProvider = configProvider;
        }

        private ApiClientDto GetApiClient(string name)
        {
            return _apiClientAppService.GetByName(name);
        }

        public bool AuthenticateRequest(IApiInput input, DateTime? timeStamp = null)
        {
            var headers = HttpContext.Current.Request.Headers;

            string clientHash = headers[DuisConstants.ApiClientHashHeader];
            string clientName = headers[DuisConstants.ApiClientUsernameHeader];
            string clientIp = headers["x-forwarded-for"] ?? HttpContext.Current.Request.UserHostAddress; // allow for request forwarding by load-balancer

            var clickatellSecondsValid = _configProvider.GetByName<int>(DuisConstants.ConfClickatellSecondsValid);
            var realTimeDifference = Math.Abs((DateTime.Now - timeStamp.Value).TotalSeconds);

            if(timeStamp.HasValue && realTimeDifference > clickatellSecondsValid)
            {
                LoggingSystem.LogMessage($"Replay request suspected from {clientIp}. Time difference was {realTimeDifference - clickatellSecondsValid} seconds more than allowed.");
            }

            var client = GetApiClient(clientName);

            if (string.IsNullOrWhiteSpace(clientHash) || client == null || string.IsNullOrWhiteSpace(client.SharedKey) || string.IsNullOrWhiteSpace(client.IpAddress))
            {
                LoggingSystem.LogMessage($"No valid client was found with name: {clientName} or client is not properly configured");
                return false;
            }

            if (clientIp != client.IpAddress)
            {
                LoggingSystem.LogMessage($"Illegal attempt was made from IP: {clientIp} to use username {clientName}");
                return false;
            }

            string serverHash = ComputeHash(input, client.SharedKey);

            if(!(serverHash.ToUpper() == clientHash.ToUpper()))
            {
                LoggingSystem.LogMessage($"Invalid hash from client: {clientName}. Supplied: {clientHash}; Expected: {serverHash}");
                return false;
            }

            return true;

        }

        public string ComputeHash(IApiInput input, string key)
        {
            // compute the hash using the details of the request combined with the key
            Dictionary<string, string> properties = new Dictionary<string, string>();

            var sortedProperties = input.GetType().GetProperties().OrderBy(x => x.Name);

            string propertiesConcat = string.Empty;

            Type myType = input.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties().Where(x => !Attribute.IsDefined(x, typeof(ApiAuthIgnoreAttribute))).OrderBy(x => x.Name));

            foreach (PropertyInfo prop in props)
            {
                string propValue = string.Empty;

                var propType = prop.PropertyType;

                var rawValue = prop.GetValue(input);

                if (propType == typeof(Decimal))
                {
                    propValue = ((Decimal)rawValue).ToString("N2").Replace(",", string.Empty);
                }

                else if (propType == typeof(DateTime))
                {
                    propValue = ((DateTime)rawValue).ToString("dd|MMM|yyyy/HH:mm:ss");
                }

                else
                {
                    propValue = Convert.ToString(rawValue);
                }

                // Do something with propValue
                if (!string.IsNullOrWhiteSpace(propValue))
                {
                    propertiesConcat = string.Format("{0}{1}", propertiesConcat, propValue);
                }
            }

            string toHash = propertiesConcat;

            string hashValue = SHA256Hash(toHash, key);

            return hashValue;
        }

        private string SHA256Hash(string message, string key)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(key);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);

            byte[] messageBytes = encoding.GetBytes(message);

            byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
            return ByteToString(hashmessage);
        }

        private static string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }
    }
}