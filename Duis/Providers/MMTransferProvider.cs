﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Fincore;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Newtonsoft.Json;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Utilities;

namespace Duis.Providers
{
    public class MMTransferProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly ITransactionAppService _transactionAppService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly FincoreSystem _fincore = null;
        private readonly TransactionProvider _transactionProvider;
        private readonly InfopoolSystem _infopoolSystem;
        private readonly TransferProvider _transferProvider;
        private readonly AccountEnquiryProvider _accountEnquiryProvider;

        //configs
        private readonly string _fincoreSuccessCode = null;
        private readonly string _fincoreDuplicateCode = null;
        private readonly string _ownBankAlphaCode = null;

        public MMTransferProvider(EnrolmentProvider enrolmentProvider,
            ITransactionAppService transactionAppService, TransactionProvider transactionProvider,
            IEnrolmentAppService enrolmentAppService, ConfigurationProvider configProvider, TransferProvider transferProvider,
             AccountEnquiryProvider accountEnquiryProvider)
        {
            _configProvider = configProvider;
            _enrolmentAppService = enrolmentAppService;
            _enrolmentProvider = enrolmentProvider;
            _transactionAppService = transactionAppService;
            _transactionProvider = transactionProvider;
            _transferProvider = transferProvider;
            _accountEnquiryProvider = accountEnquiryProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];
            _fincore = new FincoreSystem(fincoreResponsesPath);

            _fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
            _fincoreDuplicateCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreDuplicateCode);
            _ownBankAlphaCode = _configProvider.GetByName<string>(DuisConstants.ConfOwnBankAlphaCode);
            _infopoolSystem = new InfopoolSystem(_infopoolReader);
        }

        internal GetAccountBanksOutput GetOwnBank(GetAccountBanksInput input)
        {
            var user = _enrolmentAppService.GetByMsisdn(input.sender_msisdn, UserType.MobileMoney);
            var transactionType = GetAccountType(input.account_number) == AccountType.MobileMoney ?
                ApplicationConstants.MobileMoneyIntramobileTransfer : ApplicationConstants.MobileMoneyIntrabankTransfer;

            var theBank = _accountEnquiryProvider.GetBank(user, _ownBankAlphaCode, transactionType);
            var response = new GetAccountBanksOutput { account_number = input.account_number };
            response.banks.Add(theBank);
            DuisProvider.AssignResponse(response, DuisResponses._successful);
            return response;
        }

        internal AccountType GetAccountType(string accountNumber)
        {
            return accountNumber.Length == 10 ? AccountType.RegularNuban : AccountType.MobileMoney;
        }

        public void DoTransfer(FundsTransferInput input, FundsTransferOutput output, ICentralProvider _coreProvider)
        {
            try
            {
                // Authenticate
                var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, true, UserType.MobileMoney);

                // Confirm not blacklisted
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);

                // Determine transfer type
                var isIntrabank = input.dest_bank_alpha_code == _ownBankAlphaCode;
                string transactionType = GetTransactionType(input.dest_account, input.dest_bank_alpha_code);
                input.TransactionType = transactionType;
                input.Narration = GetNarration(input);

                // Consummate transaction
                _transferProvider.ProcessFundsTransfer(theUser, input, output, isIntrabank);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            _coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (_coreProvider.ReturnedTimeoutResponse)
            {
                #region Notify the customer
                var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

                var sms = isSuccessful ?
                    string.Format("Your transfer of N{0:N2} to {1} was successful", input.amount, input.dest_account) :
                    string.Format("Sorry. Your transfer of N{0:N2} to {1} was not successful", input.amount, input.dest_account);

                CentralOperations.SendNotificationSms(sms, input.msisdn);
                #endregion
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }


        private string GetNarration(FundsTransferInput input)
        {
            string narrationPrefix = "Mobile transfer";
            string narration = string.Format("{0} {1} to {2} {3}", narrationPrefix, input.msisdn, input.dest_account, input.dest_account_name);
            return narration;
        }

        private string GetTransactionType(string destinationAccount, string destinationInstAlphaCode)
        {
            var accountType = GetAccountType(destinationAccount);

            if (_ownBankAlphaCode.Equals(destinationInstAlphaCode))
            {
                return accountType == AccountType.MobileMoney ?
                    ApplicationConstants.MobileMoneyIntramobileTransfer : ApplicationConstants.MobileMoneyIntrabankTransfer;
            }

            else
            {
                return accountType == AccountType.MobileMoney ?
                    ApplicationConstants.MobileMoneyIntermobileTransfer : ApplicationConstants.MobileMoneyInterbankTransfer;
            }
        }

        internal GetAccountBanksOutput GetOtherMmos(GetAccountBanksInput input)
        {
            var mobileOperators = _infopoolSystem.GetMobileMoneyOperators();
            mobileOperators = mobileOperators.Where(x => x.AlphabeticCodeV2 == null || x.AlphabeticCodeV2.ToUpper() != _ownBankAlphaCode.ToUpper()).ToList();

            var interMobileToWalletCharge = _transactionProvider.GetSurcharge(input.sender_msisdn,
                ApplicationConstants.MobileMoneyIntermobileTransfer, UserType.MobileMoney);

            var theBanks = mobileOperators.Select(x => new Bank
            {
                code_v1 = x.NumericCodeV1,
                code_v2 = x.NumericCodeV2,
                name = x.BankName,
                name_short = x.AlphabeticCodeV2,
                transfer_charge = interMobileToWalletCharge
            });

            var response = new GetAccountBanksOutput
            {
                banks = theBanks.ToList()
            };

            DuisProvider.AssignResponse(response, DuisResponses._successful);

            return response;
        }

        internal GetAccountBanksOutput GetAccountBanks(GetAccountBanksInput input)
        {
            var result = new GetAccountBanksOutput()
            {
                account_number = input.account_number
            };

            if (input != null)
            {
                // get all banks
                DuisProvider.AssignResponse(result, DuisResponses._successful);

                var intrabankCharge = _transactionProvider
                    .GetSurcharge(input.sender_msisdn, ApplicationConstants.MobileMoneyIntrabankTransfer, UserType.MobileMoney);

                var interbankCharge = _transactionProvider
                    .GetSurcharge(input.sender_msisdn, ApplicationConstants.MobileMoneyInterbankTransfer, UserType.MobileMoney);

                var fidelityNipV2NumericCode = _configProvider.GetByName<string>(DuisConstants.ConfFidelityNipV2NumericCode);
                var allBanks = CentralOperations.GetCommercialBanks(input.sender_msisdn, intrabankCharge, interbankCharge, fidelityNipV2NumericCode);

                foreach (var bank in allBanks)
                {
                    bool isNuban = NubanVerifier.IsNubanAccount(bank.code_v1, input.account_number);

                    if (isNuban)
                    {
                        result.banks.Add(bank);
                    }
                }
            }

            return result;
        }
    }

    public enum AccountType { MobileMoney, RegularNuban };
}