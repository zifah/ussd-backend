﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Exceptions;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Models.Clickatell;
using Duis.Utilities;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Duis.Providers
{
    public class ClickatellProvider : ITransientDependency
    {
        private readonly ConfigurationProvider _configProvider;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly BillPaymentProvider _billPaymentProvider;

        private const string _clickatellSuccessCode = "0000";
        private const string _clickatellNotMappedCode = "2030";
        private const string _clickatellPendingCode = "9999";
        private const char _clickatellAuthCodeDelimiter = '|';

        public ClickatellProvider(ConfigurationProvider configProvider, EnrolmentProvider enrolmentProvider, TransactionProvider transactionProvider,
            BillPaymentProvider billPaymentProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _billPaymentProvider = billPaymentProvider;
            _transactionProvider = transactionProvider;
        }

        public ClickatellRechargeOutput ReserveFunds(ClickatellRechargeInput input)
        {
            /*   
             * Save transaction
             * Debit customer
             * Update debit response
             * Return debit response
             */
            LoggingSystem.LogMessage($"Clickatell request message: {JsonConvert.SerializeObject(input)}");

            string sender = ConvertTo11DigitFormat(input.sourceIdentifier);
            string receiver = ConvertTo11DigitFormat(input.targetIdentifier);

            var output = new BillPaymentOutput();

            bool requiresAuthentication = !receiver.Equals(sender);
            string debitAccount = null;
            EnrolmentDto theUser = null;

            // Validate transaction (Limits, Authentication, Account status)
            try
            {
                string password = string.Empty;
                string otp = string.Empty;

                if (!string.IsNullOrWhiteSpace(input.AuthCode))
                {
                    var authCodeParts = input.AuthCode.Split(_clickatellAuthCodeDelimiter);

                    password = authCodeParts.Length == 2 ? authCodeParts[1] : input.AuthCode;
                    password = FidelityBank.CoreLibraries.Utility.Engine.StringManipulation.Base64Decode(password);
                    otp = authCodeParts.Length == 2 ? authCodeParts[1] : otp;
                }

                debitAccount = GetDebitAccount(sender, input.accountIdentifier);
                theUser = _enrolmentProvider.AuthenticateUser(sender, password, requiresAuthentication);
                var accountDetails = _enrolmentProvider.ValidatePhoneAccount(sender, debitAccount, false);

                _transactionProvider.CheckLimit(input.amountNaira, theUser, ApplicationConstants.AirtimeRecharge, otp);

                var theBiller = _billPaymentProvider.GetBillerByName(_configProvider.GetByName<string>(DuisConstants.ConfClickatellBillerName));

                _billPaymentProvider.DoBillingTransaction(new BillPaymentInput
                {
                    account_number = debitAccount,
                    msisdn = sender,
                    reference_number = input.reference_number,
                    amount = input.amountNaira,
                    biller_code = theBiller.BillerCode,
                    customer_id = receiver,
                    is_via_direct_code = true,
                    selected_plan_code = null,
                    product_name = null
                },
                output,
                ApplicationConstants.AirtimeRecharge,
                theUser,
                accountDetails.SolId);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            return GetOutput(output, debitAccount, input.amountNaira, theUser);
        }

        private string ConvertTo11DigitFormat(string sourceIdentifier)
        {
            return "0" + sourceIdentifier.Remove(0, 3);
        }

        private ClickatellRechargeOutput GetOutput(BillPaymentOutput output, string accountNumber, decimal amount, EnrolmentDto theUser)
        {
            /*
             * HANDLE EACH OF THE FOLLOWING ERROR'S SPECIALLY
             * Charge Customer Failed
             * 0000: Success
             * 2010: Insufficient funds
             * 2020: Invalid authentication code or Unknown 'sourceIdentifier'
             * 2030: Account not mapped
             * 2040: Maximum amount exceeded
             * 2050: Below minimum amount
             * 2060: Internal Time out
             * 2070: 'sourceIdentifier' Blacklisted (This is currently grouped under inactive; we should considered discrimination blacklisted from disabled or lockedout)
             * 2080: Account inactive
             * 2090: Unknown Error
             */
            string responseCode = null;

            if (output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code)
            {
                responseCode = _clickatellSuccessCode;
            }

            else if (output.response_code == DuisResponses.GetDuisResponse(DuisResponses._authenticationFailed).response_code)
            {
                responseCode = "2020";
            }

            else if (output.response_code == DuisResponses.GetDuisResponse(DuisResponses._notEnrolled).response_code ||
                output.response_code == DuisResponses.GetDuisResponse(DuisResponses._accountMsisdnMismatch).response_code)
            {
                responseCode = _clickatellNotMappedCode;
            }

            else if (output.response_code == DuisResponses.GetDuisResponse(DuisResponses._insufficientFunds).response_code)
            {
                responseCode = "2010";
            }

            else if (output.response_code == DuisResponses.GetDuisResponse(DuisResponses._dailyLimitExceeded).response_code)
            {
                // get and return amount which customer can still spend
                var limit = _transactionProvider.GetLimit(amount, theUser.Msisdn, ApplicationConstants.AirtimeRecharge, UserType.InstantBanking);
                responseCode = limit.is_exceeded_limit_minimum ? "2050" : "2040";
            }

            else if (output.response_code == DuisResponses.GetDuisResponse(DuisResponses._accountBlocked).response_code)
            {
                responseCode = "2080";
            }

            else
            {
                // 2090: Unknown Error
                responseCode = "2090";
            }

            return new ClickatellRechargeOutput
            {
                responseCode = responseCode,
                reserveFundsTxnRef = output.payment_reference
            };
        }

        /// <summary>
        /// Get the full account number from the accountIdentifier sent
        /// </summary>
        /// <param name="sender">Phone number of the user initiating the transaction</param>
        /// <param name="accountIdentifier">String representing the account number selected for this transaction</param>
        /// <exception cref="CustomException"></exception>
        /// <returns></returns>
        private string GetDebitAccount(string sender, string accountIdentifier)
        {
            // Retrieve the accounts using the sender phone (use existing method in the account identifer)
            var phoneAccounts = _enrolmentProvider.GetMsisdnAccounts(sender);

            // Get the one whose masked account matches the accountIdentifier
            try
            {
                var clearAccount = phoneAccounts.accounts.Single(x => x.masked_account_number == accountIdentifier).account_number;
                return clearAccount;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, "Error decrypting Clickatell masked account");
                throw new CustomException(DuisResponses._accountMsisdnMismatch, true, null, null);
            }
        }

        public ClickatellRechargeOutput TransactResult(ClickatellRechargeInput input)
        {
            var theTransaction = _transactionProvider.GetByClientRef(input.reference_number);

            if (theTransaction == null)
            {
                // return a 404 if the transaction is not found
                return new ClickatellRechargeOutput { StatusCode = HttpStatusCode.NotFound };
            }

            var isSuccessful = input.responseCode.Equals(_clickatellSuccessCode);
            var isPending = input.responseCode.Equals(_clickatellPendingCode);

            /// Allow updates only if the ProcessorResponseCode has not been set previously
            if (theTransaction.ProcessorRespCode == null || theTransaction.ProcessorRespCode == _clickatellPendingCode)
            {
                if (isSuccessful)
                {
                    var success = DuisResponses.GetDuisResponse(DuisResponses._successful);

                    _transactionProvider.Update(new Fbp.Net.Integrations.DuisApp.Transactions.Dtos.UpdateTransactionInput
                    {
                        Id = theTransaction.Id,
                        ProcessorRespCode = input.responseCode,
                        ProcessorResponseBody = JsonConvert.SerializeObject(input),
                        Status = success.response_message,
                        StatusCode = success.response_code,
                        Success = true
                    });
                }

                else if (input.responseCode.Equals(_clickatellPendingCode))
                {
                    var unknownStatus = DuisResponses.GetDuisResponse(DuisResponses._failedAtProcessor);

                    _transactionProvider.Update(new Fbp.Net.Integrations.DuisApp.Transactions.Dtos.UpdateTransactionInput
                    {
                        Id = theTransaction.Id,
                        Status = unknownStatus.response_code,
                        StatusCode = unknownStatus.response_message,
                        ProcessorRespCode = input.responseCode,
                        ProcessorResponseBody = JsonConvert.SerializeObject(input),
                        Success = false
                    });
                }

                else
                {
                    theTransaction = _transactionProvider.ReverseTransaction(theTransaction);
                    var serviceProviderFailure = DuisResponses.GetDuisResponse(DuisResponses._failedAtProcessor);

                    _transactionProvider.Update(new Fbp.Net.Integrations.DuisApp.Transactions.Dtos.UpdateTransactionInput
                    {
                        Id = theTransaction.Id,
                        Success = false,
                        Status = serviceProviderFailure.response_message,
                        StatusCode = serviceProviderFailure.response_code,
                        ProcessorRespCode = input.responseCode,
                        ProcessorResponseBody = JsonConvert.SerializeObject(input),
                    });
                }
            }

            return new ClickatellRechargeOutput { StatusCode = HttpStatusCode.OK };
        }

        // SETUP AS ACCOUNT SPOOLING SOURCE
        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <exception cref="KeyNotFoundException">Thrown when the supplied phone is not mapped to any user</exception>
        /// <returns></returns>
        public TransactionVerifyResponse VerifyRecharge(string phone, int amountInKobo)
        {
            // Check if user is enrolled
            phone = ConvertTo11DigitFormat(phone);
            var amountNaira = Nairaify(amountInKobo);
            var user = _enrolmentProvider.GetByMsisdn(phone);

            if (user == null)
            {
                return new TransactionVerifyResponse
                {
                    responseCode = _clickatellNotMappedCode
                };
            }

            var phoneAccounts = _enrolmentProvider.GetMsisdnAccounts(phone);

            // check the user's limit
            var limit = _transactionProvider.GetLimit(amountNaira, phone, ApplicationConstants.AirtimeRecharge, UserType.InstantBanking);
            if (limit.is_limit_exceeded || limit.is_token_limit_exceeded)
            {
                bool requiresToken = !limit.is_limit_exceeded && limit.is_token_limit_exceeded;
                int maxAmtKobo = limit.count_left < 1 ? 0 : requiresToken ? Koboify(limit.max_before_token_amount) : Koboify(limit.max_amount);

                return new TransactionVerifyResponse
                {
                    requiresToken = requiresToken,
                    minimumAmount = Koboify(limit.min_amount),
                    maximumAmount = maxAmtKobo,
                    responseCode = limit.is_exceeded_limit_minimum ? "2050" : "2040",
                    accounts = requiresToken || maxAmtKobo > 0 ?  GetAccounts(phoneAccounts) : null
                };
            }

            //check user's balance
            var affordable = _transactionProvider.GetAffordable(user.AccountNumber, ApplicationConstants.AirtimeRecharge, user);

            var maxAmount = affordable.max_amount;
            var insufficientFunds = maxAmount < amountNaira;

            if (insufficientFunds)
            {
                foreach (var account in phoneAccounts.accounts.Where(x => x.account_number != user.AccountNumber))
                {
                    affordable = _transactionProvider.GetAffordable(account.account_number, ApplicationConstants.AirtimeRecharge, user);
                    maxAmount = Math.Max(maxAmount, affordable.max_amount);

                    if (amountNaira <= maxAmount)
                    {
                        /// break once any of the accounts can fund the transaction
                        insufficientFunds = false;
                        break;
                    }
                }

                if (insufficientFunds)
                {
                    int maxAmtKobo = Koboify(maxAmount);
                    return new TransactionVerifyResponse
                    {
                        minimumAmount = Koboify(affordable.min_amount),
                        maximumAmount = maxAmtKobo,
                        responseCode = "2010",
                        accounts = maxAmtKobo > 0 ? GetAccounts(phoneAccounts) : null
                    };
                }
            }

            return new TransactionVerifyResponse
            {
                accounts = GetAccounts(phoneAccounts),
                responseCode = _clickatellSuccessCode
            };
        }

        private List<AccountHolder> GetAccounts(MsisdnAccounts phoneAccounts)
        {
            bool returnBalance = _configProvider.GetByName<bool>(DuisConstants.ConfClickatellReturnBalance);
            return phoneAccounts.accounts.Select(x => new AccountHolder
            {
                account = x.masked_account_number,
                balance = returnBalance ? (int)(Convert.ToDecimal(CentralOperations.GetAccountBalance(x.account_number).BalanceAvailable) * 100) : 0
            })
                .ToList();
        }

        private int Koboify(decimal nairaAmount)
        {
            return (int)nairaAmount * 100; //assumes that the kobo value of all valid recharge amounts will be within the Int32 range
        }

        private decimal Nairaify(int koboAmount)
        {
            return (decimal)koboAmount / 100;
        }
    }
}
