﻿using Abp.Dependency;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.Configurations;
using Fbp.Net.Integrations.Configurations.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Providers
{
    public class ConfigurationProvider : ITransientDependency
    {
        private static readonly Object _lock = new Object();

        private readonly IConfigurationAppService _configurationAppService;

        private static IList<ConfigurationDto> _configCache = new List<ConfigurationDto>();

        public ConfigurationProvider()
        {

        }

        public ConfigurationProvider(IConfigurationAppService configurationAppService)
        {
            _configurationAppService = configurationAppService;
        }

        public T GetByName<T>(string name)
        {
            RefreshCache();

            T result;

            try
            {
                var config = _configCache.FirstOrDefault(x => x.Name == name);

                result = (T)Convert.ChangeType(config.Value, typeof(T));
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Error getting value of app config: {0}", name));
                throw ex;
            }

            return result;
        }

        private void RefreshCache()
        {
            lock (_lock)
            {
                if (_configCache.Count == 0)
                {
                    _configCache = _configurationAppService.GetAll();
                }
            }
        }
    }
}