﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Configuration;
using System.Linq;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Infopool.Objects;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos;
using FidelityBank.CoreLibraries.Utility.Engine;
using Fbp.Net.Integrations.DuisApp.Referrals;
using System.Globalization;

namespace Duis.Providers
{
    public class EnrolmentProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly InfopoolSystem _infopoolSystem = null;
        private readonly DuisResponse _successResponse = null;
        private readonly ConfigurationProvider _configProvider;
        private readonly IPhoneNetworkLogAppService _phoneNetworkLogAppService;
        private readonly IReferralAppService _referralAppService;

        private readonly int _oldPasswordsCount;
        private readonly int _maxPossibleFailedLoginTries;

        public EnrolmentProvider(IEnrolmentAppService enrolmentAppService,
            IPhoneNetworkLogAppService phoneNetworkLogAppService, ConfigurationProvider configProvider,
            IReferralAppService referralAppService)
        {
            _enrolmentAppService = enrolmentAppService;
            _infopoolSystem = new InfopoolSystem(_infopoolReader);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
            _phoneNetworkLogAppService = phoneNetworkLogAppService;
            _referralAppService = referralAppService;

            _configProvider = configProvider;
            _oldPasswordsCount = _configProvider.GetByName<int>(DuisConstants.ConfOldPasswordsCount);
            _maxPossibleFailedLoginTries = _configProvider.GetByName<int>(DuisConstants.ConfMaxPossibleFailedLoginTries);
        }

        public EnrolmentDto GetByAccountMsisdn(string msisdn, string accountNumber)
        {
            var result = _enrolmentAppService.GetByMsisdnAcct(msisdn, accountNumber, UserType.InstantBanking);
            return result;
        }

        public EnrolmentDto GetByAccount(string accountNumber)
        {
            var result = _enrolmentAppService.GetByAccount(accountNumber, UserType.InstantBanking);
            return result;
        }
        public EnrolmentDto GetByCustomerId(string customerId)
        {
            var result = _enrolmentAppService.GetByCustomerId(customerId);
            return result;
        }

        public EnrolmentDto GetByMsisdn(string msisdn)
        {
            var result = _enrolmentAppService.GetByMsisdn(msisdn, UserType.InstantBanking);
            return result;
        }

        public InfopoolAccount GetAccountDetails(string accountNumber)
        {
            var result = _infopoolSystem.GetAccount(accountNumber);
            return result;
        }

        /// <summary>
        /// Checks that:
        ///     - The neither the account nor phone nor the combination of both is blacklisted
        ///     - Checks that the account number matches the phone number
        /// <para>Exceptions: AccountMsisdnMismatchException, AccountBlockedException</para>
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="accountNumber"></param>
        public InfopoolAccount ValidatePhoneAccount(string msisdn, string accountNumber, bool considermPinEligiblity, string customMismatchMessage = null)
        {
            // check for blacklist on any of phone and account
            CheckBlacklisted(msisdn, accountNumber);

            var standardMsisdn = StringManipulation.StandardizeNigerianMsisdn(msisdn);
            var accountMatch = _infopoolSystem.GetAccountDetailsCba(accountNumber, standardMsisdn, considermPinEligiblity ? (bool?)true : null);

            if (accountMatch == null)
            {
                var mismatchResponse = DuisResponses.GetDuisResponse(DuisResponses._accountMsisdnMismatch);
                throw new CustomException(mismatchResponse.response_message, false, mismatchResponse.response_code,
                    customMismatchMessage ?? mismatchResponse.response_message);
            }

            return accountMatch;
        }

        /// <summary>
        /// Exceptions: CustomException
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="accountNumber"></param>
        public void CheckBlacklisted(string msisdn, string accountNumber)
        {
            bool blacklisted = _enrolmentAppService.IsBlacklisted(msisdn, accountNumber);

            if (blacklisted)
            {
                string message = string.Format("This account phone {0} {1} has been blacklisted", msisdn, accountNumber);
                var accountBlockedResponse = DuisResponses.GetDuisResponse(DuisResponses._accountBlocked);
                throw new CustomException(message, false, accountBlockedResponse.response_code, accountBlockedResponse.response_message);
            }
        }

        internal EnrolmentDto Update(UpdateEnrolmentInput updater)
        {
            var updated = _enrolmentAppService.Update(updater);

            return updated;
        }

        /// <summary>
        /// <para>Exceptions: "AlreadyEnrolledException", "AccountMsisdnMismatchException"</para>
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="accountNumber"></param>
        internal DuisResponse CheckCanEnrol(string msisdn, string accountNumber, out InfopoolAccount accountDetails)
        {
            DuisResponse response = new DuisResponse
            {
                response_code = _successResponse.response_code,
                response_message = _successResponse.response_message
            };

            accountDetails = _infopoolSystem.GetAccount(accountNumber);
            string callCentrePhone = _configProvider.GetByName<string>(DuisConstants.ConfCallCentrePhone);
            bool isEnrolmentClosed = false;

            Boolean.TryParse(ConfigurationManager.AppSettings[DuisConstants.ConfIsEnrolmentClosed], out isEnrolmentClosed);

            if (isEnrolmentClosed)
            {
                response = DuisResponses.GetDuisResponse(DuisResponses._systemUnderMaintenance);
            }

            else
            {
                if (accountDetails != null && accountDetails.IsmPinEligible)
                {
                    EnrolmentDto match = _enrolmentAppService.GetByMsisdnAcct(msisdn, accountNumber, UserType.InstantBanking);

                    if (match == null)
                    {
                        match = _enrolmentAppService.GetByCustomerId(accountDetails.CustomerId);
                    }

                    if (match != null)
                    {
                        var message = string.Format("Number is already enrolled: {0}, {1}", accountNumber, msisdn);
                        LoggingSystem.LogMessage(message);
                        response = DuisResponses.GetDuisResponse(DuisResponses._alreadyEnrolled);
                        response.response_message =
                            string.Format(
                            @"You are already provisioned for this service using another phone **{0}. Please call Fidelity TrueServe on {1} for assistance",
                            match.Msisdn.Substring(match.Msisdn.Length - 4, 4), callCentrePhone);
                    }

                    else
                    {
                        // check match of account number to phone number
                        try
                        {
                            string customMismatchMessage = string.Format(
                                "Your phone is not registered on our system. Please contact Fidelity TrueServe on {0} for assistance", callCentrePhone);

                            ValidatePhoneAccount(msisdn, accountNumber, true, customMismatchMessage);
                        }

                        catch (CustomException ex)
                        {
                            LoggingSystem.LogException(ex);
                            response.response_code = ex.ErrorCode;
                            response.response_message = ex.ErrorMessage;
                        }
                    }
                }

                else
                {
                    var doesAccountExist = accountDetails != null && !string.IsNullOrWhiteSpace(accountDetails.SolId);

                    LoggingSystem.LogMessage(string.Format("Account {0} is not {1}.", accountNumber, !doesAccountExist ? "valid" : "mPin eligible"));
                    response = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._accountInvalidOrIneligible);

                    if (!doesAccountExist)
                    {
                        response.response_message = string.Format(
                            "The account {0} provided is invalid; check again to ensure it is correct. You may call Fidelity TrueServe on {1} for assistance",
                            accountNumber, callCentrePhone);
                    }

                    else
                    {
                        response.response_message = string.Format(
                            "The account number **{0} provided is not eligible to use this service. However for manual activation please visit any of our branches",
                            accountNumber.Substring(accountNumber.Length - 4, 4));
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Exceptions: "NotEnrolledException", "AccountBlockedException"
        /// </summary>
        /// <param name="msisdn"></param>
        public EnrolmentDto CheckEnrolmentStatus(string msisdn, UserType userType)
        {
            LoggingSystem.LogMessage("Now inside CheckEnrolmentStatus");
            EnrolmentDto match = _enrolmentAppService.GetByMsisdn(msisdn, userType);

            if (match == null)
            {
                throw new CustomException(DuisResponses._notEnrolled, true, null, null);
            }

            else
            {
                if (match.Status != EnrolStatus.Activated)
                {
                    string message = string.Format("Number {0} is enrolled but is blocked", msisdn);
                    var accountBlockedResponse = DuisResponses.GetDuisResponse(DuisResponses._accountBlocked);
                    throw new CustomException(message, false, accountBlockedResponse.response_code, accountBlockedResponse.response_message);
                }
            }

            return match;
        }

        internal EnrolmentOutput Enrol(EnrolmentInput input)
        {
            InfopoolAccount account = null;

            string msisdn = input.msisdn;
            string network = input.msisdn_network;
            string accountNumber = input.account_number;
            string password = input.password;
            string reference = input.reference_number;

            var canEnrolResponse = CheckCanEnrol(msisdn, accountNumber, out account);

            EnrolmentOutput response = new EnrolmentOutput { msisdn = input.msisdn, account_number = input.account_number, response_code = canEnrolResponse.response_code, response_message = canEnrolResponse.response_message };

            //DuisResponse response = CheckCanEnrol(msisdn, accountNumber, out account);

            if (canEnrolResponse.response_code.Equals(_successResponse.response_code))
            {
                // do actual enrolment
                try
                {
                    CheckPasswordStrength(password, string.Empty, account.BirthYear);

                    var bcryptPassword = SecurityUtil.GenerateBCryptHash(password);

                    var enrolmentInput = new CreateEnrolmentInput
                    {
                        AccountNumber = accountNumber.Trim(),
                        Msisdn = msisdn.Trim(),
                        Password = bcryptPassword.Hash,
                        Status = EnrolStatus.Activated,
                        BankCustomerId = account.CustomerId,
                        Network = network,
                        UserType = UserType.InstantBanking
                    };

                    var enrolmentObject = _enrolmentAppService.Create(enrolmentInput);

                    #region Log the new user's network to Network Logs table
                    CreateOrUpdateNetworkLog(msisdn, network, accountNumber);
                    #endregion

                    if (input.referrer_phone != null && input.referrer_phone != msisdn)
                    {
                        _referralAppService.Create(new Fbp.Net.Integrations.DuisApp.Referrals.Dtos.CreateReferralInput
                        {
                            ReferrerPhone = input.referrer_phone,
                            ReferreeUserId = enrolmentObject.Id,
                            RefereeCustomerId = enrolmentObject.BankCustomerId,
                            Product = ApplicationConstants.InstantBanking,
                            RefereeAccount = enrolmentObject.AccountNumber,
                            ReferreePhone = enrolmentObject.Msisdn

                        });
                    }

                    DuisProvider.AssignResponse(response, Constants.DuisResponses._successful);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    response.response_code = ex.ErrorCode;
                    response.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
                }
            }

            return response;
        }

        internal MsisdnAccounts GetMsisdnAccounts(string msisdn)
        {
            var result = new MsisdnAccounts
            {
                msisdn = msisdn
            };

            var standardMsisdn = StringManipulation.StandardizeNigerianMsisdn(msisdn);

            var associatedUser = _enrolmentAppService.GetByMsisdn(msisdn, UserType.InstantBanking);

            var accounts = associatedUser == null ? _infopoolSystem.GetUssdEligibleAccountsByMsisdn(standardMsisdn) :
                _infopoolSystem.GetmPinEligibleAccounts(standardMsisdn, associatedUser.BankCustomerId, associatedUser.AccountNumber);

            foreach (var account in accounts)
            {
                result.accounts.Add(
                    new Account
                    {
                        account_name = account.AccountName,
                        account_number = account.AccountNumber
                    }
                );
            }

            return result;
        }

        /// <summary>
        /// <para>Exceptions: WeakPasswordException</para>
        /// </summary>
        /// <param name="password"></param>
        internal void CheckPasswordStrength(string password, string oldPasswords, string birthYear)
        {
            #region Check structure
            if (password == null || 4 > password.Length)
            {
                string message = "Your password should be a minimum of four characters";
                var errorResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidPassword);
                throw new CustomException(message, false, errorResponse.response_code, message);
            }
            #endregion

            #region Check old passwords
            if (!string.IsNullOrWhiteSpace(oldPasswords))
            {
                string[] oldPasswordsArray = oldPasswords.Split(';');
                foreach (var oldPassword in oldPasswordsArray.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    if (CoreUtility.SecurityUtil.VerifyBCrypt(password, oldPassword))
                    {
                        string message = string.Format("You cannot re-use any of your last {0} passwords", _oldPasswordsCount);
                        var errorResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidPassword);
                        throw new CustomException(message, false, errorResponse.response_code, message);
                    }
                }
            }

            #endregion

            #region Check weak passwords by algorithm
            string pinFailureMessage = SecurityUtil.IsWeakPinDuis(password, birthYear); //_enrolmentAppService.IsPasswordWeak(password);

            bool isPasswordWeak = !string.IsNullOrWhiteSpace(pinFailureMessage);

            if (isPasswordWeak)
            {
                var errorResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidPassword);
                throw new CustomException(pinFailureMessage, false, errorResponse.response_code, pinFailureMessage);
            }
            #endregion
        }

        /// <summary>
        /// Checks User enrolment existence, status of enrolment and validates supplied password
        /// <para>Exceptions: "AuthenticationFailedException",  "NotEnrolledException", "AccountBlockedException", "PasswordExpiredException"</para>
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="password"></param>
        public EnrolmentDto AuthenticateUser(string msisdn, string password, bool requiresPasswordAuth = true, UserType userType = UserType.InstantBanking)
        {
            // get user
            var user = CheckEnrolmentStatus(msisdn, userType);

            if (requiresPasswordAuth)
            {
                DoPasswordAuth(user, password);
            }

            LoggingSystem.LogMessage(string.Format("{0} successfully authenticated.", msisdn));
            return user;
        }

        public void DoPasswordAuth(EnrolmentDto user, string password)
        {
            bool isPasswordValid = false;

            if (user.ShouldResetPassword)
            {
                throw new CustomException(DuisResponses._passwordExpired, true, null, null);
            }

            if (user != null)
            {
                // verify bCrypt password: supplied against database hash
                isPasswordValid = CoreUtility.SecurityUtil.VerifyBCrypt(password, user.Password);
            }

            // throw exception if verified status is false
            UpdateUserAuth(user, isPasswordValid);
        }

        public void UpdateUserAuth(EnrolmentDto user, bool isAuthSuccessful, string customAuthErrorMessage = null)
        {
            if (isAuthSuccessful == false)
            {
                var resultantFailedAttempts = user.FailedLoginAttempts + 1;

                var leftLoginAttempts = _maxPossibleFailedLoginTries - resultantFailedAttempts;

                _enrolmentAppService.Update(new UpdateEnrolmentInput
                {
                    Id = user.Id,
                    FailedLoginAttempts = resultantFailedAttempts,
                    LastLoginAttemptTime = DateTime.Now,
                    Status = resultantFailedAttempts < _maxPossibleFailedLoginTries ? null : (EnrolStatus?)EnrolStatus.LockedOut
                });

                var errorBreakdown = DuisResponses.GetDuisResponse(DuisResponses._authenticationFailed);

                // use the outstanding login attempts or if supplied, customAuthErrorMessage as ErrorMessage
                throw new CustomException(
                    string.Format("Authentication failed for {0}; either user does not exist or password is incorrect", user.Msisdn),
                     false,
                     errorBreakdown.response_code,
                     customAuthErrorMessage ?? leftLoginAttempts.ToString()
                    );
            }

            else
            {
                _enrolmentAppService.Update(new UpdateEnrolmentInput
                {
                    Id = user.Id,
                    LastLoginTime = DateTime.Now,
                    LastLoginAttemptTime = DateTime.Now,
                    FailedLoginAttempts = 0,
                    FirstLoginTime = user.FirstLoginTime == null ? (DateTime?)DateTime.Now : null
                });
            }
        }

        /// <summary>
        /// Expects:
        /// <para>Enrolled msisdn</para>
        /// <para>Enrolment account</para>
        /// <para>New password</para>
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public EnrolmentOutput ResetPassword(EnrolmentInput input)
        {
            EnrolmentOutput response = new EnrolmentOutput() { msisdn = input.msisdn };

            DuisProvider.AssignResponse(response, DuisResponses._successful);

            string msisdn = input.msisdn;
            string accountNumber = input.account_number;
            string newPassword = input.password;

            try
            {
                EnrolmentDto enrolment = CheckEnrolmentStatus(msisdn, UserType.InstantBanking);

                if (enrolment.AccountNumber != accountNumber)
                {
                    throw new CustomException(DuisResponses._accountMsisdnMismatch, true, null, null);
                }

                ValidateBirthday(enrolment, input.birthday_DdMmYy);
                UpdatePassword(enrolment, newPassword);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            return response;
        }

        private void ValidateBirthday(EnrolmentDto user, string birthdayDdMmYy)
        {
            var accountDetails = GetAccountDetails(user.AccountNumber);
            bool isTokenValid = accountDetails.BirthdayDdMmYy == birthdayDdMmYy;

            string authFailedMessage = "The validation token (birthday) supplied is invalid";
            UpdateUserAuth(user, isTokenValid, authFailedMessage);
        }

        internal void UpdatePassword(EnrolmentDto enrolment, string newPassword)
        {
            var account = _infopoolSystem.GetAccount(enrolment.AccountNumber);
            CheckPasswordStrength(newPassword, enrolment.OldPasswords ?? enrolment.Password, account.BirthYear);

            string oldPasswordsToKeep = enrolment.OldPasswords != null ?
                enrolment.OldPasswords.Split(';').Take(_oldPasswordsCount - 1).Aggregate((i, j) => i + ";" + j) :
                enrolment.Password;

            var bcryptPassword = CoreUtility.SecurityUtil.GenerateBCryptHash(newPassword);

            var enrolmentInput = new UpdateEnrolmentInput
            {
                Id = enrolment.Id,
                Password = bcryptPassword.Hash,
                OldPasswords = string.Format("{0};{1}", bcryptPassword.Hash, oldPasswordsToKeep)
            };

            var enrolmentObject = _enrolmentAppService.Update(enrolmentInput);
        }

        public EnrolmentOutput ChangePassword(EnrolmentInput input)
        {
            EnrolmentOutput response = new EnrolmentOutput() { msisdn = input.msisdn };

            DuisProvider.AssignResponse(response, DuisResponses._successful);

            string msisdn = input.msisdn;
            string accountNumber = input.account_number;
            string newPassword = input.new_password;

            try
            {
                EnrolmentDto enrolment = AuthenticateUser(msisdn, input.password);

                if (enrolment.AccountNumber != accountNumber)
                {
                    throw new CustomException(DuisResponses._accountMsisdnMismatch, true, null, null);
                }

                UpdatePassword(enrolment, newPassword);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            return response;
        }

        public GetUserOutput GetUser(string msisdn)
        {
            var user = _enrolmentAppService.GetByMsisdn(msisdn, UserType.InstantBanking);

            var response = new GetUserOutput
            {

            };

            DuisProvider.AssignResponse(response, DuisResponses._notEnrolled);

            if (user != null)
            {
                response.msisdn = msisdn;
                response.default_account = user.AccountNumber;
                response.is_active = user.IsActive;
                response.should_reset_password = user.ShouldResetPassword;
                response.enrolment_time = user.EnrolmentTime;

                if (user.ShouldResetPassword)
                {
                    response.name = _infopoolSystem.GetAccountName(user.AccountNumber, null, AccountEnquiryProvider._fincoreResponsesPath);
                }

                DuisProvider.AssignResponse(response, DuisResponses._successful);
            }

            return response;
        }

        public void CreateOrUpdateNetworkLog(string msisdn, string network, string accountNumber)
        {
            try
            {
                msisdn = CoreUtility.StringManipulation.StandardizeNigerianMsisdn(msisdn);

                var networkLog = _phoneNetworkLogAppService.GetPhoneNetwork(msisdn);

                if (networkLog == null)
                {
                    _phoneNetworkLogAppService.Create(new CreatePhoneNetworkLogInput
                    {
                        AccountNumber = accountNumber,
                        IsTransactionInitiator = true,
                        Msisdn = msisdn,
                        Network = network
                    });
                }

                else if (!string.IsNullOrWhiteSpace(network))
                {
                    LoggingSystem.LogMessage(string.Format("Updating network log for {0} to {1}", msisdn, network));

                    _phoneNetworkLogAppService.Update(new UpdatePhoneNetworkLogInput
                    {
                        Id = networkLog.Id,
                        Network = network,
                        AccountNumber = accountNumber ?? networkLog.AccountNumber,
                        IsTransactionInitiator = true
                    });
                }
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Could not save new user's {0} network", msisdn));
            }
        }

        public DuisResponse LockoutUser(LockoutInput input)
        {
            var response = DuisResponses.GetDuisResponse(DuisResponses._inputInvalid);

            if (input != null)
            {
                var theUser = _enrolmentAppService.GetByMsisdn(input.msisdn, UserType.InstantBanking);

                if (theUser != null && theUser.AccountNumber == input.account_number)
                {
                    _enrolmentAppService.Update(new UpdateEnrolmentInput
                    {
                        Id = theUser.Id,
                        Status = EnrolStatus.LockedOut,
                        LockoutReason = string.Format("{0}{1}{2}", input.reason, string.IsNullOrWhiteSpace(theUser.LockoutReason) ? string.Empty : "||",
                        theUser.LockoutReason)
                    });

                    LoggingSystem.LogMessage(string.Format("User {0} is being locked out because: {1}", input.msisdn, input.reason));

                    return _successResponse;// DuisResponses.GetDuisResponse(DuisResponses._successful);
                }
            }

            return response;
        }

        internal PasswordValidationOutput IsPasswordValid(string password, string account, UserType userType = UserType.InstantBanking)
        {
            var result = false;
            string reason = string.Empty;

            try
            {
                var accountDetails = _infopoolSystem.GetAccount(account);

                string birthYear = accountDetails == null ? string.Empty : accountDetails.BirthYear;

                var theUser = _enrolmentAppService.GetByAccount(account, userType);

                CheckPasswordStrength(password, theUser == null ? string.Empty : theUser.OldPasswords, accountDetails.BirthYear);
                result = true;
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex, string.Format(ex.Message, password));
                reason = ex.Message;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format(ex.Message, password));
                reason = "An error occurred";
            }

            return new PasswordValidationOutput
            {
                is_valid = result,
                reason = reason
            };
        }

        internal AccountEnquiryOutput IsBlacklisted(string msisdn, string account)
        {
            var isBlacklisted = _enrolmentAppService.IsBlacklisted(msisdn, account);

            var result = new AccountEnquiryOutput
            {
                is_blacklisted = isBlacklisted
            };

            DuisProvider.AssignResponse(result, DuisResponses._successful);

            return result;
        }

        public InfopoolSystem GetInfopoolSystem()
        {
            return new InfopoolSystem(_infopoolReader);
        }

        internal SelfBlockOutput SelfBlock(SelfBlockInput input)
        {
            SelfBlockOutput response = new SelfBlockOutput();

            string msisdn = input.msisdn;
            string beneficiaryMsisdn = input.beneficiary_msisdn;
            string password = input.password;

            try
            {

                EnrolmentDto enrolment = AuthenticateUser(beneficiaryMsisdn, password);

                if (enrolment != null)
                {
                    Update(new UpdateEnrolmentInput
                    {
                        Id = enrolment.Id,
                        Status = EnrolStatus.Disabled
                    });

                    DuisProvider.AssignResponse(response, DuisResponses._successful);
                }
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            return response;
        }

        internal SelfUnlockOutput SelfUnlock(SelfUnlockInput input)
        {
            var response = new SelfUnlockOutput();

            // get user
            var user = _enrolmentAppService.GetByMsisdn(input.msisdn, UserType.InstantBanking);

            if (user == null || user.Status != EnrolStatus.LockedOut)
            {
                var tempResponse = DuisResponses.GetDuisResponse(DuisResponses._accountInvalidOrIneligible);

                return new SelfUnlockOutput { response_code = tempResponse.response_code, response_message = "Only locked out users can access this service" };
                // Your account is not eligible for this operation
            }

            // get account details
            try
            {
                var accountDetails = ValidatePhoneAccount(input.msisdn, input.account_number, false);

                DateTime birthday;

                // cater for users who supplied the birthday in an intuitive format
                if(DateTime.TryParse(input.birthday_DdMmYy, CultureInfo.CreateSpecificCulture("en-GB"), DateTimeStyles.None, out birthday))
                {
                    if(birthday != accountDetails.BirthDate)
                    {
                        LoggingSystem.LogMessage(string.Format("User {0} supplied {1} for birthday instead of {2} for self-unlock 1",
                            input.msisdn, input.birthday_DdMmYy, accountDetails.BirthdayDdMmYy));

                        throw new CustomException(DuisResponses._authenticationFailed, true, null, null);
                    }
                }

                else if (accountDetails.BirthdayDdMmYy != input.birthday_DdMmYy)
                {
                    LoggingSystem.LogMessage(string.Format("User {0} supplied {1} for birthday instead of {2} for self-unlock 2",
                        input.msisdn, input.birthday_DdMmYy, accountDetails.BirthdayDdMmYy));

                    throw new CustomException(DuisResponses._authenticationFailed, true, null, null);
                }

                var cards = _infopoolSystem.GetCustomerValidCards(user.BankCustomerId);

                if (cards.Count == 0)
                {
                    var tempResponse = DuisResponses.GetDuisResponse(DuisResponses._accountInvalidOrIneligible);

                    LoggingSystem.LogMessage(string.Format("User {0} supplied {1} for self-unlock PAN but does not have a debit card",
                        input.msisdn, input.pan_last_four_digits));

                    return new SelfUnlockOutput { response_code = tempResponse.response_code, response_message = "Only users with a debit card can access this service" };
                }

                var cardLastFourDigits = cards.Select(x => x.MaskedPAN == null ? null : x.MaskedPAN.LastCharacters(4));

                if (!cardLastFourDigits.Contains(input.pan_last_four_digits))
                {
                    LoggingSystem.LogMessage(string.Format("User {0} supplied {1} for self-unlock PAN which does not match any of their {2} cards",
                       input.msisdn, input.pan_last_four_digits, cards.Count()));

                    throw new CustomException(DuisResponses._authenticationFailed, true, null, null);
                }

                LoggingSystem.LogMessage(string.Format("User {0} was successfully authenticated for a self unlock; now unlocking...", input.msisdn));

                user = Update(new UpdateEnrolmentInput
                {
                    Id = user.Id,
                    FailedLoginAttempts = 0,
                    Status = EnrolStatus.Activated,
                    LockoutReason = string.Format("Self-unlocked on {0: dd-MM-yyyy}", DateTime.Now)
                });

                DuisProvider.AssignResponse(response, Constants.DuisResponses._successful);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
            }

            return response;
        }
    }
}