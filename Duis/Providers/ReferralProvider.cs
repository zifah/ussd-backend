﻿using Abp.Dependency;
using Duis.Exceptions;
using Duis.Models;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.Referrals;
using System;
using System.Linq;

namespace Duis.Providers
{
    public class ReferralProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly IReferralAppService _referralAppService;

        public ReferralProvider(EnrolmentProvider enrolmentProvider, IReferralAppService referralAppService, ConfigurationProvider configProvider)
        {
            _referralAppService = referralAppService;
            _enrolmentProvider = enrolmentProvider;
            _configProvider = configProvider;
        }

        internal ReferralOutput GetPoints(Models.AccountEnquiryInput input)
        {
            var output = new ReferralOutput();

            try
            {
                _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);
                var activePoints = _referralAppService.GetReferrals(input.msisdn).Count(x => x.IsActive);
                var pointsPerReferral = _configProvider.GetByName<int>(DuisConstants.ConfPointsPerReferral);
                output.points = pointsPerReferral * activePoints;
                DuisProvider.AssignResponse(output, Constants.DuisResponses._successful);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            return output;
        }
    }
}