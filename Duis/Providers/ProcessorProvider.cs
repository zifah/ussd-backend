﻿using FidelityBank.CoreLibraries.Interfaces;
using FidelityBank.CoreLibraries.Interfaces.BillPayment;
using FidelityBank.CoreLibraries.Interfaces.Cashout;
using FidelityBank.CoreLibraries.Interfaces.Transfer;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Providers
{
    public class ProcessorProvider
    {
        public static IBillPaymentProvider GetBillingProcessor(string fullyQualifiedClassName, string assemblyName)
        {
            var processorInstance = (IBillPaymentProvider)Activator.CreateInstance(assemblyName, fullyQualifiedClassName).Unwrap();
            var container = new Container(x => x.For<IBillPaymentProvider>().Use(processorInstance));
            var processor = container.GetInstance<IBillPaymentProvider>();
            return processor;
        }

        public static ICashoutProvider GetCashoutProcessor(string fullyQualifiedClassName, string assemblyName)
        {
            var processorInstance = (ICashoutProvider)Activator.CreateInstance(assemblyName, fullyQualifiedClassName).Unwrap();
            var container = new Container(x => x.For<ICashoutProvider>().Use(processorInstance));
            var processor = container.GetInstance<ICashoutProvider>();
            return processor;
        }

        public static ITransferProvider GetTransferProcessor(string fullyQualifiedClassName, string assemblyName)
        {
            var processorInstance = (ITransferProvider)Activator.CreateInstance(assemblyName, fullyQualifiedClassName).Unwrap();
            var container = new Container(x => x.For<ITransferProvider>().Use(processorInstance));
            var processor = container.GetInstance<ITransferProvider>();
            return processor;
        }
    }
}