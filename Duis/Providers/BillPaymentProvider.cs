﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using FidelityBank.CoreLibraries.Interfaces.BillPayment;
using System.Web.Helpers;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using FidelityBank.CoreLibraries.Interfaces;
using Newtonsoft.Json;
using Duis.Models.BillPayments;
using Fbp.Net.Integrations.DuisApp.Validations;

namespace Duis.Providers
{
    public class BillPaymentProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly IBillPaymentConfigAppService _billerAppService;
        public readonly IValidationAppService _validationAppService;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly ConfigurationProvider _configProvider;

        //configs
        private readonly string _fincoreSuccessCode = null;
        private readonly string _fincoreDuplicateCode = null;
        private readonly string _fidelityNipV2NumericCode = null;

        public BillPaymentProvider(EnrolmentProvider enrolmentProvider,
            TransactionProvider transactionProvider,
            IBillPaymentConfigAppService billerAppService, ConfigurationProvider configProvider,
            IValidationAppService validationAppService)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;
            _billerAppService = billerAppService;
            _validationAppService = validationAppService;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            _fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
            _fincoreDuplicateCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreDuplicateCode);
            _fidelityNipV2NumericCode = _configProvider.GetByName<string>(DuisConstants.ConfFidelityNipV2NumericCode);
        }

        public BillPaymentOutput GetBillerDetails(string billerCode)
        {
            BillPaymentOutput response = new BillPaymentOutput
            {
                response_code = _badRequestResponse.response_code,
                response_message = _badRequestResponse.response_message
            };

            // get biller by code
            var theBiller = _billerAppService.GetByCode(billerCode);

            if (theBiller != null)
            {
                // inject biller config assembly into provider
                DuisProvider.AssignResponse(response, Constants.DuisResponses._successful);
                response.requires_id_validation = theBiller.RequiresCustomerIdValidation;
                response.surcharge = theBiller.SurchargeAmount;
                response.biller_name = theBiller.BillerName;
                response.biller_short_name = theBiller.ShortName;
                response.customer_id_descriptor = theBiller.CustomerIdFieldName;
                response.plans = theBiller.BillerPlans.ToList();
            }


            return response;
        }

        public BillPaymentOutput ValidateBillPayment(BillPaymentInput input)
        {
            BillPaymentOutput response = new BillPaymentOutput
            {
                response_code = _badRequestResponse.response_code,
                response_message = _badRequestResponse.response_message
            };

            if (input != null)
            {
                // get biller by code
                var theBiller = _billerAppService.GetByCode(input.biller_code);

                if (theBiller != null)
                {
                    // inject biller config assembly into provider
                    var billingProcessor = ProcessorProvider.GetBillingProcessor(theBiller.ProviderClassFqn, theBiller.ProviderAssembly);
                    // compose bill payment input object
                    var thePlan = theBiller.GetSelectedPlan(input.selected_plan_code);

                    if (thePlan != null)
                    {
                        var billingInput = new BillPaymentIn
                        {
                            Amount = input.amount,
                            ChargeAmount = theBiller.SurchargeAmount,
                            PlanCode = thePlan.Code,
                            DestCustomerCode = input.customer_id,
                            //ProviderParameters = Json.Decode(theBiller.PaymentParametersJson),
                            ProviderParametersJson = theBiller.PaymentParametersJson
                        };

                        var procReqTime = DateTime.Now;
                        // trigger transaction service on billing provider
                        var billingResponse = billingProcessor.ValidateBillPayment(billingInput);

                        response.biller_name = theBiller.BillerName;
                        response.product_name = thePlan.Name;
                        response.surcharge = theBiller.SurchargeAmount;

                        LoggingSystem.LogMessage(string.Format("Validation response: {0}", JsonConvert.SerializeObject(billingResponse)));

                        switch (billingResponse.Status)
                        {
                            case BillPaymentStatus.Successful:
                                response.validation_text = billingResponse.ValidationText;
                                response.validation_amount = billingResponse.ValidationAmount;
                                DuisProvider.AssignResponse(response, Constants.DuisResponses._successful);

                                if (theBiller.SaveValidationResponse)
                                {
                                    _validationAppService.Create(new Fbp.Net.Integrations.DuisApp.Validations.Dtos.CreateValidationInput
                                    {
                                        Amount = response.validation_amount,
                                        BillerId = input.biller_code,
                                        BillerName = theBiller.BillerName,
                                        ClientReference = input.reference_number,
                                        CustomerId = input.customer_id,
                                        CustomerName = response.validation_text,
                                        ProviderReference = billingResponse.ProviderReference,
                                        RawResponse = billingResponse.ProviderRawResponse
                                    });
                                }

                                break;

                            default:
                                DuisProvider.AssignResponse(response, Constants.DuisResponses._failedAtProcessor);
                                response.response_message = string.Format("{0}: {1}", response.response_message, billingResponse.ResponseDescription);
                                break;
                        }
                    }
                }
            }

            return response;
        }

        public BillPaymentOutput DoBillPayment(BillPaymentInput input, string transactionType = ApplicationConstants.BillPayment, string narration = null)
        {
            var response = new BillPaymentOutput();
            ProcessBillPayment(input, response, transactionType);
            return response;
        }

        public void DoBillPaymentAsync(BillPaymentInput input, BillPaymentOutput output, ICentralProvider coreProvider)
        {
            ProcessBillPayment(input, output, ApplicationConstants.BillPayment);

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        private void SendCompletionSms(BillPaymentInput input, BillPaymentOutput output)
        {
            var account = new Duis.Models.Accounts.Account() { account_number = input.account_number.Trim() };

            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            var sms = isSuccessful ?
                string.Format("Congrats! Your {0} {1} bill payment transaction of {2:N2} for {3} was successful", output.biller_name, output.product_name, input.amount, input.customer_id) :
                string.Format("Sorry. Your {0} {1} bill payment transaction of {2:N2} for {3} was not successful", output.biller_name, output.product_name, input.amount, input.customer_id);

            var subject = DuisConstants.ApplicationName;

            var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
            smsSender.SendSMS(input.msisdn, subject, sms);
        }

        private void ProcessBillPayment(BillPaymentInput input, BillPaymentOutput output, string transactionType)
        {
            output.response_code = _badRequestResponse.response_code;
            output.response_message = _badRequestResponse.response_message;

            if (input != null)
            {
                try
                {
                    var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, input.require_password);
                    var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);
                    _transactionProvider.CheckLimit(input.amount, theUser, transactionType, input.token);
                    DoBillingTransaction(input, output, transactionType, theUser, accountDetails.SolId);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    output.response_code = ex.ErrorCode;
                    output.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }
        }

        /// <summary>
        /// Handles the following:
        /// - Debiting the customer if applicable
        /// - Contacting the bill payment provider to pricess the bill on successful payment
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="transactionType"></param>
        /// <param name="theUser"></param>
        /// <param name="solId"></param>
        public void DoBillingTransaction(BillPaymentInput input, BillPaymentOutput output, string transactionType, EnrolmentDto theUser, string solId)
        {
            // get biller by code         
            var theBiller = _billerAppService.GetByCode(input.biller_code);

            if (theBiller != null)
            {
                output.biller_name = theBiller.BillerName;

                if (theBiller.BillerSuspenseAccount != null)
                {
                    theBiller.BillerSuspenseAccount = theBiller.BillerSuspenseAccount.Replace("SOL", solId);
                }

                // save the transaction
                TransactionDto savedTransaction = SaveBillingTransaction(input, theBiller, theUser, transactionType);

                var thePlan = theBiller.HasPlans ? theBiller.GetSelectedPlan(input.selected_plan_code) : new BillerPlan()
                {
                    Code = input.selected_plan_code,
                    Name = input.product_name
                };

                if (thePlan != null)
                {
                    output.product_name = thePlan.Name;
                    // generate transaction reference
                    string billingRef = GetBillingRef(savedTransaction.DrRefIdNumeric, savedTransaction.CreationTime);

                    string narration = GetBillPaymentNarration(savedTransaction.ProcessorUserId, theBiller.ShortName, billingRef, transactionType);

                    // do transfer
                    bool requiresDebit = !theBiller.IsProviderTransactor;
                    bool isValueTaken = false;

                    if (requiresDebit)
                    {
                        savedTransaction = _transactionProvider.DoTransactionDebit(savedTransaction, billingRef, narration);
                        isValueTaken = new string[] { _fincoreSuccessCode, _fincoreDuplicateCode }.Contains(savedTransaction.DrResponseCode);
                    }

                    else
                    {
                        savedTransaction = _transactionProvider.Update(
                            new UpdateTransactionInput
                            {
                                Id = savedTransaction.Id,
                                RefId = billingRef,
                                DrNarration = narration
                            });
                    }

                    /*
                     * Below property used to be set for only cases where the customer was successfully debited and processor was called. 
                     * The value was also set to the reference from the bill processor
                     * Changed 1 March 2017 by Hafiz Adewuyi to allow for self-generated ref to be returned to Clickatell for Payd recharge
                     */
                    output.payment_reference = savedTransaction.RefId;  

                    isValueTaken = isValueTaken || !requiresDebit;

                    // if successful debit, call biller to complete
                    if (isValueTaken)
                    {
                        // inject biller config assembly into provider
                        var billingProcessor = ProcessorProvider.GetBillingProcessor(theBiller.ProviderClassFqn, theBiller.ProviderAssembly);
                        // compose bill payment input object

                        var billingInput = new BillPaymentIn
                        {
                            Amount = input.amount,
                            ChargeAmount = input.surcharge ?? theBiller.SurchargeAmount,
                            PlanCode = thePlan.Code,
                            DestCustomerCode = input.customer_id,
                            ValidationText = input.validation_text,
                            //ProviderParameters = Json.Decode(theBiller.PaymentParametersJson),
                            ProviderParametersJson = theBiller.PaymentParametersJson,
                            PayerEmail = savedTransaction.UserEmail,
                            PayerName = savedTransaction.UserFullName,
                            PayerPhone = savedTransaction.UserMsisdn,
                            ReferenceNumber = billingRef,
                            DebitAccount = input.account_number,
                            Channel = input.network
                        };

                        if (theBiller.SaveValidationResponse)
                        {
                            var validation = _validationAppService.GetBy(input.customer_id, theBiller.BillerCode);

                            billingInput.ValidationReference = validation != null ? validation.ProviderReference : null;
                        }

                        var procReqTime = DateTime.Now;
                        // trigger transaction service on billing provider
                        var billingResponse = billingProcessor.DoBillPayment(billingInput);

                        // process status and return response
                        var updater = new UpdateTransactionInput
                        {
                            Id = savedTransaction.Id,
                            ProcessorRespCode = billingResponse.ResponseCode,
                            ProcessorRespDesc = billingResponse.ResponseDescription,
                            ProcessorResponseBody = billingResponse.ProviderRawResponse,
                            Success = billingResponse.IsSuccessful,
                            ProcessorRefId = billingResponse.ProviderReference ?? billingInput.ValidationReference,
                            ProcessorReqTime = procReqTime,
                            ProcessorRespTime = DateTime.Now,
                        };

                        switch (billingResponse.Status)
                        {
                            case BillPaymentStatus.Successful:
                                DuisProvider.AssignResponse(output, DuisResponses._successful);
                                break;

                            case BillPaymentStatus.Failed:
                                DuisProvider.AssignResponse(output, DuisResponses._failedAtProcessor);
                                break;
                            case BillPaymentStatus.Unknown:
                                DuisProvider.AssignResponse(output, DuisResponses._statusUnknown);
                                break;
                        }

                        updater.Status = output.response_message;
                        updater.StatusCode = output.response_code;

                        savedTransaction = _transactionProvider.Update(updater);
                    }

                    else
                    {
                        var insufficientFundsCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreInsufficientFundsCode);

                        var responseString = savedTransaction.DrResponseCode == insufficientFundsCode ?
                            Constants.DuisResponses._insufficientFunds : Constants.DuisResponses._chargeCustomerFailed;

                        DuisProvider.AssignResponse(output, responseString);

                        _transactionProvider.Update(new UpdateTransactionInput
                        {
                            Id = savedTransaction.Id,
                            Status = output.response_message,
                            StatusCode = output.response_code,
                            Success = false
                        });
                    }
                }

                else
                {
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);

                    _transactionProvider.Update(new UpdateTransactionInput
                    {
                        Id = savedTransaction.Id,
                        Status = output.response_message,
                        StatusCode = output.response_code,
                        Success = false
                    });
                }
            }
        }

        private string GetBillingRef(int rrn, DateTime transactionDate)
        {
            string billingRef = string.Format("1016U{0}{1}", transactionDate.ToString("yyMMdd"), rrn.ToString().PadLeft(10, '0'));
            return billingRef;
        }

        private TransactionDto SaveBillingTransaction(BillPaymentInput input, BillPaymentConfigDto biller, EnrolmentDto theUser, string transactionType)
        {
            var infopoolAccount = _enrolmentProvider.GetAccountDetails(input.account_number);

            var toSave = new CreateTransactionInput
            {
                Amount = input.amount,
                ChargeAmount = input.surcharge ?? biller.SurchargeAmount,
                ClientRefId = input.reference_number,
                DrAmount = input.amount,
                DrDestination = biller.BillerSuspenseAccount,
                DrDestinationCode = _fidelityNipV2NumericCode,
                DrSource = input.account_number,
                ProcessorCode = biller.BillerCode,
                ProcessorOptionCode = input.selected_plan_code,
                ProcessorName = biller.BillerName,
                ProcessorUserId = input.customer_id,
                ProcessorValidationOutput = input.validation_text,
                Type = transactionType,
                UserMsisdn = input.msisdn,
                UserId = theUser.Id,
                UserEmail = infopoolAccount.AlertEmail,
                UserFullName = infopoolAccount.AccountName,
                IsViaDirectCode = input.is_via_direct_code
            };

            return _transactionProvider.Create(toSave);
        }

        private string GetBillPaymentNarration(string customerId, string billerName, string refId, string transactionType = ApplicationConstants.BillPayment)
        {
            var result = string.Empty;

            if (transactionType == ApplicationConstants.AirtimeRecharge)
            {
                return GetRechargeNarration(customerId, refId);
            }

            else
            {
                //770 <biller name first ten characters> **<last 7 customer id characters>~1016U1606230000000042
                //770 LCC TOLL P FOR **8804-02~1016U1606230000000042
                billerName = billerName.Substring(0, Math.Min(billerName.Length, 10));

                int customerIdMaxLength = 7 + 10 - billerName.Length;

                int startIndex = customerId.Length < customerIdMaxLength ? 0 : customerId.Length - customerIdMaxLength;
                customerId = customerId.Substring(startIndex, Math.Min(customerId.Length, customerIdMaxLength));

                if (startIndex > 0)
                {
                    customerId = string.Format("**{0}", customerId);
                }

                result = string.Format("770 {0} FOR {1} {2}", billerName, customerId, refId);
            }

            return result;
        }

        public string GetRechargeNarration(string phone, string refId)
        {
            var result = string.Empty;
            int startIndex = phone.Length < 11 ? 0 : phone.Length - 11;
            result = string.Format("770 RECHARGE FOR {0} {1}", phone.Substring(startIndex, Math.Min(phone.Length, 11)), refId);
            return result;
        }

        internal List<Biller> GetVisibleBillers()
        {
            IList<BillPaymentConfigDto> visibleBillers = _billerAppService.GetVisibleBillers();

            List<Biller> result = visibleBillers.Select(x => new Biller
            {
                name = x.BillerName,
                code = x.BillerCode,
                category = x.Category,
                short_name = x.ShortName
            }).OrderBy(x => x.name).OrderBy(x => x.category).ToList();

            return result;
        }

        public BillPaymentConfigDto GetBillerByName(string name)
        {
            return _billerAppService.GetByName(name);
        }
    }
}