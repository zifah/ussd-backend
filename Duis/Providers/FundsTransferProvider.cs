﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Fincore;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Newtonsoft.Json;
using Fbp.Net.Integrations.Configurations;
using Duis.Utilities;

namespace Duis.Providers
{
    public class FundsTransferProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly ITransactionAppService _transactionAppService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly FincoreSystem _fincore = null;
        private readonly TransactionProvider _transactionProvider;
        private readonly InfopoolSystem _infopoolSystem;
        private readonly TransferProvider _transferProvider;

        //configs
        private readonly string _fincoreSuccessCode = null;
        private readonly string _fincoreDuplicateCode = null;
        private readonly string _ownBankAlphaCode = null;

        public FundsTransferProvider(EnrolmentProvider enrolmentProvider,
            ITransactionAppService transactionAppService, TransactionProvider transactionProvider,
            IEnrolmentAppService enrolmentAppService, ConfigurationProvider configProvider, TransferProvider transferProvider)
        {
            _configProvider = configProvider;
            _enrolmentAppService = enrolmentAppService;
            _enrolmentProvider = enrolmentProvider;
            _transactionAppService = transactionAppService;
            _transactionProvider = transactionProvider;
            _transferProvider = transferProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];
            _fincore = new FincoreSystem(fincoreResponsesPath);

            _fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
            _fincoreDuplicateCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreDuplicateCode);
            _ownBankAlphaCode = _configProvider.GetByName<string>(DuisConstants.ConfOwnBankAlphaCode);
            _infopoolSystem = new InfopoolSystem(_infopoolReader);
        }

        private void ProcessFundsTransfer(FundsTransferInput input, FundsTransferOutput output)
        {
            output.response_code = _badRequestResponse.response_code;
            output.response_message = _badRequestResponse.response_message;

            if (input != null)
            {
                output.payment_reference = input.reference_number;
                // check if number is enrolled
                try
                {
                    var enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                    // check if msisdn is linked to account
                    var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.source_nuban, false);
                    var isIntrabank = input.dest_bank_alpha_code == _ownBankAlphaCode;

                    // check limit
                    string transactionType = isIntrabank ? ApplicationConstants.IntrabankTransfer : ApplicationConstants.InterbankTransfer;
                    _transactionProvider.CheckLimit(input.amount, enrolment, transactionType, input.token);

                    input.Narration = GetTransferNarration(input, isIntrabank);
                    input.TransactionType = transactionType;

                    _transferProvider.ProcessFundsTransfer(enrolment, input, output, isIntrabank);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    output.response_code = ex.ErrorCode;
                    output.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }
        }

        public void DoFundsTransfer(FundsTransferInput input, FundsTransferOutput output, ICentralProvider coreProvider)
        {
            ProcessFundsTransfer(input, output);

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                #region Notify the customer
                var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

                var sms = isSuccessful ?
                    string.Format("Your transfer of N{0:N2} to {1} was successful", input.amount, input.dest_account_name) :
                    string.Format("Sorry. Your transfer of N{0:N2} to {1} was not successful", input.amount, input.dest_account_name);

                CentralOperations.SendNotificationSms(sms, input.msisdn); 
                #endregion
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        private TransactionDto CreateTransaction(EnrolmentDto user, decimal charge, string narration, FundsTransferInput input, string transactionType)
        {
            var databaseRecord = new CreateTransactionInput
            {
                Amount = input.amount,
                ChargeAmount = charge,
                ClientRefId = input.reference_number,
                DrAmount = input.amount,
                DrSource = input.source_nuban,
                DrDestination = input.dest_nuban,
                DrDestinationCode = input.dest_bank_alpha_code,
                DrNarration = narration,
                UserId = user.Id,
                UserMsisdn = user.Msisdn,
                ProcessorValidationOutput = input.dest_account_name,
                Type = transactionType,
                IsViaDirectCode = input.is_via_direct_code
            };

            var record = _transactionAppService.Create(databaseRecord);

            return record;
        }

        /// <summary>
        /// <para>1. Pick the RRN</para>
        /// <para>2. Generate PaymentRef as 'I'+<dest_bank_code>+<RRN></para>
        /// </summary>
        /// <param name="ftRequest"></param>
        private string GetInterbankPaymentRef(int rrn, string bankCode)
        {
            string payRef = string.Format("M{0}{1}", bankCode, rrn.ToString().PadLeft(10, '0'));
            return payRef;
        }

        private decimal GetTransferCharge(string transactionType, EnrolmentDto user)
        {
            long? profileId = user == null || user.Profile == null ? null : (long?)user.Profile.Id;
            var charge = _transactionAppService.GetTransactionCharge(transactionType, profileId);

            if (profileId.HasValue && !charge.HasValue)
            {
                // profile has no charge configured for it, so use the default charge
                charge = _transactionAppService.GetTransactionCharge(transactionType, null);
            }

            if (!charge.HasValue)
            {
                throw new Exception(string.Format("No charges have been configured for {0}s for user: {1}", transactionType, user.Msisdn));
            }

            return charge.Value;
        }

        private decimal GetCharge(string transactionType, string msisdn)
        {
            var user = _enrolmentAppService.GetByMsisdn(msisdn, UserType.InstantBanking);

            return GetTransferCharge(transactionType, user);

        }

        private string GetTransferNarration(FundsTransferInput input, bool isIntrabank)
        {
            var narration = string.Empty;

            string narrationPrefix = "770 TRANSFER ";
            string customNarration = null;
            bool isCustomNarration = false;

            if (!string.IsNullOrWhiteSpace(input.narration))
            {
                customNarration = string.Format("{0}{1}", narrationPrefix, input.narration);
                isCustomNarration = true;
            }

            var sourceAccount = input.source_nuban.Trim();
            var destinationAccount = input.dest_nuban.Trim();

            string senderName = CoreUtility.StringManipulation.RemoveSpecialCharacters(DuisProvider.GetAccountName(input.source_nuban));
            int senderNameLength = senderName == null ? 0 : senderName.Length;

            // For cases where account number is passed in instead of name
            string receiverName = CoreUtility.StringManipulation.RemoveSpecialCharacters(input.dest_account_name == null ? null : input.dest_account_name.All(char.IsDigit) ? null : input.dest_account_name);
            int receiverNameLength = receiverName == null ? 0 : receiverName.Length;


            if (isIntrabank)
            {
                if (isCustomNarration)
                {
                    return customNarration;
                }

                var senderNameFirstEleven = senderNameLength == 0 ? string.Empty : senderName.Substring(0, Math.Min(senderNameLength, 11));

                var senderAcctLastTwo = sourceAccount.Substring(8, 2);


                var receiverNameFirstTwelve = receiverNameLength == 0 ? string.Empty : receiverName.Substring(0, Math.Min(receiverNameLength, 12));

                var receiverAcctLastTwo = destinationAccount.Substring(8, 2);
                narration = string.Format("{0}{1} **{2} TO {3} **{4}",
                    narrationPrefix, senderNameFirstEleven, senderAcctLastTwo, receiverNameFirstTwelve, receiverAcctLastTwo);
            }

            else
            {
                // is interBank
                var senderNameFirstTwentyOne = senderNameLength == 0 ? string.Empty : senderName.Substring(0, Math.Min(senderNameLength, 21));

                var senderAcctLastFour = sourceAccount.Substring(6, 4);

                var receiverNameFirstNineteen = receiverNameLength == 0 ? string.Empty : receiverName.Substring(0, Math.Min(receiverNameLength, 19));

                string ownNarration = string.Format("{0}TO {1} {2} {3}", narrationPrefix, receiverNameFirstNineteen, destinationAccount, input.dest_bank_name.Substring(0, 3));

                narration = customNarration ??
            string.Format("{0}|{1}FROM {2} **{3} FBP", ownNarration, narrationPrefix, senderNameFirstTwentyOne, senderAcctLastFour);
            }

            return narration;
        }

        internal decimal GetTransferCharge(string bankAlphaCode, string senderMsisdn)
        {
            bool isIntraBank = bankAlphaCode == _ownBankAlphaCode;
            string transactionType = isIntraBank ? ApplicationConstants.IntrabankTransfer : ApplicationConstants.InterbankTransfer;
            decimal charge = GetCharge(transactionType, senderMsisdn);
            return charge;
        }
    }
}