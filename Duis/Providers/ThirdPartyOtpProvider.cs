﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Exceptions;
using Duis.Models;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.ThirdPartyOtps;
using Fbp.Net.Integrations.DuisApp.ThirdPartyOtps.Dtos;
using FidelityBank.CoreLibraries.Infopool;
using FidelityBank.CoreLibraries.Utility.Engine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Duis.Providers
{
    public class ThirdPartyOtpProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly IThirdPartyOtpAppService _thirdPartyOtpService;
        private readonly TransactionProvider _transactionProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private const string _phone = "phone";
        private const string _account = "account";

        public ThirdPartyOtpProvider(EnrolmentProvider enrolmentProvider, ConfigurationProvider configProvider,
            IThirdPartyOtpAppService thirdPartyOtpService, TransactionProvider transactionProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;
            _thirdPartyOtpService = thirdPartyOtpService;
            _successResponse = DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }

        public void SendThirdPartyOtp(ThirdPartyOtpInput input, ThirdPartyOtpOutput output, ICentralProvider coreProvider)
        {
            try
            {
                // check if msisdn is linked to account
                var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);
                var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                string otpConfigs = _configProvider.GetByName<string>(DuisConstants.ConfThirdPartyOtpConfigurations);
                var theConfiguration = JsonConvert.DeserializeObject<List<ThirdPartyOtpConfiguration>>(otpConfigs).SingleOrDefault(x => x.Name.ToLower().Equals(input.purpose.ToLower()));
                var ineligibleResponse = DuisResponses.GetDuisResponse(DuisResponses._accountInvalidOrIneligible);

                if (theConfiguration == null)
                {
                    var systemError = DuisResponses.GetDuisResponse(DuisResponses._systemError);
                    throw new Exception($"OTP cannot be generated for specified purpose: {input.purpose}");
                }

                if (!theConfiguration.IsActive)
                {
                    var systemError = DuisResponses.GetDuisResponse(DuisResponses._systemError);
                    throw new Exception($"OTP disabled for purpose: {input.purpose}");
                }

                // Validate days enrolled
                if (theConfiguration.DaysActive > (int)(DateTime.Today.Subtract(theUser.EnrolmentTime.Value.Date).TotalDays))
                {
                    throw new CustomException($"{input.msisdn} has not been active for up to {theConfiguration.DaysActive} days",
                        false, ineligibleResponse.response_code, ineligibleResponse.response_message);
                }

                // validate successful transactions done
                var recentTransactions = _transactionProvider.GetRecentSuccessfulTransactions(theUser.Id, theConfiguration.TransactionsDone);
                if (recentTransactions.Count < theConfiguration.TransactionsDone)
                {
                    throw new CustomException($"{input.msisdn} has not done up to {theConfiguration.TransactionsDone} recent transactions",
                        false, ineligibleResponse.response_code, ineligibleResponse.response_message);
                }

                // generate the OTP
                var clearOtp = SecurityUtil.GetSecureRandomInteger(Convert.ToInt32($"1{new String('0', theConfiguration.Length - 1)}"), Convert.ToInt32(new String('9', theConfiguration.Length)));

                //saveIt
                var savedOtp = _thirdPartyOtpService.Create(new CreateThirdPartyOtpInput
                {
                    Otp = SecurityUtil.GenerateBCryptHash(clearOtp.ToString()).Hash,
                    OtpAccount = input.account_number,
                    UserMsisdn = input.msisdn,
                    Purpose = theConfiguration.Name,
                    UserId = theUser.Id,
                    ExpirationTime = DateTime.Now.AddSeconds(theConfiguration.ValidSeconds)
                });

                // send it via SMS
                var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                var theSms = $"Use {clearOtp} for your {theConfiguration.Name}. Expires by {savedOtp.ExpirationTime:hh:mm tt}, {savedOtp.ExpirationTime:dd MMM yyyy} WAT";
                smsSender.SendSMS(input.msisdn, DuisConstants.ApplicationName, theSms);

                DuisProvider.AssignResponse(output, DuisResponses._successful);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;
            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        public ValidateOtpOutput ValidateOtp(ValidateOtpInput input)
        {
            var output = new ValidateOtpOutput();

            try
            {
                string otpConfigs = _configProvider.GetByName<string>(DuisConstants.ConfThirdPartyOtpConfigurations);

                var theConfiguration = JsonConvert
                    .DeserializeObject<List<ThirdPartyOtpConfiguration>>(otpConfigs)
                    .SingleOrDefault(x => x.Name.ToLower().Equals(input.purpose.ToLower()));

                if (theConfiguration == null)
                {
                    throw new Exception($"OTP cannot be generated for specified purpose: {input.purpose}");
                }

                if (!theConfiguration.IsActive)
                {
                    throw new Exception($"OTP disabled for purpose: {input.purpose}");
                }

                ThirdPartyOtpDto theOtp = null;

                if (theConfiguration.LookupKeys.Count() == 1 && theConfiguration.LookupKeys[0].ToLower().Equals(_phone))
                {
                    // get latest otp by phone and purpose
                    theOtp = _thirdPartyOtpService.GetLatestOtpByPhone(input.msisdn, input.purpose);
                }

                else if (theConfiguration.LookupKeys.Count() == 1 && theConfiguration.LookupKeys[0].ToLower().Equals(_account))
                {
                    // get latest otp by account and purpose
                    theOtp = _thirdPartyOtpService.GetLatestOtpByAccount(input.account_number, input.purpose);
                }

                else
                {
                    // anything else, lookup with both account and phone plus purpose. I think this is safe
                    theOtp = _thirdPartyOtpService.GetLatestOtpByPhoneAccount(input.msisdn, input.account_number, input.purpose);
                }

                if (theOtp == null || theOtp.ExpirationTime <= DateTime.Now || theOtp.UsageTime != null)
                {
                    var passwordExpired = DuisResponses.GetDuisResponse(DuisResponses._passwordExpired);
                    throw new CustomException($@"OTP for [{input.account_number};{input.msisdn}] not found, has expired 
or has already been used. Kindly generate a new one", false, passwordExpired.response_code, "OTP does not exist or has expired");
                }

                // Now, finally! Verify the OTP. Eish, what a journey of validations it took us to get here. Life is hard sha o
                if (!SecurityUtil.VerifyBCrypt(input.otp, theOtp.Otp))
                {
                    // update the OTP record
                    // also, update the wrong attempt and times tried
                    _thirdPartyOtpService.Update(new UpdateThirdPartyOtpInput
                    {
                        Id = theOtp.Id,
                        FailedAttempts = theOtp.FailedAttempts == null ? input.otp : $"{input.otp};{theOtp.FailedAttempts}",
                        FailedTryCount = theOtp.FailedTryCount + 1,
                        LastFailedAttemptTime = DateTime.Now
                    });

                    // throw validation failed custom error
                    throw new CustomException(DuisResponses._authenticationFailed, true, null, null);
                }

                // update usage time
                _thirdPartyOtpService.Update(new UpdateThirdPartyOtpInput
                {
                    Id = theOtp.Id,
                    UsageTime = DateTime.Now,
                });

                DuisProvider.AssignResponse(output, DuisResponses._successful);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, DuisResponses._systemError);
            }

            return output;
        }
    }
}