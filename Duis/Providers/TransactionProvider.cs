﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Fincore;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Newtonsoft.Json;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using FidelityBank.CoreLibraries.Entrust;
using Duis.Utilities;

namespace Duis.Providers
{
    public class TransactionProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly ITransactionAppService _transactionAppService;
        private readonly ITransactionTypeAppService _transactionTypeAppService;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _unknownResponse = null;
        private readonly FincoreSystem _fincore = null;
        private readonly string _fidelityNipV2NumericCode = null;

        public TransactionProvider(EnrolmentProvider enrolmentProvider,
            ITransactionAppService transactionAppService, IEnrolmentAppService enrolmentAppService,
            ITransactionTypeAppService transactionTypeAppService, ConfigurationProvider configProvider)
        {
            _enrolmentProvider = enrolmentProvider;
            _enrolmentAppService = enrolmentAppService;
            _transactionAppService = transactionAppService;
            _transactionTypeAppService = transactionTypeAppService;
            _configProvider = configProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _unknownResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];
            _fidelityNipV2NumericCode = _configProvider.GetByName<string>(DuisConstants.ConfFidelityNipV2NumericCode);
            _fincore = new FincoreSystem(fincoreResponsesPath);
        }

        /// <summary>
        /// Steps:
        /// <para>1. Do the transfer</para>
        /// <para>2. Update the transaction with transfer outcome</para>
        /// <para>3. Return the updated transactions</para>
        /// </summary>
        /// <param name="existingTransaction"></param>
        /// <param name="processorReference"></param>
        /// <returns></returns>
        public TransactionDto DoTransactionDebit(TransactionDto existingTransaction, string transactionReference, string narration = null)
        {
            var ftRequestTime = DateTime.Now;

            var transactionUpdated = _transactionAppService.Update(new UpdateTransactionInput
            {
                Id = existingTransaction.Id,
                RefId = transactionReference,
                DrReqTime = ftRequestTime,
                DrNarration = narration
            });

            // use the details of the transaction to formulate a FT transaction
            var ftRequest = new FtRequest
            {
                Amount = transactionUpdated.DrAmount,
                ChargeAmount = transactionUpdated.ChargeAmount,
                FromAcct = transactionUpdated.DrSource,
                ToAcct = transactionUpdated.DrDestination,
                Narration = transactionUpdated.DrNarration,
                RetrievalRefNum = transactionUpdated.DrRefIdNumeric,
                TransactionTime = ftRequestTime,
                ToBankCode = transactionUpdated.DrDestinationCode,
            };

            // post the intrabank transfer
            var ftResponse = _fincore.DoFundsTransfer(ftRequest);
            var responseCode = ftResponse.ResponseCode;
            var responseDesc = ftResponse.ResponseDescription;

            // update the request 
            transactionUpdated = _transactionAppService.Update(new UpdateTransactionInput
            {
                Id = existingTransaction.Id,
                DrRespTime = DateTime.Now,
                DrResponseCode = responseCode,
                DrResponseDesc = responseDesc
            });

            // return updated transaction to caller
            return transactionUpdated;
        }

        public TransactionDto ReverseTransaction(TransactionDto existingTransaction, string narration = null)
        {
            var ftRequestTime = DateTime.Now;

            // use the details of the transaction to formulate a FT transaction
            var ftRequest = new FtRequest
            {
                Amount = existingTransaction.Amount,
                ChargeAmount = existingTransaction.ChargeAmount,
                FromAcct = existingTransaction.DrSource,
                ToAcct = existingTransaction.DrDestination,
                Narration = narration ?? existingTransaction.DrNarration,
                RetrievalRefNum = existingTransaction.DrRefIdNumeric,
                TransactionTime = existingTransaction.DrReqTime.Value,
                ToBankCode = existingTransaction.DrDestinationCode
            };

            // post the intrabank transfer
            var ftResponse = _fincore.ReverseFundsTransfer(ftRequest);

            if (new string[] { "00", "94" }.Contains(ftResponse.ResponseCode))
            {
                return Update(new UpdateTransactionInput
                {
                    Id = existingTransaction.Id,
                    IsReversed = true,
                    ReversalTime = DateTime.Now
                });
            }

            else
            {
                return Update(new UpdateTransactionInput
                {
                    Id = existingTransaction.Id,
                    ReversalAttempts = existingTransaction.ReversalAttempts + 1
                });
            }
        }

        public TransactionDto Update(UpdateTransactionInput updates)
        {
            var updated = _transactionAppService.Update(updates);

            return updated;
        }

        public TransactionDto Create(CreateTransactionInput newInput)
        {
            var created = _transactionAppService.Create(newInput);

            return created;
        }

        public void CheckLimit(decimal amount, EnrolmentDto enrolment, string transactionType, string token = null)
        {
            string limitExceeded = string.Empty;

            if (enrolment == null || enrolment.Id < 1)
            {
                throw new CustomException(DuisResponses._dailyLimitExceeded, true, null, null);
            }

            bool useTokenLimit = Convert.ToBoolean(ConfigurationManager.AppSettings[DuisConstants.ConfUseTokenLimit]);
            var limitCheckHolder = _transactionAppService.IsLimitExceeded(amount, enrolment, transactionType, ref limitExceeded, useTokenLimit);

            TokenStatus tokenResponse = new TokenStatus();

            if (useTokenLimit && limitCheckHolder.IsTokenLimitExceeded)
            {
                tokenResponse = ValidateToken(enrolment, token);
            }

            bool exceedsLimit = limitCheckHolder.IsLimitExceeded || (limitCheckHolder.IsTokenLimitExceeded && tokenResponse.IsInvalid);

            if (exceedsLimit)
            {
                throw new CustomException(DuisResponses._dailyLimitExceeded, true, null, null);
            }
        }

        private TokenStatus ValidateToken(EnrolmentDto enrolment, string token)
        {
            TokenStatus result = new TokenStatus();

            if (!string.IsNullOrWhiteSpace(token))
            {
                var entrustParameters = _configProvider.GetByName<string>(DuisConstants.ConfEntrustParamsJson);
                var entrustGroupName = _configProvider.GetByName<string>(DuisConstants.ConfEntrustGroupName);

                LoggingSystem.LogMessage(string.Format("Checking limit token {0}", token));

                var entrustUtil = new EntrustUtil(entrustParameters);

                var tokenResponse = entrustUtil.Authenticate(string.Format("{0}/{1}", entrustGroupName, enrolment.BankCustomerId), token);

                LoggingSystem.LogMessage(string.Format("Entrust challenge response for {0} {1} is \r\n {2}",
                    enrolment.BankCustomerId, tokenResponse, JsonConvert.SerializeObject(tokenResponse)));

                if (tokenResponse.IsTokenValid)
                {
                    _enrolmentProvider.Update(new UpdateEnrolmentInput { Id = enrolment.Id, HasToken = true });
                }

                result = new TokenStatus
                {
                    ResponseCode = tokenResponse.ResponseCode,
                    ResponseDescription = tokenResponse.ResponseDescription,
                    IsValid = tokenResponse.IsTokenValid,
                    Token = tokenResponse.SuppliedToken
                };
            }

            return result;
        }

        public LimitOutput GetLimit(decimal amount, string msisdn, string transactionType, UserType userType = UserType.InstantBanking)
        {
            LimitHolder limitHolder = null;
            string limitExceeded = string.Empty;
            var enrolment = _enrolmentAppService.GetByMsisdn(msisdn, userType);

            bool useTokenLimit = Convert.ToBoolean(ConfigurationManager.AppSettings[DuisConstants.ConfUseTokenLimit]);

            if (enrolment != null)
            {
                limitHolder = _transactionAppService.IsLimitExceeded(amount, enrolment, transactionType, ref limitExceeded, useTokenLimit);
            }

            return limitHolder == null ? null : new LimitOutput
            {
                count_left = limitHolder.CountLeft,
                is_limit_exceeded = limitHolder.IsLimitExceeded,
                is_exceeded_limit_minimum = limitHolder.IsExceededLimitMinimum,
                is_default_limit_exceeded = limitHolder.IsDefaultLimitExceeded,
                is_token_limit_exceeded = limitHolder.IsTokenLimitExceeded,
                max_amount = limitHolder.MaximumAmount,
                min_amount = limitHolder.MinimumAmount,
                max_before_token_amount = limitHolder.MaximumBeforeToken
            };
        }

        public decimal GetSurcharge(string msisdn, string transactionTypeName, UserType userType = UserType.InstantBanking)
        {
            var user = _enrolmentAppService.GetByMsisdn(msisdn, userType);

            return GetSurcharge(user, transactionTypeName);
        }

        public decimal GetSurcharge(EnrolmentDto user, string transactionTypeName)
        {
            long? profileId = user == null || user.Profile == null ? null : (long?)user.Profile.Id;
            var charge = _transactionAppService.GetTransactionCharge(transactionTypeName, profileId);

            if (profileId.HasValue && !charge.HasValue)
            {
                // profile has no charge configured for it, so use the default charge
                charge = _transactionAppService.GetTransactionCharge(transactionTypeName, null);
            }

            if (!charge.HasValue)
            {
                throw new Exception(string.Format("No charges have been configured for {0}s for user: {1}", transactionTypeName, user.Msisdn));
            }

            return charge.Value;
        }

        public decimal GetMinimumPayable(string transactionTypeName)
        {
            var transactionType = _transactionTypeAppService.GetByName(transactionTypeName);
            return transactionType.MinTransactionAmount;
        }

        public TransactionDto GetByClientRef(string clientRef)
        {
            TransactionDto retrieved = _transactionAppService.GetByClientRef(clientRef);
            return retrieved;
        }

        public TransactionDto GetByRefId(string refId)
        {
            TransactionDto retrieved = _transactionAppService.GetByRefId(refId);
            return retrieved;
        }

        internal TransactionDto TakeServiceCharge(EnrolmentDto user, string fromAccount, string solId, string clientReference, string serviceName)
        {
            string narration = string.Format("770 SERVICE CHARGE: {0}", serviceName);
            var serviceIncomeAccount = _configProvider.GetByName<string>(DuisConstants.ConfBalanceEnquiryCreditAccount).Replace("SOL", solId);

            var surcharge = GetSurcharge(user, ApplicationConstants.Service);
            var result = DoSystemInitiatedTransfer(user, fromAccount, serviceIncomeAccount, narration, surcharge, 0, ApplicationConstants.Service, clientReference, serviceName);
            return result;
        }

        internal TransactionDto DoSystemInitiatedTransfer(EnrolmentDto user, string fromAccount, string toAccount, string narration, decimal amount,
            decimal chargeAmount, string transactionType, string clientReference, string processorName = null)
        {
            // compose the transaction
            var toSave = new CreateTransactionInput
            {
                Amount = amount,
                ChargeAmount = chargeAmount,
                ClientRefId = clientReference,
                DrAmount = amount,
                DrDestination = toAccount,
                DrDestinationCode = _fidelityNipV2NumericCode,
                DrSource = fromAccount,
                Type = transactionType,
                UserId = user.Id,
                UserMsisdn = user.Msisdn,
                ProcessorName = processorName
            };

            // save the transaction
            var theTransaction = Create(toSave);
            theTransaction = DoTransactionDebit(theTransaction, null, narration);
            return theTransaction;
        }

        public void MarkForReversal(MarkForReversalInput input)
        {
            var theTransaction = Update(new UpdateTransactionInput { Id = input.transaction_id, ReverseExceptionally = input.reverse_flag });
        }

        public TransactionHistoryOutput GetHistory(long userId, int transactionCount)
        {
            try
            {
                var history = _transactionAppService.GetLatestTransactions(userId, transactionCount);

                var historyDetails = history.Select(x => new TransactionDetails
                {
                    amount = x.Amount,
                    beneficiary = x.ProcessorUserId,
                    date = x.CreationTime,
                    reference = x.RefId,
                    status = x.Status.ToString(),
                    type = GetFriendlyType(x.Type)
                }).ToList();

                var result = new TransactionHistoryOutput
                {
                    transactions = historyDetails,
                    response_code = _successResponse.response_code,
                    response_message = _successResponse.response_message
                };

                return result;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);

                var result = new TransactionHistoryOutput();

                DuisProvider.AssignResponse(result, DuisResponses._systemError);

                return result;
            }

        }

        private string GetFriendlyType(string systemType)
        {
            // there should be a mapping of system transaction type to customer friendly transaction type somewhere
            return systemType;
        }

        internal AffordableOutput GetAffordable(string accountNumber, string transactionType, EnrolmentDto theUser = null)
        {
            // get simple balance
            var simpleBalance = CentralOperations.GetAccountBalance(accountNumber);

            // get charge
            decimal charge = 0;

            try
            {
                if (theUser == null)
                {
                    var accountDetails = _enrolmentProvider.GetAccountDetails(accountNumber);
                    theUser = _enrolmentProvider.GetByCustomerId(accountDetails.CustomerId);
                }

                charge = GetSurcharge(theUser, transactionType);
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                charge = 0;
            }

            // get configured residual account balance
            decimal residualBalance = GetResidualBalance();

            // get minimum allowed spend for transaction type
            var minimumSpend = GetMinimumPayable(transactionType);

            var availableBalance = Convert.ToDecimal(simpleBalance.BalanceAvailable) - residualBalance - charge;

            var affordable = availableBalance < minimumSpend ? 0 : availableBalance;

            return new AffordableOutput
            {
                max_amount = Decimal.Floor(affordable),
                min_amount = Decimal.Floor(minimumSpend),
                account_number = accountNumber
            };
        }

        /// <summary>
        /// Generates the minimum amount which should be left in this customer's account after this transaction
        /// </summary>
        /// <returns></returns>
        private decimal GetResidualBalance()
        {
            decimal result = _configProvider.GetByName<decimal>(DuisConstants.ConfResidualBalance);
            return result;
        }

        internal IList<TransactionDto> GetRecentSuccessfulTransactions(long userId, int count)
        {
            return _transactionAppService.GetLatestSuccessfulTransactions(userId, count);
        }
    }
}