﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Exceptions;
using Duis.Models.BankAccounts;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.NewBankAccounts;
using FidelityBank.CoreLibraries.Fincore;
using FidelityBank.CoreLibraries.Infopool;
using FidelityBank.CoreLibraries.Utility.Engine;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Linq;

namespace Duis.Providers
{
    public class BankAccountProvider : ITransientDependency
    {
        private readonly ConfigurationProvider _configProvider;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly INewBankAccountAppService _newBankAccountAppService;

        public BankAccountProvider(ConfigurationProvider configProvider, EnrolmentProvider enrolmentProvider,
            INewBankAccountAppService newBankAccountAppService)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _newBankAccountAppService = newBankAccountAppService;
        }

        internal void CreateAccount(CreateAccountInput input, CreateAccountOutput output, ICentralProvider coreProvider)
        {
            try
            {
                // check that account is not enrolled on Instant banking
                if (_enrolmentProvider.GetByMsisdn(input.msisdn) != null)
                {
                    ThrowAlreadyExists(input);
                }

                // check that customer has not previously created a bank account via this channel
                var previousAttempts = _newBankAccountAppService.GetByPhone(input.msisdn);

                if (previousAttempts.FirstOrDefault(x => x.CreationStatus == CreationStatus.Successful) != null)
                {
                    ThrowAlreadyExists(input);
                }

                // check Finacle if any account exists with phone number
                var coligoParameters = _configProvider.GetByName<string>(DuisConstants.ConfColigoParameters);
                var coligoEngine = new FidelityBank.CoreLibraries.Coligo.ColigoEngine(coligoParameters);

                if (coligoEngine.GetPhoneAccounts(StringManipulation.StandardizeNigerianMsisdn(input.msisdn)).Count != 0 ||
                    coligoEngine.SearchCustomer(input.first_name, input.last_name, Convert.ToDateTime(input.birthday)) != null)
                {
                    ThrowAlreadyExists(input);
                }                

                var fiEngine = new FidelityBank.CoreLibraries.FinacleIntegrator.Engine();

                var record = _newBankAccountAppService.Create(new Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos.CreateNewBankAccountInput
                {
                    FirstName = input.first_name,
                    LastName = input.last_name,
                    Birthday = Convert.ToDateTime(input.birthday),
                    BranchCode = input.branch_code ?? _configProvider.GetByName<string>(DuisConstants.ConfDefaultBranchCode),
                    Bvn = input.bvn,
                    CreationStatus = CreationStatus.Pending,
                    PhoneNumber = input.msisdn,
                    ReferrerCode = input.referrer_code,
                    SchemeCode = _configProvider.GetByName<string>(DuisConstants.ConfNewBankAccountSchemeCode),
                    Email = input.email
                });

                var acctCreateResponse = fiEngine.CreateSavingsAccount(
                    new FidelityBank.CoreLibraries.FinacleIntegrator.Models.SimpleAccount
                    {
                        FirstName = record.FirstName,
                        LastName = record.LastName,
                        PhoneNumber = StringManipulation.StandardizeNigerianMsisdn(record.PhoneNumber),
                        DateOfBirth = record.Birthday,
                        Bvn = record.Bvn,
                        SchemeCode = record.SchemeCode,
                        SolId = record.BranchCode,
                        Email = record.Email,
                        AccountOfficerCode = _configProvider.GetByName<string>(DuisConstants.ConfDefaultAccountOfficerCode)
                    }, _configProvider.GetByName<string>(DuisConstants.ConfFinacleIntegratorAppName), input.reference_number);

                record = _newBankAccountAppService.Update(new Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos.UpdateNewBankAccountInput
                {
                    Id = record.Id,
                    AccountNumber = acctCreateResponse.SBAcctAddRs.SBAcctId.AcctId,
                    CbaResponse = JsonConvert.SerializeObject(acctCreateResponse),
                    CreationStatus = CreationStatus.Successful
                });

                output.account_number = record.AccountNumber;
                DuisProvider.AssignResponse(output, DuisResponses._successful);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            //System.Threading.Thread.Sleep(2000);

            //if (coreProvider.ReturnedTimeoutResponse)
            //{
            SendCompletionSms(input, output);
            //}
        }

        private void ThrowAlreadyExists(CreateAccountInput input)
        {
            var response = DuisResponses.GetDuisResponse(DuisResponses._alreadyEnrolled);
            throw new CustomException
                ($"New account detail belongs to existing customer: {input.first_name} {input.last_name};{input.birthday:dd MMM yyyy};{input.msisdn}",
                false, response.response_code, "You already have an account with the bank");
        }

        private void SendCompletionSms(CreateAccountInput input, CreateAccountOutput output)
        {
            //TODO: Implement correct account opening SMS
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            if (isSuccessful)
            {
                var subject = DuisConstants.ApplicationName;
                var sms = $"Your new account number is {output.account_number}. Please proceed to the branch to complete activation.";
                var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                smsSender.SendSMS(input.msisdn, subject, sms);
            }

            else
            {
                LoggingSystem.LogMessage($"DONOTSMS: Account Creation Failed for {input.account_number} {input.first_name} {input.last_name}");
            }
        }
    }
}