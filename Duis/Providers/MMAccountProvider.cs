﻿using Abp.Dependency;
using Duis.Models;
using Duis.Dto.MobileMoney;
using Fbp.Net.Integrations;
using System;
using FidelityBank.CoreLibraries.FinacleIntegrator;
using FidelityBank.CoreLibraries.FinacleIntegrator.Models;
using FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using Duis.Constants;
using Newtonsoft.Json;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Duis.Utilities;

namespace Duis.Providers
{
    public class MMAccountProvider : ITransientDependency
    {
        private readonly IEnrolmentAppService _userAppService;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly ConfigurationProvider _configProvider;

        #region MyRegion
        private readonly string _finacleIntegratorSuccessResponse = "SUCCESS";
        private readonly string _fincoreSuccessCode = null;
        #endregion
        public MMAccountProvider(IEnrolmentAppService userAppService, EnrolmentProvider enrolmentProvider, ConfigurationProvider configProvider)
        {
            _userAppService = userAppService;
            _enrolmentProvider = enrolmentProvider;
            _configProvider = configProvider;
            _fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
        }

        public MMUser GetUser(string msisdn)
        {
            MMUser result;

            var theUser = _userAppService.GetByMsisdn(msisdn, UserType.MobileMoney);

            if (theUser == null)
            {
                result = new MMUser
                {

                };

                DuisProvider.AssignResponse(result, DuisResponses._notEnrolled);
            }

            else
            {
                result = new MMUser
                {
                    customer_id = theUser.BankCustomerId,
                    is_active = theUser.IsActive,
                    is_valid = true,
                    msisdn = theUser.Msisdn,
                    name = theUser.FullName
                };

                DuisProvider.AssignResponse(result, DuisResponses._successful);
            }

            return result;
        }

        internal UserCreateResponse CreateUser(UserCreateRequest input)
        {
            // validate the information
            // validate all name fields
            // confirm is new profile
            //go ahead to create by saving to database and creating on Finacle
            ICollection<ValidationResult> validationResults = new List<ValidationResult>();
            bool isValid = ValidationUtil.TryValidateAnnotations(input, out validationResults);
            var response = new UserCreateResponse();

            try
            {
                if (!isValid)
                {
                    string errorMessage = validationResults.ElementAt(0).ErrorMessage;

                    var invalidInputResponse = DuisResponses.GetDuisResponse(DuisResponses._inputInvalid);

                    throw new CustomException(DuisResponses._inputInvalid, false, invalidInputResponse.response_code, errorMessage);
                }

                var password = EncryptPassword(input.pin);
                var payload = new CreateEnrolmentInput
                {
                    FirstName = input.first_name,
                    LastName = input.last_name,
                    Msisdn = input.msisdn,
                    AccountNumber = input.msisdn.LastCharacters(10),
                    Network = input.network,
                    Password = password,
                    OldPasswords = password,
                    BankCustomerId = input.msisdn,
                    UserType = UserType.MobileMoney,
                    Status = EnrolStatus.Activated
                };

                EnrolmentDto existingUser = _userAppService.GetByMsisdn(input.msisdn, UserType.MobileMoney);

                if (existingUser != null)
                {
                    throw new CustomException(DuisResponses._alreadyEnrolled, true, null, null);
                }

                // create on database
                var theUser = _userAppService.Create(payload);

                string schemeCode = string.Empty;
                string solId = string.Empty;
                string glSubheadCode = string.Empty;

                // create customer, then account on Finacle
                var finacleIntegrator = new Engine();
                var createCustomerResponse = finacleIntegrator.CreateCustomer(new RetCustAddRq
                {
                    CustDtls = new CustDtls
                    {
                        CustData = new CustData
                        {
                            FirstName = input.first_name,
                            LastName = input.last_name,
                            PhoneEmailDtls = new PhoneEmailDtls
                            {
                                PhoneNum = input.msisdn
                            },
                            PrimarySolId = solId,
                            RelationshipOpeningDt = DateTime.Today,
                            CreatedBySystemId = "DUIS",
                            TaxDeductionTable = "DOM",
                            BirthDt = input.Birthday.Value.Day,
                            BirthMonth = input.Birthday.Value.Month,
                            BirthYear = input.Birthday.Value.Year,
                        }
                    }

                });

                LoggingSystem.LogMessage(string.Format("Mobile money account creation response: {0}", JsonConvert.SerializeObject(createCustomerResponse)));

                if (createCustomerResponse.RetCustAddRs.Status != _finacleIntegratorSuccessResponse)
                {
                    throw new Exception("Error creating customer on Finacle but registration was successful");
                }


                var createAccountResponse = finacleIntegrator.CreateSavingsAccount(input.msisdn, createCustomerResponse.RetCustAddRs.CustId, schemeCode, FiCurrency.NGN, solId,
                     glSubheadCode, input.first_name, input.last_name, null);

                try
                {
                    var accountId = createAccountResponse.SBAcctAddRs.SBAcctId.AcctId;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, string.Format("Null reference exception testing create savings account response {0}", input.msisdn));
                    throw new Exception("Error creating savings account on Finacle after successful registration and creation of customer on Finacle");
                }

                DuisProvider.AssignResponse(response, Constants.DuisResponses._successful);

                _userAppService.Update(new UpdateEnrolmentInput
                {
                    FinacleCreationTime = DateTime.Now,
                    Id = existingUser.Id
                });
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
            }

            return response;
        }

        internal string EncryptPassword(string password)
        {
            // write proper encryption algorithm
            var encryptedPassword = SecurityUtil.GenerateBCryptHash(password);
            return encryptedPassword.Hash;
        }

        internal PasswordValidationOutput IsPasswordValid(PasswordValidationInput input)
        {
            var result = false;
            string reason = string.Empty;

            try
            {
                if(!string.IsNullOrWhiteSpace(input.account))
                {
                    return _enrolmentProvider.IsPasswordValid(input.password, input.account, UserType.MobileMoney);
                }

                _enrolmentProvider.CheckPasswordStrength(input.password, string.Empty, input.dob_DdMmYyyy.LastCharacters(4));
                result = true;
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex, string.Format(ex.Message, input.password));
                reason = ex.Message;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format(ex.Message, input.password));
                reason = "An error occurred";
            }

            return new PasswordValidationOutput
            {
                is_valid = result,
                reason = reason
            };
        }

        internal void GetBalance(AccountEnquiryInput input, AccountEnquiryOutput output, ICentralProvider coreProvider)
        {
            try
            {
                var enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, true, UserType.MobileMoney);

                // check if msisdn is linked to account
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);

                // get account balance from Fincore
                FidelityBank.CoreLibraries.Fincore.AccountBalance accountBalance = CentralOperations.GetAccountBalance(input.account_number);

                if (accountBalance == null || accountBalance.ResponseCode != _fincoreSuccessCode)
                {
                    LoggingSystem.LogMessage(string.Format("Fincore account balance response: {0}", JsonConvert.SerializeObject(accountBalance)));
                    DuisProvider.AssignResponse(output, DuisResponses._systemError);
                }

                else
                {
                    output.account_details = new Duis.Models.Accounts.Account { account_balance = accountBalance.BalanceAvailable };
                    DuisProvider.AssignResponse(output, DuisResponses._successful);
                }
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                var acctBalXml = ex.Data["AccountBalanceXml"];
                LoggingSystem.LogMessage(Convert.ToString(acctBalXml));
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

                if (isSuccessful)
                {
                    var sms = string.Format("Your mobile money account balance on {0} is {1}", input.msisdn, output.amount);
                    CentralOperations.SendNotificationSms(sms, input.msisdn);
                }

                else
                {
                    LoggingSystem.LogMessage(string.Format("Failed to get balance for mobile account: {0}", input.msisdn));
                }
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        public EnrolmentOutput ChangePassword(EnrolmentInput input)
        {
            EnrolmentOutput response = new EnrolmentOutput() { msisdn = input.msisdn };

            DuisProvider.AssignResponse(response, DuisResponses._successful);

            string msisdn = input.msisdn;
            string accountNumber = input.account_number;
            string newPassword = input.new_password;

            try
            {
                EnrolmentDto enrolment = _enrolmentProvider.AuthenticateUser(msisdn, input.password, true, UserType.MobileMoney);
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);

                if (enrolment.AccountNumber != accountNumber)
                {
                    throw new CustomException(DuisResponses._accountMsisdnMismatch, true, null, null);
                }

                _enrolmentProvider.UpdatePassword(enrolment, newPassword);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                response.response_code = ex.ErrorCode;
                response.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(response, Constants.DuisResponses._systemError);
            }

            return response;
        }
    }
}