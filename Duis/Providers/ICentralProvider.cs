﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Duis.Providers
{
    public interface ICentralProvider
    {
        #region Background process management variables
        bool IsTimeoutElapsed { set; get; }
        bool IsBackgroundJobCompleted { set; get; }
        bool ReturnedTimeoutResponse { set; get; }
        #endregion
    }
}
