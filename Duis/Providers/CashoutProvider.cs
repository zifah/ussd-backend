﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Exceptions;
using Duis.Models;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using System;
using System.Configuration;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using FidelityBank.CoreLibraries.Interfaces.Cashout;
using FidelityBank.CoreLibraries.Interfaces;
using System.Linq;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using System.Web.Helpers;
using Newtonsoft.Json;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System.Collections.Generic;

namespace Duis.Providers
{
    public class CashoutProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;

        public CashoutProvider(EnrolmentProvider enrolmentProvider,
            TransactionProvider transactionProvider,
            BillPaymentProvider billingProvider,
            IAirtimeRechargeBillerAppService airtimeBillerAppService,
            IPhoneNetworkLogAppService phoneNetworkLogAppService,
            ConfigurationProvider configProvider, IEnrolmentAppService enrolmentAppService)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _enrolmentAppService = enrolmentAppService;
            _transactionProvider = transactionProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
        }

        public void DoCashout(CashoutInput input, CashoutOutput output, ICentralProvider coreProvider)
        {
            DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    string transactionType = ApplicationConstants.CardlessCashout;
                    var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);
                    var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);
                    _transactionProvider.CheckLimit(input.amount, theUser, transactionType, input.token);
                    DoCashoutTransaction(input, output, theUser, transactionType);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    output.response_code = ex.ErrorCode;
                    output.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse || output.response_code == _successResponse.response_code)
            {
                SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        internal List<string> GetChannels()
        {
            var commaSeparatedChannels = _configProvider.GetByName<string>(DuisConstants.ConfCommaSeparatedCashoutChannels);
            return commaSeparatedChannels.Trim().Split(',').ToList();
        }

        public void DoCashoutTransaction(CashoutInput input, CashoutOutput output, EnrolmentDto theUser, string transactionType)
        {
            TransactionDto savedTransaction = SaveCashoutTransaction(input, theUser, transactionType);

            // generate transaction reference
            string cashoutRef = GetCashoutRef(savedTransaction.DrRefIdNumeric, savedTransaction.CreationTime);

            savedTransaction = _transactionProvider.Update(new UpdateTransactionInput
            {
                Id = savedTransaction.Id,
                RefId = cashoutRef
            });

            string cashoutProviderFqnAssembly = _configProvider.GetByName<string>(DuisConstants.ConfCashoutProviderFqnAndAssembly);
            string cashoutParameters = _configProvider.GetByName<string>(DuisConstants.ConfCashoutParameters);
            var splitProcessorString = cashoutProviderFqnAssembly.Split(',');
            var cashoutProcessor = ProcessorProvider.GetCashoutProcessor(splitProcessorString[0], splitProcessorString[1]);

            var cashoutInput = new CashoutIn
            {
                Amount = savedTransaction.Amount,
                OneTimePin = input.transaction_pin,
                ProviderParameters = Json.Decode(cashoutParameters),
                CustomerEmail = savedTransaction.UserEmail,
                CustomerName = savedTransaction.UserFullName,
                CustomerPhone = input.msisdn,
                ReferenceNumber = cashoutRef,
                AccountNumber = input.account_number,
                WithdrawalChannel = input.withdrawal_channel
            };

            var procReqTime = DateTime.Now;
            // trigger transaction service on billing provider
            var cashoutResponse = cashoutProcessor.DoCashout(cashoutInput);

            // process status and return response
            var updater = new UpdateTransactionInput
            {
                Id = savedTransaction.Id,
                ProcessorRespCode = cashoutResponse.ResponseCode,
                ProcessorRespDesc = cashoutResponse.ResponseDescription,
                ProcessorResponseBody = cashoutResponse.ProviderRawResponse,
                Success = cashoutResponse.IsSuccessful,
                ProcessorRefId = cashoutResponse.ProviderReference,
                ProcessorReqTime = procReqTime,
                ProcessorRespTime = DateTime.Now,
            };

            switch (cashoutResponse.Status)
            {
                case CashoutStatus.Successful:
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._successful);
                    output.paycode = cashoutResponse.Paycode;
                    output.expiration_date = cashoutResponse.ExpirationDate;
                    break;

                case CashoutStatus.Failed:
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._chargeCustomerFailed);
                    output.response_message = cashoutResponse.DisplayMessage;
                    break;

                case CashoutStatus.Unknown:
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._statusUnknown);
                    break;
            }

            updater.Status = output.response_message;
            updater.StatusCode = output.response_code;

            savedTransaction = _transactionProvider.Update(updater);
            output.payment_reference = savedTransaction.ProcessorRefId;
        }

        private string GetCashoutRef(int rrn, DateTime transactionDate)
        {
            string billingRef = string.Format("1016{0}{1}", transactionDate.ToString("yyMMdd"), rrn.ToString().PadLeft(10, '0'));
            return billingRef;
        }

        private TransactionDto SaveCashoutTransaction(CashoutInput input, Fbp.Net.Integrations.DuisApp.Enrolments.Dtos.EnrolmentDto theUser, string transactionType)
        {
            var infopoolAccount = _enrolmentProvider.GetAccountDetails(input.account_number);

            decimal surcharge = _transactionProvider.GetSurcharge(theUser, transactionType);

            var toSave = new CreateTransactionInput
            {
                Amount = input.amount,
                ChargeAmount = surcharge,
                ClientRefId = input.reference_number,
                DrAmount = input.amount,
                DrSource = input.account_number,
                Type = ApplicationConstants.CardlessCashout,
                UserMsisdn = input.msisdn,
                UserId = theUser.Id,
                UserEmail = infopoolAccount.AlertEmail,
                UserFullName = infopoolAccount.AccountName,
                IsViaDirectCode = input.is_via_direct_code
            };

            return _transactionProvider.Create(toSave);
        }

        private string GetNarration(string msisdn)
        {
            var result = string.Format("770 Cardless cashout from {0}", msisdn);
            return result;
        }

        internal void SendCompletionSms(CashoutInput input, CashoutOutput output)
        {
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            string paycodeUsageMessage = _configProvider.GetByName<string>(input.withdrawal_channel == "POS" ? 
                DuisConstants.ConfCashoutWithdrawalAdvicePOS : DuisConstants.ConfCashoutWithdrawalAdviceATM);

            var sms = isSuccessful ?
                string.Format("Your PAYCODE of N{0:N2} is {1}. It will expire at {2:hh:mm:ss tt} on {2:dd MMM yyyy}. {3}", input.amount, output.paycode, output.expiration_date, paycodeUsageMessage) :
                string.Format(string.Format("Sorry, your {0} transaction was not successful. Please retry shortly", ApplicationConstants.CardlessCashout));

            var subject = DuisConstants.ApplicationName;

            var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
            smsSender.SendSMS(input.msisdn, subject, sms);
        }
    }
}