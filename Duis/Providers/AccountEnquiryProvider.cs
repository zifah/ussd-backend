﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Fincore;
using System.Web.Hosting;
using System.Timers;
using Newtonsoft.Json;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using FidelityBank.CoreLibraries.Infopool.Objects;
using Duis.Utilities;
using Duis.Interfaces;

namespace Duis.Providers
{
    public class AccountEnquiryProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly FundsTransferProvider _ftProvider;
        private readonly ConfigurationProvider _configProvider;
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly InfopoolSystem _infopoolSystem = null;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _unknownResponse = null;
        public static readonly string _fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];

        //configs
        private readonly string _fincoreSuccessCode = null;
        private readonly string _fincoreDuplicateCode = null;
        private readonly string _fidelityNipV2NumericCode = null;
        private readonly string _ownBankAlphaCode = null;

        public AccountEnquiryProvider(EnrolmentProvider enrolmentProvider, FundsTransferProvider ftProvider,
            IEnrolmentAppService enrolmentAppService, TransactionProvider transactionProvider, ConfigurationProvider configProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _enrolmentAppService = enrolmentAppService;
            _ftProvider = ftProvider;
            _transactionProvider = transactionProvider;
            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _unknownResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
            _infopoolSystem = new InfopoolSystem(_infopoolReader);

            _fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
            _fincoreDuplicateCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreDuplicateCode);
            _fidelityNipV2NumericCode = _configProvider.GetByName<string>(DuisConstants.ConfFidelityNipV2NumericCode);
            _ownBankAlphaCode = _configProvider.GetByName<string>(DuisConstants.ConfOwnBankAlphaCode);
        }
                
        /// <summary>
        /// Returns the accounts and BVN linked to the user specified by input.msisdn (Phone number)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="coreProvider"></param>
        public void GetAccountService(AccountEnquiryInput input, MsisdnAccounts output, ICentralProvider coreProvider)
        {
            // authenticate request
            EnrolmentDto enrolment = null;
            InfopoolAccount accountDetails = null;

            try
            {
                enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);
                // retrieve account details
                var phoneAccounts = _infopoolSystem.GetCustomerAccounts(enrolment.BankCustomerId);

                output.bvn = phoneAccounts.Select(x => x.BVN).FirstOrDefault(x => !string.IsNullOrWhiteSpace(x));
                output.accounts = phoneAccounts.Select(x => new Models.Accounts.Account
                {
                    account_number = x.AccountNumber,
                    account_type = x.Type
                }).ToList();

                DuisProvider.AssignResponse(output, Constants.DuisResponses._successful);
                // return response and run debit task in background
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            //TODO: Take service charge
            if (output.response_code == _successResponse.response_code)
            {
                try
                {
                    var transaction = _transactionProvider
                        .TakeServiceCharge(enrolment, input.account_number, accountDetails.SolId, input.reference_number, ApplicationConstants.AccountEnquiry);

                    _transactionProvider.Update(new UpdateTransactionInput
                    {
                        Id = transaction.Id,
                        Success = transaction.DrResponseCode.Equals(_fincoreSuccessCode),
                        Status = _successResponse.response_message,
                        StatusCode = _successResponse.response_code
                    });
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                }

            }

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendGetAccountSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        public void SendGetAccountSms(AccountEnquiryInput input, MsisdnAccounts output)
        {
            var isSuccessful = output.response_code == _successResponse.response_code;

            if (isSuccessful)
            {
                var subject = DuisConstants.ApplicationName; //string.Format("{0}|{1}", DuisProvider._transactionSmsSubject, ApplicationConstants.BalanceEnquiry);


                var sms = string.Format(@"BVN: {0}

                    Accounts:", output.bvn);

                for (int i = 0; i < output.accounts.Count; i++)
                {
                    var theAccount = output.accounts[i];
                    sms = string.Format(@"
                        {0} ({1})", theAccount.account_number, theAccount.account_type);
                }

                var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                smsSender.SendSMS(input.msisdn, subject, sms);
            }

            else
            {
                LoggingSystem.LogMessage(string.Format("DONOTSMS: Sorry, we could not get your balance. Please, try again"));
            }
        }

        public void GetAccountBalance(AccountEnquiryInput input, AccountEnquiryOutput output, ICentralProvider coreProvider)
        {
            LoggingSystem.LogMessage("Started background check account balance");

            try
            {
                var enrolment = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);

                // check if msisdn is linked to account
                var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);

                // get account balance from Fincore
                var accountBalance = CentralOperations.GetAccountBalance(input.account_number);

                if (accountBalance == null || accountBalance.ResponseCode != _fincoreSuccessCode)
                {
                    DuisProvider.AssignResponse(output, DuisResponses._systemError);
                }

                else
                {
                    var narration = GetBalanceEnquiryNarration(input);
                    var enquiryFee = _transactionProvider.GetSurcharge(input.msisdn, ApplicationConstants.BalanceEnquiry);

                    var suspenseAccount = _configProvider.GetByName<string>(DuisConstants.ConfBalanceEnquiryCreditAccount).Replace("SOL", accountDetails.SolId);

                    var theTransaction = SaveEnquiryTransaction(input, enrolment, ApplicationConstants.BalanceEnquiry, narration, enquiryFee, suspenseAccount);

                    if (enquiryFee > 0)
                    {
                        theTransaction = _transactionProvider.DoTransactionDebit(theTransaction, null);
                    }

                    var wasDebited = new string[] { _fincoreSuccessCode, _fincoreDuplicateCode }.Contains(theTransaction.DrResponseCode);

                    output.account_details = new Models.Accounts.Account
                    {
                        account_number = input.account_number,
                        account_balance = StringManipulation.ToMoney(accountBalance.BalanceAvailable),
                        cleared_balance = StringManipulation.ToMoney(accountBalance.BalanceCleared),
                        uncleared_balance = StringManipulation.ToMoney(accountBalance.BalanceUncleared),
                        lien = StringManipulation.ToMoney(accountBalance.AmountLien),
                        minimum_balance = StringManipulation.ToMoney(accountBalance.BalanceMinimum),
                        account_name = StringManipulation.ToMoney(accountBalance.AccountName)
                    };

                    DuisProvider.AssignResponse(output, DuisResponses._successful);

                    _transactionProvider.Update(new UpdateTransactionInput
                    {
                        Id = theTransaction.Id,
                        Status = output.response_message,
                        StatusCode = output.response_code,
                        Success = wasDebited
                    });
                }
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                var acctBalXml = ex.Data["AccountBalanceXml"];
                LoggingSystem.LogMessage(Convert.ToString(acctBalXml));
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        private void SendCompletionSms(AccountEnquiryInput input, AccountEnquiryOutput output)
        {
            var account = new Duis.Models.Accounts.Account() { account_number = input.account_number.Trim() };

            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            if (isSuccessful)
            {
                var subject = DuisConstants.ApplicationName; //string.Format("{0}|{1}", DuisProvider._transactionSmsSubject, ApplicationConstants.BalanceEnquiry);

                var balances = output.account_details;

                var sms = string.Format(@"Account balance for {0}:

{1}

{2}

{3}

{4}

{5}",
                 account.masked_account_number,
                 balances.account_balance == null ? string.Empty : string.Format("Available: {0}  ", balances.account_balance),
                 balances.cleared_balance == null ? string.Empty : string.Format("Cleared: {0}  ", balances.cleared_balance),
                 balances.uncleared_balance == null ? string.Empty : string.Format("Uncleared: {0}  ", balances.uncleared_balance),
                 balances.lien == null ? string.Empty : string.Format("Lien: {0}  ", balances.lien),
                 balances.minimum_balance == null ? string.Empty : string.Format("Minimum: {0}  ", balances.minimum_balance));

                var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                smsSender.SendSMS(input.msisdn, subject, sms);
            }

            else
            {
                LoggingSystem.LogMessage(string.Format("DONOTSMS: Sorry, we could not get your balance. Please, try again"));
            }
        }

        public GetAccountBanksOutput GetAccountBanks(GetAccountBanksInput input)
        {
            var result = new GetAccountBanksOutput()
            {
                account_number = input.account_number
            };

            if (input != null)
            {
                // get all banks
                DuisProvider.AssignResponse(result, DuisResponses._successful);


                var intrabankCharge = _transactionProvider.GetSurcharge(input.sender_msisdn, ApplicationConstants.IntrabankTransfer);
                var interbankCharge = _transactionProvider.GetSurcharge(input.sender_msisdn, ApplicationConstants.InterbankTransfer);

                var allBanks = CentralOperations.GetCommercialBanks(input.sender_msisdn, intrabankCharge, interbankCharge, _fidelityNipV2NumericCode);

                foreach (var bank in allBanks)
                {
                    bool isNuban = NubanVerifier.IsNubanAccount(bank.code_v1, input.account_number);

                    if (isNuban)
                    {
                        result.banks.Add(bank);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets details of specified bank, which will include surcharge to the customer for the specified transaction type
        /// </summary>
        /// <param name="user"></param>
        /// <param name="bankAlphaCode"></param>
        /// <param name="transactionType"></param>
        /// <returns></returns>
        internal Bank GetBank(EnrolmentDto user, string bankAlphaCode, string transactionType)
        {
            Bank bank = null;

            // get the bank details from Infopool using alpha code
            bank = GetBank(bankAlphaCode);

            // get the charge for selected transaction type
            bank.transfer_charge = _transactionProvider.GetSurcharge(user, transactionType);

            return bank;
        }

        private Bank GetBank(string alphaCode)
        {
            var theBank = _infopoolSystem.GetNipBankByAlphaCode(alphaCode);
            return new Bank
            {
                code_v2 = theBank.NumericCodeV2,
                code_v1 = theBank.NumericCodeV1,
                name = theBank.BankName,
                name_short = theBank.AlphabeticCodeV2
            };
        }

        public string GetAccountName(string accountNumber, string bankNipV2Code)
        {
            string nipV2CodeNormalized = bankNipV2Code == _fidelityNipV2NumericCode ? null : bankNipV2Code;

            string accountName = _infopoolSystem.GetAccountName(accountNumber, nipV2CodeNormalized, _fincoreResponsesPath);           

            accountName = StringManipulation.ToTitleCase(accountName);
            return accountName;
        }

        private string GetBalanceEnquiryNarration(AccountEnquiryInput input)
        {
            var narration = string.Empty;

            narration = string.Format("770 BALANCE INQUIRY ON **{0} VIA {1}", input.account_number.Substring(6, 4), input.msisdn.Trim());

            return narration;
        }

        private TransactionDto SaveEnquiryTransaction(AccountEnquiryInput input,
            EnrolmentDto theUser, string transactionType, string narration, decimal fee, string suspenseAccount)
        {
            var toSave = new CreateTransactionInput
            {
                Amount = fee,
                ClientRefId = input.reference_number,
                DrAmount = fee,
                DrDestination = suspenseAccount,
                DrDestinationCode = _fidelityNipV2NumericCode,
                DrNarration = narration,
                DrSource = theUser.AccountNumber,   //uses the user's default account number for debit, irrespective of the enquiry account
                Type = transactionType,
                UserMsisdn = input.msisdn,
                UserId = theUser.Id,
                ProcessorUserId = input.account_number
            };

            return _transactionProvider.Create(toSave);
        }

        public AccountEnquiryOutput GetBalanceEnquiryCharge(string phone)
        {
            var result = new AccountEnquiryOutput
            {
                amount = _transactionProvider.GetSurcharge(phone, ApplicationConstants.BalanceEnquiry)
            };

            return result;
        }

        public string GetOnlineBankingUsername(string customerId)
        {
            return _infopoolSystem.GetOnlineBankingUsername(customerId);
        }

        internal GetAccountOfficerOutput GetAccountOfficer(GetAccountOfficerInput input)
        {
            var output = new GetAccountOfficerOutput { };

            try
            {
                // Do not authenticate
                // check if msisdn is linked to account
                var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);

                // get account balance from Fincore
                var accountOfficer = _infopoolSystem.GetAccountOfficer(input.account_number);

                output.account_officer_email = accountOfficer.Email;
                output.account_officer_name = accountOfficer.Name;
                output.account_officer_phone = accountOfficer.Phone;

                DuisProvider.AssignResponse(output, DuisResponses._successful);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            return output;
        }
    }
}