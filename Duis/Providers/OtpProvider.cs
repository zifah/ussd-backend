﻿using Abp.Dependency;
using Duis.Constants;
using Duis.Exceptions;
using Duis.Models;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.Adverts;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using FidelityBank.CoreLibraries.Infopool;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
namespace Duis.Providers
{
    public class OtpProvider : ITransientDependency
    {
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly UserTokenProvider _tokenProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly AccountEnquiryProvider _accountEnquiryProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;

        public OtpProvider(EnrolmentProvider enrolmentProvider,
            ConfigurationProvider configProvider, TransactionProvider transactionProvider,
            AccountEnquiryProvider accountEnquiryProvider, UserTokenProvider tokenProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _tokenProvider = tokenProvider;
            _transactionProvider = transactionProvider;
            _accountEnquiryProvider = accountEnquiryProvider;
            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }

        public OtpOutput GetUsername(OtpInput input)
        {
            var response = new OtpOutput();

            try
            {
                var user = _enrolmentProvider.GetByMsisdn(input.msisdn);
                var username = _accountEnquiryProvider.GetOnlineBankingUsername(user.BankCustomerId);

                if (string.IsNullOrWhiteSpace(username))
                {
                    DuisProvider.AssignResponse(response, DuisResponses._notEnrolled);
                    response.response_message = "Not registered for OTP";
                }

                else
                {
                    DuisProvider.AssignResponse(response, DuisResponses._successful);
                    response.username = username;
                }
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Error getting online banking username for {0}", input == null ? null : input.msisdn));
                DuisProvider.AssignResponse(response, DuisResponses._systemError);
            }

            return response;
        }

        public OtpOutput GetCharge(OtpInput input)
        {
            var response = new OtpOutput();

            try
            {
                var result = _transactionProvider.GetSurcharge(input.msisdn, ApplicationConstants.OtpGeneration);
                DuisProvider.AssignResponse(response, DuisResponses._successful);
                response.convenience_fee = result;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Error getting {0} surcharge for {1}",
                    ApplicationConstants.OtpGeneration,
                    input == null ? null : input.msisdn));
                DuisProvider.AssignResponse(response, DuisResponses._systemError);
            }

            return response;
        }

        public void GetOtp(OtpInput input, OtpOutput output, ICentralProvider coreProvider)
        {
            LoggingSystem.LogMessage("Started background check account balance");

            try
            {
                var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password);
                var username = _accountEnquiryProvider.GetOnlineBankingUsername(theUser.BankCustomerId);
                // get account balance from Fincore

                long otpLifetimeMinutes = _configProvider.GetByName<long>(DuisConstants.ConfOtpLifetimeMinutes);

                var start = DateTime.Now;
                var otpOutput = _tokenProvider.GenerateOtp(username, otpLifetimeMinutes);

                output.otp = otpOutput.otp;
                output.expiration_time = otpOutput.expiration_time;
                output.response_code = otpOutput.response_code;
                output.response_message = otpOutput.response_message;

                var end = DateTime.Now;

                if (output.response_code == _successResponse.response_code)
                {
                    // Do account to income account transfer
                    var charge = _transactionProvider.GetSurcharge(theUser, ApplicationConstants.OtpGeneration);
                    var otpIncomeAccount = _configProvider.GetByName<string>(DuisConstants.ConfOtpIncomeAccount);
                    var fincoreSuccessCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreSuccessCode);
                    var fincoreDuplicateCode = _configProvider.GetByName<string>(DuisConstants.ConfFincoreDuplicateCode);

                    string narration = GetNarration(input);

                    var accountDetails = _enrolmentProvider.GetAccountDetails(theUser.AccountNumber);

                    var chargeTransaction = _transactionProvider.DoSystemInitiatedTransfer
                        (theUser, theUser.AccountNumber, otpIncomeAccount.Replace("SOL", accountDetails.SolId),
                        narration, charge, 0, ApplicationConstants.OtpGeneration,
                        input.reference_number);

                    string[] successResponseCodes = new string[] { fincoreSuccessCode, fincoreDuplicateCode };

                    // Do cost to stock transfer
                    if (!successResponseCodes.Contains(chargeTransaction.DrResponseCode))
                    {
                        // delete generated OTP and expiration time
                        output.otp = null;
                        output.expiration_time = null;
                        DuisProvider.AssignResponse(output, DuisResponses._chargeCustomerFailed);
                        output.response_message = string.Format("Unable to take token charge due to {0}", chargeTransaction.DrResponseDesc);
                    }

                    _transactionProvider.Update(new Fbp.Net.Integrations.DuisApp.Transactions.Dtos.UpdateTransactionInput
                    {
                        Id = chargeTransaction.Id,
                        IsViaDirectCode = input.is_via_direct_code,
                        ProcessorReqTime = start,
                        ProcessorRespTime = end,
                        Status = output.response_code,
                        StatusCode = output.response_message
                    });
                }
            }

            catch(CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse || output.response_code == _successResponse.response_code)
            {
                SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        private string GetNarration(OtpInput input)
        {
            return "770 OTP GENERATION CHARGE";
        }

        private void SendCompletionSms(OtpInput input, OtpOutput output)
        {
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            if (isSuccessful)
            {
                var subject = DuisConstants.ApplicationName; //string.Format("{0}|{1}", DuisProvider._transactionSmsSubject, ApplicationConstants.BalanceEnquiry);

                var sms = string.Format("Your requested OTP is {0}. It will expire at {1:HH:mm} on {1:dd MMM yyyy}", output.otp, output.expiration_time);

                var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                smsSender.SendSMS(input.msisdn, subject, sms);
            }

            else
            {
                LoggingSystem.LogMessage(string.Format("DONOTSMS: Sorry, we could not generate OTP. Please, try again"));
            }
        }
    }
}