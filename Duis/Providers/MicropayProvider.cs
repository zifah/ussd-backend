﻿using Duis.Models;
using System;
using System.Configuration;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using FidelityBank.CoreLibraries.Interfaces.BillPayment;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using Newtonsoft.Json;

namespace Duis.Providers
{
    public class MicroPayProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly BillPaymentProvider _billingProvider;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly IBillPaymentConfigAppService _billerAppService;
        private readonly string _microPayBillerName = null;
        private const string _microPay = "MicroPay";

        public MicroPayProvider(EnrolmentProvider enrolmentProvider,
            TransactionProvider transactionProvider,
            BillPaymentProvider billingProvider,
            IBillPaymentConfigAppService billerAppService,
            ConfigurationProvider configProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;
            _billingProvider = billingProvider;
            _billerAppService = billerAppService;

            _microPayBillerName = _configProvider.GetByName<string>(DuisConstants.ConfMicroPayBillerName);

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);
        }


        public MicroPayOutput Validate(MicroPayInput input)
        {
            var output = new MicroPayOutput();

            DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    // load the remita biller dll
                    BillPaymentConfigDto biller = _billerAppService.GetByName(_microPayBillerName);

                    // compose validation input
                    var validationInput = new BillPaymentIn
                    {
                        PayerPhone = input.msisdn,
                        PlanCode = input.merchant_code,
                        Amount = input.amount,
                        ProviderParametersJson = biller.PaymentParametersJson
                    };

                    // fire a validate request
                    var _billingProcessor = ProcessorProvider.GetBillingProcessor(biller.ProviderClassFqn, biller.ProviderAssembly);
                    var validationResponse = _billingProcessor.ValidateBillPayment(validationInput);

                    LoggingSystem.LogMessage(JsonConvert.SerializeObject(validationResponse));

                    if (validationResponse.IsSuccessful)
                    {
                        output.biller_name = validationResponse.ValidationBillerName;
                        output.payment_amount = validationResponse.ValidationAmount;
                        output.surcharge_amount = validationResponse.ValidationCharge;
                        DuisProvider.AssignResponse(output, DuisResponses._successful);
                    }

                    else
                    {
                        DuisProvider.AssignResponse(output, DuisResponses._validationFailed);
                        output.response_message = !string.IsNullOrWhiteSpace(validationResponse.ResponseDescription) ?
                            validationResponse.ResponseDescription : null; // null so the client can know to display what they want
                    }

                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }

            return output;
        }

        public void Pay(MicroPayInput input, MicroPayOutput output, ICentralProvider coreProvider)
        {
            DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    // load the remita biller dll
                    BillPaymentConfigDto biller = _billerAppService.GetByName(_microPayBillerName);

                    // compose validation input
                    var billingInput = new BillPaymentInput
                    {
                        account_number = input.account_number,
                        msisdn = input.msisdn,
                        password = input.password,
                        reference_number = input.reference_number,
                        amount = input.amount,
                        surcharge = input.surcharge,
                        // CONCEPT:  MICROPAY IS THE BILLER AND EACH MERCHANT UNDER THE SCHEME IS A PLAN
                        biller_code = biller.BillerCode,
                        selected_plan_code = input.merchant_code,
                        is_via_direct_code = input.is_via_direct_code,
                        token = input.token,
                        network = input.msisdn_network.Substring(0, 3), //NIBSS used only the first three characters of the network name
                        customer_id = input.msisdn
                    };
                    
                    var billingOutput = _billingProvider.DoBillPayment(billingInput);

                    LoggingSystem.LogMessage(JsonConvert.SerializeObject(billingOutput));

                    // compose a response with the feedback
                    output.response_code = billingOutput.response_code;
                    output.response_message = billingOutput.response_message;
                    output.payment_reference = billingOutput.payment_reference;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendCompletionSms(input, output);
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        private void SendCompletionSms(MicroPayInput input, MicroPayOutput output)
        {
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            var sms = isSuccessful ?
                string.Format("Your {0} payment of {1:N2} to {2} was not successful", _microPay, input.amount, input.merchant_name) :
                string.Format(string.Format("Sorry, your {0} payment of {1:N2} to {2} was not successful",
                _microPay, input.amount, input.merchant_name));

            var subject = DuisConstants.ApplicationName;

            var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
            smsSender.SendSMS(input.msisdn, subject, sms);
        }
    }
}