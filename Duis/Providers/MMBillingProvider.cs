﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Duis.Models.Accounts;
using FidelityBank.CoreLibraries.Fincore;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Newtonsoft.Json;
using Fbp.Net.Integrations.Configurations;
using FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Utilities;

namespace Duis.Providers
{
    public class MMBillingProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly IEnrolmentAppService _enrolmentAppService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private readonly InfopoolSystem _infopoolSystem;
        private readonly BillPaymentProvider _billPaymentProvider = null;


        public MMBillingProvider(EnrolmentProvider enrolmentProvider,
            ITransactionAppService transactionAppService, TransactionProvider transactionProvider,
            IEnrolmentAppService enrolmentAppService, ConfigurationProvider configProvider,
             AccountEnquiryProvider accountEnquiryProvider, BillPaymentProvider billPaymentProvider)
        {
            _configProvider = configProvider;
            _enrolmentAppService = enrolmentAppService;
            _enrolmentProvider = enrolmentProvider;
            _billPaymentProvider = billPaymentProvider;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];

            _infopoolSystem = new InfopoolSystem(_infopoolReader);
        }
        
        public void DoBillPayment(BillPaymentInput input, BillPaymentOutput output, ICentralProvider coreProvider)
        {
            try
            {
                var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, true, UserType.MobileMoney);
                _enrolmentProvider.CheckBlacklisted(input.msisdn, null);
                var theAccount = _infopoolSystem.GetAccount(input.account_number);
                _billPaymentProvider.DoBillingTransaction(input, output, ApplicationConstants.MobileMoneyBillPayment, theUser, theAccount.SolId);
            }

            catch (CustomException ex)
            {
                LoggingSystem.LogException(ex);
                output.response_code = ex.ErrorCode;
                output.response_message = ex.ErrorMessage;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex);
                DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                #region Notify the customer
                var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

                var sms = isSuccessful ?
               string.Format("Congrats! Your {0} {1} bill payment transaction of {2:N2} for {3} was successful", output.biller_name, output.product_name, input.amount, input.customer_id) :
               string.Format("Sorry. Your {0} {1} bill payment transaction of {2:N2} for {3} was not successful", output.biller_name, output.product_name, input.amount, input.customer_id);

                CentralOperations.SendNotificationSms(sms, input.msisdn);
                #endregion
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }
    }
}