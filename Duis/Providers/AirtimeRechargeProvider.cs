﻿using Duis.Models;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Configuration;
using System.Linq;
using InfopoolSystem = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Exceptions;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using FidelityBank.CoreLibraries.Utility.Engine;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos;
using Newtonsoft.Json;

namespace Duis.Providers
{
    public class AirtimeRechargeProvider : ITransientDependency
    {
        private static readonly string _infopoolReader = ConfigurationManager.ConnectionStrings[DuisConstants.AppInfopoolReader].ConnectionString;
        private readonly EnrolmentProvider _enrolmentProvider;
        private readonly TransactionProvider _transactionProvider;
        private readonly BillPaymentProvider _billingProvider;
        private readonly IAirtimeRechargeBillerAppService _airtimeBillerAppService;
        private readonly IPhoneNetworkLogAppService _phoneNetworkLogAppService;
        private readonly ConfigurationProvider _configProvider;
        private readonly DuisResponse _successResponse = null;
        private readonly DuisResponse _badRequestResponse = null;
        private static string _twilioSId;
        private static string _twilioAuthToken;

        public AirtimeRechargeProvider(EnrolmentProvider enrolmentProvider,
            TransactionProvider transactionProvider,
            BillPaymentProvider billingProvider,
            IAirtimeRechargeBillerAppService airtimeBillerAppService,
            IPhoneNetworkLogAppService phoneNetworkLogAppService,
            ConfigurationProvider configProvider)
        {
            _configProvider = configProvider;
            _enrolmentProvider = enrolmentProvider;
            _transactionProvider = transactionProvider;
            _billingProvider = billingProvider;
            _airtimeBillerAppService = airtimeBillerAppService;
            _phoneNetworkLogAppService = phoneNetworkLogAppService;

            _successResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._successful);
            _badRequestResponse = Constants.DuisResponses.GetDuisResponse(Constants.DuisResponses._invalidRequestError);

            _twilioSId = _configProvider.GetByName<string>(DuisConstants.ConfTwilioSId);
            _twilioAuthToken = _configProvider.GetByName<string>(DuisConstants.ConfTwilioAuthToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn">Should be supplied in 11-digit format</param>
        /// <param name="isInitiator"></param>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        private PhoneNetwork GetPhoneNetwork(string msisdn, bool isInitiator = false, string accountNumber = null, bool useExternal = false)
        {
            PhoneNetwork result = PhoneNetwork.UNKNOWN;
            string standardMsisdn = CoreUtility.StringManipulation.StandardizeNigerianMsisdn(msisdn);

            if (msisdn != null)
            {
                // try first from database
                var networkLog = _phoneNetworkLogAppService.GetPhoneNetwork(standardMsisdn);
                var dateUpdated = networkLog == null ? DateTime.MinValue : networkLog.LastModificationTime ?? networkLog.CreationTime;
                var networkCheckValidityDays = _configProvider.GetByName<int>(DuisConstants.ConfPhoneNetworkCheckDaysValid);
                var lastValidityDay = dateUpdated.Date.AddDays(networkCheckValidityDays);

                if (DateTime.Today <= lastValidityDay && !useExternal)
                {
                    Enum.TryParse<PhoneNetwork>(networkLog.Network.ToUpper(), out result);
                }

                else
                {
                    if (useExternal)
                    {
                        result = PhoneUtil.GetPhoneNetworkTwilio(standardMsisdn, _twilioSId, _twilioAuthToken);
                        LoggingSystem.LogMessage(string.Format("Network gotten for {0} using external (Twilio) is {1}", msisdn, result));
                    }

                    else
                    {
                        string network = _phoneNetworkLogAppService.GetPhoneNetworkFromPrefix(msisdn);

                        if (network == null)
                        {
                            LoggingSystem.LogMessage(string.Format("No network prefix matched {0}", msisdn));
                        }

                        else
                        {
                            Enum.TryParse(network, out result);
                            LoggingSystem.LogMessage(string.Format("Network gotten for {0} using prefix is {1}", msisdn, result));
                        }
                    }


                    if (result != PhoneNetwork.UNKNOWN)
                    {
                        LoggingSystem.LogMessage(string.Format("{0} is a(n) {1} line", standardMsisdn, result));

                        if (networkLog == null)
                        {
                            _phoneNetworkLogAppService.Create(new CreatePhoneNetworkLogInput
                            {
                                AccountNumber = accountNumber,
                                IsTransactionInitiator = isInitiator,
                                Msisdn = standardMsisdn,
                                Network = result.ToString()
                            });
                        }

                        else
                        {
                            _phoneNetworkLogAppService.Update(new UpdatePhoneNetworkLogInput
                            {
                                Id = networkLog.Id,
                                Network = result.ToString(),
                                AccountNumber = accountNumber ?? networkLog.AccountNumber,
                                IsTransactionInitiator = isInitiator
                            });
                        }
                    }

                    else
                    {
                        LoggingSystem.LogMessage(string.Format("Could not get network for {0}; exiting", msisdn));
                    }
                }
            }
            return result;
        }

        internal void DoRecharge(AirtimeRechargeInput input, AirtimeRechargeOutput output, ICentralProvider coreProvider)
        {
            DuisProvider.AssignResponse(output, DuisResponses._invalidRequestError);

            if (input != null)
            {
                try
                {
                    var theUser = _enrolmentProvider.AuthenticateUser(input.msisdn, input.password, RequiresAuthentication(input));
                    var accountDetails = _enrolmentProvider.ValidatePhoneAccount(input.msisdn, input.account_number, false);
                    _transactionProvider.CheckLimit(input.amount, theUser, ApplicationConstants.AirtimeRecharge, input.token);
                    DoRechargeTransaction(input, output, ApplicationConstants.AirtimeRecharge, theUser, accountDetails.SolId);
                }

                catch (CustomException ex)
                {
                    LoggingSystem.LogException(ex);
                    output.response_code = ex.ErrorCode;
                    output.response_message = ex.ErrorMessage;
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex);
                    DuisProvider.AssignResponse(output, Constants.DuisResponses._systemError);
                }
            }

            coreProvider.IsBackgroundJobCompleted = true;

            // wait two seconds to allow main thread complete
            System.Threading.Thread.Sleep(2000);

            if (coreProvider.ReturnedTimeoutResponse)
            {
                SendCompletionSms(input, output);
            }

            if (output.response_code == _successResponse.response_code && !input.is_self_recharge && input.send_beneficiary_sms)
            {
                try
                {
                    string rechargeSource = "Fidelity Bank 770";
                    var sms = string.Format("Hi. {0} just recharged your phone with N{1:N2} airtime via {2}", input.sender_name, input.amount, rechargeSource);

                    var subject = DuisConstants.ApplicationName;

                    var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
                    smsSender.SendSMS(input.beneficiary_msisdn, subject, sms);
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, "Error while sending third party airtime recharge beneficiary SMS");
                }
            }

            LoggingSystem.LogMessage(JsonConvert.SerializeObject(output));
        }

        public void DoRechargeTransaction(AirtimeRechargeInput input, AirtimeRechargeOutput output, string transactionType, EnrolmentDto theUser, string solId)
        {
            _enrolmentProvider.CreateOrUpdateNetworkLog(input.msisdn, input.msisdn_network, input.account_number);
            var beneficiaryMsisdn = input.is_self_recharge ? input.msisdn.Trim() : input.beneficiary_msisdn.Trim();

            /// This is set so it can be used in the SMS sending method where applicable
            input.beneficiary_msisdn = beneficiaryMsisdn;

            // get the network
            string phoneNetwork = PhoneNetwork.UNKNOWN.ToString();

            if (input.is_self_recharge && !string.IsNullOrWhiteSpace(input.msisdn_network))
            {
                phoneNetwork = input.msisdn_network;
            }

            else
            {
                phoneNetwork = GetPhoneNetwork(beneficiaryMsisdn, input.is_self_recharge, input.is_self_recharge ? input.account_number : null).ToString();
            }

            if (phoneNetwork != PhoneNetwork.UNKNOWN.ToString())
            {
                // get the biller plan
                var airtimeBiller = _airtimeBillerAppService.GetByNetwork(phoneNetwork.ToString());

                // create a bill payment input
                if (airtimeBiller != null)
                {
                    var billingInput = new BillPaymentInput
                    {
                        account_number = input.account_number,
                        msisdn = input.msisdn,
                        password = input.password,
                        reference_number = input.reference_number,
                        amount = input.amount,
                        biller_code = airtimeBiller.Biller.BillerCode,
                        customer_id = beneficiaryMsisdn,
                        selected_plan_code = airtimeBiller.TopupPlanCode,
                        is_via_direct_code = input.is_via_direct_code,
                        token = input.token,
                        surcharge = input.send_beneficiary_sms ? (decimal?)_configProvider.GetByName<decimal>(DuisConstants.ConfRechargeNotifySmsCost) : null
                    };

                    try
                    {
                        var validationOutput = _billingProvider.ValidateBillPayment(billingInput);

                        if (validationOutput != null && validationOutput.response_code == _successResponse.response_code)
                        {
                            //trigger a bill payment
                            //var billingOutput = _billingProvider.DoBillPayment(billingInput, ApplicationConstants.AirtimeRecharge);
                            var billingOutput = new BillPaymentOutput();
                            _billingProvider.DoBillingTransaction(billingInput, billingOutput, transactionType, theUser, solId);

                            #region Retry transaction for ported line
                            if (billingOutput.response_code != _successResponse.response_code)
                            {
                                var theTransaction = _transactionProvider.GetByClientRef(input.reference_number);
                                string[] portedLineProcessorCodes = _configProvider.GetByName<string>(DuisConstants.ConfPortedLineFailureCodesCommaDel).Trim().Trim(',').Split(',');

                                if (theTransaction != null && portedLineProcessorCodes.Contains(theTransaction.ProcessorRespCode))
                                {
                                    // get the accurate network
                                    var accuratePhoneNetwork = GetPhoneNetwork(beneficiaryMsisdn, input.is_self_recharge, input.is_self_recharge ? input.account_number : null, true).ToString();

                                    //retry the transaction with a slight modification to the clientRefId
                                    string modifiedClientRef = string.Format("R{0}", billingInput.reference_number);

                                    if (accuratePhoneNetwork != PhoneNetwork.UNKNOWN.ToString() && accuratePhoneNetwork != phoneNetwork)
                                    {
                                        LoggingSystem.LogMessage(string.Format("Recharge transaction {0} will now be retried due to ported line", billingInput.reference_number));
                                        airtimeBiller = _airtimeBillerAppService.GetByNetwork(phoneNetwork.ToString());
                                        billingInput.reference_number = modifiedClientRef;
                                        billingInput.biller_code = airtimeBiller.Biller.BillerCode;
                                        billingInput.selected_plan_code = airtimeBiller.TopupPlanCode;
                                        billingOutput = _billingProvider.DoBillPayment(billingInput, ApplicationConstants.AirtimeRecharge);
                                    }
                                }
                            }
                            #endregion

                            // compose a response with the feedback
                            output.response_code = billingOutput.response_code;
                            output.response_message = billingOutput.response_message;
                            output.payment_reference = billingOutput.payment_reference;
                        }

                        else
                        {
                            LoggingSystem.LogMessage(string.Format("Validation of {0} with {1} failed", beneficiaryMsisdn, phoneNetwork));
                            DuisProvider.AssignResponse(output, DuisResponses._validationFailed);
                        }
                    }

                    catch (Exception ex)
                    {
                        LoggingSystem.LogException(ex, string.Format("Recharge error {0} {1}", beneficiaryMsisdn, input.reference_number));
                        DuisProvider.AssignResponse(output, DuisResponses._systemError);
                    }
                }
            }

            output.network = phoneNetwork.ToString();
        }

        private void SendCompletionSms(AirtimeRechargeInput input, AirtimeRechargeOutput output)
        {
            var isSuccessful = output.response_code == DuisResponses.GetDuisResponse(DuisResponses._successful).response_code;

            var sms = isSuccessful ?
                string.Format("Your recharge of {0} with N{1:N2} was successful", input.beneficiary_msisdn, input.amount) :
                string.Format("Sorry, your recharge transaction was not successful");

            var subject = DuisConstants.ApplicationName;

            var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
            smsSender.SendSMS(input.msisdn, subject, sms);
        }

        internal bool RequiresAuthentication(AirtimeRechargeInput input)
        {
            return !input.is_self_recharge;
        }

        internal PhoneEnquiryOutput GetNetwork(string msisdn)
        {
            var response = new PhoneEnquiryOutput
            {
                msisdn = msisdn
            };

            DuisProvider.AssignResponse(response, DuisResponses._failedAtProcessor);

            if (!string.IsNullOrWhiteSpace(msisdn))
            {
                var network = GetPhoneNetwork(msisdn);

                if (network != FidelityBank.CoreLibraries.Utility.Engine.PhoneNetwork.UNKNOWN)
                {
                    DuisProvider.AssignResponse(response, DuisResponses._successful);
                    response.network = network.ToString();
                }
            }

            return response;
        }
    }
}