﻿
using System;

namespace Duis.Exceptions
{
    /// <summary>
    /// <para>Custom exceptions for the Duis framework</para>
    /// <para>Message passed in via constructor should be in the format: ErrorCode~ErrorMessage</para>
    /// </summary>
    public class CustomException : System.Exception
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="processMessage">Specifies whether or not the ErrorCode and ErrorMessage properties should be filled by processing message</param>
        /// <param name="errorCode">Error code that will be received by client</param>
        /// <param name="errorMessage">Error message that will be received by client</param>
        public CustomException(string message, bool processMessage, string errorCode, string errorMessage) : base(message)
        {
            if (processMessage)
            {
                ErrorCode = GetMessageComponent(MessageComponent.Code, message);
                ErrorMessage = GetMessageComponent(MessageComponent.Message, message);
            }

            else
            {
                ErrorCode = errorCode;
                ErrorMessage = errorMessage;
            }
        }
        public CustomException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected CustomException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }

        /// <summary>
        /// ErrorCode
        /// </summary>
        public virtual string ErrorCode
        {
            set; get;
        }

        /// <summary>
        /// Error Message
        /// </summary>
        public virtual string ErrorMessage
        {
            set; get;
        }

        private string GetMessageComponent(MessageComponent component, string message)
        {
            string result = null;

            string[] components = null;

            if (Message != null && (components = message.Split('~')).Length == 2)
            {
                result = components[(int)component];
            }

            return result;
        }
        public enum MessageComponent { Code = 0, Message = 1 }
    }
}