﻿using Duis.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Duis.Dto.MobileMoney
{
    public class UserCreateRequest : IApiInput
    {
        private const string _msisdnRegex = @"^[0-9]{11}$";
        private const string _nameRegex = @"^[a-zA-Z-.']+$";
        private const string _birthdayRegex = @"^(0[1-9]|[1-2][0-9]|31(?!(?:0[2469]|11))|30(?!02))(0[1-9]|1[0-2])([12]\d{3})$";
        private const string _pinRegex = "^[0-9]{4}$";

        [RegularExpression(_msisdnRegex, ErrorMessage = "Phone number is missing or not valid")]
        public string msisdn { set; get; }
        public string network { set; get; }
        [Required(ErrorMessage = "First name is required")]
        [RegularExpression(_nameRegex, ErrorMessage = "First name contains invalid characters")]
        public string first_name { set; get; }
        [Required(ErrorMessage = "Last name is required")]
        [RegularExpression(_nameRegex, ErrorMessage = "Last name contains invalid characters")]
        public string last_name { set; get; }
        [RegularExpression(_birthdayRegex, ErrorMessage = "Date of birth should be entered in format: DDMMYYYY e.g. 27051960")]
        public string dob_DdMmYyyy { set; get; }
        [Required(ErrorMessage="PIN is required")]
        [RegularExpression(_pinRegex, ErrorMessage = "PIN must be a 4-digit number")]
        public string pin { set; get; }
        public DateTime? Birthday
        {
            get
            {
                DateTime? result;
                
                try
                {
                    int day = Convert.ToInt32(dob_DdMmYyyy.Substring(0, 2));
                    int month = Convert.ToInt32(dob_DdMmYyyy.Substring(2, 2));
                    int year = Convert.ToInt32(dob_DdMmYyyy.Substring(4, 4));
                    result = new DateTime(year, month, day);
                }

                catch
                {
                    result = null;
                }

                return result;
            }
        }
        public string reference_number { set; get; }
    }


    public class UserCreateResponse : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}