﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Providers;
using Fbp.Net.Integrations;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    public class MicroPayController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public MicroPayController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [HttpPost]
        [ResponseType(typeof(MicroPayOutput))]
        [Route("api/MicroPay/Validate")]
        public HttpResponseMessage Validate([FromBody]MicroPayInput value)
        {
            MicroPayOutput result = _duisProvider.ValidateMicroPay(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [ResponseType(typeof(MicroPayOutput))]
        [Route("api/MicroPay/Pay")]
        public HttpResponseMessage Pay([FromBody]MicroPayInput value)
        {
            MicroPayOutput result = _duisProvider.DoMicroPay(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/MicroPay/GetLimits")]
        [ResponseType(typeof(LimitOutput))]
        public HttpResponseMessage GetLimits([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.BillPayment, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}

