﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class InfoUpdateController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public InfoUpdateController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }        

        public HttpResponseMessage Post([FromBody]InfoUpdateInput value)
        {
            var result = _duisProvider.DoInfoUpdate(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        /// <summary>
        /// Returns an array of information which is updatable
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            string[] result = _duisProvider.GetUpdatableInformation();
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        } 

    }
}
