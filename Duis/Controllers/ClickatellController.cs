﻿using Duis.Constants;
using Duis.Filters;
using Duis.Interfaces;
using Duis.Models.Clickatell;
using Duis.Providers;
using Fbp.Net.Integrations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    [GenericExceptionFilter]
    public class ClickatellController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public ClickatellController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [HttpPost]
        [Route("api/Clickatell/Transactions/Recharge/Start")]
        [ResponseType(typeof(ClickatellRechargeOutput))]
        public HttpResponseMessage InitiateAirtimePurchase([FromBody]ClickatellRechargeInput input)
        {
            ClickatellRechargeOutput result = _duisProvider.ClickatellReserveFunds(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/Clickatell/Transactions/Recharge/End")]
        [ResponseType(typeof(ClickatellRechargeOutput))]
        public HttpResponseMessage CompleteAirtimePurchase([FromBody]ClickatellRechargeInput input)
        {
            var result = _duisProvider.ClickatellTransactResult(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpGet]
        [Route("api/Clickatell/transactions/recharge/verifications")]
        [ResponseType(typeof(TransactionVerifyResponse))]
        public HttpResponseMessage GetPhoneAccounts(string sourceIdentifier, int amount)
        {
            try
            {
                var result = _duisProvider.ClickatellVerifyRecharge(sourceIdentifier, amount);
                var finalResponse = Request.CreateResponse(result);
                return finalResponse;
            }

            catch (KeyNotFoundException ex)
            {
                LoggingSystem.LogException(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "This phone does not belong to a registered user"));
            }
        }

        public void AuthenticateRequest(string username, string password, DateTime timeStamp, IApiInput body)
        {
            //HttpContext.Current.
        }

    }
}

