﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Models.Cards;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class CardsController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public CardsController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [Route("api/Cards")]
        [ResponseType(typeof(AccountCards))]
        public HttpResponseMessage Get([FromUri]AccountEnquiryInput input)
        {
            var result = _duisProvider.GetCards(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/Cards/Control")]
        [ResponseType(typeof(CardControlOutput))]
        public HttpResponseMessage Control([FromBody]CardControlInput input)
        {
            CardControlOutput result = _duisProvider.ControlCard(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/Cards/CancelReissuance")]
        [ResponseType(typeof(CardControlOutput))]
        public HttpResponseMessage CancelReissuance([FromBody]CardControlInput input)
        {
            CardControlOutput result = _duisProvider.CancelReissuance(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
