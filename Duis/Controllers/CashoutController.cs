﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Providers;
using Fbp.Net.Integrations;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    public class CashoutController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public CashoutController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [HttpPost]
        [Route("api/Cashout")]
        [ResponseType(typeof(CashoutOutput))]
        public HttpResponseMessage Post([FromBody]CashoutInput value)
        {
            CashoutOutput result = _duisProvider.DoCashout(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Cashout/GetAffordable")]

        [ResponseType(typeof(AffordableOutput))]
        public HttpResponseMessage GetAffordable(string accountNumber)
        {
            AffordableOutput result = _duisProvider.GetAffordable(accountNumber, ApplicationConstants.CardlessCashout);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Cashout/GetLimits")]
        public HttpResponseMessage GetLimits([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.CardlessCashout, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Cashout/GetChannels")]
        [ResponseType(typeof(List<string>))]
        public HttpResponseMessage GetChannels()
        {
            List<string> result = _duisProvider.GetCashoutChannels();
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
