﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class UserTokenController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public UserTokenController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [HttpPost]
        [Route("api/UserToken/Link")]
        [ResponseType(typeof(TokenOutput))]
        public HttpResponseMessage Link([FromBody]TokenInput input)
        {
            TokenOutput result = _duisProvider.LinkToken(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/UserToken/Unlink")]
        [ResponseType(typeof(TokenOutput))]
        public HttpResponseMessage Unlink([FromBody]TokenInput input)
        {
            TokenOutput result = _duisProvider.UnlinkToken(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/UserToken/Resync")]
        [ResponseType(typeof(TokenOutput))]
        public HttpResponseMessage Resync([FromBody]TokenInput input)
        {
            TokenOutput result = _duisProvider.ResyncToken(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
