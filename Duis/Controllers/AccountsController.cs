﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class AccountsController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public AccountsController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [Route("api/accounts/GetUser")]
        [ResponseType(typeof(GetUserOutput))]
        public HttpResponseMessage GetUser(string msisdn)
        {
            GetUserOutput result = _duisProvider.GetUser(msisdn);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/accounts/GetAccountBanks")]
        [ResponseType(typeof(GetAccountBanksOutput))]
        public HttpResponseMessage GetAccountBanks([FromUri]GetAccountBanksInput input)
        {
            GetAccountBanksOutput accounts = _duisProvider.GetAccountBanks(input);
            var finalResponse = Request.CreateResponse(accounts);
            return finalResponse;
        }

        // GET: api/Accounts/5
        [Route("api/accounts/GetPhoneAccounts")]
        [ResponseType(typeof(MsisdnAccounts))]
        public HttpResponseMessage GetPhoneAccounts(string phone)
        {
            MsisdnAccounts accounts = _duisProvider.GetAccountsByPhone(phone);
            var finalResponse = Request.CreateResponse(accounts);
            return finalResponse;
        }

        // GET: api/Accounts/5
        [Route("api/accounts/RememberAccount")]
        [ResponseType(typeof(MsisdnAccounts))]
        public HttpResponseMessage RememberAccount([FromBody] AccountEnquiryInput input)
        {
            MsisdnAccounts accounts = _duisProvider.RememberAccounts(input);
            var finalResponse = Request.CreateResponse(accounts);
            return finalResponse;
        }

        // GET: api/Accounts/5
        [Route("api/accounts/GetAccountName")]
        [ResponseType(typeof(AccountEnquiryOutput))]
        public HttpResponseMessage GetAccountName(string accountNumber, string bankCodeNipV2)
        {
            var accountName = _duisProvider.GetAccountName(accountNumber, bankCodeNipV2);
            AccountEnquiryOutput response = new AccountEnquiryOutput();

            if (string.IsNullOrWhiteSpace(accountName))
            {
                response.response_code = _defaultResponse.response_code;
                response.response_message = _defaultResponse.response_message;
            }

            else
            {
                response.account_name = accountName;
                response.response_code = _successResponse.response_code;
                response.response_message = _successResponse.response_message;
            }

            var finalResponse = Request.CreateResponse(response);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/CanEnrol")]
        [ResponseType(typeof(EnrolmentOutput))]
        public HttpResponseMessage CanEnrol([FromBody]EnrolmentInput input)
        {
            // collect phone and account number
            // check that MSISDN is not already enrolled
            // check that account matches phone
            // check that account is mPin eligible
            // if all checks are passed; return good to proceed with enrolment response
            // else return appropriate error code
            // empty json object
            var response = new EnrolmentOutput
            {
                response_code = _defaultResponse.response_code,
                response_message = _defaultResponse.response_message,
            };

            if (input != null)
            {
                response = _duisProvider.CanEnrol(input);
            }

            var finalResponse = Request.CreateResponse<EnrolmentOutput>(response);
            return finalResponse;
        }

        // POST: api/Accounts
        /// <summary>
        /// This api is for completing an enrolment
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("api/accounts")]
        [ResponseType(typeof(EnrolmentOutput))]
        public HttpResponseMessage Post([FromBody]EnrolmentInput value)
        {
            var enrolmentResult = _duisProvider.Enrol(value);
            var finalResponse = Request.CreateResponse(enrolmentResult);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/GetAccountBalance")]
        [ResponseType(typeof(AccountEnquiryOutput))]
        public HttpResponseMessage GetAccountBalance([FromBody]AccountEnquiryInput value)
        {
            var accountBalance = _duisProvider.GetAccountBalance(value);
            var finalResponse = Request.CreateResponse(accountBalance);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/ChangePassword")]
        [ResponseType(typeof(EnrolmentOutput))]
        public HttpResponseMessage ChangePassword([FromBody]EnrolmentInput value)
        {
            var enrolmentResult = _duisProvider.ChangePassword(value);
            var finalResponse = Request.CreateResponse(enrolmentResult);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/ResetPassword")]
        [ResponseType(typeof(EnrolmentOutput))]
        public HttpResponseMessage ResetPassword([FromBody]EnrolmentInput value)
        {
            var enrolmentResult = _duisProvider.ResetPassword(value);
            var finalResponse = Request.CreateResponse(enrolmentResult);
            return finalResponse;
        }

        [HttpGet]
        [Route("api/accounts/IsPasswordValid")]
        [ResponseType(typeof(PasswordValidationOutput))]
        public HttpResponseMessage IsPasswordValid(string password, string account)
        {
            var isPasswordValid = _duisProvider.IsPasswordValid(password, account);
            var finalResponse = Request.CreateResponse(isPasswordValid);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/Auth")]
        [ResponseType(typeof(EnrolmentOutput))]
        public HttpResponseMessage Auth([FromBody]EnrolmentInput value)
        {
            EnrolmentOutput enrolmentResult = _duisProvider.Authenticate(value);
            var finalResponse = Request.CreateResponse(enrolmentResult);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/Lock")]
        [ResponseType(typeof(DuisResponse))]
        public HttpResponseMessage Lock([FromBody]LockoutInput value)
        {
            var output = _duisProvider.LockoutUser(value);
            var finalResponse = Request.CreateResponse(output);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/SelfBlock")]
        [ResponseType(typeof(SelfBlockOutput))]
        public HttpResponseMessage SelfBlock([FromBody]SelfBlockInput value)
        {
            SelfBlockOutput output = _duisProvider.SelfBlock(value);
            var finalResponse = Request.CreateResponse(output);
            return finalResponse;
        }


        [HttpPost]
        [Route("api/accounts/SelfUnlock")]
        [ResponseType(typeof(SelfUnlockOutput))]
        public HttpResponseMessage SelfBlock([FromBody]SelfUnlockInput value)
        {
            SelfUnlockOutput output = _duisProvider.SelfUnlock(value);
            var finalResponse = Request.CreateResponse(output);
            return finalResponse;
        }

        [Route("api/accounts")]
        [ResponseType(typeof(Account))]
        public HttpResponseMessage Get(string accountNumber)
        {
            var output = _duisProvider.GetAccountDetails(accountNumber);
            var finalResponse = Request.CreateResponse(output);
            return finalResponse;
        }

        [Route("api/accounts/GetBalEnqCharge")]
        [ResponseType(typeof(AccountEnquiryOutput))]
        public HttpResponseMessage GetBalEnqCharge(string phone)
        {
            AccountEnquiryOutput result = _duisProvider.GetBalanceEnquiryCharge(phone);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/accounts/CheckBlacklist")]
        [ResponseType(typeof(AccountEnquiryOutput))]
        public HttpResponseMessage CheckBlacklist(AccountEnquiryInput input)
        {
            AccountEnquiryOutput output = _duisProvider.CheckBlacklist(input);
            var finalResponse = Request.CreateResponse(output);
            return finalResponse;
        }
    }
}
