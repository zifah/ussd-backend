﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Fbp.Net.Integrations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class FundsTransferController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public FundsTransferController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [Route("api/FundsTransfer")]
        [ResponseType(typeof(FundsTransferOutput))]
        public HttpResponseMessage Post([FromBody]FundsTransferInput value)
        {
            var result = _duisProvider.DoFundsTransfer(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/FundsTransfer/GetAffordableIntra")]
        [ResponseType(typeof(AffordableOutput))]
        public HttpResponseMessage GetAffordableIntra(string accountNumber)
        {
            AffordableOutput result = _duisProvider.GetAffordable(accountNumber, ApplicationConstants.IntrabankTransfer);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/FundsTransfer/GetAffordableInter")]
        [ResponseType(typeof(AffordableOutput))]
        public HttpResponseMessage GetAffordableInter(string accountNumber)
        {
            AffordableOutput result = _duisProvider.GetAffordable(accountNumber, ApplicationConstants.InterbankTransfer);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/FundsTransfer/GetLimitsIntra")]
        [ResponseType(typeof(LimitOutput))]
        public HttpResponseMessage GetLimitsIntra([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.IntrabankTransfer, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/FundsTransfer/GetLimitsInter")]
        [ResponseType(typeof(LimitOutput))]
        public HttpResponseMessage GetLimitsInter([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.InterbankTransfer, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
