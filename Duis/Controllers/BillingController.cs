﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Models.BillPayments;
using Duis.Providers;
using Fbp.Net.Integrations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class BillingController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public BillingController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [Route("api/Billing")]
        [ResponseType(typeof(BillPaymentOutput))]
        public HttpResponseMessage Post([FromBody]BillPaymentInput value)
        {
            var result = _duisProvider.DoBillPayment(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/Billing/ValidateBill")]
        [ResponseType(typeof(BillPaymentOutput))]
        public HttpResponseMessage ValidateBill([FromBody]BillPaymentInput value)
        {
            var result = _duisProvider.ValidateBillPayment(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Billing/GetBiller")]
        [ResponseType(typeof(BillPaymentOutput))]
        public HttpResponseMessage GetBiller(string billerCode)
        {
            var result = _duisProvider.GetBillerDetails(billerCode);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        /// <summary>
        /// Returns a Dictionary<string, string> of billers with Key: Biller Code, Value: Biller Short Name
        /// </summary>
        /// <returns></returns>
        [Route("api/Billing")]
        [ResponseType(typeof(List<Biller>))]
        public HttpResponseMessage Get()
        {
            List<Biller> billers = _duisProvider.GetVisibleBillers();
            var finalResponse = Request.CreateResponse(billers);
            return finalResponse;
        }

        [Route("api/Billing/GetAffordable")]
        [ResponseType(typeof(AffordableOutput))]
        public HttpResponseMessage GetAffordable(string accountNumber)
        {
            AffordableOutput result = _duisProvider.GetAffordable(accountNumber, ApplicationConstants.BillPayment);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Billing/GetLimits")]
        [ResponseType(typeof(LimitOutput))]
        public HttpResponseMessage GetLimits([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.BillPayment, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
