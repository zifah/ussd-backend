﻿using Abp;
using Duis.Constants;
using System.Web.Http;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class OtpController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public OtpController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [HttpGet]
        [Route("api/Otp/GetCharge")]
        [ResponseType(typeof(OtpOutput))]
        public HttpResponseMessage GetCharge([FromUri]OtpInput input)
        {
            OtpOutput result = _duisProvider.GetOtpCharge(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpGet]
        [Route("api/Otp/GetUsername")]
        [ResponseType(typeof(OtpOutput))]
        public HttpResponseMessage GetUsername([FromUri]OtpInput input)
        {
            OtpOutput result = _duisProvider.GetOtpUsername(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/Otp/Generate")]
        [ResponseType(typeof(OtpOutput))]
        public HttpResponseMessage Generate([FromBody]OtpInput input)
        {
            OtpOutput result = _duisProvider.GenerateOtp(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
