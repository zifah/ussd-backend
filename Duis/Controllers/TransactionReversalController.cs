﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class TransactionReversalController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public TransactionReversalController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [Route("api/TransactionReversal")]
        [ResponseType(typeof(MarkForReversalOutput))]
        public HttpResponseMessage Post([FromBody]MarkForReversalInput input)
        {
            MarkForReversalOutput result = _duisProvider.MarkForReversal(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
