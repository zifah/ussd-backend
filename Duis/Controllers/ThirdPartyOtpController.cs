﻿using Duis.Constants;
using Duis.Filters;
using Duis.Models;
using Duis.Providers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    [GenericExceptionFilter]
    public class ThirdPartyOtpController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public ThirdPartyOtpController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [HttpPost]
        [Route("api/ThirdPartyOtp/Generate")]
        [ResponseType(typeof(ThirdPartyOtpOutput))]
        public HttpResponseMessage GenerateOtp([FromBody]ThirdPartyOtpInput input)
        {
            var result = _duisProvider.GenerateThirdPartyOtp(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/ThirdPartyOtp/Validate")]
        [ResponseType(typeof(ValidateOtpOutput))]
        public HttpResponseMessage ValidateOtp([FromBody]ValidateOtpInput input)
        {
            var result = _duisProvider.ValidateThirdPartyOtp(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}

