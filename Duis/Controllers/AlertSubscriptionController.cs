﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Models.AlertSubscriptions;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class AlertSubscriptionController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public AlertSubscriptionController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }
        

        [ResponseType(typeof(AlertSubscriptionOutput))]
        public HttpResponseMessage Post([FromBody]AlertSubscriptionInput value)
        {
            AlertSubscriptionOutput result = _duisProvider.ManageAlertSubscription(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        /// <summary>
        /// Returns a list of alerts which customer can subscribe to
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [ResponseType(typeof(Dictionary<string, string>))]
        public HttpResponseMessage Get()
        {
            Dictionary<string, string> result = _duisProvider.GetAlertProducts();
             var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        } 

    }
}
