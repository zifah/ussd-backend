﻿using Duis.Constants;
using System.Web.Http;
using Duis.Models;
using Duis.Providers;
using System.Net.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    public class ServiceController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public ServiceController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [HttpGet]
        [Route("api/Service/GetCharge")]
        [ResponseType(typeof(AccountEnquiryOutput))]
        public HttpResponseMessage GetCharge([FromUri]AccountEnquiryInput input)
        {
            AccountEnquiryOutput result = _duisProvider.GetServiceCharge(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
