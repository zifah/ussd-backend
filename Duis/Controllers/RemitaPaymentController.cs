﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Providers;
using Fbp.Net.Integrations;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    public class RemitaPaymentController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public RemitaPaymentController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [HttpPost]
        [ResponseType(typeof(RemitaPaymentOutput))]
        [Route("api/RemitaPayment/Validate")]
        public HttpResponseMessage Validate([FromBody]RemitaPaymentInput value)
        {
            RemitaPaymentOutput result = _duisProvider.ValidateRemitaPayment(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [ResponseType(typeof(RemitaPaymentOutput))]
        [Route("api/RemitaPayment/Pay")]
        public HttpResponseMessage Pay([FromBody]RemitaPaymentInput value)
        {
            RemitaPaymentOutput result = _duisProvider.DoRemitaPayment(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/RemitaPayment/GetLimits")]
        [ResponseType(typeof(LimitOutput))]
        public HttpResponseMessage GetLimits([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.BillPayment, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}

