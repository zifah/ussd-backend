﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Fbp.Net.Integrations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class MMCashoutController : ApiController
    {
        private readonly MobileMoneyProvider _mmProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public MMCashoutController()
            : base()
        {
            _mmProvider = new MobileMoneyProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [Route("api/MMCashout")]
        [ResponseType(typeof(CashoutOutput))]
        public HttpResponseMessage Post([FromBody]CashoutInput input)
        {
            CashoutOutput result = _mmProvider.DoCashout(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
