﻿using Duis.Constants;
using Duis.Models;
using Duis.Providers;
using Fbp.Net.Integrations;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    public class RechargeController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public RechargeController()
            : base()
        {
            _duisProvider = new DuisProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [HttpPost]
        [Route("api/Recharge/BuyAirtime")]
        [ResponseType(typeof(AirtimeRechargeOutput))]
        public HttpResponseMessage BuyAirtime([FromBody]AirtimeRechargeInput input)
        {
            var result = _duisProvider.DoAirtimeRecharge(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/Recharge/RequiresAuthentication")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RequiresAuthentication([FromBody]AirtimeRechargeInput value)
        {
            var result = _duisProvider.RequiresAuthentication(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpGet]
        [Route("api/Recharge/Network")]
        [ResponseType(typeof(PhoneEnquiryOutput))]
        public HttpResponseMessage Network(string msisdn)
        {
            PhoneEnquiryOutput result = _duisProvider.GetNetwork(msisdn);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Recharge/GetAffordable")]
        [ResponseType(typeof(AffordableOutput))]
        public HttpResponseMessage GetAffordable(string accountNumber)
        {
            AffordableOutput result = _duisProvider.GetAffordable(accountNumber, ApplicationConstants.AirtimeRecharge);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/Recharge/GetLimits")]
        [ResponseType(typeof(LimitOutput))]
        public HttpResponseMessage GetLimits([FromUri]LimitInput input)
        {
            LimitOutput result = _duisProvider.GetLimits(input.msisdn, ApplicationConstants.AirtimeRecharge, input.amount);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}