﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Fbp.Net.Integrations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class MMTransferController : ApiController
    {
        private readonly MobileMoneyProvider _mmProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public MMTransferController()
            : base()
        {
            _mmProvider = new MobileMoneyProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }

        [Route("api/MMTransfer")]
        [ResponseType(typeof(FundsTransferOutput))]
        public HttpResponseMessage Post([FromBody]FundsTransferInput input)
        {
            var result = _mmProvider.DoFundsTransfer(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/MMTransfer/GetMMOs")]
        [ResponseType(typeof(GetAccountBanksOutput))]
        public HttpResponseMessage GetMMOs([FromUri]GetAccountBanksInput input)
        {
            GetAccountBanksOutput result = _mmProvider.GetMmos(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }


        [Route("api/MMTransfer/GetOwnBank")]
        [ResponseType(typeof(GetAccountBanksOutput))]
        public HttpResponseMessage GetOwnBank([FromUri]GetAccountBanksInput input)
        {
            GetAccountBanksOutput result = _mmProvider.GetOwnBank(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/MMTransfer/GetAccountBanks")]
        [ResponseType(typeof(GetAccountBanksOutput))]
        public HttpResponseMessage GetAccountBanks([FromUri]GetAccountBanksInput input)
        {
            GetAccountBanksOutput result = _mmProvider.GetAccountBanks(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
