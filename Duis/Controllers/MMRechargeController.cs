﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Fbp.Net.Integrations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class MMRechargeController : ApiController
    {
        private readonly MobileMoneyProvider _mmProvider = null;
        private readonly DuisResponse _defaultResponse = null;
        private readonly DuisResponse _successResponse = null;

        public MMRechargeController()
            : base()
        {
            _mmProvider = new MobileMoneyProvider();
            _defaultResponse = DuisResponses.GetDuisResponse(DuisResponses._invalidRequestError);
            _successResponse = DuisResponses.GetDuisResponse(DuisResponses._successful);
        }


        [HttpPost]
        [Route("api/MMRecharge")]
        [ResponseType(typeof(AirtimeRechargeOutput))]
        public HttpResponseMessage BuyAirtime([FromBody]AirtimeRechargeInput input)
        {
            AirtimeRechargeOutput result = _mmProvider.DoAirtimeRecharge(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/MMRecharge/RequiresAuthentication")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RequiresAuthentication([FromBody]AirtimeRechargeInput value)
        {
            bool result = _mmProvider.RequiresAuthentication(value);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
