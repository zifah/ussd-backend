﻿using Duis.Constants;
using Duis.Models.BankAccounts;
using Duis.Models.Bvn;
using Duis.Providers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    /// <summary>
    /// Controller for Bank Account Opening
    /// </summary>
    public class BankAccountController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;
        
        public BankAccountController()
            : base()
        {
            _duisProvider = new DuisProvider();
        }


        [Route("api/BankAccount")]
        [ResponseType(typeof(CreateAccountOutput))]
        public HttpResponseMessage Post([FromBody]CreateAccountInput input)
        {
            CreateAccountOutput result = _duisProvider.CreateBankAccount(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
