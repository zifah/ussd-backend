﻿using Abp;
using Duis.Constants;
using Duis.Dto.MobileMoney;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class MMAccountsController : ApiController
    {
        private readonly MobileMoneyProvider _mobileMoneyProvider = null;
        public MMAccountsController()
            : base()
        {
            _mobileMoneyProvider = new MobileMoneyProvider();
        }

        [Route("api/mmaccounts")]
        [ResponseType(typeof(MMUser))]
        public HttpResponseMessage GetUser(string msisdn)
        {
            MMUser result = _mobileMoneyProvider.GetUser(msisdn);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }

        [Route("api/mmaccounts")]
        [ResponseType(typeof(UserCreateResponse))]
        public HttpResponseMessage Post([FromBody]UserCreateRequest value)
        {
            //ModelState.is
            UserCreateResponse enrolmentResult = _mobileMoneyProvider.CreateUser(value);
            var finalResponse = Request.CreateResponse(enrolmentResult);
            return finalResponse;
        }

        [HttpGet]
        [Route("api/mmaccounts/IsPasswordValid")]
        [ResponseType(typeof(PasswordValidationOutput))]
        public HttpResponseMessage IsPasswordValid([FromUri]PasswordValidationInput input)
        {
            PasswordValidationOutput isPasswordValid = _mobileMoneyProvider.IsPasswordValid(input);
            var finalResponse = Request.CreateResponse(isPasswordValid);
            return finalResponse;
        }

        [HttpPost]
        [Route("api/mmaccounts/Balance")]
        [ResponseType(typeof(AccountEnquiryOutput))]
        public HttpResponseMessage Balance([FromBody]AccountEnquiryInput input)
        {
            AccountEnquiryOutput accountBalance = _mobileMoneyProvider.GetAccountBalance(input);
            var finalResponse = Request.CreateResponse(accountBalance);
            return finalResponse;
        }


        [HttpPost]
        [Route("api/mmaccounts/ChangePassword")]
        [ResponseType(typeof(EnrolmentOutput))]
        public HttpResponseMessage Balance([FromBody]EnrolmentInput input)
        {
            EnrolmentOutput accountBalance = _mobileMoneyProvider.ChangePassword(input);
            var finalResponse = Request.CreateResponse(accountBalance);
            return finalResponse;
        }
    }
}
