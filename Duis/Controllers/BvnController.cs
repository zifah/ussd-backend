﻿using Duis.Constants;
using Duis.Models.Bvn;
using Duis.Providers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Duis.Controllers
{
    public class BvnController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;

        public BvnController()
            : base()
        {
            _duisProvider = new DuisProvider();
        }


        [Route("api/Bvn")]
        [ResponseType(typeof(BvnOutput))]
        public HttpResponseMessage Get([FromUri]BvnInput input)
        {
            BvnOutput result = _duisProvider.GetBvn(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
