﻿using Abp;
using Duis.Constants;
using Duis.Models;
using Duis.Models.Accounts;
using Duis.Models.Cards;
using Duis.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;

namespace Duis.Controllers
{
    public class AccountOfficerController : ApiController
    {
        private readonly DuisProvider _duisProvider = null;

        public AccountOfficerController()
            : base()
        {
            _duisProvider = new DuisProvider();
        }


        [Route("api/AccountOfficer")]
        [ResponseType(typeof(GetAccountOfficerOutput))]
        public HttpResponseMessage Post([FromBody]GetAccountOfficerInput input)
        {
            GetAccountOfficerOutput result = _duisProvider.GetAccountOfficer(input);
            var finalResponse = Request.CreateResponse(result);
            return finalResponse;
        }
    }
}
