﻿using System;

namespace Duis.CustomAttributes
{
    /// <summary>
    /// Do not use this property to generate the Api authentication hash
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ApiAuthIgnoreAttribute : Attribute
    {

    }
}