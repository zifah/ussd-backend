﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis
{
    public class DuisConstants
    {
        public const string ApplicationName = "DuisApp";
        //PROCESSORS
        public const string Nip = "NIP";
        public const string ValueAlertSubscriptionsApi = "AlertSubscriptionsApi";
        public const string ValueProfectus = "Profectus";
        public const string ValueLeftLoginAttempts = "LeftLoginAttempts";

        //AppSettings
        public const string AppFincoreResponsesFilePath = "FincoreResponsesFilePath";
        public const string AppIsDummyMode = "IsDummyMode";
        public const string AppInfopoolReader = "InfopoolReader"; 
        public const string AppInfopoolWriter = "InfopoolWriter";
        public const string AppUseDuisEngine = "UseDuisEngine";
        public const string AppEnableSwaggerDocs = "EnableSwaggerDocs";
        public const string AppAlertProductsListJson = "AlertProductsListJson";
        public const string AppViacard = "Viacard";
        public const string AppReissuanceCancellation = "Card Reissuance Cancellation";
        public const string AppFakeBvnPhone = "FakeBvnPhone";

        public const string ConfFincoreSuccessCode = "FincoreSuccessCode";
        public const string ConfFincoreDuplicateCode = "FincoreDuplicateCode";
        public const string ConfFincoreInsufficientFundsCode = "FincoreInsufficientFundsCode";
        public const string ConfTwilioSId = "TwilioSId";
        public const string ConfTwilioAuthToken = "TwilioAuthToken";
        public const string ConfPhoneNetworkCheckDaysValid = "PhoneNetworkCheckDaysValid";
        public const string ConfCashoutProviderFqnAndAssembly = "CashoutProviderFqnAndAssembly";
        public const string ConfCashoutParameters = "CashoutParameters";
        public const string ConfCashoutWithdrawalAdviceATM = "CashoutWithdrawalAdviceATM";
        public const string ConfCashoutWithdrawalAdvicePOS = "CashoutWithdrawalAdvicePOS";
        public const string ConfCashoutSuspenseAccount = "CashoutSuspenseAccount";
        public const string ConfProviderTimeoutSeconds = "ProviderTimeoutSeconds";
        public const string ConfTransactionSmsSubject = "TransactionSmsSubject";
        public const string ConfOldPasswordsCount = "OldPasswordsCount";
        public const string ConfBalanceEnquiryCreditAccount = "BalanceEnquiryCreditAccount";
        public const string ConfAlertApiRootUrl = "AlertApiRootUrl";
        public const string ConfAlertApiClientId = "AlertApiClientId";
        public const string ConfAlertApiAccessKey = "AlertApiAccessKey";
        public const string ConfAlertApiSuccessCode = "AlertApiSuccessCode";
        public const string ConfFidelityNipV2NumericCode = "FidelityNipV2NumericCode";
        public const string ConfOwnBankAlphaCode = "OwnBankAlphaCode";
        public const string ConfMaxPossibleFailedLoginTries = "MaxPossibleFailedLoginTries";
        public const string ConfIsEnrolmentClosed = "IsEnrolmentClosed";
        public const string ConfRemitaPaymentBillerName = "RemitaPaymentBillerName";
        public const string ConfPortedLineFailureCodesCommaDel = "PortedLineFailureCodesCommaDel";
        public const string ConfResidualBalance = "ResidualBalance";
        public const string ConfUseTokenLimit = "UseTokenLimit";
        public const string ConfProfectusRootUrl = "ProfectusRootUrl";
        public const string ConfProfectusTokenUrl = "ProfectusTokenUrl";
        public const string ConfProfectusCardControlEndpoint = "ProfectusCardControlEndpoint";
        public const string ConfProfectusClientId = "ProfectusClientId";
        public const string ConfProfectusSecretKey = "ProfectusSecretKey";
        public const string ConfCallCentrePhone = "CallCentrePhone";
        public const string ConfEntrustParamsJson = "EntrustParamsJson";
        public const string ConfEntrustGroupName = "EntrustGroupName";
        public const string ConfOtpLifetimeMinutes = "OtpLifetimeMinutes";
        public const string ConfOtpIncomeAccount = "OtpIncomeAccount";
        public const string ConfMicroPayBillerName = "MicroPayBillerName";

        public const string ConfTokenCharge = "TokenCharge";
        public const string ConfTokenSurcharge = "TokenSurcharge";
        public const string ConfTokenCost = "TokenCost";
        public const string ConfTokenIncomeAccount = "TokenIncomeAccount";
        public const string ConfTokenVatAccount = "TokenVatAccount";
        public const string ConfTokenCostDebitAccount = "TokenCostDebitAccount";
        public const string ConfTokenCostCreditAccount = "TokenCostCreditAccount";
        public const string ConfTokenLinkageComment = "TokenLinkageComment";

        public const string ConfTransferSettings = "TransferSettings";


        public const string ConfCardReissuanceSecret = "CardReissuanceSecret";

        public const string ConfCbaNarrationLength = "CbaNarrationLength";

        public const string ConfPointsPerReferral = "PointsPerReferral";

        public const string ConfServiceChargeAccount = "ServiceChargeAccount";

        public const string ConfReissuanceLienRemovalCharge = "ReissuanceLienRemovalCharge";

        public const string ConfBvnValidationSuccessCode = "BvnValidationSuccessCode";

        public const string ConfDefaultAccountOfficerCode = "DefaultAccountOfficerCode";
        public const string ConfFinacleIntegratorAppName = "FinacleIntegratorAppName";
        public const string ConfNewBankAccountSchemeCode = "NewBankAccountSchemeCode";
        public const string ConfDefaultBranchCode = "DefaultBranchCode";

        public const string ConfCommaSeparatedCashoutChannels = "CommaSeparatedCashoutChannels";
        public const string ConfColigoParameters = "ColigoParameters";
        public const string ConfClickatellBillerName = "ClickatellBillerName";
        public const string ConfClickatellReturnBalance = "ClickatellReturnBalance";
        public const string ConfRechargeNotifySmsCost = "RechargeNotifySmsCost";
        public const string ConfThirdPartyOtpConfigurations = "ThirdPartyOtpConfigurations";

        /// <summary>
        /// The number of seconds from the time indicated on a Clickatell request after which it is no longer valid
        /// </summary>
        public const string ConfClickatellSecondsValid = "ClickatellSecondsValid";

        public const string ApiClientUsernameHeader = "username";
        public const string ApiClientHashHeader = "password";
    }
}