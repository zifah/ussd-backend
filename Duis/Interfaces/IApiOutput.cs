﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Duis.Interfaces
{
    public interface IApiOutput
    {
        string response_code { set; get; }
        string response_message { set; get; }
    }
}
