﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Duis.Interfaces
{
    public interface IApiInput
    {
        string reference_number { set; get; }
    }
}
