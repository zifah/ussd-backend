﻿using Duis.Interfaces;
using Duis.Providers;
using System.Configuration;
using System;
using System.Web.Hosting;
using Fbp.Net.Integrations;
using Duis.Constants;
using Abp.Dependency;
using Abp;
using FidelityBank.CoreLibraries.Infopool;
using FidelityBank.CoreLibraries.Utility.Engine;
using Duis.Models.Accounts;
using System.Collections.Generic;
using System.Linq;
using FidelityBank.CoreLibraries.Fincore;

namespace Duis.Utilities
{
    public class CentralOperations
    {
        private ICentralProvider _centralProvider;
        private RequestLogProvider _requestLogProvider;
        private static readonly AbpBootstrapper _bootstrapper = AbpDIManager.AbpBootstraper;

        public CentralOperations(ICentralProvider centralProvider)
        {
            _centralProvider = centralProvider;

            _requestLogProvider = _bootstrapper.IocManager.Resolve<RequestLogProvider>();
        }

        public static void SendNotificationSms(string text, string recipientPhone)
        {
            var subject = DuisConstants.ApplicationName;
            var smsSender = new InfopoolSystem(DuisProvider._infopoolWriter);
            smsSender.SendSMS(recipientPhone, subject, text);
        }

        private void SetTimeoutElasped(object sender, EventArgs e)
        {
            _centralProvider.IsTimeoutElapsed = true;
            LoggingSystem.LogMessage(string.Format("Timeout has elapsed: {0}", _centralProvider.IsTimeoutElapsed));
        }
                
        internal TOut Process<TIn, TOut>(TIn input, decimal? amount, string msisdn, string accountNumber, Action<TIn, TOut, ICentralProvider> processor)
            where TIn : IApiInput
            where TOut : IApiOutput, new()
        {
            var processOutput = new TOut();
            var providerTimeoutMilliSeconds = Convert.ToInt32(ConfigurationManager.AppSettings[DuisConstants.ConfProviderTimeoutSeconds]) * 1000;

            HostingEnvironment.QueueBackgroundWorkItem(ct => processor(input, processOutput, _centralProvider));

            var timer = new System.Timers.Timer(providerTimeoutMilliSeconds);
            timer.Elapsed += SetTimeoutElasped;
            timer.Start();

            while (_centralProvider.IsTimeoutElapsed == false && _centralProvider.IsBackgroundJobCompleted == false)
            {
                // wait for the first of the above to occur
                System.Threading.Thread.Sleep(100);
            }

            timer.Close();
            timer.Dispose();

            LoggingSystem.LogMessage(string.Format("Broke out from the while loop: Elapsed:{0} JobCompleted:{1}", 
                _centralProvider.IsTimeoutElapsed, _centralProvider.IsBackgroundJobCompleted));

            if (!_centralProvider.IsBackgroundJobCompleted)
            {
                _centralProvider.ReturnedTimeoutResponse = true;
                var timeoutResponse = new TOut();
                DuisProvider.AssignResponse(timeoutResponse, DuisResponses._timeoutError);
                LoggingSystem.LogMessage(string.Format("About to return timeout error as provider has not yet responded"));
                _requestLogProvider.LogRequest(msisdn, accountNumber, amount, timeoutResponse.response_code, timeoutResponse.response_message, input.reference_number);
                return timeoutResponse;
            }

            _requestLogProvider.LogRequest(msisdn, accountNumber, amount, processOutput.response_code, processOutput.response_message, input.reference_number);

            return processOutput;
        }

        internal TOut ProcessSync<TIn, TOut>(TIn input, decimal? amount, string msisdn, string accountNumber, Func<TIn, TOut> processor)
            where TIn : IApiInput
            where TOut : IApiOutput, new()
        {
            TOut result = default(TOut);

            if (input != null)
            {
                result = processor(input);
                _requestLogProvider.LogRequest(msisdn, accountNumber, amount, result.response_code, result.response_message, input.reference_number);
            }

            return result;
        }


        /// <summary>
        /// <para>Sample input: 08012345678</para>
        /// <para>Sample output: 080****5678</para>
        /// </summary>
        /// <param name="mobileNumber">Eleven-digit mobile numbers only; will return NULL for non-conforming input</param>
        /// <returns></returns>
        internal static string MaskMobileNumber(string mobileNumber)
        {
            if (mobileNumber == null || mobileNumber.Length != 11)
            {
                return null;
            }

            return string.Format("{0}****{1}", mobileNumber.Substring(0, 3), mobileNumber.LastCharacters(4));
        }

        public static List<Bank> GetCommercialBanks(string senderMsisdn, decimal intraTransferCharge, decimal interTransferCharge, string ownBankNumericCodeV2)
        {
            var infopoolSystem = new InfopoolSystem(DuisProvider._infopoolReader);
            var allNipBanks = infopoolSystem.GetNipCommercialBanks();
            var allBanks = new List<Bank>();

            foreach (var bank in allNipBanks.Where(x => x.IsCommercialBank == 1))
            {
                allBanks.Add(new Bank
                {
                    code_v1 = bank.NumericCodeV1,
                    code_v2 = bank.NumericCodeV2,
                    name = bank.BankName,
                    name_short = bank.AlphabeticCodeV2,
                    transfer_charge = bank.NumericCodeV2 == ownBankNumericCodeV2 ? intraTransferCharge : interTransferCharge
                });
            }

            return allBanks;
        }

        public static AccountBalance GetAccountBalance(string accountNumber)
        {
            string fincoreResponsesPath = ConfigurationManager.AppSettings[DuisConstants.AppFincoreResponsesFilePath];
            var fincore = new FincoreSystem(fincoreResponsesPath);
            return fincore.GetAccountBalance(accountNumber);
        }
    }
}
