﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Utilities
{
    public class WebUtility
    {
        public static string GetClientIPAddress()
        {
            string clientAddress = HttpContext.Current.Request.UserHostAddress;
            return clientAddress;
        }
    }
}