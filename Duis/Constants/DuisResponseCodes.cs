﻿using Duis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis.Constants
{
    public class DuisResponses
    {
        // fields
        public const string _successful = "00~Successful";
        public const string _accountMsisdnMismatch = "01~Phone number does not match account";
        public const string _accountInvalidOrIneligible = "02~Account invalid or ineligible";
        public const string _alreadyEnrolled = "03~Account is already enrolled";
        public const string _invalidPassword = "04~PIN does not meet required specifications";
        public const string _notEnrolled = "05~Phone number is not enrolled";
        public const string _accountBlocked = "06~Account blocked";
        public const string _authenticationFailed = "07~Authentication failed";
        public const string _dailyLimitExceeded = "08~Daily limit exceeded";
        public const string _inputInvalid = "09~Input is not valid";
        public const string _chargeCustomerFailed = "10~Issuer or switch inoperative";
        public const string _failedAtProcessor = "11~Service provider failure";
        public const string _statusUnknown = "12~Unknown status";
        public const string _validationFailed = "13~Could not validate transaction";
        public const string _passwordExpired = "14~Password should be reset";
        public const string _tokenError = "14~Token error";
        public const string _emptySetError = "15~Empty set error";
        public const string _insufficientFunds = "51~Insufficient funds";
        public const string _systemError = "96~System error";
        public const string _systemUnderMaintenance = "97~System temporarily under maintenance";
        public const string _timeoutError = "98~Timeout error";
        public const string _invalidRequestError = "99~Request format error";
        public static DuisResponse GetDuisResponse(string serialResponse)
        {
            var result = new DuisResponse();
            var splitResults = serialResponse.Split('~');

            if(splitResults.Length == 2)
            {
                result.response_code = splitResults[0];
                result.response_message = splitResults[1];
            }

            return result;
        }



    }

    public class DuisResponse : IApiOutput
    {
        public string response_code { set; get; }
        public string response_message { set; get; }
    }
}