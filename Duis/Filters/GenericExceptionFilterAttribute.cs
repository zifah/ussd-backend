﻿using Fbp.Net.Integrations;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Duis.Filters
{
    public class GenericExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            LoggingSystem.LogException(actionExecutedContext.Exception, "An unhandled exception has occurred. GenericExceptionFilter to the rescue.");
            actionExecutedContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError) {
                  Content = new StringContent("An unhandled exception has occurred. Contact System Administrator"),
                  //ReasonPhrase = "Unhandled Exception!", 
            };
        }
    }
}