﻿using FluentMigrator.VersionTableInfo;

namespace FidelityBank.MTNOnDemand.DbMigrations
{
    [VersionTableMetaData]
    public class VersionTable : DefaultVersionTableMetaData
    {
        public override string TableName
        {
            get
            {
                return "MyAppVersionInfo";
            }
        }
    }
}
