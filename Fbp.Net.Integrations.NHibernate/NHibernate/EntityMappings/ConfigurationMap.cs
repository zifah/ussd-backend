﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class ConfigurationMap : EntityMap<Configuration, long>
    {
        public ConfigurationMap()
            : base("Configurations")
        {
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.Name).Unique();
            Map(x => x.Value).Length(10000);
            Map(x => x.Description);
            Map(x => x.IsDeleted);
        }
    }
}
