﻿using Abp.Domain.Entities.Auditing;
using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.RawRequests;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class RawRequestMap : EntityMap<RawRequest, long>
    {
        public RawRequestMap()
            : base("RawRequests")
        {
            Map(x => x.RequestString).Length(int.MaxValue);
            Map(x => x.ResponseString).Length(int.MaxValue);
            Map(x => x.RelatedObjectId);
            Map(x => x.SourceIpAddress);
            Map(x => x.ObjectName);
            Map(x => x.MethodName);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
        }
    }
}
