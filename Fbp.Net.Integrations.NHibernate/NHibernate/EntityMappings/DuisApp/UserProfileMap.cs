﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class UserProfileMap : EntityMap<UserProfile, long>
    {
        public UserProfileMap()
            : base("UserProfiles")
        {
            Map(x => x.Name).Unique().Not.Nullable();
            Map(x => x.Description).Not.Nullable().Length(10000);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
        }
    }
}
