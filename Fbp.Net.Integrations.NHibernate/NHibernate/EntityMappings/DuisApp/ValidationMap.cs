﻿using Abp.NHibernate.EntityMappings;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class ValidationMap : EntityMap<DuisApp.Validations.Validation, long>
    {
        public ValidationMap()
            : base("Validations")
        {
            Map(x => x.BillerName);
            Map(x => x.BillerId).Index("ind_biller_customer_id");
            Map(x => x.CustomerId).Index("ind_biller_customer_id");
            Map(x => x.CustomerName);
            Map(x => x.ProviderReference);
            Map(x => x.Amount);
            Map(x => x.RawResponse).Length(1000);
            Map(x => x.ClientReference);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
        }
    }
}