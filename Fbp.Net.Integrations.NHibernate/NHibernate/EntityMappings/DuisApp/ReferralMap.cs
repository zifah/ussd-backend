﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Referrals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class ReferralMap : EntityMap<Referral, long>
    {
        public ReferralMap()
            : base("Referrals")
        {
            Map(x => x.ReferrerPhone).Index("ind_referrer_phone");
            Map(x => x.ReferreePhone);
            Map(x => x.RefereeAccount);
            Map(x => x.ReferreeUserId);
            Map(x => x.RefereeCustomerId);
            Map(x => x.CreationTime).Index("ind_creation_time");
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.IsActive).Index("ind_is_active");
        }
    }
}