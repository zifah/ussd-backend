﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.NetworkPrefixes;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class NetworkPrefixMap : EntityMap<NetworkPrefix, long>
    {
        public NetworkPrefixMap()
            : base("NetworkPrefixes")
        {
            Map(x => x.Prefix).Unique().Index("ind_msisdn_prefix").Not.Nullable();
            Map(x => x.Network).Not.Nullable();
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
        }
    }
}
