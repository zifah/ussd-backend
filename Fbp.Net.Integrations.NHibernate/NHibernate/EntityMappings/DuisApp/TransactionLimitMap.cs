﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.TransactionLimits;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class TransactionLimitMap : EntityMap<TransactionLimit, long>
    {
        public TransactionLimitMap()
            : base("TransactionLimits")
        {
            References<Enrolment>(x => x.User, "UserId").UniqueKey("ind_user_transactiontype_profile").Index("ind_user");
            Map(x => x.DailyAmount);
            Map(x => x.DailyCount);
            Map(x => x.MaxTransactionAmount);
            Map(x => x.MaximumBeforeTokenAmount);
            References<TransactionType>(x => x.TransactionType, "TransactionTypeId").UniqueKey("ind_user_transactiontype_profile").Not.Nullable();
            References<UserProfile>(x => x.UserProfile, "UserProfileId").UniqueKey("ind_user_transactiontype_profile");
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
        }
    }
}
