﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Blacklists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class BlacklistMap : EntityMap<Blacklist, long>
    {
        public BlacklistMap()
            : base("Blacklist")
        {
            Map(x => x.AccountNumber).UniqueKey("ind_account_msisdn").Index("ind_account");
            Map(x => x.Msisdn).UniqueKey("ind_account_msisdn").Index("ind_phone");
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.IsActive);
        }
    }
}
