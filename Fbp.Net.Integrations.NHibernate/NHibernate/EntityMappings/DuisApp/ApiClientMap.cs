﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.ApiClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class ApiClientMap : EntityMap<ApiClient, long>
    {
        public ApiClientMap()
            : base("ApiClients")
        {
            Map(x => x.Name).UniqueKey("ind_name");
            Map(x => x.IpAddress);
            Map(x => x.SharedKey);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.IsActive);
            Map(x => x.RequiresAuth);
        }
    }
}