﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class TransactionMap : EntityMap<Transaction, long>
    {
        public TransactionMap()
            : base("Transactions")
        {
            Map(x => x.RefId).Index("ind_ref_id");
            Map(x => x.ClientRefId).Unique().Index("ind_client_ref");
            Map(x => x.UserId).Index("ind_user_ref"); ;
            Map(x => x.Type).Index("ind_type");
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.ChargeAmount);
            Map(x => x.CreationTime).Index("ind_creation_time");
            Map(x => x.Status);
            Map(x => x.StatusCode);
            Map(x => x.Success);
            Map(x => x.DrAmount);
            Map(x => x.DrSource);
            Map(x => x.DrDestination);
            Map(x => x.DrDestinationCode);
            Map(x => x.DrNarration);
            Map(x => x.DrRefId);
            Map(x => x.DrReqTime);
            Map(x => x.DrRespTime);
            Map(x => x.DrResponseCode);
            Map(x => x.DrResponseDesc);
            Map(x => x.IsReversed);
            Map(x => x.ReversalAttempts);
            Map(x => x.ProcessorCode);
            Map(x => x.ProcessorOptionCode);
            Map(x => x.ProcessorName);
            Map(x => x.ProcessorRefId);
            Map(x => x.ProcessorUserId);
            Map(x => x.IsViaDirectCode);
            Map(x => x.UserEmail);
            Map(x => x.UserFullName);
            Map(x => x.UserMsisdn);
            Map(x => x.ProcessorValidationOutput);
            Map(x => x.ProcessorReqTime);
            Map(x => x.ProcessorRespTime);
            Map(x => x.ProcessorRespCode);
            Map(x => x.ProcessorRespDesc);
            Map(x => x.ProcessorResponseBody).Length(10000);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.ReverseExceptionally);
            Map(x => x.ReversalTime);
        }
    }
}
