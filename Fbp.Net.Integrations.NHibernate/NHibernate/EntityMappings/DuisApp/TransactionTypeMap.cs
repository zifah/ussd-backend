﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class TransactionTypeMap : EntityMap<TransactionType, long>
    {
        public TransactionTypeMap()
            : base("TransactionTypes")
        {
            Map(x => x.Name).Unique();
            Map(x => x.DefaultDailyAmount);
            Map(x => x.DefaultDailyCount);
            Map(x => x.DefaultMaxTransactionAmount);
            Map(x => x.MinTransactionAmount);
            Map(x => x.MaximumDailyAmount);
            Map(x => x.MaximumDailyCount);
            Map(x => x.MaximumTransactionAmount);
            Map(x => x.MaximumBeforeTokenAmount);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
        }
    }
}
