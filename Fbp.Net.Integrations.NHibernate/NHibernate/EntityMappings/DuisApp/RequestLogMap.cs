﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.RequestLogs;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class RequestLogMap : EntityMap<RequestLog, long>
    {
        public RequestLogMap()
            : base("RequestLogs")
        {
            Map(x => x.Url);
            Map(x => x.HttpMethod);
            Map(x => x.AccountNumber).Index("ind_account_number");
            Map(x => x.Msisdn).Index("ind_msisdn");
            Map(x => x.Amount);
            Map(x => x.ClientReference);
            Map(x => x.ResponseCode);
            Map(x => x.ResponseMessage).Length(10000);
            Map(x => x.OriginatingAddress);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
        }
    }
}
