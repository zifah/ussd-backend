﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.TransactionCharges;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class TransactionChargeMap : EntityMap<TransactionCharge, long>
    {
        public TransactionChargeMap()
            : base("TransactionCharges")
        {
            Map(x => x.Amount).Not.Nullable();
            References<TransactionType>(x => x.TransactionType, "TransactionTypeId").Not.Nullable().UniqueKey("ind_profile_transactiontype");
            References<UserProfile>(x => x.UserProfile, "UserProfileId").UniqueKey("ind_profile_transactiontype"); ;
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime).Not.Nullable();
        }
    }
}
