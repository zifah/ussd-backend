﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class PhoneNetworkLogMap : EntityMap<PhoneNetworkLog, long>
    {
        public PhoneNetworkLogMap()
            : base("PhoneNetworkLogs")
        {
            Map(x => x.Msisdn).Unique().Index("ind_phone_number");
            Map(x => x.Network);
            Map(x => x.IsTransactionInitiator); 
            Map(x => x.AccountNumber);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
        }
    }
}
