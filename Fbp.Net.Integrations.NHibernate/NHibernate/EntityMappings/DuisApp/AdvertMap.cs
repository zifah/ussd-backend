﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Adverts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class AdvertMap : EntityMap<Advert, long>
    {
        public AdvertMap()
            : base("Adverts")
        {
            Map(x => x.Name).UniqueKey("ind_name");
            Map(x => x.Content).Length(10000);
            Map(x => x.ApplicableModulesListJson).Length(500);
            Map(x => x.UserExcludeDictionaryJson).Length(500);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.IsActive);
        }
    }
}