﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.WeakPasswords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class WeakPasswordMap : EntityMap<WeakPassword, long>
    {
        public WeakPasswordMap()
            : base("WeakPasswords")
        {
            Map(x => x.Password).Unique().Not.Nullable().Index("ind_weak_password");
            Map(x => x.IsDeleted).Not.Nullable();
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime).Not.Nullable();
        }
    }
}
