﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.ThirdPartyOtps;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class ThirdPartyOtpMap : EntityMap<ThirdPartyOtp, long>
    {
        public ThirdPartyOtpMap()
            : base("ThirdPartyOtps")
        {
            Map(x => x.UserMsisdn).Index("ind_msisdn");
            Map(x => x.UserId).Index("ind_user");
            Map(x => x.OtpAccount).Index("ind_account");
            Map(x => x.Purpose).Index("ind_purpose");
            Map(x => x.CreationTime).Index("ind_creation_time");
            Map(x => x.ExpirationTime);
            Map(x => x.UsageTime);
            Map(x => x.FailedAttempts);
            Map(x => x.FailedTryCount);
            Map(x => x.LastFailedAttemptTime);
            Map(x => x.Otp);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
        }
    }
}