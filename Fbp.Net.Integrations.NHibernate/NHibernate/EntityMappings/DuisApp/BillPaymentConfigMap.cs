﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class BillPaymentConfigMap : EntityMap<BillPaymentConfig, long>
    {
        public BillPaymentConfigMap()
            : base("BillPaymentConfigs")
        {
            Map(x => x.BillerCode).Unique();
            Map(x => x.BillerName).Unique().Not.Nullable();
            Map(x => x.ShortName).Unique().Not.Nullable();
            Map(x => x.Category).Not.Nullable();
            Map(x => x.IsHidden).Not.Nullable();
            Map(x => x.ProductName);
            Map(x => x.CustomerIdFieldName);
            Map(x => x.ProviderName);
            Map(x => x.ProviderFullyQualifiedName);
            Map(x => x.PaymentParametersJson).Length(10000);
            Map(x => x.JsonPlans).Length(10000);
            Map(x => x.HasPlans).Not.Nullable();
            Map(x => x.IsProviderTransactor).Not.Nullable();
            Map(x => x.BillerSuspenseAccount);
            Map(x => x.RequiresCustomerIdValidation);
            Map(x => x.SurchargeAmount);
            Map(x => x.SaveValidationResponse);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.IsActive);
        }
    }
}
