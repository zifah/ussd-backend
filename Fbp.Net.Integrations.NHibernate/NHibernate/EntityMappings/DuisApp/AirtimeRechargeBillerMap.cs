﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class AirtimeRechargeBillerMap : EntityMap<AirtimeRechargeBiller, long>
    {
        public AirtimeRechargeBillerMap()
            : base("AirtimeRechargeBillers")
        {
            References<BillPaymentConfig>(x => x.Biller).Not.Nullable();
            Map(x => x.Network).Unique();
            Map(x => x.TopupPlanCode);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.IsActive);
        }
    }
}
