﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class EnrolmentMap : EntityMap<Enrolment, long>
    {
        public EnrolmentMap()
            : base("Users")
        {
            Map(x => x.AccountNumber).Unique().Length(10).Not.Nullable();
            References<UserProfile>(x => x.Profile, "ProfileId");
            Map(x => x.Msisdn).UniqueKey("ind_username_type").Length(11).Not.Nullable();
            Map(x => x.Network).Not.Nullable();
            Map(x => x.BankCustomerId).Unique().Not.Nullable();
            Map(x => x.Status).Not.Nullable();
            Map(x => x.Password).Not.Nullable();
            Map(x => x.EnrolmentTime);
            Map(x => x.FirstLoginTime);
            Map(x => x.LastLoginTime);
            Map(x => x.LastLoginAttemptTime);
            Map(x => x.FailedLoginAttempts);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
            Map(x => x.CreationTime);
            Map(x => x.OldPasswords).Length(10000);
            Map(x => x.LockoutReason).Length(10000);
            Map(x => x.HasToken);
            Map(x => x.UserType).UniqueKey("ind_username_type");
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.FinacleCreationTime);
            Map(x => x.Comment);
        }
        
    }
}
