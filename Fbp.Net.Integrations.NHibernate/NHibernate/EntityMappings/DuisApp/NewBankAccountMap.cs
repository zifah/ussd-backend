﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.DuisApp.NewBankAccounts;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class NewBankAccountMap : EntityMap<NewBankAccount, long>
    {
        public NewBankAccountMap()
            : base("NewBankAccounts")
        {
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.PhoneNumber).Index("ind_phone");
            Map(x => x.Email);
            Map(x => x.AccountNumber);
            Map(x => x.SchemeCode);
            Map(x => x.Birthday);
            Map(x => x.Bvn);
            Map(x => x.CustomerId);
            Map(x => x.BranchCode);
            Map(x => x.ReferrerCode);
            Map(x => x.CbaResponse).Length(10000);
            Map(x => x.CreationStatus);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);
            Map(x => x.LastModifierUserId);
        }
    }
}