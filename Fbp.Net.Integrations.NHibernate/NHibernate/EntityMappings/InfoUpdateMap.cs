﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.InfoUpdates;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class InfoUpdateMap : EntityMap<InfoUpdate, long>
    {
        public InfoUpdateMap()
            : base("InfoUpdates")
        {
            Map(x => x.InfoType);
            Map(x => x.NewValue).Length(Int32.MaxValue);
            Map(x => x.AccountNumber);
            Map(x => x.Msisdn);
            Map(x => x.UserId);
            Map(x => x.Reason);
            Map(x => x.ClientReference);
            Map(x => x.CreationTime);
            Map(x => x.LastModificationTime);            
            Map(x => x.LastModifierUserId);
            Map(x => x.IsDeleted);
        }
    }
}
