﻿using Abp.NHibernate.EntityMappings;
using Fbp.Net.Integrations.LoggedSMSs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.EntityMappings
{
    public class LoggedSMSMap : EntityMap<LoggedSms, long>
    {
        public LoggedSMSMap()
            : base("LoggedSMSs")
        {
            Map(x => x.Subject);
            Map(x => x.Recipient);
            Map(x => x.CreationTime);
            Map(x => x.LastUpdateTime);
        }
    }
}
