﻿using Abp.NHibernate;
using Fbp.Net.Integrations.InfoUpdates;
using Fbp.Net.Integrations.RawRequests;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class InfoUpdateRepository : IntegrationsRepositoryBase<InfoUpdate, long>, IInfoUpdateRepository
    {
        public InfoUpdateRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {
        }
    }
}
