﻿using Abp.NHibernate;
using Fbp.Net.Integrations.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class ConfigurationRepository : IntegrationsRepositoryBase<Configuration, long>,  IConfigurationRepository
    {
        public ConfigurationRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {

        }

        public Configuration GetByName(string name)
        {
            var query = GetAll();

            var result = query.FirstOrDefault(x => x.Name == name);

            return result;
        }
    }
}
