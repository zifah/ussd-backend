﻿
using Abp.NHibernate;
using Fbp.Net.Integrations.RawRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class RawRequestRepository : IntegrationsRepositoryBase<RawRequest, long>,  IRawRequestRepository
    {
        public RawRequestRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {

        }
    }
}
