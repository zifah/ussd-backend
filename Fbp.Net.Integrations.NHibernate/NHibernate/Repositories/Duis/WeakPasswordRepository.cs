﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.WeakPasswords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class WeakPasswordRepository : IntegrationsRepositoryBase<WeakPassword, long>,  IWeakPasswordRepository
    {
        public WeakPasswordRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {
        }

        public WeakPassword GetByPassword(string password)
        {
            var query = GetAll();

            var result = query.SingleOrDefault(x => x.Password == password);

            // cater for case insensitivity in database searches
            if(result != null && result.Password != password)
            {
                result = null;
            }

            return result;
        }
    }
}
