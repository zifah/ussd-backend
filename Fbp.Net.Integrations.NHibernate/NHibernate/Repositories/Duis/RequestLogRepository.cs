﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs
{
    public class RequestLogRepository : IntegrationsRepositoryBase<RequestLog, long>, IRequestLogRepository
    {
        public RequestLogRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }
    }
}
