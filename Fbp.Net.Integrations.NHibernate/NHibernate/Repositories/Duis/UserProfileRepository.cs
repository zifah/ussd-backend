﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class UserProfileRepository : IntegrationsRepositoryBase<UserProfile, long>,  IUserProfileRepository
    {
        public UserProfileRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {
        }
    }
}
