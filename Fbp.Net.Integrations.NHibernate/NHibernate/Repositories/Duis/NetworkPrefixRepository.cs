﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.NetworkPrefixes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class NetworkPrefixRepository : IntegrationsRepositoryBase<NetworkPrefix, long>,  INetworkPrefixRepository
    {
        public NetworkPrefixRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {
        }

        public NetworkPrefix GetPhoneNetwork(string msisdn)
        {
            string firstFour = msisdn.Substring(0, 4);
            string firstFive = msisdn.Substring(0, 5);
            string firstSix = msisdn.Substring(0, 6);

            var query = GetAll();

            var result = query.Where(
                x => x.Prefix == firstFour || 
                    x.Prefix == firstFive || 
                    x.Prefix == firstSix)
                    .ToList()
                    .OrderByDescending(x => x.Prefix.Length).FirstOrDefault();

            return result;
        }
    }
}
