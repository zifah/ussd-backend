﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System.Linq;

namespace Fbp.Net.Integrations.DuisApp.Validations
{
    public class ValidationRepository : IntegrationsRepositoryBase<Validation, long>, IValidationRepository
    {
        public ValidationRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }

        public Validation GetBy(string customerId, string billerId)
        {
            var query = GetAll();

            var result = query.OrderByDescending(x => x.Id).FirstOrDefault(x => x.CustomerId == customerId
            && x.BillerId == billerId);

            return result;
        }
    }
}
