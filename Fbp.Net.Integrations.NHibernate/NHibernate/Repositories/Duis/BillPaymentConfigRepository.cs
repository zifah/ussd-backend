﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.RawRequests;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class BillPaymentConfigRepository : IntegrationsRepositoryBase<BillPaymentConfig, long>, IBillPaymentConfigRepository
    {
        public BillPaymentConfigRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {
        }

        public BillPaymentConfig GetByCode(string billerCode)
        {
            var query = GetAll();
            var result = query.SingleOrDefault(x => x.BillerCode == billerCode);
            return result;
        }

        public BillPaymentConfig GetByName(string billerName)
        {
            var query = GetAll();
            var result = query.SingleOrDefault(x => x.BillerName == billerName);
            return result;
        }
    }
}

