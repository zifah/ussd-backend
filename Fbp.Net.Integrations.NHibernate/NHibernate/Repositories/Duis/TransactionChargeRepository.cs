﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.TransactionCharges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class TransactionChargeRepository : IntegrationsRepositoryBase<TransactionCharge, long>, ITransactionChargeRepository
    {
        public TransactionChargeRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {
        }

        public TransactionCharge GetByTransactionType(string transactionTypeName, long? profileId)
        {
            var query = GetAll();

            TransactionCharge result = null;

            if(profileId.HasValue)
            {
                result = query.FirstOrDefault(x => x.TransactionType.Name == transactionTypeName && x.UserProfile.Id == profileId.Value);
            }

            else
            {
                result = query.FirstOrDefault(x => x.TransactionType.Name == transactionTypeName && x.UserProfile == null );
            }

            return result;
        }
    }
}
