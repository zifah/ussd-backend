﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.RawRequests;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class AirtimeRechargeBillerRepository : IntegrationsRepositoryBase<AirtimeRechargeBiller, long>, IAirtimeRechargeBillerRepository
    {
        public AirtimeRechargeBillerRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {
        }

        public AirtimeRechargeBiller GetByNetwork(string networkName)
        {
            var query = GetAll();

            var result = query.SingleOrDefault(x => x.Network == networkName);

            return result;
        }
    }
}

