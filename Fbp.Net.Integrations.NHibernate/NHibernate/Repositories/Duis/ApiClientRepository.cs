﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.ApiClients
{
    public class ApiClientRepository : IntegrationsRepositoryBase<ApiClient, long>, IApiClientRepository
    {
        public ApiClientRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }

        public ApiClient GetByName(string name)
        {
            var query = GetAll();
            return query.SingleOrDefault(x => x.Name == name);
        }
    }
}
