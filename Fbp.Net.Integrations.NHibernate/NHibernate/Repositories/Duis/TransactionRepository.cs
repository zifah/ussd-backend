﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class TransactionRepository : IntegrationsRepositoryBase<Transaction, long>, ITransactionRepository
    {
        public TransactionRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }

        public IList<Transaction> GetTransactionsLast24Hours(long userId)
        {
            var query = GetAll();

            var last24Hours = DateTime.Now.AddDays(-1);

            var result = query.Where(x => x.UserId == userId && x.CreationTime >= last24Hours).ToList();

            return result;
        }

        /// <summary>
        /// deprecated
        /// </summary>
        /// <param name="successfulDebitCode"></param>
        /// <param name="transactionTypes"></param>
        /// <returns></returns>
        public IList<Transaction> GetDebitedButFailed(string successfulDebitCode, string[] transactionTypes)
        {
            var processingDelayMinutes = 5;

            var result = Session
                .QueryOver<Transaction>()
                .WhereRestrictionOn(x => x.Type).IsIn(transactionTypes)
                .And(x => x.DrResponseCode == successfulDebitCode)
                .And(x => x.StatusCode != null || 
                    (x.DrRespTime != null && x.DrRespTime <= DateTime.Now.AddMinutes(-processingDelayMinutes)))
                .And(x => x.Success == false)
                .And(x => x.IsReversed == false)
                .List();

            return result;
        }

        public IList<Transaction> GetForReversal(string successfulDebitCode, string[] transactionTypes, int daysBacklog)
        {
            var processingDelayMinutes = 5;

            var result = Session
                .QueryOver<Transaction>()
                .WhereRestrictionOn(x => x.Type).IsIn(transactionTypes)
                .And(x => x.DrResponseCode == successfulDebitCode)
                .And(x => x.StatusCode != null ||
                    (x.DrRespTime != null && x.DrRespTime <= DateTime.Now.AddMinutes(-processingDelayMinutes)))
                .And(x => x.Success == false)
                .And(x => x.IsReversed == false)
                .And(x => x.ReverseExceptionally == true || x.CreationTime >= DateTime.Today.AddDays(-daysBacklog))
                .List();

            return result;
        }

        public Transaction GetByClientRef(string clientRef)
        {
            var query = GetAll();

            var result = query.SingleOrDefault(x => x.ClientRefId == clientRef);

            return result;
        }


        public Transaction GetByRefId(string refId)
        {
            var query = GetAll();
            var result = query.SingleOrDefault(x => x.RefId == refId);
            return result;
        }

        public IList<Transaction> GetLatestTransactions(long userId, int count)
        {
            var query = GetAll();
            var result = query.Where(x => x.UserId == userId).OrderByDescending(x => x.CreationTime).Take(count).ToList();
            return result;
        }

        public IList<Transaction> GetLatestSuccessfulTransactions(long userId, int count)
        {
            var query = GetAll();
            var result = query.Where(x => x.UserId == userId && x.Success).OrderByDescending(x => x.Id).Take(count).ToList();
            return result;
        }
    }
}
