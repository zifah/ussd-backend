﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts
{
    public class NewBankAccountRepository : IntegrationsRepositoryBase<NewBankAccount, long>, INewBankAccountRepository
    {
        public NewBankAccountRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }

        public IList<NewBankAccount> GetByPhone(string phone)
        {
            var query = GetAll();
            var result = query.Where(x => x.PhoneNumber == phone).ToList();
            return result;
        }
    }
}
