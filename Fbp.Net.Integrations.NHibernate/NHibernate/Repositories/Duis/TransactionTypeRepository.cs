﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class TransactionTypeRepository : IntegrationsRepositoryBase<TransactionType, long>,  ITransactionTypeRepository
    {
        public TransactionTypeRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {

        }

        public TransactionType GetByName(string name)
        {
            var query = GetAll();
            var result = query.SingleOrDefault(x => x.Name == name);
            return result;
        }
    }
}
