﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Blacklists
{
    public class BlacklistRepository : IntegrationsRepositoryBase<Blacklist, long>, IBlacklistRepository
    {
        public BlacklistRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }

        public IList<Blacklist> GetByAccountMSISDN(string accountNumber, string msisdn)
        {
            var query = GetAll();

            IList<Blacklist> result = new List<Blacklist>();

            var isMSISDNBlank = string.IsNullOrWhiteSpace(msisdn);
            var isAccountBlank = string.IsNullOrWhiteSpace(accountNumber);

            var bothEmpty = isMSISDNBlank && isAccountBlank;
            var bothSupplied = !isMSISDNBlank && !isAccountBlank;

            if (bothEmpty)
            {
                //return empty list
            }

            else if (bothSupplied)
            {
                result = query.Where(x => 
                    (x.AccountNumber == accountNumber && x.Msisdn == msisdn) ||
                    (x.AccountNumber == accountNumber && (x.Msisdn == string.Empty || x.Msisdn == null)) ||
                    (x.Msisdn == msisdn && (x.AccountNumber == string.Empty || x.AccountNumber == null))).ToList();
            }

            else if (!isAccountBlank)
            {
                result = query.Where(x => x.AccountNumber == accountNumber).ToList();
            }

            else if (!isMSISDNBlank)
            {
                result = query.Where(x => x.Msisdn == msisdn).ToList();
            }

            return result.ToList();
        }
    }
}
