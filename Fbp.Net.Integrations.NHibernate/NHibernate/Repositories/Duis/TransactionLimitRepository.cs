﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.TransactionLimits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class TransactionLimitRepository : IntegrationsRepositoryBase<TransactionLimit, long>,  ITransactionLimitRepository
    {
        public TransactionLimitRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {
        }

        public TransactionLimit GetByUserTxnType(long txnTypeId, long userId)
        {
            TransactionLimit result = null;

            var query = GetAll();

            result = query.SingleOrDefault(x => x.TransactionType.Id == txnTypeId && x.User.Id == userId);

            return result;
        }

        public IList<TransactionLimit> GetCustomLimits(long userId, long? profileId)
        {
            var query = GetAll();

            var result = query.Where(x => x.User.Id == userId).ToList();

            if(profileId.HasValue)
            {
                var profileLimits = query.Where(x => x.UserProfile.Id == profileId.Value);

                foreach(var limit in profileLimits)
                {
                    // if a user does not have a custom limit for a particular transaction type, the profile based limit is used for the transaction
                    if(result.SingleOrDefault(x => x.TransactionType.Id.Equals(limit.TransactionType.Id)) == null)
                    {
                        result.Add(limit);
                    }
                }
            }

            return result;
        }
    }
}
