﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class EnrolmentRepository : IntegrationsRepositoryBase<Enrolment, long>,  IEnrolmentRepository
    {
        public EnrolmentRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {

        }

        public Enrolment GetByMsisdn(string msisdn, UserType userType)
        {
            var query = GetAll();

            var result = query.SingleOrDefault(x => x.Msisdn == msisdn && x.UserType == userType);

            return result;
        }

        public Enrolment GetByAccount(string accountNumber, UserType userType)
        {
            var query = GetAll();

            var result = query.SingleOrDefault(x => x.AccountNumber == accountNumber && x.UserType == userType);

            return result;
        }

        public IList<Enrolment> GetByMsisdnAcct(string msisdn, string accountNumber, UserType userType)
        {
            var result = new List<Enrolment>();

            var query = GetAll();

            result = query.Where(x => (x.AccountNumber == accountNumber || x.Msisdn == msisdn) && x.UserType == userType).ToList();

            return result;
        }

        public Enrolment GetByCustomerId(string customerId)
        {
            var query = GetAll();
            var result = query.SingleOrDefault(x => x.BankCustomerId == customerId);
            return result;
        }
    }
}
