﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Referrals
{
    public class ReferralRepository : IntegrationsRepositoryBase<Referral, long>, IReferralRepository
    {
        public ReferralRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {
        }

        public IList<Referral> GetReferrals(string referrerPhone)
        {
            var query = GetAll();
            var result = query.Where(x => x.ReferrerPhone == referrerPhone);
            return result.ToList();
        }
    }
}
