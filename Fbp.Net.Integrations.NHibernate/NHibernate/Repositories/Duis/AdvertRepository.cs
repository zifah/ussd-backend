﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Adverts
{
    public class AdvertRepository : IntegrationsRepositoryBase<Advert, long>, IAdvertRepository
    {
        public AdvertRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {

        }
    }
}
