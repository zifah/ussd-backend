﻿using Abp.NHibernate;
using Fbp.Net.Integrations.NHibernate.Repositories;
using System.Linq;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps
{
    public class ThirdPartyOtpRepository : IntegrationsRepositoryBase<ThirdPartyOtp, long>, IThirdPartyOtpRepository
    {
        public ThirdPartyOtpRepository(ISessionProvider sessionProvider)
            : base(sessionProvider)
        {
        }

        public ThirdPartyOtp GetLatestOtpByPhone(string phone, string purpose)
        {
            var query = GetAll();
            var result = query.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserMsisdn == phone && x.Purpose == purpose);             
            return result;
        }

        public ThirdPartyOtp GetLatestOtpByAccount(string account, string purpose)
        {
            var query = GetAll();
            var result = query.OrderByDescending(x => x.Id).FirstOrDefault(x => x.OtpAccount == account && x.Purpose == purpose);
            return result;
        }

        public ThirdPartyOtp GetLatestOtpByPhoneAccount(string phone, string account, string purpose)
        {
            var query = GetAll();
            var result = query
                .OrderByDescending(x => x.Id)
                .FirstOrDefault(x => x.UserMsisdn == phone && x.OtpAccount == account && x.Purpose == purpose);
            return result;
        }
    }
}
