﻿using Abp.NHibernate;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using System.Linq;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class PhoneNetworkLogRepository : IntegrationsRepositoryBase<PhoneNetworkLog, long>,  IPhoneNetworkLogRepository
    {
        public PhoneNetworkLogRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {
        }

        public PhoneNetworkLog GetPhoneNetworkLog(string msisdn)
        {
            var query = GetAll();

            var result = query.SingleOrDefault(x => x.Msisdn == msisdn);

            return result;
        }
    }
}
