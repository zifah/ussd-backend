﻿using Abp.NHibernate;
using Fbp.Net.Integrations.LoggedSMSs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.NHibernate.Repositories
{
    public class LoggedSMSRepository : IntegrationsRepositoryBase<LoggedSms, long>,  ILoggedSMSRepository
    {
        public LoggedSMSRepository(ISessionProvider sessionProvider) 
            : base(sessionProvider)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn">Phone number in +234... format</param>
        /// <returns></returns>
        public IList<LoggedSms> GetByRecipient(string msisdn)
        {
            var query = GetAll();
            var result = query.Where(x => x.Recipient.Equals(msisdn)).ToList();
            return result;
        }

        public LoggedSms GetLastLoggedByRecipient(string msisdn) 
        {
            var query = GetAll();
            var result = query.Where(x => x.Recipient == msisdn).OrderByDescending(x => x.CreationTime).FirstOrDefault();
            return result;
        }

        public IList<LoggedSms> GetByDate(DateTime date)
        {
            var query = GetAll();
            var result = query.Where(x => x.CreationTime.Date.Equals(date)).ToList();
            return result;
        }
    }
}
