﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;

namespace FidelityBank.MTNOnDemand
{
    [DependsOn(typeof(AbpWebApiModule), typeof(IntegrationsApplicationModule))]
    public class MTNOnDemandWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(IntegrationsApplicationModule).Assembly, "app")
                .Build();
        }
    }
}
