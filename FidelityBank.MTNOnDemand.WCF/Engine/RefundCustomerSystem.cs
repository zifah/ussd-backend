﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.AllowedSchemeCodes;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests.Dtos;
using FidelityBank.MTNOnDemand.Configurations;
using FidelityBank.MTNOnDemand.DrawdownRequests;
using FidelityBank.MTNOnDemand.Fincore;
using FidelityBank.MTNOnDemand.FTRequests.Dtos;
using FidelityBank.MTNOnDemand.RefundCustomerRequests;
using FidelityBank.MTNOnDemand.RefundCustomerRequests.Dtos;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class RefundCustomerSystem : ITransientDependency
    {
        private readonly IRefundCustomerRequestAppService _refundCustomerRequestSystemAppService;
        private readonly IChargeCustomerRequestAppService _chargeCustomerRequestSystemAppService;
        private readonly IDrawdownRequestAppService _drawdownAppService;
        private readonly FidelityBank.MTNOnDemand.FTRequests.IFTRequestAppService _ftRequestSystemAppService;
        private readonly ConfigurationSystem _configService;
        private static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];


        public RefundCustomerSystem(IRefundCustomerRequestAppService refundCustomerRequestSystemAppService,
            IChargeCustomerRequestAppService chargeCustomerRequestSystemAppService,
            FidelityBank.MTNOnDemand.FTRequests.IFTRequestAppService ftRequestSystemAppService,
            IDrawdownRequestAppService drawdownAppService, ConfigurationSystem configService)
        {
            _refundCustomerRequestSystemAppService = refundCustomerRequestSystemAppService;
            _chargeCustomerRequestSystemAppService = chargeCustomerRequestSystemAppService;
            _ftRequestSystemAppService = ftRequestSystemAppService;
            _drawdownAppService = drawdownAppService;
            _configService = configService;
        }

        public static bool ValidateInput(RefundCustomerInput input)
        {
            return input != null && input.RefCode != null && input.DestinationAccount != null && input.RequestTime != null;
        }

        public RefundCustomerRequestDTO ValidateAndCreateRequest(RefundCustomerInput input)
        {
            if (input == null)
            {
                throw new Exception("Request was empty");
            }

            RefundCustomerRequestDTO refundToTreat = null;

            refundToTreat = _refundCustomerRequestSystemAppService.GetByRefCode(input.RefCode);

            if (refundToTreat == null)
            {
                var ccRequest = ValidateNewRequest(input);

                refundToTreat = CreateNewRequest(input, ccRequest);
            }

            return refundToTreat;
        }

        private RefundCustomerRequestDTO CreateNewRequest(RefundCustomerInput input, ChargeCustomerRequestDTO ccRequest)
        {
            var newRefundRequest = new CreateRefundCustomerRequestInput
            {
                Amount = ccRequest.Amount,
                ClientNarration = input.Description,
                ClientRequestTime = input.RequestTime,
                SourceAccount = ccRequest.SourceAccount,
                DestinationAccount = ccRequest.DestinationAccount,
                MSISDN = ccRequest.MSISDN,
                RefCode = ccRequest.RefCode,
                ServerNarration = ccRequest.ServerNarration // GetNarration(input)
            };

            var newRefundDto = _refundCustomerRequestSystemAppService.CreateRefundCustomerRequest(newRefundRequest);

            return newRefundDto;
        }

        /// <summary>
        /// Will throw an exception in the case of any validation error
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private ChargeCustomerRequestDTO ValidateNewRequest(RefundCustomerInput input)
        {
            var chargeCustomerRequest = _chargeCustomerRequestSystemAppService.GetByRefCode(input.RefCode);

            // check that charge customer request with same ref code exists
            if (chargeCustomerRequest == null)
            {
                throw new CustomException("Original transaction was not found.");
            }

            // check that original transaction was successful
            if (chargeCustomerRequest.Status != ChargeCustomerStatus.Successful)
            {
                throw new CustomException("Original transaction was not successful.");
            }

            // check that phone and source account match those in the charge customer request
            if (chargeCustomerRequest.MSISDN != input.MSISDN || APIUtilities.MaskTenDigitAccountNumber(chargeCustomerRequest.SourceAccount) != input.SourceAccount)
            {
                throw new CustomException("Details do not match original request.");
            }

            #region Validate reversal date restriction
            var requestDate = chargeCustomerRequest.ClientRequestTime.Date;

            var dateSettlements = _drawdownAppService.GetBySweepDate(requestDate);

            if (dateSettlements.Count > 0)
            {
                throw new CustomException(string.Format("Settlement transaction has been passed for {0}. No reversals after EOD.", requestDate));
            }

            #endregion


            return chargeCustomerRequest;
        }

        private string GetNarration(RefundCustomerInput input)
        {
            return string.Format("MTN302 rchg. reversal {0} #{1}", input.MSISDN, input.RefCode);
        }

        internal RefundCustomerOutput TreatRequest(RefundCustomerRequestDTO refundRequest)
        {
            // check if response exist for request
            if (refundRequest.ResponseCode == new CoreSystem()._genericSuccessCode || 
                refundRequest.ServerNarration == APIConstants.ValueDefaultErrorChargebackNarration)
            {
                return new RefundCustomerOutput
                {
                    ErrorCode = refundRequest.ResponseCode,
                    ErrorDetails = refundRequest.ResponseDescription
                };
            }

            // get any previous reversal FT
            var ftRevRequest = _ftRequestSystemAppService.GetByParentRef(FTRequests.FTPurpose.RefundCustomer,
                refundRequest.RefCode).FirstOrDefault();

            if (ftRevRequest == null)
            {
                var matchingRequest = _ftRequestSystemAppService.GetByParentRef(FTRequests.FTPurpose.ChargeCustomer
                , refundRequest.RefCode).FirstOrDefault();

                ftRevRequest = CreateAndSaveFTRequest(matchingRequest);
            }

            var ftEngine = CoreSystem.bootstrapper.IocManager.Resolve<FTRequestSystem>();
            var ftResponse = ftEngine.DoFT(ftRevRequest);

            _ftRequestSystemAppService.UpdateFTRequest(
                new UpdateFTRequestInput
                {
                    ResponseCode = ftResponse.ResponseCode,
                    ResponseDescription = ftResponse.ResponseDescription,
                    RetRefNumber = ftRevRequest.RetRefNumber,
                    ResponseContent = JsonConvert.SerializeObject(ftResponse),
                    Direction = ftRevRequest.Direction
                });

            return InterpretAndUpdate(ftResponse, refundRequest);
        }

        private RefundCustomerOutput InterpretAndUpdate(FTResponse fincoreResponse, RefundCustomerRequestDTO refundRequest)
        {
            var apiResponse = new RefundCustomerOutput();

            var coreSystem = new CoreSystem();

            var fincoreSuccessCode = _configService.GetByName<string>(APIConstants.DbFincoreSuccessCode, APIConstants.ValueDefaultFincoreSuccessCode);
            var fincoreDuplicateCode = _configService.GetByName<string>(APIConstants.DbFincoreDuplicateCode, APIConstants.ValueDefaultFincoreDuplicateCode);


            if (fincoreResponse.ResponseCode == fincoreSuccessCode || fincoreResponse.ResponseCode == fincoreDuplicateCode)
            {
                // SET STATUS OF ORIGINAL TRANSACTION TO REVERSED
                _chargeCustomerRequestSystemAppService.UpdateChargeCustomerRequest(
                    new UpdateChargeCustomerRequestInput
                    {
                        RefCode = refundRequest.RefCode,
                        Status = ChargeCustomerStatus.Reversed
                    });

                apiResponse = CoreSystem.APIComposeAndRespond<RefundCustomerOutput>(coreSystem._genericSuccessCode,
                    coreSystem._genericSuccessMessage);
            }

            else
            {
                apiResponse = CoreSystem.APIComposeAndRespond<RefundCustomerOutput>(coreSystem._genericErrorCode,
                    fincoreResponse.ResponseDescription);
            }

            _refundCustomerRequestSystemAppService.UpdateRefundCustomerRequest(
                    new UpdateRefundCustomerRequestInput
                    {
                        FincoreResponseCode = fincoreResponse.ResponseCode,
                        RefCode = refundRequest.RefCode,
                        ResponseCode = apiResponse.ErrorCode,
                        ResponseDescription = apiResponse.ErrorDetails
                    });

            return apiResponse;
        }

        private FTRequestDTO CreateAndSaveFTRequest(FTRequestDTO matchingRequest)
        {
            var newFTRequest = new CreateFTRequestInput
                            {
                                Amount = matchingRequest.Amount,
                                DestinationAccount = matchingRequest.DestinationAccount,
                                Direction = FTRequests.FTRequestOrReversal.Reversal,
                                ParentRef = matchingRequest.ParentRef,
                                Purpose = FTRequests.FTPurpose.RefundCustomer,
                                RetRefNumber = matchingRequest.RetRefNumber,
                                SourceAccount = matchingRequest.SourceAccount,
                                Narration = matchingRequest.Narration,  //string.Format("Rvrsl {0}", matchingRequest.Narration)
                                FincoreRequestTime = matchingRequest.FincoreRequestTime
                            };

            return _ftRequestSystemAppService.CreateFTRequest(newFTRequest);
        }

        internal void Save(CreateRefundCustomerRequestInput input)
        {
            _refundCustomerRequestSystemAppService.CreateRefundCustomerRequest(input);
        }
    }
}