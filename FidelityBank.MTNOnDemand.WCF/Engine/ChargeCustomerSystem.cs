﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.AllowedSchemeCodes;
using FidelityBank.MTNOnDemand.Blacklists;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests.Dtos;
using FidelityBank.MTNOnDemand.Configurations;
using FidelityBank.MTNOnDemand.Fincore;
using FidelityBank.MTNOnDemand.GetAccountRequests;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class ChargeCustomerRequestSystem : ITransientDependency
    {
        private readonly IChargeCustomerRequestAppService _chargeCustomerRequestSystemAppService;
        private readonly IConfigurationAppService _configurationAppService;
        private readonly IAllowedSchemeCodeAppService _allowedSchemeCodeAppService;
        private readonly IGetAccountRequestAppService _getAccountsService;
        private readonly IBlacklistAppService _blacklistAppService;
        private static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];

        public ChargeCustomerRequestSystem(IChargeCustomerRequestAppService chargeCustomerRequestSystemAppService,
            IConfigurationAppService configurationAppService, IAllowedSchemeCodeAppService allowedSchemeCodeAppService,
            IGetAccountRequestAppService getAccountsService, IBlacklistAppService blacklistAppService)
        {
            _chargeCustomerRequestSystemAppService = chargeCustomerRequestSystemAppService;
            _configurationAppService = configurationAppService;
            _allowedSchemeCodeAppService = allowedSchemeCodeAppService;
            _getAccountsService = getAccountsService;
            _blacklistAppService = blacklistAppService;
        }

        internal ChargeCustomerRequestDTO SaveChargeCustomerRequest(CreateChargeCustomerRequestInput request)
        {
            return _chargeCustomerRequestSystemAppService.CreateChargeCustomerRequest(request);
        }

        internal void UpdateChargeCustomerRequest(UpdateChargeCustomerRequestInput request)
        {
            _chargeCustomerRequestSystemAppService.UpdateChargeCustomerRequest(request);
        }

        public ChargeCustomerRequestDTO GetByRefCode(string refCode)
        {
            var result = _chargeCustomerRequestSystemAppService.GetByRefCode(refCode);
            return result;
        }

        /// <summary>
        /// Returns destination suspense account if all validations are successful
        /// </summary>
        /// <param name="input"></param>
        /// <param name="destinationAccount"></param>
        /// <returns></returns>
        internal string ValidateRequest(ChargeCustomerInput input)
        {
            if (input == null)
            {
                throw new Exception("Request was NULL.");
            }

            input.SourceAccount = GetUnmaskedAccountNumber(input);

            var isBlacklisted = _blacklistAppService.IsBlacklistMatch(input.SourceAccount, input.MSISDN);
            
            if(isBlacklisted)
            {
                throw new AccountBlockedException("The account/MSISDN has been blacklisted.");
            }

            ValidateEnrollment(input);

            // get account details
            var infopoolAccount = InfopoolSystem.GetAccountDetails(input.SourceAccount, APIUtilities.StandardizeNigerianMsisdn(input.MSISDN));
                //new FincoreSystem(_fincoreResponseFilePath).GetAccountDetails(input.SourceAccount, input.MSISDN);

            if (infopoolAccount == null)
            {
                throw new CustomException("Account number does not match supplied MSISDN");
            }

            #region Do Generic Checks
            // No longer checking account eligibility since only mPin eligible accounts are returned at lookup stage
            // ConfirmAccountEligibility(fincoreAccount);
            // confirm that daily limit has not been exceeded

            CheckDailyLimitExceeded(input);
            #endregion

            var destinationAccount = GetBranchSuspenseAccount(infopoolAccount.SolId);
            return destinationAccount;
        }

        private void ValidateEnrollment(ChargeCustomerInput input)
        {
            var phone = input.MSISDN;
            var shouldCheckEnrollment = ShouldCheckEnrollment(phone);

            if(shouldCheckEnrollment)
            {
                InfopoolSystem.CheckmPinStatus(phone);
            }

            LoggingSystem.LogMessage(string.Format("{0} is {1} enrolled.", phone, shouldCheckEnrollment ? "now confirmed" : "already"));
            // check for enrollment on any of the accounts
            // if not enrolled, throw not enrolled exception
        }

        private string GetUnmaskedAccountNumber(ChargeCustomerInput input)
        {
            var account = input.SourceAccount;
            // get latest GetAccountRequest with this number
            var latestGArequest = _getAccountsService.GetLatestBy(input.MSISDN);

            if (latestGArequest != null)
            {
                // deserialize the returned accounts
                var accounts = JsonConvert.DeserializeObject<IList<Account>>(latestGArequest.ResponseAccountDetails);

                // return the full account number from the deserialized object
                var ourAccount = accounts.FirstOrDefault(x => APIUtilities.AreAccountsEqual(account, x.Number));

                if (ourAccount != null)
                {
                    account = ourAccount.Number;
                }
            }

            return account;
        }

        private void CheckDailyLimitExceeded(ChargeCustomerInput input)
        {
            //TODO: Write limit checking logic
            // get 24 hour limit
            decimal twentyFourHourLimit = _configurationAppService.GetByName<decimal>(APIConstants.DbChargeCustomer24HrLimit, 0);

            var upperBoundDate = DateTime.Now;//input.RequestTime;

            decimal maxAmountAllowed = _chargeCustomerRequestSystemAppService
                .GetMaxAllowedAmount(input.MSISDN, twentyFourHourLimit, upperBoundDate);

            if (maxAmountAllowed < input.Amount)
            {
                throw new LimitExceededException("Daily limit has been exceeded. Please, try again later.");
            }
        }

        public void ConfirmAccountEligibility(AccountDetails fincoreAccount)
        {
            InfopoolAccount account = null;

            try
            {
                account = InfopoolSystem.GetAccount(fincoreAccount.Number);
            }

            catch (Exception ex)
            {
                LoggingSystem.LogMessage(
                    string.Format("Falling back to local scheme codes table to confirm account eligibility for {0} due to Infopool communication error.", fincoreAccount.Number));
                LoggingSystem.LogException(ex);
                CheckSchemeCode(fincoreAccount.SchemeCode);
            }

            if (!account.IsmPinEligible)
            {
                throw new AccountBlockedException("Transaction not permitted to customer");
            }
        }

        private void CheckSchemeCode(string schemeCode)
        {
            var isSchemeCodeAllowed = _allowedSchemeCodeAppService.IsSchemeCodeAllowed(schemeCode);

            //TODO: Write scheme code validation logic

            if (isSchemeCodeAllowed == false)
            {
                throw new AccountBlockedException("Transaction not permitted to customer");
            }
        }

        internal string GetBranchSuspenseAccount(string solId)
        {
            var suspenseAccount = _configurationAppService.GetByName<string>(APIConstants.DbMTNOdSuspenseAccount, null);
            int suspenseAccountLength = _configurationAppService.GetByName<int>(APIConstants.DbMTNOdSuspenseAccountLength, 0);

            if (suspenseAccount != null)
            {
                suspenseAccount = suspenseAccount.ToUpper();

                if (suspenseAccount.Length == suspenseAccountLength && suspenseAccount.StartsWith("NGN"))
                {
                    return suspenseAccount.Replace("SOL", solId);
                }

                else
                {
                    throw new CustomException("Incorrect suspense account configuration.");
                }
            }

            throw new CustomException("Incorrect suspense account configuration.");
        }

        internal string GetNarration(ChargeCustomerInput input)
        {
            return string.Format("MTN ONDEMAND AIRTIME RECHARGE FOR {0}", input.MSISDN);
        }

        public static bool ValidateInput(ChargeCustomerInput input)
        {
            //TODO: do preliminary validation on other fields
            return input != null && input.RequestTime != null && input.RefCode != null;
        }

        public bool ShouldCheckEnrollment(string phone)
        {
            var enrollmentCheckIntervalDays = _configurationAppService.GetByName<int>(APIConstants.DbEnrollmentCheckIntervalDays,
               APIConstants.ValueDefaultEnrollmentCheckIntervalDays);

            var latestTransaction = _chargeCustomerRequestSystemAppService.GetLatestSuccessfulByMSISDN(phone);

            var shouldCheckEnrollment = latestTransaction == null || latestTransaction.CreationTime.Date <= DateTime.Today.AddDays(-enrollmentCheckIntervalDays);
            return shouldCheckEnrollment;
        }
    }
}