﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.Configurations;
using FidelityBank.MTNOnDemand.Fincore;
using FidelityBank.MTNOnDemand.FTRequests.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class FTRequestSystem : ITransientDependency
    {
        private readonly FidelityBank.MTNOnDemand.FTRequests.IFTRequestAppService _ftRequestAppService;
        private readonly IConfigurationAppService _configService;
        private static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];
        private static readonly object Locker = new object();
        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public FTRequestSystem()
        {

        }

        public FTRequestSystem(FidelityBank.MTNOnDemand.FTRequests.IFTRequestAppService ftRequestAppService,
            IConfigurationAppService configService)
        {
            _ftRequestAppService = ftRequestAppService;
            _configService = configService;
        }

        private bool IsRRNUsed(int rrn)
        {
            var result = _ftRequestAppService.IsRRNUsed(rrn);
            return result;
        }

        public FTRequestDTO CreateRequest(CreateFTRequestInput input)
        {
            return _ftRequestAppService.CreateFTRequest(input);
        }

        public FTRequestDTO UpdateRequest(UpdateFTRequestInput input)
        {
            return _ftRequestAppService.UpdateFTRequest(input);
        }

        public IList<FTRequestDTO> GetFTRequest(FidelityBank.MTNOnDemand.FTRequests.FTPurpose purpose, string refCode)
        {
            var result = _ftRequestAppService.GetByParentRef(purpose, refCode);
            return result;
        }

        public FTResponse DoFT(FTRequestDTO ftRequest)
        {
            var fincore = new FincoreSystem(_fincoreResponseFilePath);

            // transfer was already successful. Don't retry to prevent duplicate response from Fincore
            if (ftRequest.ResponseCode == _configService.GetByName<string>(APIConstants.DbFincoreSuccessCode, APIConstants.ValueDefaultFincoreSuccessCode))
            {
                return new FTResponse
                {
                    ResponseCode = ftRequest.ResponseCode,
                    ResponseDescription = ftRequest.ResponseDescription
                };
            }

            else
            {
                FTRequest fincoreRequest = new FTRequest
                            {
                                Amount = (double)ftRequest.Amount,
                                FromAcct = ftRequest.SourceAccount,
                                ToAcct = ftRequest.DestinationAccount,
                                RetrievalRefNum = ftRequest.RetRefNumber,
                                Narration = ftRequest.Narration,
                                TransactionTime = ftRequest.FincoreRequestTime
                            };

                FTResponse fincoreOutput = null;

                if (ftRequest.Direction == FTRequests.FTRequestOrReversal.Request)
                {
                    fincoreOutput = fincore.DoFundsTransfer(fincoreRequest);
                }

                else if (ftRequest.Direction == FTRequests.FTRequestOrReversal.Reversal)
                {
                    fincoreOutput = fincore.ReverseFundsTransfer(fincoreRequest);
                }

                return fincoreOutput;
            }
        }

        /// <summary>
        /// Requests only
        /// </summary>
        /// <param name="input"></param>
        public void AsyncDoFTWithUpdate(FTRequestDTO input)
        {
            LoggingSystem.LogMessage(string.Format("Started {0} FT with RRN: {1} and parent ref: {2} at {3}.", input.Purpose, input.RetRefNumber, input.ParentRef, DateTime.Now));
            var ftResponse = DoFT(input);

            input = UpdateRequest(new UpdateFTRequestInput
            {
                ResponseCode = ftResponse.ResponseCode,
                ResponseDescription = ftResponse.ResponseDescription,
                ResponseContent = JsonConvert.SerializeObject(ftResponse),
                RetRefNumber = input.RetRefNumber,
                Direction = input.Direction
            });
            LoggingSystem.LogMessage(string.Format("Ended {0} FT with RRN: {1} and parent ref: {2} at {3}.", input.Purpose, input.RetRefNumber, input.ParentRef, DateTime.Now));
        }

        /// <summary>
        /// Generates an used retrieval reference number for a funds transfer operation
        /// </summary>
        /// <returns>RRN</returns>
        public int GetNewFTRRN()
        {
            int rrn = random.Value.Next(1, Int32.MaxValue);

            var exists = IsRRNUsed(rrn);

            if (exists)
            {
                return GetNewFTRRN();
            }

            return rrn;
        }
    }

    public class FTRequestAsyncInput
    {
        public FTRequestDTO Request { set; get; }
        public ManualResetEvent ResetEvent { set; get; }
    }
}