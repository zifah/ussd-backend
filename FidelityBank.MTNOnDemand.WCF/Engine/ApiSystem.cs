﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using FidelityBank.MTNOnDemand.Fincore;
using System.ServiceModel.Web;
using FidelityBank.MTNOnDemand.RawRequests.Dtos;
using Abp.Dependency;
using FidelityBank.MTNOnDemand.RawRequests;
using FidelityBank.MTNOnDemand.GetAccountRequests.Dtos;
using Newtonsoft.Json;
using Abp;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests.Dtos;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using System.Configuration;
using FidelityBank.MTNOnDemand.FTRequests.Dtos;
using FidelityBank.MTNOnDemand.RefundCustomerRequests.Dtos;
using FidelityBank.MTNOnDemand.DrawdownRequests.Dtos;
using FidelityBank.MTNOnDemand.DrawdownRequests;
using FidelityBank.MTNOnDemand.InstantBankingEnrols.Dtos;
using CoreUtility = FidelityBank.CoreLibraries.Utility;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class ApiSystem : CoreSystem
    {
        //engines
        private static ChargeCustomerRequestSystem _ccEngine = null;
        private static FTRequestSystem _ftEngine = null;
        private static RefundCustomerSystem _refundEngine = null;
        private static DrawdownRequestSystem _drawdownEngine = null;
        private static SMSSendingSystem _smsEngine = null;
        private static EnrolInstantBankingSystem _enrolmentSystem = null;

        public ApiSystem()
            : base()
        {
            //engine initializations
            _ccEngine = bootstrapper.IocManager.Resolve<ChargeCustomerRequestSystem>();
            _ftEngine = bootstrapper.IocManager.Resolve<FTRequestSystem>();
            _refundEngine = bootstrapper.IocManager.Resolve<RefundCustomerSystem>();
            _drawdownEngine = bootstrapper.IocManager.Resolve<DrawdownRequestSystem>();
            _smsEngine = bootstrapper.IocManager.Resolve<SMSSendingSystem>();
            _enrolmentSystem = bootstrapper.IocManager.Resolve<EnrolInstantBankingSystem>();
        }

        internal GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input)
        {
            var engine = bootstrapper.IocManager.Resolve<GetAccountRequestSystem>();
            var result = engine.GetAccountsByMSISDN(input);
            return result;
        }

        internal ChargeCustomerOutput ChargeCustomer(ChargeCustomerInput input)
        {
            LoggingSystem.LogMessage(string.Format("Start ChargeCustomer #{0}", input.RefCode));

            ChargeCustomerRequestDTO existing = null;

            //TODO: Check if transaction exists in the database
            if (DoesTransactionExist(input.RefCode, out existing))
            {
                // get status of current transaction or retry if no status yet and revert
                if (existing.ResponseCode != null)
                {
                    return APIComposeAndRespond<ChargeCustomerOutput>(existing.ResponseCode, existing.ResponseDescription);
                }

                else
                {
                    return CompleteChargeCustomerRequest(existing);
                }

                // if no status yet, check for previous FTRequest to Fincore
                // if exists, check for status of Fincore request
                // if status exists at Fincore, update request status and respond
                // If not ex
                // otherwise, return existing status
            }

            else
            {
                #region Compose request
                var newRequest = new CreateChargeCustomerRequestInput
                {
                    Amount = input.Amount,
                    ClientNarration = input.Description,
                    ClientRequestTime = input.RequestTime,
                    SourceAccount = input.SourceAccount,
                    MSISDN = input.MSISDN,
                    ServerNarration = _ccEngine.GetNarration(input),
                    RefCode = input.RefCode,
                    //DestinationAccount = destinationAccount
                };
                #endregion

                string destinationAccount = null;

                try
                {
                    destinationAccount = _ccEngine.ValidateRequest(input);
                    newRequest.SourceAccount = input.SourceAccount;
                }

                catch (LimitExceededException ex)
                {
                    var code = _configEngine.GetByName<string>(APIConstants.DbLimitExceededErrorCode, _genericErrorCode);
                    var message = ex.Message;
                    return SaveErrorRequest(newRequest, code, message);
                }

                catch (AccountBlockedException ex)
                {
                    var code = _configEngine.GetByName<string>(APIConstants.DbAccountBlockedErrorCode, _genericErrorCode);
                    var message = ex.Message;
                    return SaveErrorRequest(newRequest, code, message);
                }

                catch (NotEnrolledException ex)
                {
                    _smsEngine.SendEnrollmentSMS(input.MSISDN);
                    var code = _configEngine.GetByName<string>(APIConstants.DbNotEnrolledErrorCode, _genericErrorCode);
                    var message = ex.Message;
                    return SaveErrorRequest(newRequest, code, message);
                }

                catch (CustomException ex)
                {
                    return SaveErrorRequest(newRequest, _genericErrorCode, ex.Message);
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex,
                        string.Format("Error processing ChargeCustomerRequest #{0}.", input == null ? "(Input was null)" : input.RefCode));
                    throw ex;
                }

                newRequest.DestinationAccount = destinationAccount;
                var savedCcRequest = _ccEngine.SaveChargeCustomerRequest(newRequest);

                return DoFT(savedCcRequest);
                //Populate output
            }
        }

        private ChargeCustomerOutput SaveErrorRequest(CreateChargeCustomerRequestInput input, string code, string message)
        {
            input.ResponseCode = code;
            input.ResponseDescription = message;
            input.Status = FidelityBank.MTNOnDemand.ChargeCustomerRequests.ChargeCustomerStatus.Failed;
            _ccEngine.SaveChargeCustomerRequest(input);
            return APIComposeAndRespond<ChargeCustomerOutput>(code, message);
        }

        private RefundCustomerOutput SaveErrorRequest(CreateRefundCustomerRequestInput input, string code, string message)
        {
            input.ResponseCode = code;
            input.ResponseDescription = message;
            _refundEngine.Save(input);
            return APIComposeAndRespond<RefundCustomerOutput>(code, message);
        }

        private DrawdownOutput SaveErrorRequest(CreateDrawdownRequestInput input, string code, string message)
        {
            input.ResponseCode = code;
            input.ResponseDescription = message;
            _drawdownEngine.Save(input);
            return APIComposeAndRespond<DrawdownOutput>(code, message);
        }

        private ChargeCustomerOutput DoFT(ChargeCustomerRequestDTO ccRequest, FTRequestDTO ftRequest = null)
        {
            LoggingSystem.LogMessage(string.Format("Started ChargeCustomer FT for #{0}", ccRequest.RefCode));
            var fincore = new FincoreSystem(_fincoreResponseFilePath);

            if (ftRequest == null)
            {
                ftRequest = ComposeAndSaveFTRequest(ccRequest);
            }

            if (ftRequest.ResponseCode == null)
            {
                try
                {
                    #region Transfer funds
                    var fincoreOutput = _ftEngine.DoFT(ftRequest);
                    //fincore.DoFundsTransfer(fincoreRequest);

                    _ftEngine.UpdateRequest(
                        new UpdateFTRequestInput
                        {
                            RetRefNumber = ftRequest.RetRefNumber,
                            ResponseCode = fincoreOutput.ResponseCode,
                            ResponseDescription = fincoreOutput.ResponseDescription,
                            ResponseContent = JsonConvert.SerializeObject(fincoreOutput),
                            Direction = FTRequests.FTRequestOrReversal.Request
                        });

                    LoggingSystem.LogMessage(string.Format("Ended ChargeCustomer FT for #{0}", ccRequest.RefCode));
                    return InterpretFincoreResponse(fincoreOutput.ResponseCode, fincoreOutput.ResponseDescription, ccRequest.RefCode);
                    #endregion
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, string.Format("Error doing FT with RRN: {0} at {1}", ftRequest.RetRefNumber, DateTime.Now));
                    SetHTTPStatusCode(500);
                    LoggingSystem.LogMessage(string.Format("Ended ChargeCustomer FT for #{0}", ccRequest.RefCode));
                    return APIComposeAndRespond<ChargeCustomerOutput>(_genericErrorCode, _genericErrorMessage);
                }
            }

            else
            {
                LoggingSystem.LogMessage(string.Format("Ended ChargeCustomer FT for #{0}", ccRequest.RefCode));
                return InterpretFincoreResponse(ftRequest.ResponseCode, ftRequest.ResponseDescription, ccRequest.RefCode);
            }
        }

        private ChargeCustomerOutput InterpretFincoreResponse(string fincoreResponseCode, string fincoreResponseDesc, string ccRequestRefCode)
        {
            if (fincoreResponseCode == APIConstants.ValueDefaultFincoreSuccessCode || fincoreResponseCode == APIConstants.ValueDefaultFincoreDuplicateCode)
            {
                var apiResponse = APIComposeAndRespond<ChargeCustomerOutput>(APIConstants.ValueDefaultSuccessCode,
                    APIConstants.ValueDefaultSuccessMessage);

                UpdateCCRequest(apiResponse, fincoreResponseCode, ChargeCustomerRequests.ChargeCustomerStatus.Successful, ccRequestRefCode);

                return apiResponse;
            }

            else
            {
                var fincoreInsufficientFundsCode = _configEngine.GetByName<string>(APIConstants.DbFincoreInsufficientFundsCode, APIConstants.ValueDefaultFincoreInsufficientFundsCode);
                var insufficientFundsResponseCode = _configEngine.GetByName<string>(APIConstants.DbInsufficientFundsErrorCode, APIConstants.ValueDefaultInsufficientFundsErrorCode);

                var responseErrorCode = _genericErrorCode;

                if (fincoreResponseCode.Equals(fincoreInsufficientFundsCode))
                {
                    responseErrorCode = insufficientFundsResponseCode;
                }

                var apiResponse = APIComposeAndRespond<ChargeCustomerOutput>(responseErrorCode, fincoreResponseDesc);
                UpdateCCRequest(apiResponse, fincoreResponseCode, ChargeCustomerRequests.ChargeCustomerStatus.Failed, ccRequestRefCode);
                return apiResponse;
            }
        }

        private ChargeCustomerOutput CompleteChargeCustomerRequest(ChargeCustomerRequestDTO existing)
        {
            var ftRequest = _ftEngine.GetFTRequest(FidelityBank.MTNOnDemand.FTRequests.FTPurpose.ChargeCustomer, existing.RefCode).FirstOrDefault();

            return DoFT(existing, ftRequest);
        }

        private FTRequestDTO ComposeAndSaveFTRequest(ChargeCustomerRequestDTO ccRequest)
        {
            var ftRequest = new CreateFTRequestInput
            {
                Amount = ccRequest.Amount,
                SourceAccount = ccRequest.SourceAccount,
                DestinationAccount = ccRequest.DestinationAccount,
                Purpose = FidelityBank.MTNOnDemand.FTRequests.FTPurpose.ChargeCustomer,
                ParentRef = ccRequest.RefCode,
                RetRefNumber = _ftEngine.GetNewFTRRN(),
                Direction = FTRequests.FTRequestOrReversal.Request,
                Narration = ccRequest.ServerNarration,
                FincoreRequestTime = ccRequest.CreationTime
            };

            var output = _ftEngine.CreateRequest(ftRequest);

            return output;
        }

        private bool DoesTransactionExist(string refCode, out ChargeCustomerRequestDTO existing)
        {
            var engine = bootstrapper.IocManager.Resolve<ChargeCustomerRequestSystem>();
            existing = engine.GetByRefCode(refCode);

            return existing != null;
        }

        internal RefundCustomerOutput RefundCustomer(RefundCustomerInput input)
        {
            FidelityBank.MTNOnDemand.RefundCustomerRequests.Dtos.RefundCustomerRequestDTO refundRequest = null;

            try
            {
                refundRequest = _refundEngine.ValidateAndCreateRequest(input);
            }

            catch (CustomException ex)
            {
                var newRefundRequest = new CreateRefundCustomerRequestInput
                {
                    Amount = input.Amount,
                    ClientNarration = input.Description,
                    ClientRequestTime = input.RequestTime,
                    SourceAccount = input.SourceAccount,
                    DestinationAccount = input.DestinationAccount,
                    MSISDN = input.MSISDN,
                    RefCode = input.RefCode,
                    ServerNarration = APIConstants.ValueDefaultErrorChargebackNarration
                };

                return SaveErrorRequest(newRefundRequest, _genericErrorCode, ex.Message);
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex,
                        string.Format("Error processing RefundCustomerRequest #{0}.", input == null ? "(Input was null)" : input.RefCode));

                throw ex;
            }

            var output = _refundEngine.TreatRequest(refundRequest);

            return output;
        }

        /// <summary>
        /// Commits each call to API endpoint to database before processing
        /// </summary>
        internal void SaveRawRequest(string methodName, string body, string objectType)
        {
            var request = new CreateRawRequestInput
            {
                Content = body,
                MethodName = methodName,
                ObjectName = objectType,
                SourceIPAddress = APIUtilities.GetClientIPAddress()
            };

            var engine = bootstrapper.IocManager.Resolve<RawRequestSystem>();
            engine.SaveRawRequest(request);
        }

        internal DrawdownOutput Drawdown(DrawdownInput input)
        {
            DrawdownRequestDTO drawdownRequest = null;

            DrawdownOutput output = null;

            try
            {
                drawdownRequest = _drawdownEngine.ValidateAndCreateRequest(input);
            }

            catch (CustomException ex)
            {
                var newRefundRequest = new CreateDrawdownRequestInput
                {
                    ClientRequestTime = input.RequestTime,
                    BulkAmount = input.BulkAmount,
                    SweepDate = input.SweepDate,
                    RefCode = input.RefCode,
                    Status = DrawdownStatus.Failed
                };

                return SaveErrorRequest(newRefundRequest, _genericErrorCode, ex.Message);
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex,
                        string.Format("Error processing DrawdownRequest #{0}.", input == null ? "(Input was null)" : input.RefCode));

                throw ex;
            }

            try
            {
                output = _drawdownEngine.TreatRequest(drawdownRequest);
            }

            catch
            {
                string responseCode = string.Empty;
                string responseDescription = string.Empty;

                responseCode = _genericSuccessCode;
                responseDescription = _genericSuccessMessage;

                APIUtilities.SetHTTPStatusCode(500);

                _drawdownEngine.UpdateDrawdownRequest(new UpdateDrawdownRequestInput
                {
                    Status = DrawdownStatus.Failed,
                    RefCode = drawdownRequest.RefCode,
                    ResponseCode = responseCode,
                    ResponseDescription = responseDescription
                });

                output = CoreSystem.APIComposeAndRespond<DrawdownOutput>(responseCode, responseDescription);
            }

            return output;
        }

        public void SetHTTPStatusCode(int code)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)code;
        }

        private void UpdateCCRequest(ChargeCustomerOutput apiResponse, string fincoreResponse,
            FidelityBank.MTNOnDemand.ChargeCustomerRequests.ChargeCustomerStatus status, string refCode)
        {
            _ccEngine.UpdateChargeCustomerRequest(
                            new UpdateChargeCustomerRequestInput
                            {
                                FincoreResponseCode = fincoreResponse,
                                ResponseCode = apiResponse.ErrorCode,
                                ResponseDescription = apiResponse.ErrorDetails,
                                RefCode = refCode,
                                Status = status
                            });
        }

        public bool AuthenticateRequest(IAPIInput input)
        {
            var authEngine = bootstrapper.IocManager.Resolve<AuthenticationSystem>();

            return authEngine.AuthenticateRequest(input);
        }

        internal EnrolInstantBankingOutput EnrolInstantBanking(EnrolInstantBankingInput input)
        {
            var response = new EnrolInstantBankingOutput
            {
                RefCode = input.RefCode,
            };

            // check if enrolment exists for this customer
            InstantBankingEnrolDto existing = _enrolmentSystem.GetByAccountMsisdn(input.Msisdn, input.AccountNumber);

            if (existing == null)
            {
                try
                {
                    existing = _enrolmentSystem.Create(input);
                }

                catch (AccountMsisdnMismatchException ex)
                {
                    var responseCode = _configEngine.GetByName<string>(APIConstants.DbAccountMsisdnMismatchErrorCode,
                        APIConstants.ValueDefaultAccountMsisdnMismatchErrorCode);

                    response.ErrorCode = responseCode;
                    response.ErrorDetails = ex.Message;
                    return response;
                    //exit with appropriate response code
                }

                response.ErrorCode = _genericSuccessCode;
                response.ErrorDetails = APIConstants.ValueDefaultEnrolPasswordSelectMessage;
            }

            else
            {
                // if there's password, and enrollment status is notactivated, set password and set account status to activated and return good response to Qrios
                if (existing.Status == InstantBankingEnrols.EnrolStatus.PendingPinSelection)
                {
                    if (string.IsNullOrWhiteSpace(input.Password))
                    {
                        response.ErrorCode = _genericErrorCode;
                        response.ErrorDetails = "Password was expected but not supplied.";
                    }

                    else
                    {
                        var bcryptPassword = CoreUtility.Engine.SecurityUtil.GenerateBCryptHash(input.Password);

                        string password = bcryptPassword.Hash;

                        var updateObject = new UpdateInstantBankingEnrolInput
                        {
                            Id = existing.Id,
                            Password = password,
                            DateEnrolled = DateTime.Now,
                            Status = InstantBankingEnrols.EnrolStatus.Activated
                        };

                        var upToDate = _enrolmentSystem.Update(updateObject);
                        response.ErrorCode = _genericSuccessCode;
                        response.ErrorDetails = "Enrolment successful.";
                    }
                }

                else if (existing.Status == InstantBankingEnrols.EnrolStatus.UpdateMode)
                {
                    var updateObject = new UpdateInstantBankingEnrolInput
                    {
                        Id = existing.Id,
                        AccountNumber = existing.AccountNumber,
                        Msisdn = existing.Msisdn,
                        Password = string.Empty,
                        //Customer has to select new password after account vital information is updated
                        Status = InstantBankingEnrols.EnrolStatus.PendingPinSelection
                    };

                    var upToDate = _enrolmentSystem.Update(updateObject);
                    response.ErrorCode = _genericSuccessCode;
                    response.ErrorDetails = APIConstants.ValueDefaultEnrolPasswordSelectMessage;
                }

                else
                {
                    //every other status means no change should be made to the enrolment as enrolment is completed already
                    var responseCode = _configEngine.GetByName<string>(APIConstants.DbAlreadyEnrolledErrorCode,
                        APIConstants.ValueDefaultAlreadyEnrolledErrorCode);
                    response.ErrorCode = responseCode;
                    response.ErrorDetails = "Customer is already enrolled.";
                }
            }

            return response;
        }
    }
}