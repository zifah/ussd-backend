﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class DummyApiSystem : CoreSystem
    {
        public DummyApiSystem()
            : base()
        {

        }

        public ChargeCustomerOutput ChargeCustomer(ChargeCustomerInput input)
        {
            var response = new ChargeCustomerOutput() { ErrorCode = _genericErrorCode, ErrorDetails = _genericErrorMessage };

            return response;
        }

        public GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input)
        {
            var response = new GetAccountsByMSISDNOutput { ErrorCode = _genericSuccessCode, ErrorDetails = _genericSuccessMessage };

            if (GetAccountRequestSystem.ValidateInput(input))
            {
                response.ErrorCode = _genericSuccessCode;
                response.ErrorDetails = _genericSuccessMessage;

                response.Accounts.Add(
                    new Account
                    {
                        Balance = Convert.ToDecimal(input.MSISDN.Substring(8, 3)),
                        Name = "Bartek Piekarski",
                        Number = input.MSISDN.Substring(0, 10)
                    });

                response.Accounts.Add(
                    new Account
                    {
                        Balance = Convert.ToDecimal(input.MSISDN.Substring(5, 3)),
                        Name = "Bartek Piekarski",
                        Number = new String(input.MSISDN.Reverse().ToArray()).Substring(0, 10)
                    });
            }

            return response;
        }

        public RefundCustomerOutput RefundCustomer(RefundCustomerInput input)
        {
            var response = new RefundCustomerOutput() { ErrorCode = _genericErrorCode, ErrorDetails = _genericErrorMessage };

            return response;
        }

        public DrawdownOutput Drawdown(DrawdownInput input)
        {
            var response = new DrawdownOutput { ErrorCode = _genericErrorCode, ErrorDetails = _genericErrorMessage };

            return response;
        }

        internal EnrolInstantBankingOutput EnrolInstantBanking(EnrolInstantBankingInput input)
        {
            var response = new EnrolInstantBankingOutput { ErrorCode = _genericSuccessCode, ErrorDetails = _genericSuccessMessage };
            return response;
        }
    }
}