﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.Fincore;
using FidelityBank.MTNOnDemand.DrawdownRequests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using FidelityBank.MTNOnDemand.Configurations;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests;
using FidelityBank.MTNOnDemand.DrawdownRequests.Dtos;
using FidelityBank.MTNOnDemand.DrawdownConfigurations;
using FidelityBank.MTNOnDemand.DrawdownConfigurations.Dtos;
using FTRequestsDTOs = FidelityBank.MTNOnDemand.FTRequests.Dtos.FTRequestDTO;
using FidelityBank.MTNOnDemand.FTRequests.Dtos;
using FidelityBank.MTNOnDemand.FTRequests;
using System.Threading;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class DrawdownRequestSystem : ITransientDependency
    {
        private static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];

        private readonly IConfigurationAppService _configService;
        private readonly IDrawdownRequestAppService _drawdownService;
        private readonly IChargeCustomerRequestAppService _ccService;
        private readonly IDrawdownConfigurationAppService _drawdownConfigService;
        private readonly FTRequestSystem _ftService;

        public DrawdownRequestSystem(IConfigurationAppService configService, IDrawdownRequestAppService drawdownService,
            IChargeCustomerRequestAppService ccService, IDrawdownConfigurationAppService drawdownConfigService,
            FTRequestSystem ftService)
        {
            _configService = configService;
            _drawdownService = drawdownService;
            _ccService = ccService;
            _drawdownConfigService = drawdownConfigService;
            _ftService = ftService;
        }

        public static bool ValidateInput(DrawdownInput input)
        {
            return input != null && input.BulkAmount > 0 && input.RefCode != null && input.RequestTime != null && input.SweepDate != null;
        }

        internal void Save(DrawdownRequests.Dtos.CreateDrawdownRequestInput input)
        {
            _drawdownService.CreateDrawdownRequest(input);
        }

        internal DrawdownRequestDTO ValidateAndCreateRequest(DrawdownInput input)
        {
            DrawdownRequestDTO response = _drawdownService.GetByRefCode(input.RefCode);

            if (response == null)
            {
                // confirm sweep date is not more than one day ago
                ValidateSweepDate(input);
                // confirm transaction amount is not more that total unreversed amount for specified date
                ValidateSettlementAmount(input);
                //create new request

                response = _drawdownService.CreateDrawdownRequest(new CreateDrawdownRequestInput
                {
                    BulkAmount = input.BulkAmount,
                    ClientRequestTime = input.RequestTime,
                    RefCode = input.RefCode,
                    SweepDate = input.SweepDate,
                    SourceAccount = GetDrawdownSourceAccount()
                });
            }

            return response;
        }

        private IList<DrawdownConfigurationDTO> GetDrawdownDestinationAccounts(out int numberOfAccounts)
        {
            var beneficiaryAccounts = _drawdownConfigService.GetAll(true);
            numberOfAccounts = beneficiaryAccounts.Count;
            return beneficiaryAccounts;//JsonConvert.SerializeObject(beneficiaryAccounts);
        }

        private string GetDrawdownSourceAccount()
        {
            string holdingAccount = _configService.GetByName<string>(APIConstants.DbDrawdownSourceAccount, string.Empty);

            if (string.IsNullOrWhiteSpace(holdingAccount))
            {
                throw new Exception("Invalid configuration; drawdown source account is missing");
            }

            return holdingAccount;
        }

        private void ValidateSettlementAmount(DrawdownInput input)
        {
            decimal totalUnreversedAmount = _ccService.GetTotalUnreversed(input.SweepDate);

            if (totalUnreversedAmount < input.BulkAmount)
            {
                throw new CustomException(string.Format("Settlement amount is too high."));
            }
        }

        private void ValidateSweepDate(DrawdownInput input)
        {
            var isDrawdownDue = DateTime.Today > input.SweepDate.Date;

            if (!isDrawdownDue)
            {
                throw new CustomException(string.Format("Auto-sweep cannot be done until T+1 from sweep date."));
            }

            //int maxSweepBacklogDays = _configService.GetByName<int>(APIConstants.DbMaxSweepBacklogDays, APIConstants.ValueDefaultMaxSweepBacklogDays);
            //DateTime minimumDate = DateTime.Today.AddDays(-maxSweepBacklogDays);

            //var isDateTooOld = input.SweepDate.Date < minimumDate;

            //if (isDateTooOld)
            //{
            //    throw new CustomException(string.Format("Auto-sweep must be done within {0} day{1} from transaction date.",
            //        maxSweepBacklogDays, maxSweepBacklogDays == 1 ? string.Empty : "(s)"));
            //}
        }

        internal DrawdownOutput TreatRequest(DrawdownRequestDTO drawdownRequest)
        {
            if (drawdownRequest.ResponseCode != null)
            {
                return new DrawdownOutput
                {
                    ErrorCode = drawdownRequest.ResponseCode,
                    ErrorDetails = drawdownRequest.ResponseDescription
                };
            }

            // check for pre-existing request with same refCode and sweepDate; replay if no status or partially completed
            var previouslyRecordedFTs = _drawdownService.GetFTsForSweepDate(drawdownRequest.SweepDate);

            var drawdownConfigs = _drawdownConfigService.GetAll(true);

            if (previouslyRecordedFTs.Count == 0)
            {

                if (drawdownConfigs.Sum(x => x.Percentage) != 100)
                {
                    throw new CustomException("Drawdown configuration does not sum up to 100%");
                }

                previouslyRecordedFTs = ConvertConfigToFTs(drawdownConfigs, drawdownRequest);

            }

            try
            {
                LoggingSystem.LogMessage(string.Format("About to process FTs for drawdown #{0}", drawdownRequest.RefCode));
                ProcessFTs(previouslyRecordedFTs);
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Error processing FTs for drawdown #{0}", drawdownRequest.RefCode));
                throw new CustomException("System error; please, try later.");
            }

            LoggingSystem.LogMessage(string.Format("About to start post FT process for drawdown #{0}", drawdownRequest.RefCode));
            LoggingSystem.LogMessage("Started getting FT requests for drawdown from DB after processing");
            previouslyRecordedFTs = _drawdownService.GetFTsForSweepDate(drawdownRequest.SweepDate);
            LoggingSystem.LogMessage("Finished getting FT requests for drawdown from DB after processing");
            var response = PostFTProcessing(previouslyRecordedFTs, drawdownRequest, drawdownConfigs.Select(x => x.AccountNumber).ToArray());
            LoggingSystem.LogMessage(string.Format("Finished post FT process for drawdown #{0}", drawdownRequest.RefCode));
            return response;
        }

        private DrawdownOutput PostFTProcessing(IList<FTRequestDTO> ftRequests, DrawdownRequestDTO drawdownRequest, string[] destinationAccounts = null)
        {
            var fincoreResponseCodes = ftRequests.Select(x => x.ResponseCode).Distinct();
            var codesList = string.Empty;

            foreach (var code in fincoreResponseCodes)
            {
                codesList = string.Format("{1}{0};", codesList, code);
            }

            LoggingSystem.LogMessage(string.Format("Unique Fincore codes for Drawdown #{0}: {1}", drawdownRequest.RefCode, codesList));

            var defaultFincoreSuccessCode = _configService.GetByName<string>(APIConstants.DbFincoreSuccessCode,
                APIConstants.ValueDefaultFincoreSuccessCode);

            var defaultFincoreDuplicateCode = _configService.GetByName<string>(APIConstants.DbFincoreDuplicateCode,
                APIConstants.ValueDefaultFincoreDuplicateCode);


            var coreSystem = new CoreSystem();

            string responseCode = string.Empty;
            string responseDescription = string.Empty;

            var successfulCodes = new string[] { defaultFincoreSuccessCode, defaultFincoreDuplicateCode };

            if (fincoreResponseCodes.Count() == 1 && successfulCodes.Contains(fincoreResponseCodes.Single()))
            {
                responseCode = coreSystem._genericSuccessCode;
                responseDescription = coreSystem._genericSuccessMessage;

                _drawdownService.UpdateDrawdownRequest(new UpdateDrawdownRequestInput
                {
                    Status = DrawdownStatus.Completed,
                    RefCode = drawdownRequest.RefCode,
                    DestinationAccounts = destinationAccounts == null ? null : JsonConvert.SerializeObject(destinationAccounts),
                    SplitCount = ftRequests.Count,
                    ResponseCode = responseCode,
                    ResponseDescription = responseDescription
                });
            }

            else
            {
                responseCode = coreSystem._genericErrorCode;
                responseDescription = "Drawdown could not be completed. Please, try again.";

                _drawdownService.UpdateDrawdownRequest(new UpdateDrawdownRequestInput
                {
                    RefCode = drawdownRequest.RefCode,
                    DestinationAccounts = destinationAccounts == null ? null : JsonConvert.SerializeObject(destinationAccounts),
                    SplitCount = ftRequests.Count,
                    ResponseCode = responseCode,
                    ResponseDescription = responseDescription
                });

                //throw new CustomException(string.Format("Drawdown could not be completed. Please, retry with same refCode #{0}", drawdownRequest.RefCode));
            }

            return CoreSystem.APIComposeAndRespond<DrawdownOutput>(responseCode, responseDescription);
        }

        private void ProcessFTs(IList<FTRequestsDTOs> ftRequests)
        {
            ThreadPool.SetMinThreads(ftRequests.Count, ftRequests.Count);

            var tasks = new List<Task>();

            foreach (var request in ftRequests)
            {
                _ftService.AsyncDoFTWithUpdate(request);
            }

            //foreach (var request in ftRequests)
            //{
            //    var task = Task.Factory.StartNew(() => { _ftService.AsyncDoFTWithUpdate(request); });
            //    tasks.Add(task);
            //}

            //foreach (var task in tasks)
            //{
            //    task.Wait(30000);
            //}            

            //if (doneEvents.Count > 0)
            //{
            //    try
            //    {
            //        WaitHandle.WaitAll(doneEvents.ToArray());
            //    }

            //    catch (Exception ex)
            //    {
            //        LoggingSystem.LogException(ex, "Unable too wait for child threads. Too many threads, probably. Exiting now");
            //        return;
            //    }
            //}

            LoggingSystem.LogMessage("Successfully processed all drawdown FTs");
        }

        /// <summary>
        /// Generates and saves new FT entries (to handle sweep) to database
        /// </summary>
        /// <param name="drawdownConfigs"></param>
        /// <param name="drawdownRequest"></param>
        /// <returns></returns>
        private IList<FTRequestsDTOs> ConvertConfigToFTs(IList<DrawdownConfigurationDTO> drawdownConfigs, DrawdownRequestDTO drawdownRequest)
        {
            IList<FTRequestsDTOs> ftRequests = new List<FTRequestsDTOs>();
            // generate the create request inputs

            var dictionary = new Dictionary<int, decimal>();
            // Get split amount
            for (int i = 0; i < drawdownConfigs.Count; i++)
            {
                dictionary.Add(drawdownConfigs[i].Id, drawdownConfigs[i].Percentage);
            }

            var ratios = APIUtilities.RatioSplitter(drawdownRequest.BulkAmount, dictionary);

            foreach (var config in drawdownConfigs)
            {
                var ftRequestDto = _ftService.CreateRequest(new CreateFTRequestInput
                {
                    Amount = ratios[config.Id],
                    SourceAccount = drawdownRequest.SourceAccount,
                    DestinationAccount = config.AccountNumber,
                    Direction = FTRequests.FTRequestOrReversal.Request,
                    ParentRef = drawdownRequest.RefCode,
                    Purpose = FTRequests.FTPurpose.Drawdown,
                    RetRefNumber = _ftService.GetNewFTRRN(),
                    Narration = string.Format("MTN302 income for {0:dd MMM yyyy}", drawdownRequest.SweepDate),
                    FincoreRequestTime = drawdownRequest.CreationTime
                });

                ftRequests.Add(ftRequestDto);
            }

            // save the create inputs
            // return the list
            return ftRequests;
        }

        internal void UpdateDrawdownRequest(UpdateDrawdownRequestInput input)
        {
            _drawdownService.UpdateDrawdownRequest(input);
        }
    }
}