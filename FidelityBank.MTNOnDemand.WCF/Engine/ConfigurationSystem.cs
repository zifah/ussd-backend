﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.Configurations;
using FidelityBank.MTNOnDemand.Configurations.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class ConfigurationSystem : ITransientDependency
    {
        private static readonly Object _lock = new Object();

        private readonly IConfigurationAppService _configurationAppService;

        private static IList<ConfigurationDTO> _configCache = new List<ConfigurationDTO>();

        public ConfigurationSystem()
        {

        }

        public ConfigurationSystem(IConfigurationAppService configurationAppService)
        {
            _configurationAppService = configurationAppService;
        }

        public T GetByName<T>(string name, T defaultValue)
        {
            RefreshCache();

            T result = defaultValue;

            try
            {
                var config = _configCache.FirstOrDefault(x => x.Name == name); // _configurationAppService.GetByName<T>(name, defaultValue);

                if (config != null)
                {
                    result = (T)Convert.ChangeType(config.Value, typeof(T));
                }
            }

            catch (Exception ex)
            {
                LoggingSystem.LogException(ex, string.Format("Error getting value of app config: {0}. Default value {1} was used", name, defaultValue));
            }

            return result;
        }

        private void RefreshCache()
        {
            lock (_lock)
            {
                if (_configCache.Count == 0)
                {
                    _configCache = _configurationAppService.GetAll();
                }
            }
        }
    }
}