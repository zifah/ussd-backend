﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests;
using FidelityBank.MTNOnDemand.Fincore;
using FidelityBank.MTNOnDemand.GetAccountRequests;
using FidelityBank.MTNOnDemand.GetAccountRequests.Dtos;
using FidelityBank.MTNOnDemand.LoggedSMSs;
using FidelityBank.MTNOnDemand.LoggedSMSs.Dtos;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class GetAccountRequestSystem : ITransientDependency
    {
        private readonly IGetAccountRequestAppService _getAccountRequestAppService;
        private readonly IChargeCustomerRequestAppService _chargeCustomerRequestAppService;
        private readonly ConfigurationSystem _configSystem;
        private readonly SMSSendingSystem _smsSendingSystem;
        private static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];


        public GetAccountRequestSystem(IGetAccountRequestAppService getAccountRequestAppService, ConfigurationSystem configSystem,
             SMSSendingSystem smsSendingSystem, IChargeCustomerRequestAppService chargeCustomerRequestAppService)
        {
            _getAccountRequestAppService = getAccountRequestAppService;
            _configSystem = configSystem;
            _chargeCustomerRequestAppService = chargeCustomerRequestAppService;
            _smsSendingSystem = smsSendingSystem;
        }


        internal void SaveNewRequest(CreateGetAccountRequestInput record)
        {
            _getAccountRequestAppService.CreateGetAccountRequest(record);
        }

        internal GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input)
        {
            LoggingSystem.LogMessage("Entered GABM");
            var response = new GetAccountsByMSISDNOutput();

            var theAccounts = new List<Account>();

            var record = new CreateGetAccountRequestInput
            {
                ClientRequestTime = input.RequestTime,
                MSISDN = input.MSISDN
            };

            APIUtilities.SetResponseCodeDesc(response, APIConstants.ValueDefaultErrorCode, "Bad input");

            if (input != null && APIUtilities.ValidateMSISDN(input.MSISDN))
            {

                //TODO: Query Fincore for linked accounts
                var msisdn = input.MSISDN;
                var fincore = new FincoreSystem(_fincoreResponseFilePath);

                int accountNumberLength = _configSystem.GetByName<int>(APIConstants.DbAccountNumberLength, APIConstants.ValueDefaultAccountNumberLength);

                try
                {
                    LoggingSystem.LogMessage("Started Fincore get MSISDN accounts");
                    var standardMsisdn = APIUtilities.StandardizeNigerianMsisdn(msisdn);
                    var msisdnAccounts = InfopoolSystem.GetAccountByMsisdn(standardMsisdn); //fincore.GetAccountsByMsisdn(msisdn);
                    LoggingSystem.LogMessage("Completed Fincore get MSISDN accounts.");

                    if (msisdnAccounts.Count() > 0)
                    {
                        try
                        {
                            foreach (var acct in msisdnAccounts)
                            {
                                if (!string.IsNullOrWhiteSpace(acct.AccountNumber) && acct.AccountNumber.Length.Equals(accountNumberLength))
                                {
                                    theAccounts.Add(new Account
                                    {
                                        Name = GetAccountName(acct.AccountNumber, acct.AccountName),
                                        Number = acct.AccountNumber,
                                        MaskedNumber = APIUtilities.MaskTenDigitAccountNumber(acct.AccountNumber)
                                    });
                                }
                            }

                            // there used to be a call to check customer enrollment status on mPin right here

                            record.ResponseAccountDetails = JsonConvert.SerializeObject(theAccounts);

                            foreach (var account in theAccounts)
                            {
                                account.Number = account.MaskedNumber;
                            }

                            response.Accounts = theAccounts;

                            APIUtilities.SetResponseCodeDesc(response, APIConstants.ValueDefaultSuccessCode, APIConstants.ValueDefaultSuccessMessage);
                        }

                        catch (NotEnrolledException ex)
                        {
                            var errorCodeNotEnrolled = _configSystem.GetByName<string>(APIConstants.DbNotEnrolledErrorCode, APIConstants.ValueDefaultNotEnrolledErrorCode);
                            _smsSendingSystem.SendEnrollmentSMS(msisdn);
                            APIUtilities.SetResponseCodeDesc(response, errorCodeNotEnrolled, ex.Message);
                        }

                        catch (AccountBlockedException ex)
                        {
                            var errorCodeAccountBlocked = _configSystem.GetByName<string>(APIConstants.DbAccountBlockedErrorCode, APIConstants.ValueDefaultErrorMessage);
                            APIUtilities.SetResponseCodeDesc(response, errorCodeAccountBlocked, ex.Message);
                        }
                    }

                    else
                    {
                        var msisdnNotFoundCode = _configSystem.GetByName<string>(APIConstants.DbMsisdnNotFoundErrorCode,
                            APIConstants.ValueDefaultMsisdnNotFoundErrorCode);

                        APIUtilities.SetResponseCodeDesc(response, msisdnNotFoundCode, "MSISDN not found");
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, string.Format("Error getting accounts linked to {0} from Infopool.", msisdn));
                    APIUtilities.SetResponseCodeDesc(response, APIConstants.ValueDefaultErrorCode, "Error getting information from source!");
                    APIUtilities.SetHTTPStatusCode(500); // so client can retry the request
                }
            }

            record.ResponseCode = response.ErrorCode;
            record.ResponseDescription = response.ErrorDetails;

            SaveNewRequest(record);

            return response;
        }

        

        private string GetAccountName(string accountNumber, string accountName)
        {
            var dictionary = new Dictionary<string, string>();

            var savings = _configSystem.GetByName<string>(APIConstants.DbSavingsAccountStarters, APIConstants.ValueSavingsAccountStarters);
            var current = _configSystem.GetByName<string>(APIConstants.DbCurrentAccountStarters, APIConstants.ValueCurrentAccountStarters);

            savings.Split(',').ToList().ForEach(x => dictionary.Add(x, "SAVINGS"));
            current.Split(',').ToList().ForEach(x => dictionary.Add(x, "CURRENT"));

            LoggingSystem.LogMessage(string.Format("Account to get type of: {0}", accountNumber));

            string accountType = dictionary[accountNumber[0].ToString()];

            return string.Format("{0}({1})", APIUtilities.MaskTenDigitAccountNumber(accountNumber), accountType);
        }

        /// <summary>
        /// This method is deprecated
        /// </summary>
        /// <param name="list"></param>
        /// <param name="phone">The 11-digit phone number </param>
        private void CheckmPinStatus(List<Account> list, string phone)
        {
            LoggingSystem.LogMessage("Entered checkmPin status");
            var enrollmentCheckIntervalDays = _configSystem.GetByName<int>(APIConstants.DbEnrollmentCheckIntervalDays,
                APIConstants.ValueDefaultEnrollmentCheckIntervalDays);

            var latestTransaction = _chargeCustomerRequestAppService.GetLatestSuccessfulByMSISDN(phone);

            var shouldCheckEnrollment = latestTransaction == null || latestTransaction.CreationTime.Date <= DateTime.Today.AddDays(-enrollmentCheckIntervalDays);

            if (shouldCheckEnrollment)
            {
                // run the query on all account numbers
                IList<string> accountNumbers = list.Select(x => x.Number).ToList();

                var account = InfopoolSystem.IsmPinActive(accountNumbers);
                // do not return account number list for an invalid account
                if (account == null || !account.IsmPinActive)
                {
                    LoggingSystem.LogMessage(string.Format("{0} is not enrolled.", phone));
                    throw new NotEnrolledException("Enrollment error.");
                }

                else if (!string.IsNullOrWhiteSpace(account.HoldResponseCode))
                {
                    LoggingSystem.LogMessage(string.Format("Latest Instant Banking card for {0} has been hotlisted because value is {1}.", phone, account.HoldResponseCode));
                    throw new AccountBlockedException("Account is blocked.");
                }
            }

            LoggingSystem.LogMessage(string.Format("{0} is {1} enrolled.", phone, shouldCheckEnrollment ? "now confirmed" : "already"));
            LoggingSystem.LogMessage("Exiting checkmPin status");
        }        

        public static bool ValidateInput(GetAccountsByMSISDNInput input)
        {
            return input != null && input.RequestTime != null;
        }
    }
}