﻿using FidelityBank.MTNOnDemand.WCF.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class InfopoolSystem
    {
        private const string InfopoolGeneral = "InfopoolGeneral";
        private const string InfopoolCustom = "InfopoolCustom";

        /// <summary>
        /// Make this method private. Outside subscribers should be disconnected in due time
        /// </summary>
        /// <param name="accountNumbers"></param>
        /// <returns></returns>
        public static InfopoolAccount IsmPinActive(IList<string> accountNumbers)
        {
            #region Serialize account numbers
            var accountsList = string.Empty;
            var serialAccounts = string.Empty;

            for (int i = 0; i < accountNumbers.Count; i++)
            {
                accountsList = string.Format("{0},@account_{1}", accountsList, i);
                serialAccounts = string.Format("{0},{1}", serialAccounts, accountNumbers[i]);
            }

            accountsList = accountsList.Trim(',');
            serialAccounts = serialAccounts.Trim(',');

            #endregion

            LoggingSystem.LogMessage(string.Format("Start mPin enrollment check {0}", serialAccounts));
            string connectionString = ConfigurationManager.ConnectionStrings[InfopoolGeneral].ConnectionString;

            InfopoolAccount result = new InfopoolAccount { };

            // Provide the query string with a parameter placeholder.
            string queryString =
                @"select top 1 a.account_id account_number, b.date_activated date_activated, b.card_status is_active, b.hold_rsp_code hold_rsp_code
from [172.25.32.31].postcard.dbo.pc_card_accounts a with(nolock)
inner join [172.25.32.31].postcard.dbo.pc_cards_1_A b with(nolock) on (a.pan = b.pan and a.issuer_nr = b.issuer_nr and a.seq_nr = b.seq_nr)
where a.account_id in (#accounts_list#)
and b.card_program = 'FBP_QTMOBILE'
and b.card_status = '1'
and b.date_activated > '01 Jan 2000'
order by b.date_activated desc;";

            queryString = queryString.Replace("#accounts_list#", accountsList);
            LoggingSystem.LogMessage(accountsList);

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                for (int i = 0; i < accountNumbers.Count; i++)
                {
                    command.Parameters.Add(new SqlParameter(string.Format("@account_{0}", i),
                        System.Data.SqlDbType.NVarChar)).Value = accountNumbers[i];
                }


                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();

                    using (command)
                    {
                        var dataReader = command.ExecuteReader();

                        using (dataReader)
                        {
                            if (dataReader.Read())
                            {
                                LoggingSystem.LogMessage(string.Format("Returned information from Postcard for accounts: {0}", serialAccounts));
                                // assign account number
                                result.AccountNumber = Convert.ToString(dataReader["account_number"]);

                                // assign mPin eligible
                                result.IsmPinActive = Convert.ToBoolean(dataReader["is_active"]);

                                //assign mPinPhone
                                //result.MPinPhone = Convert.ToString(dataReader["phone_number"]);

                                result.DateActivated = Convert.ToDateTime(dataReader["date_activated"]);

                                result.HoldResponseCode = Convert.ToString(dataReader["hold_rsp_code"]);

                                LoggingSystem.LogMessage(string.Format("mPin account: {0}", JsonConvert.SerializeObject(result)));
                            }

                            else
                            {
                                LoggingSystem.LogMessage(string.Format("Did not return information from Postcard for accounts: {0}", serialAccounts));
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, string.Format("System error confirming mPin status for accounts: {0}.", serialAccounts));
                    throw ex;
                }
            }

            LoggingSystem.LogMessage(string.Format("Emd mPin enrollment check {0}", serialAccounts));
            return result;
        }

        /// <summary>
        /// IsmPinEligible is always true because the lookup table contains only mPin eligible numbers
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public static InfopoolAccount GetAccount(string accountNumber)
        {
            LoggingSystem.LogMessage(string.Format("Start GetAccountDetails {0}", accountNumber));
            string connectionString = ConfigurationManager.ConnectionStrings[InfopoolGeneral].ConnectionString;

            InfopoolAccount result = new InfopoolAccount
            {
                AccountNumber = accountNumber,
                IsmPinEligible = false
            };

            // Provide the query string with a parameter placeholder.
            string queryString =
                @"select phone, account_number from bx_ondemand_lookup_base where account_number = @account_number;";

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.Add(new SqlParameter("@account_number", System.Data.SqlDbType.NVarChar)).Value = accountNumber;

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    using (command)
                    {
                        var dataReader = command.ExecuteReader();

                        using (dataReader)
                        {
                            if (dataReader.Read())
                            {
                                // assign account number
                                result.AccountNumber = Convert.ToString(dataReader["account_number"]);

                                // assign mPin eligible
                                result.IsmPinEligible = true; //dataReader["mpin_eligible"] != null ? Convert.ToBoolean(dataReader["mpin_eligible"]) : false;

                                //assign mPinPhone
                                result.MPinPhone = Convert.ToString(dataReader["phone"]);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, "Could not confirm account mPin eligibility.");
                    throw ex;
                }
            }

            LoggingSystem.LogMessage(string.Format("End GetAccountDetails {0}", accountNumber));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn">IThis should be provided in +234 format</param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        public static void SendSMS(string msisdn, string subject, string content)
        {
            string connectionString = ConfigurationManager.ConnectionStrings[InfopoolCustom].ConnectionString;

            InfopoolAccount result = new InfopoolAccount { };

            // Provide the query string with a parameter placeholder.
            string queryString =
                @"insert into ebanking.dbo.SMS_Log (phone,message,Batch_ID) values (@phone,@message,@subject);";

            #region Serialize account numbers
            var accountsList = string.Empty;
            var serialAccounts = string.Empty;

            #endregion

            LoggingSystem.LogMessage(string.Format("About to log {0} for {1} on Infopool", subject, msisdn));

            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.Add(new SqlParameter("@phone", System.Data.SqlDbType.NVarChar)).Value = msisdn;
                command.Parameters.Add(new SqlParameter("@message", System.Data.SqlDbType.NVarChar)).Value = content;
                command.Parameters.Add(new SqlParameter("@subject", System.Data.SqlDbType.NVarChar)).Value = subject;


                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    using (command)
                    {
                        connection.Open();
                        var rowsAffected = command.ExecuteNonQuery();
                        LoggingSystem.LogMessage(string.Format("Successfully inserted {0} rows into infopool SMS table", rowsAffected));
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, "An error occurred. Could not log SMS to infopool.");
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn">This is in +234... format</param>
        /// <returns></returns>
        public static IList<InfopoolAccount> GetAccountByMsisdn(string msisdn)
        {
            LoggingSystem.LogMessage(string.Format("Start GetAccountByMsisdn {0}", msisdn));

            string connectionString = ConfigurationManager.ConnectionStrings[InfopoolGeneral].ConnectionString;

            var result = new List<InfopoolAccount>();

            // Provide the query string with a parameter placeholder.
            string queryString =
                @"select account_number, account_name from bx_ondemand_lookup_base where phone = @phone_number order by account_number;";

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.Add(new SqlParameter("@phone_number", System.Data.SqlDbType.NVarChar)).Value = msisdn;

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    using (command)
                    {
                        var dataReader = command.ExecuteReader();

                        using (dataReader)
                        {
                            while (dataReader.Read())
                            {
                                result.Add(
                                    new InfopoolAccount
                                    {
                                        AccountNumber = Convert.ToString(dataReader["account_number"]),
                                        AccountName = Convert.ToString(dataReader["account_name"]),
                                        IsmPinEligible = true
                                    });
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, string.Format("System error. Could not do msisdn lookup for {0}!", msisdn));
                    throw ex;
                }
            }

            LoggingSystem.LogMessage(string.Format("End GetAccountByMsisdn {0}", msisdn));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="account">Ten-digit account number</param>
        /// <param name="msisdn">msisdn in standard format</param>
        /// <returns></returns>
        internal static InfopoolAccount GetAccountDetails(string account, string msisdn)
        {
            LoggingSystem.LogMessage(string.Format("Start GetAccountDetails {0}-{1}", account, msisdn));
            string connectionString = ConfigurationManager.ConnectionStrings[InfopoolGeneral].ConnectionString;

            InfopoolAccount result = null;

            // Provide the query string with a parameter placeholder.
            string queryString =
                @"select account_number, account_name, schm_code, sol_id, bvn from bx_ondemand_lookup_base
                        where account_number = @account_number and phone = @phone;";

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.Add(new SqlParameter("@account_number", System.Data.SqlDbType.NVarChar)).Value = account;
                command.Parameters.Add(new SqlParameter("@phone", System.Data.SqlDbType.NVarChar)).Value = msisdn;

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    using (command)
                    {
                        var dataReader = command.ExecuteReader();

                        using (dataReader)
                        {
                            if (dataReader.Read())
                            {
                                result = new InfopoolAccount
                                    {
                                        AccountNumber = Convert.ToString(dataReader["account_number"]),
                                        AccountName = Convert.ToString(dataReader["account_name"]),
                                        SolId = Convert.ToString(dataReader["sol_id"]),
                                        SchmCode = Convert.ToString(dataReader["schm_code"]),
                                        IsmPinEligible = true, 
                                        MPinPhone = msisdn,
                                        BVN = Convert.ToString(dataReader["bvn"])
                                    };
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogException(ex, string.Format("System error. Could not do account + msisdn lookup for {0}, {1}!", account, msisdn));
                    throw ex;
                }
            }

            LoggingSystem.LogMessage(string.Format("End GetAccountDetails {0}-{1}", account, msisdn));
            return result;
        }

        /// <summary>
        /// Will throw exception if there is an enrollment problem
        /// </summary>
        /// <param name="phone"></param>
        public static void CheckmPinStatus(string phone)
        {
            //get accounts
            var standardMsisdn = APIUtilities.StandardizeNigerianMsisdn(phone);
            var accounts = GetAccountByMsisdn(standardMsisdn);

            var numbers = accounts.Select(x => x.AccountNumber).ToList();

            if (numbers.Count() > 0)
            {
                var account = IsmPinActive(numbers);
                // do not return account number list for an invalid account
                if (account == null || !account.IsmPinActive || !string.IsNullOrWhiteSpace(account.HoldResponseCode))
                {
                    LoggingSystem.LogMessage(string.Format("{0} is not enrolled.", phone));
                    throw new NotEnrolledException("Enrollment error.");
                }

                LoggingSystem.LogMessage(string.Format("{0} is enrolled on Instant banking.", phone));
            }

            else
            {
                LoggingSystem.LogMessage(string.Format("{0} is not linked to any accounts.", phone));
                throw new NotEnrolledException("Enrollment error.");
            }
        }
    }

    public class InfopoolAccount
    {
        public string AccountNumber { set; get; }

        public string AccountName { set; get; }

        public bool IsmPinEligible { set; get; }

        public string MPinPhone { set; get; }

        public bool IsmPinActive { set; get; }

        public DateTime DateActivated { set; get; }

        public string HoldResponseCode { set; get; }

        public string SolId { set; get; }

        public string SchmCode { set; get; }

        public string BVN { set; get; }

        public string FinaclePhone { set; get; }
        public string AlertPhone { set; get; }
    }
}