﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.APIClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.ServiceModel.Web;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class AuthenticationSystem : ITransientDependency
    {
        public readonly IAPIClientAppService _apiClientEngine;

        public AuthenticationSystem()
        {

        }

        public AuthenticationSystem(IAPIClientAppService apiClientEngine)
        {
            _apiClientEngine = apiClientEngine;
        }

        public bool AuthenticateRequest(IAPIInput input)
        {
            WebHeaderCollection headers = WebOperationContext.Current.IncomingRequest.Headers;

            string clientHash = headers[APIConstants.APIHeaderHashKey];
            string clientId = headers[APIConstants.APIClientId];

            var key = GetClientKey(clientId);

            if (string.IsNullOrWhiteSpace(clientHash) || string.IsNullOrWhiteSpace(key))
            {
                // client is not properly set-up since there's no key
                return false;
            }

            else
            {
                string serverHash = ComputeHash(input, key);

                return serverHash.ToUpper() == clientHash.ToUpper();
            }
        }

        public string ComputeHash(IAPIInput input, string key)
        {
            // compute the hash using the details of the request combined with the key
            Dictionary<string, string> properties = new Dictionary<string, string>();

            var sortedProperties = input.GetType().GetProperties().OrderBy(x => x.Name);

            string propertiesConcat = string.Empty;

            Type myType = input.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties().OrderBy(x => x.Name));

            foreach (PropertyInfo prop in props)
            {
                string propValue = string.Empty;

                var propType = prop.PropertyType;
                
                var rawValue = prop.GetValue(input);

                if (propType == typeof(Decimal))
                {
                    propValue = ((Decimal)rawValue).ToString("N2").Replace(",", string.Empty);
                }

                else if (propType == typeof(DateTime))
                {
                    propValue = ((DateTime)rawValue).ToString("dd|MMM|yyyy/HH:mm:ss");
                }

                else
                {
                    propValue = Convert.ToString(rawValue);
                }

                // Do something with propValue
                if (!string.IsNullOrWhiteSpace(propValue))
                {
                    propertiesConcat = string.Format("{0}{1}", propertiesConcat, propValue);
                }
            }

            string toHash = propertiesConcat;

            string hashValue = SHA256Hash(toHash, key);

            return hashValue;
        }

        private string SHA256Hash(string message, string key)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(key);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);

            byte[] messageBytes = encoding.GetBytes(message);

            byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
            return ByteToString(hashmessage);
        }

        private static string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }

        private string GetClientKey(string clientId)
        {
            string key = string.Empty;
            var apiClient = _apiClientEngine.GetByClientId(clientId);

            if (apiClient != null)
            {
                key = apiClient.SharedKey;
            }

            return key;
        }
    }
}