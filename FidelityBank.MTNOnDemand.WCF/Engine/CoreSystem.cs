﻿using Abp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class CoreSystem
    {
        public string _genericErrorCode { private set; get; }
        public string _genericErrorMessage { private set; get; }
        public string _genericSuccessCode { private set; get; }
        public string _genericSuccessMessage { private set; get; }

        protected static ConfigurationSystem _configEngine = null;
        protected static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];
        public static readonly AbpBootstrapper bootstrapper = AbpDIManager.AbpBootstraper;

        public CoreSystem()
        {
            _configEngine = bootstrapper.IocManager.Resolve<ConfigurationSystem>();
            _genericSuccessCode = _configEngine.GetByName<string>(APIConstants.DbGenericSuccessCode, APIConstants.ValueDefaultSuccessCode);
            _genericSuccessMessage = APIConstants.ValueDefaultSuccessMessage;
            _genericErrorCode = _configEngine.GetByName<string>(APIConstants.DbGenericFailureCode, APIConstants.ValueDefaultErrorCode);
            _genericErrorMessage = APIConstants.ValueDefaultErrorMessage;
        }

        public static T APIComposeAndRespond<T>(string responseCode, string responseDescription) where T : class, IAPIOutput
        {
            var response = Activator.CreateInstance<T>();
            response.ErrorCode = responseCode;
            response.ErrorDetails = responseDescription;
            return response;
        }
    }
}