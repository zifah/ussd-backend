﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text.RegularExpressions;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class APIUtilities
    {
        public static string GetClientIPAddress()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            string ip = endpoint.Address;
            return ip;
        }

        internal static void SetResponseCodeDesc(IAPIOutput output, string responseCode, string responseDescription)
        {
            if (output != null)
            {
                output.ErrorCode = responseCode;
                output.ErrorDetails = responseDescription;
            }
        }

        internal static bool ValidateMSISDN(string msisdn)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            return msisdn != null && msisdn.Length == 11 && regex.IsMatch(msisdn);
        }

        internal static void SetHTTPStatusCode(int code)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)code;
        }

        /// <summary>
        /// First digit; and last 3-digits of ten-digit account number e.g. 5******937
        /// </summary>
        public static string MaskTenDigitAccountNumber(string accountNumber)
        {
            string result = null;

            if (!string.IsNullOrWhiteSpace(accountNumber) && accountNumber.Length == 10)
            {
                result = string.Format("{0}*****{1}", accountNumber.Substring(0, 2), accountNumber.Substring(7, 3));
            }

            return result;
        }

        internal static bool AreAccountsEqual(string maskedAccount, string clearAccount)
        {
            return MaskTenDigitAccountNumber(clearAccount).Equals(maskedAccount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn">Should be in 11-digit format. E.g.: 08012345678 will produce +2348012345678</param>
        /// <returns></returns>
        internal static string StandardizeNigerianMsisdn(string msisdn)
        {
            return string.Format("+234{0}", msisdn.Substring(1));
        }

        internal static Dictionary<int, decimal> RatioSplitter(decimal bulkAmount, Dictionary<int, decimal> personRatio)
        {
            var result = new Dictionary<int, decimal>();

            var numberOfEntries = personRatio.Count;

            // generate the create request inputs
            for (int i = 0; i < numberOfEntries; i++)
            {
                decimal amount = 0;
                var entry = personRatio.ElementAt(i);
                amount = decimal.Round(entry.Value * bulkAmount / 100, 3);

                result.Add(entry.Key, amount);
            }

            var rankings = new Dictionary<int, int>();

            foreach (var entry in result)
            {
                rankings.Add(entry.Key, Convert.ToInt32(entry.Value.ToString().Last().ToString()));
            }

            rankings.OrderByDescending(x => x.Value);

            decimal totalSplit = 0;

            for (int i = 0; i < numberOfEntries; i++)
            {
                decimal amount = 0;
                var matchAmount = result[rankings.ElementAt(i).Key];

                if (i == numberOfEntries - 1)
                {
                    amount = decimal.Round(bulkAmount - totalSplit, 2);
                }

                else
                {
                    amount = decimal.Round(matchAmount, 2);
                }

                result[rankings.ElementAt(i).Key] = amount;
                totalSplit += amount;
            }

            return result;
        }
    }
}