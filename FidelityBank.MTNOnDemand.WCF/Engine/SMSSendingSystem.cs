﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.ChargeCustomerRequests;
using FidelityBank.MTNOnDemand.Fincore;
using FidelityBank.MTNOnDemand.GetAccountRequests;
using FidelityBank.MTNOnDemand.GetAccountRequests.Dtos;
using FidelityBank.MTNOnDemand.LoggedSMSs;
using FidelityBank.MTNOnDemand.LoggedSMSs.Dtos;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class SMSSendingSystem : ITransientDependency
    {
        private readonly ILoggedSMSAppService _loggedSMSAppService;
        private readonly ConfigurationSystem _configSystem;


        public SMSSendingSystem(ILoggedSMSAppService loggedSMSAppService, ConfigurationSystem configSystem)
        {
            _loggedSMSAppService = loggedSMSAppService;
            _configSystem = configSystem;
        }

        internal void LogSMS(CreateLoggedSMSInput record)
        {
            _loggedSMSAppService.CreateLoggedSMS(record);
        }

        public void SendEnrollmentSMS(string msisdn)
        {
            //Get number ready for infopool: ASSUMPTION: MSISDN is 11-digits in 08012345678 format
            if (msisdn != null)
            {
                msisdn = APIUtilities.StandardizeNigerianMsisdn(msisdn);

                var lastLogged = _loggedSMSAppService.GetLastLoggedByRecipient(msisdn);
                var smsIntervalDays = _configSystem.GetByName<int>(APIConstants.DbSMSIntervalDays, APIConstants.ValueDefaultSMSIntervalDays);

                if (lastLogged == null || lastLogged.CreationTime <= DateTime.Now.Date.AddDays(-smsIntervalDays))
                {
                    try
                    {
                        // log SMS on infopool
                        var enrollmentSMS = _configSystem.GetByName<string>(APIConstants.DbNotEnrolledSMS, APIConstants.ValueDefaultNotEnrolledSMS);
                        InfopoolSystem.SendSMS(msisdn, APIConstants.ValueDefaultEnrollmentSMSSubject, enrollmentSMS);

                        //record SMS log locally
                        _loggedSMSAppService.CreateLoggedSMS(new CreateLoggedSMSInput
                        {
                            Recipient = msisdn,
                            Subject = APIConstants.ValueDefaultEnrollmentSMSSubject
                        });
                    }

                    catch (Exception ex)
                    {
                        LoggingSystem.LogMessage(string.Format("Error logging enrollment SMS on infopool for {0}", msisdn));
                        LoggingSystem.LogException(ex);
                        //LIFE GOES ON ANYWAY!
                    }
                }
            }
        }
    }
}