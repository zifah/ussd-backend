﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.RawRequests;
using FidelityBank.MTNOnDemand.RawRequests.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class RawRequestSystem : ITransientDependency
    {
        private readonly IRawRequestAppService _rawRequestSystemAppService;

        public RawRequestSystem(IRawRequestAppService rawRequestSystemAppService)
        {
            _rawRequestSystemAppService = rawRequestSystemAppService;
        }


        internal void SaveRawRequest(CreateRawRequestInput request)
        {
            _rawRequestSystemAppService.CreateRawRequest(request);
        }
    }
}