﻿using Abp.Dependency;
using FidelityBank.MTNOnDemand.Fincore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using FidelityBank.MTNOnDemand.WCF.Exceptions;
using FidelityBank.MTNOnDemand.FTRequests.Dtos;
using FidelityBank.MTNOnDemand.FTRequests;
using System.Threading;
using System.Threading.Tasks;
using FidelityBank.MTNOnDemand.InstantBankingEnrols;
using FidelityBank.MTNOnDemand.InstantBankingEnrols.Dtos;
using CoreInfopool = FidelityBank.CoreLibraries.Infopool.InfopoolSystem;
using CoreUtility = FidelityBank.CoreLibraries.Utility.Engine;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class EnrolInstantBankingSystem : ITransientDependency
    {
        private static readonly string _fincoreResponseFilePath = ConfigurationManager.AppSettings[APIConstants.AppFincoreResponsesFilePath];
        private readonly IInstantBankingEnrolAppService _enrolmentAppService;


        public EnrolInstantBankingSystem(IInstantBankingEnrolAppService enrolmentAppService)
        {
            _enrolmentAppService = enrolmentAppService;
        }

        public InstantBankingEnrolDto GetByAccountMsisdn(string msisdn, string accountNumber)
        {
            var result = _enrolmentAppService.GetByMsisdnAcct(msisdn, accountNumber);
            return result;
        }

        internal InstantBankingEnrolDto Create(EnrolInstantBankingInput input)
        {
            //check that account matches MSISDN
            string infopoolReader = ConfigurationManager.ConnectionStrings[APIConstants.AppInfopoolReader].ConnectionString;
            var infopoolSys = new CoreInfopool(infopoolReader);
            var standardMsisdn = CoreUtility.StringManipulation.StandardizeNigerianMsisdn(input.Msisdn);
            var accountMatch = infopoolSys.GetAccountDetails(input.AccountNumber, standardMsisdn);

            if (accountMatch == null)
            {
                throw new AccountMsisdnMismatchException("Supplied msisdn does not match supplied account number.");
            }

            var creator = new CreateInstantBankingEnrolInput
            {
                AccountNumber = input.AccountNumber,
                Msisdn = input.Msisdn,
                Status = EnrolStatus.PendingPinSelection
            };

            var created = _enrolmentAppService.CreateInstantBankingEnrol(creator);

            return created;
        }

        internal InstantBankingEnrolDto Update(UpdateInstantBankingEnrolInput updater)
        {
           var updated = _enrolmentAppService.UpdateInstantBankingEnrol(updater);

            return updated;
        }
    }
}