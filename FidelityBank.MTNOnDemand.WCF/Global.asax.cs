﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            LoggingSystem.LogMessage("Entered application start");
            Abp.Dependency.IocManager.Instance.IocContainer.AddFacility<Castle.Facilities.Logging.LoggingFacility>(f => f.UseLog4Net().WithConfig("log4net.config"));
            LoggingSystem.LogMessage("Exited application srart");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}