﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF.Exceptions
{
    public class AccountMsisdnMismatchException : System.Exception
    {
        public AccountMsisdnMismatchException(string message) : base(message) { }
        public AccountMsisdnMismatchException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected AccountMsisdnMismatchException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}