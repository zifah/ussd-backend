﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF.Exceptions
{
    public class LimitExceededException : System.Exception
    {
        public LimitExceededException(string message) : base(message) { }
        public LimitExceededException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected LimitExceededException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}