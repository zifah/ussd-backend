﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class RefundCustomerInput : IAPIInput
    {
        /// <summary>
        /// 18-digit code
        /// </summary>
        [DataMember]
        public string RefCode { set; get; }

        /// <summary>
        ///  Just here for extra validation
        /// </summary>
        [DataMember]
        public string MSISDN { set; get; }

        /// <summary>
        /// Customer account here. Here for extra validation
        /// </summary>
        [DataMember]
        public string SourceAccount { set; get; }

        /// <summary>
        /// Blank
        /// </summary>
        [DataMember]
        public string DestinationAccount { set; get; }

        /// <summary>
        /// Amount is in Naira
        /// </summary>
        [DataMember]
        public decimal Amount { set; get; }

        /// <summary>
        /// Optional
        /// </summary>
        [DataMember]
        public string Description { set; get; }

        [DataMember]
        public DateTime RequestTime { set; get; }
    }
}
