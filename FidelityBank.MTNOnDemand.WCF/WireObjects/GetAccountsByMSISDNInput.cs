﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class GetAccountsByMSISDNInput : IAPIInput
    {
        /// <summary>
        /// Format is: 08012345678
        /// </summary>
        [DataMember]
        public string MSISDN { set; get; }

        [DataMember]
        public DateTime RequestTime { set; get; }
    }
}
