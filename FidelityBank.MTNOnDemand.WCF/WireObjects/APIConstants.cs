﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class APIConstants
    {      
        // For inconclusive scenarios, HTTP error response will be returned (exception, aah)

        //Operations
        public const string ChargeCustomer = "ChargeCustomer";
        public const string RefundCustomer = "RefundCustomer";

        //Accounts
        /// <summary>
        /// <para>Sample: NGNSOLXXXXXXXX</para>
        /// <para>SOL will be replaced by SolId</para>
        /// </summary>
        public const string DbMTNOdSuspenseAccount = "BranchSuspenseAccount";
        public const string DbMTNOdSuspenseAccountLength = "BranchSuspenseAccountLength";    

        //ChargeCustomerConstants
        public const string DbChargeCustomer24HrLimit = "ChargeCustomer24HrLimit";
        public const string DbLimitExceededErrorCode = "CodeLimitExceeded";
        public const string DbAccountBlockedErrorCode = "CodeAccountBlocked";
        public const string DbInsufficientFundsErrorCode = "CodeInsufficientFunds";
        public const string DbNotEnrolledErrorCode = "CodeNotEnrolled";
        public const string DbMsisdnNotFoundErrorCode = "CodeMSISDNNotFound";
        public const string DbAccountMsisdnMismatchErrorCode = "CodeAccountMsisdnMismatch";
        public const string DbAlreadyEnrolledErrorCode = "CodeAlreadyEnrolled";
        public const string DbGenericFailureCode = "CodeGenericFailure";
        public const string DbGenericSuccessCode = "CodeGenericSuccess";
        public const string DbMaxSweepBacklogDays = "MaxSweepBacklogDays";
        public const string DbDrawdownSourceAccount = "DrawdownSourceAccount";
        public const string DbFincoreSuccessCode = "FincoreSuccessCode";
        public const string DbFincoreDuplicateCode = "FincoreDuplicateCode";
        public const string DbEnrollmentCheckIntervalDays = "EnrollmentCheckIntervalDays";
        public const string DbCurrentAccountStarters = "CurrentAccountStarters";
        public const string DbSavingsAccountStarters = "SavingsAccountStarters";
        public const string DbAccountNumberLength = "AccountNumberLength";
        public const string DbFincoreInsufficientFundsCode = "FincoreInsufficientFundsCode";
        public const string DbNotEnrolledSMS = "NotEnrolledSMS";
        public const string DbSMSIntervalDays = "SMSIntervalDays";

        //Values        
        public const string ValueDefaultSuccessCode = "00";
        public const string ValueDefaultErrorCode = "99";
        public const string ValueDefaultSuccessMessage = "Successful";
        public const string ValueDefaultErrorMessage = "System error!";
        public const int ValueDefaultMaxSweepBacklogDays = 1;
        public const string ValueDefaultFincoreSuccessCode = "00";
        public const string ValueDefaultFincoreDuplicateCode = "94";
        public const int ValueDefaultEnrollmentCheckIntervalDays = 7;
        public const string ValueCurrentAccountStarters = "4,5,9";
        public const string ValueSavingsAccountStarters = "6";
        public const int ValueDefaultAccountNumberLength = 10;
        public const string ValueDefaultFincoreInsufficientFundsCode = "51";
        public const string ValueDefaultInsufficientFundsErrorCode = "01";
        public const string ValueDefaultNotEnrolledErrorCode = "04";
        public const string ValueDefaultNotEnrolledSMS = "You have not been provisioned for this service. Please dial *770*0# to enroll.";
        public const int ValueDefaultSMSIntervalDays = 7;
        public const string ValueDefaultEnrollmentSMSSubject = "MTN302ACTIVATE";
        public const string ValueDefaultErrorChargebackNarration = "Error request";
        public const string ValueDefaultMsisdnNotFoundErrorCode = "05";
        public const string ValueDefaultAlreadyEnrolledErrorCode = "06";
        public const string ValueDefaultAccountMsisdnMismatchErrorCode = "07";
        public const string ValueDefaultEnrolPasswordSelectMessage = "Ok! Proceed to select password.";

        //AppSettings
        public const string AppFincoreResponsesFilePath = "FincoreResponsesFilePath";
        public const string AppIsDummyMode = "IsDummyMode";
        public const string AppInfopoolReader = "InfopoolReader";

        public const string APIHeaderHashKey = "Hash";
        public const string APIClientId = "ClientId";
    }
}