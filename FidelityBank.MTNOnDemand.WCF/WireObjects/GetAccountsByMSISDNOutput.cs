﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class GetAccountsByMSISDNOutput : IAPIOutput
    {
        public GetAccountsByMSISDNOutput()
        {
            Accounts = new List<Account> { };
        }

        [DataMember]
        /// <summary>
        /// Format is: 08012345678
        /// </summary>
        public List<Account> Accounts { set; get; }

        [DataMember]
        public string ErrorCode { set; get; }

        [DataMember]
        public string ErrorDetails { set; get; }
    }
}
