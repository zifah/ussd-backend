﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public interface IAPIInput
    {
        /// <summary>
        /// To cater for time discrepancies between client and server systems. 
        /// <para>Most important for FT requests where you need to aggregate all transaction for a certain day</para>
        /// </summary>
        DateTime RequestTime { set; get; }
    }
}