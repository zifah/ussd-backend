﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class EnrolInstantBankingInput : IAPIInput
    {
        [DataMember]
        public string Msisdn { set; get; }

        [DataMember]
        public string AccountNumber { set; get; }

        [DataMember]
        public string Password { set; get; }

        [DataMember]
        public string RefCode { set; get; }        

        [DataMember]
        public DateTime RequestTime { set; get; }
    }
}
