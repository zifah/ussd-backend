﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public class RawSoapRequest
    {
        public RawSoapRequest()
        {
            RequestTime = DateTime.Now; //(DateTime)SqlDateTime.MinValue;
        }

        public string Content { set; get; }
        public string SourceIPAddress { set; get; }
        public DateTime RequestTime { private set; get; }
        public string MethodName { set; get; }
        public long RelatedObjectId { set; get; }
    }
}