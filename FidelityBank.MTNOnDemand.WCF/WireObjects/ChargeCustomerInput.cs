﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class ChargeCustomerInput : IAPIInput
    {
        [DataMember]
        public string MSISDN { set; get; }

        [DataMember]
        public string SourceAccount { set; get; }

        /// <summary>
        /// Amount is in Naira
        /// </summary>
        [DataMember]
        public decimal Amount { set; get; }

        [DataMember]
        public string RefCode { set; get; }

        /// <summary>
        /// Optional
        /// </summary>
        [DataMember]
        public string Description { set; get; }

        /// <summary>
        /// Ignore
        /// </summary>
        [DataMember]        
        public string DestinationAccount { set; get; }
        
        [DataMember]
        public DateTime RequestTime { set; get; }
    }
}
