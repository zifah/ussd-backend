﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class ChargeCustomerOutput : IAPIOutput
    {
        public ChargeCustomerOutput()
        {

        }

        [DataMember]
        public string ErrorCode { set; get; }

        [DataMember]
        public string ErrorDetails { set; get; }
    }
}
