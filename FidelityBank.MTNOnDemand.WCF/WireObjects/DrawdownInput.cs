﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class DrawdownInput : IAPIInput
    {
        /// <summary>
        /// Amount is in Naira
        /// </summary>
        [DataMember]
        public decimal BulkAmount { set; get; }

        [DataMember]
        public string RefCode { set; get; }

        [DataMember]
        public DateTime SweepDate { set; get; }

        [DataMember]
        public DateTime RequestTime { set; get; }
    }
}
