﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    public interface IAPIOutput
    {
        string ErrorCode { set; get; }
        string ErrorDetails { set; get; }
    }
}