﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DataContract(Namespace = "https://fidelitybank.ng")]
    public class Account
    {
        [DataMember]
        public string Number { set; get; }

        [DataMember]
        public string Name { set; get; }

        [DataMember]
        public decimal Balance { set; get; }

        public string MaskedNumber { set; get; }
    }
}