﻿using Abp.Modules;
using Fbp.Net.Integrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FidelityBank.MTNOnDemand.WCF
{
    [DependsOn(typeof(MTNOnDemandDataModule), typeof(IntegrationsApplicationModule))]
    public class MTNOnDemandWCFModule : AbpModule
    {
        public override void Initialize()
        {
            LoggingSystem.LogMessage("Started initializing WCFModule");
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            LoggingSystem.LogMessage("Finished initializing WCFModule");
        }
    }
}