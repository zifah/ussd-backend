﻿using Abp;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FidelityBank.MTNOnDemand.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MTN302" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MTN302.svc or MTN302.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(Namespace = "https://fidelitybank.ng", AddressFilterMode = AddressFilterMode.Any)]
    public class MTN302 : IMTN302
    {
        private ApiSystem _engine = null;
        private static bool _isDummyMode = false;

        public MTN302()
        {
            _engine = new ApiSystem();

            Boolean.TryParse(ConfigurationManager.AppSettings[APIConstants.AppIsDummyMode], out _isDummyMode);
        }

        public GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input)
        {
            LoggingSystem.LogMessage("Entered GetAccountsByMSISDN");

            if (_isDummyMode)
            {
                return new DummyApiSystem().GetAccountsByMSISDN(input);
            }

            LoggingSystem.LogMessage("About to save raw request");

            SaveRawRequest(input);

            LoggingSystem.LogMessage("Authenticating request");
            try
            {
                AuthenticateRequest(input);
            }

            catch (SecurityException ex)
            {
                _engine.SetHTTPStatusCode(401);
                return new GetAccountsByMSISDNOutput { ErrorCode = _engine._genericErrorCode, ErrorDetails = ex.Message };
            }

            LoggingSystem.LogMessage("About to start main processing");

            var response = _engine.GetAccountsByMSISDN(input);

            LoggingSystem.LogMessage("About to return response to client");
            return response;
        }

        public ChargeCustomerOutput ChargeCustomer(ChargeCustomerInput input)
        {
            if (_isDummyMode)
            {
                return new DummyApiSystem().ChargeCustomer(input);
            }

            SaveRawRequest(input);

            try
            {
                AuthenticateRequest(input);
            }

            catch (SecurityException ex)
            {
                _engine.SetHTTPStatusCode(401);
                return new ChargeCustomerOutput { ErrorCode = _engine._genericErrorCode, ErrorDetails = ex.Message };
            }

            var response = _engine.ChargeCustomer(input);
            return response;
        }

        public RefundCustomerOutput RefundCustomer(RefundCustomerInput input)
        {
            if (_isDummyMode)
            {
                return new DummyApiSystem().RefundCustomer(input);
            }


            SaveRawRequest(input);

            try
            {
                AuthenticateRequest(input);
            }

            catch (SecurityException ex)
            {
                _engine.SetHTTPStatusCode(401);
                return new RefundCustomerOutput { ErrorCode = _engine._genericErrorCode, ErrorDetails = ex.Message };
            }

            var response = _engine.RefundCustomer(input);
            return response;
        }

        public DrawdownOutput Drawdown(DrawdownInput input)
        {
            if (_isDummyMode)
            {
                return new DummyApiSystem().Drawdown(input);
            }

            SaveRawRequest(input);

            try
            {
                AuthenticateRequest(input);
            }

            catch(SecurityException ex)
            {
                _engine.SetHTTPStatusCode(401);
                return new DrawdownOutput { ErrorCode = _engine._genericErrorCode, ErrorDetails = ex.Message };
            }

            DrawdownOutput response = _engine.Drawdown(input);
            return response;
        }

        public EnrolInstantBankingOutput EnrolInstantBanking(EnrolInstantBankingInput input)
        {
            if (_isDummyMode)
            {
                return new DummyApiSystem().EnrolInstantBanking(input);
            }

            SaveRawRequest(input);

            try
            {
                AuthenticateRequest(input);
            }

            catch (SecurityException ex)
            {
                _engine.SetHTTPStatusCode(401);
                return new EnrolInstantBankingOutput { ErrorCode = _engine._genericErrorCode, ErrorDetails = ex.Message };
            }

            EnrolInstantBankingOutput response = _engine.EnrolInstantBanking(input);
            return response;
        }

        private void AuthenticateRequest(IAPIInput input)
        {
            var isAuthValid = _engine.AuthenticateRequest(input);

            if(!isAuthValid)
            {
                throw new SecurityException("An unknown error occured!");
            }
        }

        private void SaveRawRequest(IAPIInput input, [CallerMemberName] string caller = null)
        {
            var messageBody = JsonConvert.SerializeObject(input);
            var typeName = input == null ? "NULL" : input.GetType().Name;
            _engine.SaveRawRequest(caller, messageBody, typeName);
        }
    }
}
