﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FidelityBank.MTNOnDemand.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMTN302" in both code and config file together.
    [ServiceContract(Namespace = "https://fidelitybank.ng")]
    public interface IMTN302
    {
        [OperationContract]
        [WebGet]
        GetAccountsByMSISDNOutput GetAccountsByMSISDN(GetAccountsByMSISDNInput input);

        [OperationContract]
        [WebGet]
        ChargeCustomerOutput ChargeCustomer(ChargeCustomerInput input);

        [OperationContract]
        [WebGet]
        RefundCustomerOutput RefundCustomer(RefundCustomerInput input);

        [OperationContract]
        [WebGet]
        DrawdownOutput Drawdown(DrawdownInput input);

        [OperationContract]
        [WebGet]
        EnrolInstantBankingOutput EnrolInstantBanking(EnrolInstantBankingInput input);
    }
}
