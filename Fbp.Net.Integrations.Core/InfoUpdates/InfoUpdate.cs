﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.InfoUpdates
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("InfoUpdates")]
    public class InfoUpdate : Entity<long>, IHasCreationTime, IModificationAudited, ISoftDelete
    {
        public InfoUpdate()
        {
            CreationTime = DateTime.Now;
        }

        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual CustomerInfoKey InfoType { set; get; }
        public virtual string NewValue { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual string Msisdn { set; get; }
        public virtual long UserId { set; get; }
        public virtual string Reason { set; get; }
        public virtual bool IsDeleted { get; set; }
        public virtual string ClientReference { set; get; }
    }
}
