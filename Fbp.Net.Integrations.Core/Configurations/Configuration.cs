﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.Configurations
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("Configurations")]
    public class Configuration : Entity<long>, IHasCreationTime, ISoftDelete, IModificationAudited
    {
        public Configuration()
        {

        }

        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
        public virtual string Name { set; get; }
        public virtual string Value { set; get; }
        public virtual string Description { set; get; }
        public virtual bool IsDeleted { set; get; }
    }
}
