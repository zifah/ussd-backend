﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.UserProfiles
{
    /// <summary>
    /// Default profile is NULL
    /// </summary>
    [Table("UserProfiles")]
    public class UserProfile : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public UserProfile()
        {
        }

        public virtual string Name { set; get; }
        public virtual string Description { set; get; }   
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
