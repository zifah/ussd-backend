﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.Validations
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("Validations")]
    public class Validation : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public Validation()
        {
        }

        public virtual string BillerId { set; get; }
        public virtual string BillerName { set; get; }
        public virtual string ClientReference { set; get; }
        public virtual string RawResponse { set; get; }
        public virtual string CustomerId { set; get; }
        public virtual string CustomerName { set; get; }
        public virtual decimal Amount { set; get; }
        public virtual string ProviderReference { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
    }
}
