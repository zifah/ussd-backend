﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Validations
{
    public interface IValidationRepository : IRepository<Validation, long>
    {
        /// <summary>
        /// Return the latest saved validation
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="billerId"></param>
        /// <returns></returns>
        Validation GetBy(string customerId, string billerId);
    }
}
