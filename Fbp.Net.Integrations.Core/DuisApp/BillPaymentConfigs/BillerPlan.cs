﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs
{
    public class BillerPlan
    {
        public string Name { set; get; }
        public string Code { set; get; }
        public decimal Amount { set; get; }
        public bool IsDefault { set; get; }
        public bool IsAmountFixed { set; get; }
        public bool UseValidationAmount { set; get; }

        public string NameAmount
        {
            get
            {
                return string.Format("{0}{1}", Name, Amount > 0 ? " - N"+Amount : string.Empty);
            }
        }
    }
}
