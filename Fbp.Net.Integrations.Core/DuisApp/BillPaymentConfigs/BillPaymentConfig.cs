﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs
{
    /// <summary>
    /// 
    /// </summary>
    [Table("BillPaymentConfigs")]
    public class BillPaymentConfig : Entity<long>, IHasCreationTime, IModificationAudited, IPassivable
    {
        public BillPaymentConfig()
        {
        }

        /// <summary>
        /// USSD code for this biller
        /// </summary>
        public virtual string BillerCode { set; get; }
        public virtual string BillerName { set; get; }
        public virtual string ProductName { set; get; }
        public virtual string PaymentParametersJson { set; get; }
        public virtual string ProviderName { set; get; }
        public virtual string ProviderFullyQualifiedName { set; get; }

        /// <summary>
        /// e.g. phone number for topup; smart card number for cable TV; tolling account number for tolling
        /// </summary>
        public virtual string CustomerIdFieldName { set; get; }
        public virtual decimal SurchargeAmount { set; get; }
        public virtual bool RequiresCustomerIdValidation { set; get; }
        public virtual bool HasPlans { set; get; }
        public virtual bool IsProviderTransactor { set; get; }
        public virtual string JsonPlans { set; get; }
        public virtual string BillerSuspenseAccount { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual bool IsActive { set; get; }
        public virtual IList<BillerPlan> BillerPlans
        {
            get
            {
                var result = new List<BillerPlan>();
                try
                {
                    result = JsonConvert.DeserializeObject<List<BillerPlan>>(this.JsonPlans);
                }

                catch
                {

                }
                return result;
            }
        }

        public virtual string ShortName { set; get; }
        public virtual bool IsHidden { set; get; }
        public virtual string Category { get; set; }
        public virtual bool SaveValidationResponse { set; get; }
    }
}
