﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs
{
    public interface IBillPaymentConfigRepository : IRepository<BillPaymentConfig, long>
    {
        BillPaymentConfig GetByCode(string billerCode);
        BillPaymentConfig GetByName(string billerName);
    }
}
