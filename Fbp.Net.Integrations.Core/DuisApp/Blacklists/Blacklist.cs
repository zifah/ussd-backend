﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.Blacklists
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("Blacklist")]
    public class Blacklist : Entity<long>, IHasCreationTime, IPassivable, IModificationAudited
    {
        public Blacklist()
        {
            IsActive = true;
        }

        public virtual string Msisdn { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }

        public virtual bool IsActive { set; get; }
    }
}
