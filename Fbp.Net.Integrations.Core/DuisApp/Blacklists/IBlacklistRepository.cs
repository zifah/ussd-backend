﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Blacklists
{
    public interface IBlacklistRepository : IRepository<Blacklist, long>
    {
        IList<Blacklist> GetByAccountMSISDN(string accountNumber, string msisdn);
    }
}
