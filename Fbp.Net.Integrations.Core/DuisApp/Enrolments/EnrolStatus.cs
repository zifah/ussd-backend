﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Enrolments
{
    public enum EnrolStatus
    {
        /// <summary>
        /// Status is set to 'Activated' once PIN is selected
        /// </summary>
        Activated = 0,
        PendingPinSelection,
        LockedOut,
        Disabled,
        /// <summary>
        /// <para>This mode will allow either the MSISDN or the account number be updated via a new enrolment</para>
        /// <para>An enrolment will have to be manually set to this status</para>
        /// </summary>
        UpdateMode
    }
}
