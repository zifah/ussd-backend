﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Enrolments
{
    public interface IEnrolmentRepository : IRepository<Enrolment, long>
    {
        Enrolment GetByMsisdn(string msisdn, UserType userType);
        Enrolment GetByAccount(string accountNumber, UserType userType);
        IList<Enrolment> GetByMsisdnAcct(string msisdn, string accountNumber, UserType userType);
        Enrolment GetByCustomerId(string customerId);
    }
}
