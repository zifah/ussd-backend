﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Fbp.Net.Integrations.DuisApp.UserProfiles;

namespace Fbp.Net.Integrations.DuisApp.Enrolments
{
    /// <summary>
    /// USSD engine users
    /// </summary>
    [Table("Users")]
    public class Enrolment : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public Enrolment()
        {
            CreationTime = DateTime.Now;
            Status = EnrolStatus.Disabled;
        }

        public virtual string Msisdn { set; get; }
        public virtual UserProfile Profile { set; get; }
        public virtual string Network { set; get; }
        public virtual string BankCustomerId { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
        public virtual string AccountNumber { set; get; }
        public virtual string Password { set; get; }
        public virtual DateTime? FirstLoginTime { set; get; }
        public virtual DateTime? LastLoginTime { set; get; }
        public virtual DateTime? LastLoginAttemptTime { set; get; }
        public virtual int FailedLoginAttempts { set; get; }
        public virtual EnrolStatus Status { set; get; }
        public virtual DateTime? EnrolmentTime { set; get; }
        public virtual string OldPasswords {set; get;}
        public virtual string LockoutReason { set; get; }
        public virtual bool? HasToken { set; get; }
        public virtual UserType UserType { set; get; }
        public virtual string Comment { set; get; }

        public virtual string FirstName { set; get; }
        public virtual string LastName { set; get; }
        public virtual DateTime? FinacleCreationTime { set; get; }

        public virtual string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

        public virtual bool IsActive
        {
            get
            {
                return Status == EnrolStatus.Activated;
            }
        }

        public virtual bool ShouldResetPassword
        {
            get
            {
                return string.IsNullOrWhiteSpace(Password);
            }
        }
    }
}
