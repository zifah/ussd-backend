﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Adverts
{
    public interface IAdvertRepository : IRepository<Advert, long>
    {
    }
}
