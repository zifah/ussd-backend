﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fbp.Net.Integrations.DuisApp.Adverts
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("Adverts")]
    public class Advert : Entity<long>, IHasCreationTime, IPassivable, IModificationAudited
    {
        public Advert()
        {
            IsActive = true;
        }

        public virtual string Name { set; get; }
        public virtual string Content { set; get; }
        /// <summary>
        /// Property-value pairs which would exclude a user from getting this ad
        /// </summary>
        public virtual string UserExcludeDictionaryJson { set; get; }
        public virtual string ApplicableModulesListJson { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual Dictionary<string, string> UserExcludeDictionary
        {
            get
            {
                var result = new Dictionary<string, string>();

                try
                {
                    result = JsonConvert.DeserializeObject<Dictionary<string, string>>(this.UserExcludeDictionaryJson);
                }

                catch
                {
                }

                return result;
            }
        }

        public virtual List<string> ApplicableModulesList
        {
            get
            {
                var result = new List<string>();

                try
                {
                    result = JsonConvert.DeserializeObject<List<string>>(this.ApplicableModulesListJson);
                }

                catch
                {

                }

                return result;
            }
        }

        public virtual bool IsActive { set; get; }
    }
}
