﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.UserProfiles;

namespace Fbp.Net.Integrations.DuisApp.TransactionLimits
{
    /// <summary>
    /// 
    /// </summary>
    [Table("TransactionLimits")]
    public class TransactionLimit : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public TransactionLimit()
        {
        }

        public virtual Enrolment User { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public virtual TransactionType TransactionType { set; get; }
        public virtual decimal DailyAmount { set; get; }
        public virtual int DailyCount { set; get; }
        public virtual decimal MaxTransactionAmount { set; get; }
        public virtual decimal? MaximumBeforeTokenAmount { set; get; }   
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
