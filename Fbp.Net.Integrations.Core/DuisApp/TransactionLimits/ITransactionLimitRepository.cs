﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionLimits
{
    public interface ITransactionLimitRepository : IRepository<TransactionLimit, long>
    {
        TransactionLimit GetByUserTxnType(long txnTypeId, long userId);
        IList<TransactionLimit> GetCustomLimits(long userId, long? profileId);
    }
}
