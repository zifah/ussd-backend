﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;

namespace Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers
{
    /// <summary>
    /// 
    /// </summary>
    [Table("AirtimeRechargeBillers")]
    public class AirtimeRechargeBiller : Entity<long>, IHasCreationTime, IModificationAudited, IPassivable
    {
        /// <summary>
        /// USSD code for this biller
        /// </summary>
        public virtual BillPaymentConfig Biller { set; get; }
        public virtual string Network { set; get; }
        public virtual string TopupPlanCode { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual bool IsActive { set; get; }
    }
}
