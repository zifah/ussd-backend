﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionCharges
{
    public interface ITransactionChargeRepository : IRepository<TransactionCharge, long>
    {
        TransactionCharge GetByTransactionType(string transactionTypeName, long? profileId);
    }
}
