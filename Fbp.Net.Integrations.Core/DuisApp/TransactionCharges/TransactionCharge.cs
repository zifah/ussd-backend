﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;

namespace Fbp.Net.Integrations.DuisApp.TransactionCharges
{
    /// <summary>
    /// 
    /// </summary>
    [Table("TransactionCharges")]
    public class TransactionCharge : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public TransactionCharge()
        {
        }

        public virtual UserProfile UserProfile { set; get; }
        public virtual TransactionType TransactionType { set; get; }
        public virtual Decimal Amount { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
