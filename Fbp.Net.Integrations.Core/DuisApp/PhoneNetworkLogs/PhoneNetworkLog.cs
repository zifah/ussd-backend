﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs
{
    /// <summary>
    /// 
    /// </summary>
    [Table("PhoneNetworkLogs")]
    public class PhoneNetworkLog : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public PhoneNetworkLog()
        {
        }

        public virtual string Msisdn { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual bool IsTransactionInitiator  { set; get; }
        public virtual string Network { set; get; }    
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
