﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs
{
    public interface IPhoneNetworkLogRepository : IRepository<PhoneNetworkLog, long>
    {
        /// <summary>
        /// msisdn in standard format: +2348012345678
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        PhoneNetworkLog GetPhoneNetworkLog(string msisdn);
    }
}
