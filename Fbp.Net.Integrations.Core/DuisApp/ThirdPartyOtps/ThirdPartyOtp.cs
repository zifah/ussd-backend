﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps
{
    [Table("ThirdPartyOtps")]
    public class ThirdPartyOtp : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public virtual long UserId { set; get; }
        public virtual string UserMsisdn { set; get; }
        public virtual string OtpAccount { set; get; }
        public virtual string Purpose { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime ExpirationTime {set; get; }
        public virtual DateTime? UsageTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual int FailedTryCount { set; get; }
        /// <summary>
        /// Contains the list of OTPs which the customer has tried to use against this OTP record
        /// </summary>
        public virtual string FailedAttempts { set; get; }
        public virtual DateTime? LastFailedAttemptTime { set; get; }
        public virtual string Otp { set; get; }

    }
}
