﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps
{
    public interface IThirdPartyOtpRepository : IRepository<ThirdPartyOtp, long>
    {
        ThirdPartyOtp GetLatestOtpByPhone(string phone, string purpose);

        ThirdPartyOtp GetLatestOtpByAccount(string account, string purpose);

        ThirdPartyOtp GetLatestOtpByPhoneAccount(string phone, string account, string purpose);
    }
}
