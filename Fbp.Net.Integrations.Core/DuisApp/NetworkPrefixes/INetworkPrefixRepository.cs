﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.NetworkPrefixes
{
    public interface INetworkPrefixRepository : IRepository<NetworkPrefix, long>
    {
        /// <summary>
        /// msisdn in 11-digit format; e.g. 08012345678
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        NetworkPrefix GetPhoneNetwork(string msisdn);
    }
}
