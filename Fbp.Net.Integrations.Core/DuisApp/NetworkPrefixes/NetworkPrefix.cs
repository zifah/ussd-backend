﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.NetworkPrefixes
{
    /// <summary>
    /// 
    /// </summary>
    [Table("NetworkPrefixes")]
    public class NetworkPrefix : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public NetworkPrefix()
        {
        }

        public virtual string Prefix { set; get; }
        public virtual string Network { set; get; }    
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
