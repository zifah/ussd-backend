﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.WeakPasswords
{
    /// <summary>
    /// Default profile is NULL
    /// </summary>
    [Table("WeakPasswords")]
    public class WeakPassword : Entity<long>, IHasCreationTime, IModificationAudited, ISoftDelete
    {
        public WeakPassword()
        {
        }

        public virtual string Password { set; get; } 
        public virtual bool IsDeleted { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
