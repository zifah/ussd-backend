﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.Transactions
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Transactions")]
    public class Transaction : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public Transaction()
        {
        }

        public virtual decimal Amount { set; get; }
        public virtual decimal ChargeAmount { set; get; }
        public virtual string ClientRefId { set; get; }
        public virtual string RefId { set; get; }
        public virtual string Type { set; get; }
        public virtual long UserId { set; get; }
        public virtual string UserMsisdn { set; get; }
        public virtual string UserEmail { set; get; }
        public virtual string UserFullName { set; get; }
        public virtual string Status { set; get; }
        public virtual string StatusCode { set; get; }
        public virtual bool Success { set; get; }
        public virtual bool IsReversed { set; get; }
        public virtual int ReversalAttempts { set; get; }
        public virtual decimal DrAmount { set; get; }
        public virtual string DrSource { set; get; }
        public virtual string DrDestination { set; get; }
        public virtual string DrDestinationCode { set; get; }
        public virtual string DrRefId { set; get; }
        public virtual string DrNarration { set; get; }
        public virtual string DrResponseCode {set; get;}
        public virtual string DrResponseDesc { set; get; }
        public virtual DateTime? DrReqTime { set; get; }
        public virtual DateTime? DrRespTime { set; get; }
        public virtual string ProcessorName { set; get; }
        /// <summary>
        /// Will be biller code for bill payments
        /// </summary>
        public virtual string ProcessorCode { set; get; }
        public virtual string ProcessorOptionCode { set; get; }
        public virtual string ProcessorValidationOutput { set; get; }
        /// <summary>
        /// e.g. DSTV card number for bill payments; dest phone number for airtime recharge
        /// </summary>
        public virtual string ProcessorUserId { set; get; }
        public virtual string ProcessorRefId { set; get; }
        public virtual string ProcessorRespCode { set; get; }
        public virtual string ProcessorRespDesc { set; get; }
        public virtual string ProcessorResponseBody { set; get; }
        public virtual DateTime? ProcessorReqTime { set; get; }
        public virtual DateTime? ProcessorRespTime { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
        public virtual bool IsViaDirectCode { set; get; }
        public virtual bool ReverseExceptionally { set; get; }
        public virtual DateTime? ReversalTime { set; get; }
    }
}
