﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Transactions
{
    public interface ITransactionRepository : IRepository<Transaction, long>
    {
        IList<Transaction> GetTransactionsLast24Hours(long userId);
        IList<Transaction> GetDebitedButFailed(string successfulDebitCode, string[] transactionTypes);
        IList<Transaction> GetForReversal(string successfulDebitCode, string[] transactionTypes, int daysBacklog);
        Transaction GetByClientRef(string clientRef);
        Transaction GetByRefId(string refId);
        IList<Transaction> GetLatestTransactions(long userId, int count);
        IList<Transaction> GetLatestSuccessfulTransactions(long userId, int count);
    }
}
