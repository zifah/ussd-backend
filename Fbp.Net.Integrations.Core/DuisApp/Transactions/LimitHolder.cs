﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Transactions
{
    /// <summary>
    /// <para>Default limit exceeded type is max</para>
    /// </summary>
    public class LimitHolder
    {
        public decimal MinimumAmount { set; get; }
        public decimal MaximumAmount { set; get; }
        public int CountLeft { set; get; }
        public bool IsLimitExceeded { set; get; }
        public decimal MaximumBeforeToken { set; get; }
        public bool IsTokenLimitExceeded { set; get; }
        public bool IsDefaultLimitExceeded
        {
            get
            {
                return DefaultCountLeft < 1 || DefaultMaxAmount < 1;
            }
        }
        public bool IsExceededLimitMinimum { set; get; }
        public int DefaultCountLeft { set; get; }
        public decimal DefaultMaxAmount { set; get; }
    }
}
