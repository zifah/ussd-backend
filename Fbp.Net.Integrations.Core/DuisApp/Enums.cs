﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp
{
    public enum LimitType { Default, Custom }

    public enum LimitTransactionType { Payment = 1, Transfer = 2, AirtimeRecharge = 10, BillPayment = 11, IntrabankTransfer = 20, InterbankTransfer = 21, CashOut = 22 }

    public static class Constants
    {
        public static readonly Dictionary<LimitTransactionType, LimitTransactionType> ChildParentTransactionTypeMapping
            = new Dictionary<LimitTransactionType, LimitTransactionType>() 
            {{LimitTransactionType.AirtimeRecharge, LimitTransactionType.Payment},
            {LimitTransactionType.BillPayment, LimitTransactionType.Payment},
            {LimitTransactionType.CashOut, LimitTransactionType.Transfer},
            {LimitTransactionType.IntrabankTransfer, LimitTransactionType.Transfer},
            {LimitTransactionType.InterbankTransfer, LimitTransactionType.Transfer}};
    }
}
