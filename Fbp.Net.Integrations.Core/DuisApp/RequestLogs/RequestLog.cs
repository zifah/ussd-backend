﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("RequestLogs")]
    public class RequestLog : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public RequestLog()
        {

        }

        public virtual string Msisdn { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual string Url { set; get; }
        public virtual string HttpMethod { set; get; }
        public virtual decimal? Amount { set; get; }
        public virtual string ClientReference { set; get; }
        public virtual string ResponseCode { set; get; }
        public virtual string ResponseMessage { set; get; }
        public virtual string OriginatingAddress { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
