﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.AccountInfoCaches
{
    public interface IAccountInfoCacheRepository : IRepository<AccountInfoCache, long>
    {
    }
}
