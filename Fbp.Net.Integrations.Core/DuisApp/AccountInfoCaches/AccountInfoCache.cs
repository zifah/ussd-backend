﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.AccountInfoCaches
{
    /// <summary>
    /// 
    /// </summary>
    [Table("AccountInfoCaches")]
    public class AccountInfoCache : Entity<long>, IHasCreationTime, IModificationAudited, IPassivable
    {
        
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual bool IsActive { set; get; }
    }
}
