﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.DuisApp.TransactionTypes
{
    /// <summary>
    /// 
    /// </summary>
    [Table("TransactionTypes")]
    public class TransactionType : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public TransactionType()
        {
        }

        public virtual string Name { set; get; }
        public virtual decimal DefaultDailyAmount { set; get; }
        public virtual decimal DefaultDailyCount { set; get; }
        public virtual decimal MinTransactionAmount { set; get; }
        public virtual decimal DefaultMaxTransactionAmount { set; get; }
        public virtual decimal MaximumDailyAmount { set; get; }
        public virtual decimal MaximumDailyCount { set; get; }
        public virtual decimal MaximumTransactionAmount { set; get; }
        public virtual decimal MaximumBeforeTokenAmount { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
