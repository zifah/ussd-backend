﻿using Abp.Domain.Repositories;
using System.Collections.Generic;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts
{
    public interface INewBankAccountRepository : IRepository<NewBankAccount, long>
    {
        IList<NewBankAccount> GetByPhone(string phone);
    }
}
