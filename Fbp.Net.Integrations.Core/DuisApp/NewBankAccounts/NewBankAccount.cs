﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("NewBankAccounts")]
    public class NewBankAccount : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public NewBankAccount()
        {
        }

        public virtual string FirstName { set; get; }
        public virtual string LastName { set; get; }
        public virtual string AccountNumber { set; get; }
        public virtual string CustomerId { set; get; }
        public virtual string SchemeCode { set; get; }
        public virtual string Bvn { set; get; }
        public virtual DateTime Birthday { set; get; }
        public virtual string PhoneNumber { set; get; }
        public virtual string Email { set; get; }
        public virtual string ReferrerCode { set; get; }
        /// <summary>
        /// Or SolId
        /// </summary>
        public virtual string BranchCode { set; get; }
        public virtual string CbaResponse { set; get; }
        public virtual CreationStatus? CreationStatus { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
    }

    public enum CreationStatus { Pending, Successful, Failed };
}
