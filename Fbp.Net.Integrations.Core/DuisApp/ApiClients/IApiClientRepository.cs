﻿using Abp.Domain.Repositories;

namespace Fbp.Net.Integrations.DuisApp.ApiClients
{
    public interface IApiClientRepository : IRepository<ApiClient, long>
    {
        ApiClient GetByName(string name);
    }
}
