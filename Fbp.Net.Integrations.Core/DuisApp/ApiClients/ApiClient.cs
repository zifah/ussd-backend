﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fbp.Net.Integrations.DuisApp.ApiClients
{
    [Table("ApiClients")]
    public class ApiClient : Entity<long>, IHasCreationTime, IPassivable, IModificationAudited
    {
        public ApiClient()
        {
            IsActive = true;
        }

        public virtual string Name { set; get; }
        public virtual string IpAddress { set; get; }        
        public virtual string SharedKey { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual bool IsActive { set; get; }
        public virtual bool RequiresAuth { set; get; }
    }
}
