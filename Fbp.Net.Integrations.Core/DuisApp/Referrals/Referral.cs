﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fbp.Net.Integrations.DuisApp.Referrals
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("Referrals")]
    public class Referral : Entity<long>, IHasCreationTime, IPassivable, IModificationAudited
    {
        public Referral()
        {

        }

        public virtual long ReferreeUserId { set; get; }
        public virtual string ReferreePhone { set; get; }
        public virtual string RefereeAccount { set; get; }
        public virtual string RefereeCustomerId { set; get; }
        public virtual string ReferrerPhone { set; get; }
        public virtual string Product { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual bool IsActive { set; get; }
    }
}
