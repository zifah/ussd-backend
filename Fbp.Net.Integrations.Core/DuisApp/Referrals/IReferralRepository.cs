﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Referrals
{
    public interface IReferralRepository : IRepository<Referral, long>
    {
        IList<Referral> GetReferrals(string referrerPhone);
    }
}
