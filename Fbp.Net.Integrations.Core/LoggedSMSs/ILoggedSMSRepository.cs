﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.LoggedSMSs
{
    public interface ILoggedSMSRepository : IRepository<LoggedSms, long>
    {
        IList<LoggedSms> GetByRecipient(string msisdn);
        LoggedSms GetLastLoggedByRecipient(string msisdn);
        IList<LoggedSms> GetByDate(DateTime date);
    }
}
