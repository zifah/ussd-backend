﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.LoggedSMSs
{
    /// <summary>
    /// Contains split beneficiaries of the drawdown process
    /// </summary>
    [Table("LoggedSMSs")]
    public class LoggedSms : Entity<long>, IHasCreationTime
    {
        public LoggedSms()
        {
            CreationTime = DateTime.Now;
        }

        public virtual string Subject { set; get; }
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastUpdateTime { set; get; }
        public virtual string Recipient { set; get; }
    }
}
