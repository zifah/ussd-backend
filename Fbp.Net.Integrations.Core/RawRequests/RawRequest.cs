﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Fbp.Net.Integrations.RawRequests
{
    /// <summary>
    /// ATM terminal
    /// </summary>
    [Table("RawRequests")]
    public class RawRequest : Entity<long>, IHasCreationTime, IModificationAudited
    {
        public RawRequest()
        {
        }

        public virtual string RequestString { set; get; }
        public virtual string ResponseString { set; get; }

        public virtual string SourceIpAddress { set; get; }

        /// <summary>
        /// example: ChargeCustomerInput
        /// </summary>
        public virtual string ObjectName { set; get; }
        
        public virtual string MethodName { set; get; }

        /// <summary>
        /// <para>Id of the object which was created on main table to service this request</para>
        /// <para>It will be repeated for multiple requests for an idempotent transaction</para>
        /// </summary>        
        public virtual long? RelatedObjectId { set; get; }
        /// <summary>
        /// No need to ever explicitly set this
        /// </summary>
        public virtual DateTime CreationTime { set; get; }
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { get; set; }
    }
}
