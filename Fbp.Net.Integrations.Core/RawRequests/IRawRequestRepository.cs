﻿using Abp.Domain.Repositories;
using Fbp.Net.Integrations.RawRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.RawRequests
{
    public interface IRawRequestRepository : IRepository<RawRequest, long>
    {

    }
}
