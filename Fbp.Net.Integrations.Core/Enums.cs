﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations
{
    public enum CustomerInfoKey { Email = 1, BVN = 2 }
    public enum UserType { InstantBanking = 1, MobileMoney = 2 }
}
