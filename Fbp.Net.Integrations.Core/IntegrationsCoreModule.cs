﻿using System.Reflection;
using Abp.Modules;

namespace Fbp.Net.Integrations
{
    public class IntegrationsCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
