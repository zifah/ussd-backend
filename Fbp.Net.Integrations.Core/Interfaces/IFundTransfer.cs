﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Fbp.Net.Integrations.Interfaces
{
    public interface IFundTransfer
    {
        Decimal Amount { set; get; }
        Decimal ChargeAmount { set; get; }
        string SourceAccount { set; get; }
        string DestinationAccount { set; get; }
        int RetRefNumber { set; get; }
        string ClientReference { set; get; }
        string ResponseCode { set; get; }
        string ResponseDescription { set; get; }
        string Narration { set; get; }
        DateTime RequestTime { set; get; }

        /// <summary>
        /// Serialized content
        /// </summary>
        string ResponseContent { set; get; }
        string Purpose { set; get; }
    }
}
