﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.Interfaces
{
    public interface IUpdaterDto : IInputDto
    {
        long Id { set; get; }
    }
}
