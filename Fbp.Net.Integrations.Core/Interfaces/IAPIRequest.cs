﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Fbp.Net.Integrations.MTNOnDemand.Interfaces
{
    public interface IAPIRequest
    {
        /// <summary>
        /// To cater for time discrepancies between client and server systems. 
        /// <para>Most important for FT requests where you need to aggregate all transaction for a certain day</para>
        /// </summary>
        DateTime ClientRequestTime { set; get; }

        string ResponseCode { set; get; }

        string ResponseDescription { set; get; }

        DateTime? LastUpdateTime { set; get; }
    }
}
