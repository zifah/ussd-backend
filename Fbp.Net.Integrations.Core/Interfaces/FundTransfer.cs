﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Fbp.Net.Integrations.Interfaces;

namespace Fbp.Net.Integrations.Interfaces
{
    public abstract class FundTransfer : Entity<long>, IHasCreationTime, IModificationAudited, IFundTransfer
    {
        public FundTransfer()
        {
            CreationTime = DateTime.Now;
        }

        public virtual Decimal Amount { set; get; }
        public virtual Decimal ChargeAmount { set; get; }
        public virtual string SourceAccount { set; get; }
        public virtual string DestinationAccount { set; get; }
        public virtual int RetRefNumber { set; get; }
        public virtual string ClientReference { set; get; }
        public virtual string ClientUsername { set; get; }
        public virtual string ResponseCode { set; get; }
        public virtual string ResponseDescription { set; get; }
        public virtual string Narration { set; get; }
        public virtual DateTime RequestTime { set; get; }

        /// <summary>
        /// Serialized content
        /// </summary>
        public virtual string ResponseContent { set; get; }
        public virtual string Purpose { set; get; }

        // auditing
        public virtual DateTime? LastModificationTime { set; get; }
        public virtual long? LastModifierUserId { set; get; }
        public virtual DateTime CreationTime { set; get; }

    }
}
