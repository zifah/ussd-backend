﻿using Abp.Web.Mvc.Views;

namespace FidelityBank.MTNOnDemand.Web.Views
{
    public abstract class MTNOnDemandWebViewPageBase : MTNOnDemandWebViewPageBase<dynamic>
    {

    }

    public abstract class MTNOnDemandWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected MTNOnDemandWebViewPageBase()
        {
            LocalizationSourceName = IntegrationsConsts.LocalizationSourceName;
        }
    }
}