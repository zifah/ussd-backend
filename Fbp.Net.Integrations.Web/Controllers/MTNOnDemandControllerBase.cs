﻿using Abp.Web.Mvc.Controllers;

namespace FidelityBank.MTNOnDemand.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class MTNOnDemandControllerBase : AbpController
    {
        protected MTNOnDemandControllerBase()
        {
            LocalizationSourceName = IntegrationsConsts.LocalizationSourceName;
        }
    }
}