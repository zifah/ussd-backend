﻿using System.Web.Mvc;

namespace FidelityBank.MTNOnDemand.Web.Controllers
{
    public class HomeController : MTNOnDemandControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}