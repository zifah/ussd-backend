﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FidelityBank.MTNOnDemand.Fincore
{
    [Serializable()]
    public class Account
    {
        [XmlElement("acct_number")]
        public string Number { set; get; }

        [XmlElement("acct_name")]
        public string Name { set; get; }

        [XmlElement("acct_balance")]
        public string Balance { set; get; }

        public decimal BalanceNumeric
        {
            get
            {
                decimal result = 0;

                if (!string.IsNullOrWhiteSpace(Balance))
                {
                    Decimal.TryParse(Balance, out result);
                }

                return result;
            }
        }
    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("accounts")]
    public class Accounts
    {
        public Accounts()
        {

        }

        [XmlElement("account")]
        public Account[] AccountList { set; get; }

        [XmlElement("mobile")]
        public string MSISDN { set; get; }

        [XmlElement("response_code")]
        public string ResponseCode { set; get; }
    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("account_details")]
    public class AccountDetails
    {
        [XmlElement("acct_number")]
        public string Number { set; get; }

        [XmlElement("acct_crncy")]
        public string Currency { set; get; }

        [XmlElement("acct_name")]
        public string Name { set; get; }

        [XmlElement("sol_id")]
        public string SolId { set; get; }

        [XmlElement("schm_code")]
        public string SchemeCode { set; get; }

        [XmlElement("acct_opn_date")]
        public string OpenedDateText { set; get; }

        [XmlElement("status")]
        public string Status { set; get; }

        [XmlElement("response_code")]
        public string ResponseCode { set; get; }


        public bool IsActive
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Status) && Status.Trim().ToLower() == "active";
            }
        }

        public DateTime? OpenedDate
        {
            get
            {
                try
                {
                    var value = Convert.ToDateTime(OpenedDateText);
                    return value;
                }

                catch
                {
                    return null;
                }
            }
        }
    }
}
