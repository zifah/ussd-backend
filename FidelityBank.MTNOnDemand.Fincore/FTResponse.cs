﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.Fincore
{
    public class FTResponse
    {
        public virtual string ResponseCode { set; get; }
        public virtual string ResponseDescription { set; get; }
    }
}
