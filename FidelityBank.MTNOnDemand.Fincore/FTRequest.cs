﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FidelityBank.MTNOnDemand.Fincore
{
    public class FTRequest
    {
        public virtual double Amount { set; get; }
        public virtual DateTime TransactionTime { set; get; }
        public virtual int RetrievalRefNum { set; get; }
        public virtual string FromAcct { set; get; }
        public virtual string ToAcct { set; get; }
        public virtual string Narration { set; get; }
    }
}
