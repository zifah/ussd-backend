﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FidelityBank.MTNOnDemand.Fincore.FincoreService;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;

namespace FidelityBank.MTNOnDemand.Fincore
{
    /// <summary>
    /// <para>Each instance of this class should be used for one method call only</para>
    /// <para>Throws exception only when there is an error talking to Fincore</para>
    /// </summary>
    public class FincoreSystem
    {
        //delegate GetAccountDetailsFa
        private static string _responsesFilePath;

        public FincoreSystem(string responsesFilePath)
        {
            _responsesFilePath = responsesFilePath;
        }

        private FinCoreIBSoapClient GetFincoreClient()
        {
            var fincoreClient = new FinCoreIBSoapClient();
            return fincoreClient;
        }

        /// <summary>
        /// <para>Returns null if MSISDN is not found</para>
        /// <para>Throws an exception if there is error communicating with API</para>
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        public Accounts GetAccountsByMsisdn(string msisdn)
        {
            var client = GetFincoreClient();
            Accounts result = null;

            try
            {
                var response = client.GetAccountsByMobileNumber(msisdn);

                result = AccountsXmlToList(response);

                client.Close();
            }

            catch (Exception ex)
            {
                client.Abort();
                throw ex;
            }

            return result;
        }

        private Accounts AccountsXmlToList(string xml)
        {
            Accounts accounts = null;

            //TODO: Deserialize XML
            XmlSerializer serializer = new XmlSerializer(typeof(Accounts));

            try
            {
                accounts = (Accounts)serializer.Deserialize(new StringReader(xml));
            }

            catch (Exception ex)
            {
                //Log: There was an error deserializing the API response. Possibly bad response.
            }

            return accounts;
        }

        /// <summary>
        /// <para>Response will be null if no response from Fincore</para>
        /// <para>Throw exception if exception on web service call. Exeception should be handled in calling library</para>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public FTResponse DoFundsTransfer(FTRequest request)
        {
            FTResponse response = null;
            var client = GetFincoreClient();

            if (request != null)
            {
                double chargeAmount = 0;
                bool vatInclusive = false;

                try
                {
                    var responseCode = client.FundTransfer(request.TransactionTime, request.FromAcct, request.ToAcct,
                        request.Amount, chargeAmount, request.Narration, request.RetrievalRefNum, vatInclusive);

                    response = new FTResponse
                    {
                        ResponseCode = responseCode, 
                        ResponseDescription = GetDescription(responseCode, _responsesFilePath)
                    };

                    client.Close();
                }

                catch (Exception ex)
                {
                    client.Abort();
                    throw ex;
                }
            }

            return response;
        }

        /// <summary>
        /// Source account must be suspense account
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public FTResponse ReverseFundsTransfer(FTRequest request)
        {
            FTRevResponse response = null;
            var client = GetFincoreClient();

            if (request != null)
            {
                double chargeAmount = 0;

                try
                {
                    var responseCode = client.FundTransferRev(request.TransactionTime, request.FromAcct, request.ToAcct,
                        request.Amount, chargeAmount, request.Narration, request.RetrievalRefNum);

                    response = new FTRevResponse
                    {
                        ResponseCode = responseCode,
                        ResponseDescription = GetDescription(responseCode, _responsesFilePath)
                    };

                    client.Close();
                }

                catch (Exception ex)
                {
                    client.Abort();
                    throw ex;
                }
            }

            return response;
        }

        public AccountDetails GetAccountDetails(string accountNumber)
        {
            var client = GetFincoreClient();
            AccountDetails details = null;

            try
            {
                string acctDetailsXml = client.GetAccountDetails(accountNumber);
                XmlSerializer serializer = new XmlSerializer((typeof(AccountDetails)));
                details = (AccountDetails)serializer.Deserialize(new StringReader(acctDetailsXml));
                client.Close();
            }

            catch (Exception ex)
            {
                client.Abort();
                throw ex;
            }

            return details;
        }

        /// <summary>
        /// Will return accountDetails only if phone is linked to account on Finacle
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public AccountDetails GetAccountDetails(string accountNumber, string msisdn)
        {
            var msissdnAccounts = GetAccountsByMsisdn(msisdn);

            if (msissdnAccounts.AccountList.Select(x => x.Number).Contains(accountNumber))
            {
                return GetAccountDetails(accountNumber);
            }

            return null;
        }

        private string GetDescription(string responseCode, string responsesFilePath)
        {
            var dictionary = DeserializeFincoreResponseCodes(responsesFilePath);

            string description = null;

            try
            {
                description = dictionary[responseCode];
            }

            catch 
            {
                throw;
            }

            return description;
        }

        private static Dictionary<string, string> DeserializeFincoreResponseCodes(string filePath)
        {
            var document = XDocument.Load(filePath);

            var dictionary = document.Root.Elements("add")
                .ToDictionary(
                    xe => xe.Attribute("key").Value,
                    xe => xe.Attribute("value").Value);

            return dictionary;
        }
    }
}
