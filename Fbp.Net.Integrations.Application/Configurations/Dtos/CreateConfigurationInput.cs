﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.Configurations.Dtos
{
    public class CreateConfigurationInput : IInputDto
    {
        public CreateConfigurationInput()
        {

        }

        [Required]
        public string Name { set; get; }
        [Required]
        public string Value { set; get; }
        [Required]
        public string Description { set; get; }
    }
}
