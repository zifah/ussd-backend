﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.Configurations.Dtos
{
    public class UpdateConfigurationInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
     
        //public string Name { set; get; }
       
        public string Value { set; get; }
       
        public string Description { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            if (Value == null && Description == null)
            {
                results.Add(new ValidationResult("All properties can not be null in order to update a Configuration!",
                    new[] {"Value", "Description" }));
            }
        }
    }
}
