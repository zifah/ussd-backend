﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.Configurations.Dtos
{

    [AutoMapFrom(typeof(Configuration))]
    public class ConfigurationDto : EntityDto<long>
    {
        public string Name { set; get; }
        public string Value { set; get; }
        public string Description { set; get; }
        public DateTime CreationTime { set; get; }
        public bool IsDeleted { set; get; }
    }
}
