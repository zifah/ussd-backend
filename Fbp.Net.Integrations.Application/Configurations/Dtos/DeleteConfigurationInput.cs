﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.Configurations.Dtos
{
    public class DeleteConfigurationInput : IInputDto
    {
        public DeleteConfigurationInput()
        {

        }

        public string Name { set; get; }
        public long? Id { set; get; }
    }
}
