﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Fbp.Net.Integrations.MTNOnDemand.Interfaces;
using AutoMapper;
using System.Collections.Generic;
using Fbp.Net.Integrations.Configurations.Dtos;
using Fbp.Net.Integrations.Exceptions;

namespace Fbp.Net.Integrations.Configurations
{
    public class ConfigurationAppService : IConfigurationAppService
    {
        private readonly IConfigurationRepository _ownRepository;

        private readonly CoreAppService<Configuration, ConfigurationDto, CreateConfigurationInput,
            UpdateConfigurationInput, IConfigurationRepository> _coreAppService;

        public ConfigurationAppService(IConfigurationRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<Configuration, ConfigurationDto, CreateConfigurationInput, UpdateConfigurationInput, IConfigurationRepository>
            (_ownRepository);
        }

        public ConfigurationDto Create(CreateConfigurationInput input)
        {
            ConfigurationDto result = _coreAppService.Create(input);
            return result;
        }

        public virtual ConfigurationDto Update(UpdateConfigurationInput input)
        {
            var result = _coreAppService.Update(input);
            return result;
        }

        public void DeleteConfiguration(DeleteConfigurationInput input)
        {
            if (input != null && input.Id.HasValue)
            {
                var existing = _ownRepository.Get(input.Id.Value);

                if (existing != null)
                {
                    existing.IsDeleted = true;
                }
            }
        }

        public ConfigurationDto GetByName(string name)
        {
            ConfigurationDto result = null;

            var retrieved = _ownRepository.GetByName(name);
            result = Mapper.Map<ConfigurationDto>(retrieved);

            return result;
        }

        public T GetByName<T>(string name, T defaultValue)
        {
            T returnValue = defaultValue;
            string configValue = null;
            ConfigurationDto dbConfig = null;
            var destinationType = typeof(T);


            if ((dbConfig = GetByName(name)) != null)
            {
                configValue = dbConfig.Value;

                try
                {
                    returnValue = (T)Convert.ChangeType(configValue, destinationType);
                }

                catch (Exception ex)
                {
                    LoggingSystem.LogMessage(string.Format("Error getting DbAppSetting: {0}", name));
                    LoggingSystem.LogException(ex);
                }
            }

            return returnValue;
        }

        public T GetByName<T>(string name)
        {
            T result;
            string configValue = null;
            var destinationType = typeof(T);
            ConfigurationDto dbConfig = GetByName(name);

            try
            {
                configValue = dbConfig.Value;
                result = (T)Convert.ChangeType(configValue, destinationType);
                return result;
            }

            catch (Exception ex)
            {
                LoggingSystem.LogMessage(string.Format("Could not get or convert DbAppSetting: {0} to specified type: {1}", name, destinationType));
                LoggingSystem.LogException(ex);
                throw new InvalidConfigurationException(string.Format("Database configuration {0} either does not exist or cannot be converted to specified type: {1}", name, destinationType));
            }
        }

        public IList<ConfigurationDto> GetAll()
        {
            var temp = _ownRepository.GetAllList();
            var result = Mapper.Map<IList<ConfigurationDto>>(temp);
            return result;
        }
    }
}