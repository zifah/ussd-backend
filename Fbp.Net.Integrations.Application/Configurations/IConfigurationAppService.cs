﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Fbp.Net.Integrations.Configurations.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.Configurations
{
    public interface IConfigurationAppService : ICoreAppService<ConfigurationDto, CreateConfigurationInput, UpdateConfigurationInput>
    {
        void DeleteConfiguration(DeleteConfigurationInput input);
        ConfigurationDto GetByName(string name);
        T GetByName<T>(string name, T defaultValue);
        T GetByName<T>(string name);
        IList<ConfigurationDto> GetAll();
    }
}
