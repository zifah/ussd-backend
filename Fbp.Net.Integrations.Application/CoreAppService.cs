﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fbp.Net.Integrations.Interfaces;
using Abp.Domain.Entities;

namespace Fbp.Net.Integrations
{
    internal class CoreAppService<T, TDto, TCreateInput, TUpdateInput, TIRepository> : ICoreAppService<TDto, TCreateInput, TUpdateInput> 
        where T : Entity<long> where TIRepository : IRepository<T, long>
        where TDto : EntityDto<long>
        where TCreateInput : class, IInputDto
        where TUpdateInput : class, IUpdaterDto
    {
        protected readonly TIRepository _ownRepository;

        public CoreAppService(TIRepository ownRepository)
        {
            _ownRepository = ownRepository;
        }

        public virtual TDto Create(TCreateInput input)
        {
            TDto result = null;

            if (input != null)
            {
                var newObject = Activator.CreateInstance<T>();
                var exclusionList = new List<string>();
                newObject = input.CopyProperties<TCreateInput, T>(newObject, exclusionList);
                var savedObject = _ownRepository.Insert(newObject);
                result = Mapper.Map<TDto>(savedObject);
            }

            return result;
        }

        public virtual TDto Update(TUpdateInput input)
        {
            TDto result = null;

            if (input != null)
            {
                var existing = _ownRepository.Get(input.Id);

                if (existing != null)
                {
                    var exclusionList = new List<string>();
                    exclusionList.Add("Id");
                    var updated = input.CopyProperties<TUpdateInput, T>(existing, exclusionList);
                    result = Mapper.Map<TDto>(updated);
                }
            }

            return result;
        }
    }
}
