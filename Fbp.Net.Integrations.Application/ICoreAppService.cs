﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations
{
    public interface ICoreAppService<TDto, TCreateInput, TUpdateInput> : IApplicationService 
        where TDto : EntityDto<long>
        where TCreateInput : class
    {
        TDto Create(TCreateInput input);
        TDto Update(TUpdateInput input);
    }
}
