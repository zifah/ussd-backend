﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.RawRequests.Dtos
{
    public class CreateRawRequestInput : IInputDto
    {
        public CreateRawRequestInput()
        {
        }
        [Required]
        public string RequestString { set; get; }
        [Required]
        public string ResponseString { set; get; }
        [Required]
        public string MethodName { set; get; }
        public long? RelatedObjectId { set; get; }
        [Required]
        public string SourceIpAddress { set; get; }
        [Required]
        public string ObjectName { set; get; }
    }
}
