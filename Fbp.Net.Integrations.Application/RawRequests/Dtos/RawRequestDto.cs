﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.RawRequests.Dtos
{
    [AutoMapFrom(typeof(RawRequest))]
    public class RawRequestDto : EntityDto<long>
    {
        public string RequestString { set; get; }
        public string ResponseString { set; get; }
        public string SourceIpAddress { set; get; }

        public string ObjectName { set; get; }

        public string MethodName { set; get; }
     
        public long? RelatedObjectId { set; get; }

        public DateTime CreationTime { set; get; }

        public DateTime? LastModificationTime { set; get; }
    
    }
}
