﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Fbp.Net.Integrations.RawRequests.Dtos;
using Fbp.Net.Integrations.RawRequests;

namespace Fbp.Net.Integrations.RawRequests
{
    public class RawRequestAppService : IRawRequestAppService
    {
        private readonly IRawRequestRepository _ownRepository;
        private readonly CoreAppService<RawRequest, RawRequestDto, CreateRawRequestInput,
            UpdateRawRequestInput, IRawRequestRepository> _coreAppService;

        public RawRequestAppService(IRawRequestRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<RawRequest, RawRequestDto, CreateRawRequestInput,
            UpdateRawRequestInput, IRawRequestRepository>(ownRepository);
        }

        public virtual RawRequestDto Create(CreateRawRequestInput input)
        {
            var result = _coreAppService.Create(input);
            return result;
        }

        public virtual RawRequestDto Update(UpdateRawRequestInput input)
        {
            var result = _coreAppService.Update(input);
            return result;
        }
    }
}
