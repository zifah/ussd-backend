﻿using Abp.Application.Services;
using Fbp.Net.Integrations.RawRequests.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.RawRequests
{
    public interface IRawRequestAppService : ICoreAppService<RawRequestDto, CreateRawRequestInput, UpdateRawRequestInput>
    {
        
    }
}
