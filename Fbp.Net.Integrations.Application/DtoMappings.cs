﻿using AutoMapper;
using Fbp.Net.Integrations.Configurations;
using Fbp.Net.Integrations.Configurations.Dtos;
using Fbp.Net.Integrations.InfoUpdates;
using Fbp.Net.Integrations.InfoUpdates.Dtos;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers.Dtos;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos;
using Fbp.Net.Integrations.DuisApp.TransactionCharges;
using Fbp.Net.Integrations.DuisApp.TransactionCharges.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using Fbp.Net.Integrations.DuisApp.TransactionTypes.Dtos;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos;
using Fbp.Net.Integrations.DuisApp.WeakPasswords;
using Fbp.Net.Integrations.DuisApp.WeakPasswords.Dtos;
using Fbp.Net.Integrations.DuisApp.RequestLogs;
using Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos;
using Fbp.Net.Integrations.DuisApp.Validations.Dtos;

namespace Fbp.Net.Integrations
{
    internal static class DtoMappings
    {
        public static void Map()
        {
            //I specified mapping for AssignedPersonId since NHibernate does not fill Task.AssignedPersonId
            //If you will just use EF, then you can remove ForMember definition.

            // DUIS
            Mapper.CreateMap<Configuration, ConfigurationDto>();
            Mapper.CreateMap<Enrolment, EnrolmentDto>();
            Mapper.CreateMap<PhoneNetworkLog, PhoneNetworkLogDto>();
            Mapper.CreateMap<AirtimeRechargeBiller, AirtimeRechargeBillerDto>();
            Mapper.CreateMap<BillPaymentConfig, BillPaymentConfigDto>();
            Mapper.CreateMap<Transaction, TransactionDto>();
            Mapper.CreateMap<UserProfile, UserProfileDto>();
            Mapper.CreateMap<InfoUpdate, InfoUpdateDto>();
            Mapper.CreateMap<TransactionType, TransactionTypeDto>();
            Mapper.CreateMap<TransactionCharge, TransactionChargeDto>();
            Mapper.CreateMap<WeakPassword, WeakPasswordDto>();
            Mapper.CreateMap<RequestLog, RequestLogDto>();
            Mapper.CreateMap<DuisApp.Validations.Validation, ValidationDto>();
            Mapper.CreateMap<DuisApp.NewBankAccounts.NewBankAccount, DuisApp.NewBankAccounts.Dtos.NewBankAccountDto>();
            Mapper.CreateMap<DuisApp.ThirdPartyOtps.ThirdPartyOtp, DuisApp.ThirdPartyOtps.Dtos.ThirdPartyOtpDto>();
        }
    }
}
