﻿using Abp.Runtime.Validation;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations
{
    public static class Utility
    {
        public static TDestination CopyProperties<TSource, TDestination>(this TSource source, TDestination destination, IList<string> excludedProperties)
            where TSource : class
            where TDestination : class
        {
            PropertyInfo[] sourceProps = typeof(TSource).GetProperties();
            PropertyInfo[] destProps = typeof(TDestination).GetProperties();

            if (destination == null)
            {
                destination = Activator.CreateInstance<TDestination>();
            }

            foreach (var sourceProp in sourceProps)
            {
                var destProp = destProps.SingleOrDefault(x => !excludedProperties.Contains(x.Name) && x.Name == sourceProp.Name);

                if (destProp != null)
                {
                    var sourceValue = sourceProp.GetValue(source);

                    // copy across only non-null values
                    if (sourceValue != null)
                    {
                        destProp.SetValue(destination, sourceValue);
                    }
                }
            }
            return destination;
        }

        internal static void AddValidationErrors<T>(T instance, List<ValidationResult> results) where T : IUpdaterDto, ICustomValidate
        {
            var classType = typeof(T);

            var updatableProperties = classType.GetProperties().Where(x => x.Name != "Id");

            var hasNonNullProperty = false;

            foreach (var property in updatableProperties)
            {
                hasNonNullProperty = property.GetValue(instance) != null;

                if (hasNonNullProperty)
                {
                    break;
                }
            }

            if (!hasNonNullProperty)
            {
                results.Add(
                    new ValidationResult(
                        string.Format("All properties can not be null in order to update a {0}!", classType.Name),
                    updatableProperties.Select(x => x.Name)));
            }
        }
    }
}
