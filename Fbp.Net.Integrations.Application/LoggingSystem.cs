﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations
{
    public class LoggingSystem
    {
        public static void LogException(Exception ex, string message)
        {
            LogMessage(message);
            LogException(ex);
        }

        public static void LogException(Exception ex)
        {
            var exceptionMessage = ex.Message;
            string innerExceptionMessage = null;

            if (ex.InnerException != null)
            {
                innerExceptionMessage = ex.InnerException.Message;
            }

            string fullMessage = string.Format("Exception: {0}; Inner exception: {1}/r/n Stack Trace: {2}", exceptionMessage, innerExceptionMessage, ex.StackTrace);

            LogMessage(fullMessage);
            Console.WriteLine();
        }

        public static void LogMessage(string message)
        {
            //Console.WriteLine(string.Format("Information: {0};", message));
            StreamWriter log;

            string logFilePath =  ConfigurationManager.AppSettings[ApplicationConstants.AppSettingLogFilePath];

            try
            {
                var directoryName = Path.GetDirectoryName(logFilePath);
                var fileName = Path.GetFileName(logFilePath);

                directoryName = string.Format("{0}\\{1}", directoryName, DateTime.Today.ToString("yyyyMMdd"));

                var logFile = string.Format("{0}\\{1}", directoryName, fileName);

                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }

                if (!File.Exists(logFile))
                {
                    log = new StreamWriter(logFile);
                }
                else
                {
                    log = File.AppendText(logFile);
                }

                // Write to the file:
                log.WriteLine(DateTime.Now);
                log.WriteLine(message);
                log.WriteLine();

                // Close the stream:
                log.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error logging information: {0} to file. Error is {1}.", message, ex.Message);
            }
        }

    }
}
