﻿using AutoMapper;
using Fbp.Net.Integrations.InfoUpdates.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.InfoUpdates
{
    public class InfoUpdateAppService : IInfoUpdateAppService
    {
        private readonly IInfoUpdateRepository _ownRepository;
        private readonly CoreAppService<InfoUpdate, InfoUpdateDto, CreateInfoUpdateInput,
            UpdateInfoUpdateInput, IInfoUpdateRepository> _coreAppService;

        private static readonly object _locker = new object();
        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public InfoUpdateAppService(IInfoUpdateRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<InfoUpdate, InfoUpdateDto, CreateInfoUpdateInput,
            UpdateInfoUpdateInput, IInfoUpdateRepository>(ownRepository);
        }

        public InfoUpdateDto Create(CreateInfoUpdateInput input)
        {
            InfoUpdateDto result = _coreAppService.Create(input);
            return result;
        }

        public InfoUpdateDto Update(UpdateInfoUpdateInput input)
        {
            var result = _coreAppService.Update(input);
            return result;
        }
    }
}
