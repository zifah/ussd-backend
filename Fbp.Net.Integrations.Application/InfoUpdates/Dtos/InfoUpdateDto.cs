﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.InfoUpdates.Dtos
{
    [AutoMapFrom(typeof(InfoUpdate))]
    public class InfoUpdateDto : EntityDto<long>
    {
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
        public CustomerInfoKey InfoType { set; get; }
        public string NewValue { set; get; }
        public string AccountNumber { set; get; }
        public string CustomerUsername { set; get; }
        public string ClientName { set; get; }
        public string Reason { set; get; }
        public string ClientReference { set; get; }
        public bool IsDeleted { get; set; }
    }
}
