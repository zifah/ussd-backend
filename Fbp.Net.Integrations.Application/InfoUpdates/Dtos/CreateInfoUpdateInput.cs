﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.InfoUpdates.Dtos
{
    public class CreateInfoUpdateInput : IInputDto
    {
        public CustomerInfoKey InfoType { set; get; }
        public string NewValue { set; get; }
        public string AccountNumber { set; get; }
        public string Msisdn { set; get; }
        public string Reason { set; get; }
        public long UserId { set; get; }
        public string ClientReference { set; get; }
    }
}
