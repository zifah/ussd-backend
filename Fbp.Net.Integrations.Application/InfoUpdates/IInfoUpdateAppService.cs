﻿using Abp.Application.Services;
using Fbp.Net.Integrations.InfoUpdates.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.InfoUpdates
{
    public interface IInfoUpdateAppService : ICoreAppService<InfoUpdateDto, CreateInfoUpdateInput, UpdateInfoUpdateInput>
    {
    }
}
