﻿using System.Reflection;
using Abp.Modules;

namespace Fbp.Net.Integrations
{
    [DependsOn(typeof(IntegrationsCoreModule))]
    public class IntegrationsApplicationModule : AbpModule
    {
        public override void Initialize()
        {

            LoggingSystem.LogMessage("Started initializing IntegrationsApplicationModule");
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //We must declare mappings to be able to use AutoMapper
            DtoMappings.Map();

            LoggingSystem.LogMessage("Finished initializing IntegrationsApplicationModule");
        }
    }
}
