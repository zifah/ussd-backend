﻿using Abp.Application.Services;

namespace FidelityBank.MTNOnDemand
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class IntegrationsAppServiceBase : ApplicationService
    {
        protected IntegrationsAppServiceBase()
        {
            LocalizationSourceName = IntegrationsConsts.LocalizationSourceName;
        }
    }
}