﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Fbp.Net.Integrations.DuisApp.Validations.Dtos
{
    public class CreateValidationInput : IInputDto
    {
        public CreateValidationInput()
        {

        }

        [Required]
        public string BillerId { set; get; }
        [Required]
        public string BillerName { set; get; }
        [Required]
        public string ClientReference { set; get; }
        public string RawResponse { set; get; }
        public string CustomerId { set; get; }
        public string CustomerName { set; get; }
        public decimal Amount { set; get; }
        public string ProviderReference { set; get; }
    }
}
