﻿using Abp.Runtime.Validation;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Validations.Dtos
{
    public class UpdateValidationInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
        public string BillerId { set; get; }
        public string BillerName { set; get; }
        public string ClientReference { set; get; }
        public string RawResponse { set; get; }
        public string CustomerId { set; get; }
        public string CustomerName { set; get; }
        public decimal Amount { set; get; }
        public string ProviderReference { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
