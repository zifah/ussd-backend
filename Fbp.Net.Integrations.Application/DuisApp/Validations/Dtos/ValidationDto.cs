﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Validations.Dtos
{
    [AutoMapFrom(typeof(Validation))]
    public class ValidationDto : EntityDto<long>
    {
        public string BillerId { set; get; }
        public string BillerName { set; get; }
        public string ItemId { set; get; }
        public string ItemName { set; get; }
        public string ClientReference { set; get; }
        public string RawResponse { set; get; }
        public string CustomerId { set; get; }
        public string CustomerName { set; get; }
        public decimal Amount { set; get; }
        public string ProviderReference { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
    }
}
