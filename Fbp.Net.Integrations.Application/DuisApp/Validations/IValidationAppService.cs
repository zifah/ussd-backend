﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.Validations.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Validations
{
    public interface IValidationAppService : ICoreAppService<ValidationDto, CreateValidationInput, UpdateValidationInput>
    {
        ValidationDto GetBy(string customerId, string billerId);
    }
}
