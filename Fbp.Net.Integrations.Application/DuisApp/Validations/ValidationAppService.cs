﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.Validations.Dtos;
using System;
using System.Threading;

namespace Fbp.Net.Integrations.DuisApp.Validations
{
    public class ValidationAppService : IValidationAppService
    {
        private readonly IValidationRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<Validation, ValidationDto, CreateValidationInput,
            UpdateValidationInput, IValidationRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public ValidationAppService(IValidationRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<Validation, ValidationDto, CreateValidationInput,
            UpdateValidationInput, IValidationRepository>(ownRepository);
        }
        public ValidationDto Create(CreateValidationInput input)
        {
            ValidationDto result = _coreAppService.Create(input);
            return result;
        }

        public ValidationDto Update(UpdateValidationInput input)
        {
            ValidationDto result = _coreAppService.Update(input);
            return result;
        }


        public ValidationDto GetBy(string customerId, string billerId)
        {
            var raw = _ownRepository.GetBy(customerId, billerId);

            var result = Mapper.Map<Validation, ValidationDto>(raw);

            return result;
        }

    }
}
