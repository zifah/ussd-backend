﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.ThirdPartyOtps.Dtos;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps
{
    public class ThirdPartyOtpAppService : IThirdPartyOtpAppService
    {
        private readonly IThirdPartyOtpRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<ThirdPartyOtp, ThirdPartyOtpDto, CreateThirdPartyOtpInput,
            UpdateThirdPartyOtpInput, IThirdPartyOtpRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public ThirdPartyOtpAppService(IThirdPartyOtpRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<ThirdPartyOtp, ThirdPartyOtpDto, CreateThirdPartyOtpInput,
            UpdateThirdPartyOtpInput, IThirdPartyOtpRepository>(ownRepository);
        }
        public ThirdPartyOtpDto Create(CreateThirdPartyOtpInput input)
        {
            ThirdPartyOtpDto result = _coreAppService.Create(input);
            return result;
        }

        public ThirdPartyOtpDto Update(UpdateThirdPartyOtpInput input)
        {
            ThirdPartyOtpDto result = _coreAppService.Update(input);
            return result;
        }

        public ThirdPartyOtpDto GetLatestOtpByPhone(string phone, string purpose)
        {
            return Mapper.Map<ThirdPartyOtpDto>(_ownRepository.GetLatestOtpByPhone(phone, purpose));
        }

        public ThirdPartyOtpDto GetLatestOtpByAccount(string account, string purpose)
        {
            return Mapper.Map<ThirdPartyOtpDto>(_ownRepository.GetLatestOtpByAccount(account, purpose));
        }

        public ThirdPartyOtpDto GetLatestOtpByPhoneAccount(string phone, string account, string purpose)
        {
            return Mapper.Map<ThirdPartyOtpDto>(_ownRepository.GetLatestOtpByPhoneAccount(phone, account, purpose));
        }
    }
}
