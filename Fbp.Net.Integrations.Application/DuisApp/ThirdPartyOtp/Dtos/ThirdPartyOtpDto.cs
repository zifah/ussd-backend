﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps.Dtos
{
    [AutoMapFrom(typeof(ThirdPartyOtp))]
    public class ThirdPartyOtpDto : EntityDto<long>
    {
        public long UserId { set; get; }
        public string UserMsisdn { set; get; }
        public string OtpAccount { set; get; }
        public string Purpose { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime ExpirationTime { set; get; }
        public DateTime? UsageTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
        public int FailedTryCount { set; get; }
        public string FailedAttempts { set; get; }
        public DateTime? LastFailedAttemptTime { set; get; }
        public string Otp { set; get; }
    }
}
