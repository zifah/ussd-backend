﻿using Abp.Runtime.Validation;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps.Dtos
{
    public class UpdateThirdPartyOtpInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
        
        public DateTime? UsageTime { set; get; }
        public int? FailedTryCount { set; get; }
        public string FailedAttempts { set; get; }
        public DateTime? LastFailedAttemptTime { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
