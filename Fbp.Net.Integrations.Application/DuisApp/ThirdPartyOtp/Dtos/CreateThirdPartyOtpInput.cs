﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps.Dtos
{
    public class CreateThirdPartyOtpInput : IInputDto
    {
        public CreateThirdPartyOtpInput()
        {
        }

        [Required]
        public long UserId { set; get; }
        [Required]
        public string UserMsisdn { set; get; }
        public string OtpAccount { set; get; }
        [Required]
        public string Purpose { set; get; }
        [Required]
        public DateTime ExpirationTime { set; get; }
        [Required]
        public string Otp { set; get; }
    }
}
