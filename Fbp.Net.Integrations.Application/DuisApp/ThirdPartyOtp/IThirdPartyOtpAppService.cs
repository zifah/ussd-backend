﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.ThirdPartyOtps.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.ThirdPartyOtps
{
    public interface IThirdPartyOtpAppService : ICoreAppService<ThirdPartyOtpDto, CreateThirdPartyOtpInput, UpdateThirdPartyOtpInput>
    {
        ThirdPartyOtpDto GetLatestOtpByPhone(string phone, string purpose);

        ThirdPartyOtpDto GetLatestOtpByAccount(string account, string purpose);

        ThirdPartyOtpDto GetLatestOtpByPhoneAccount(string phone, string account, string purpose);
    }
}
