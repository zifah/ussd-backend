﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.TransactionCharges;
using Fbp.Net.Integrations.DuisApp.TransactionTypes.Dtos;
using Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionCharges.Dtos
{
    [AutoMapFrom(typeof(TransactionCharge))]
    public class TransactionChargeDto : EntityDto<long>
    {
        public UserProfileDto UserProfile { set; get; }
        public TransactionTypeDto TransactionType { set; get; }
        public Decimal Amount { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
    }
}
