﻿using Abp.Application.Services.Dto;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionCharges.Dtos
{
    public class CreateTransactionChargeInput : IInputDto
    {
        public CreateTransactionChargeInput()
        {

        }
        public UserProfile UserProfile { get { return UserProfileId.HasValue ? new UserProfile { Id = UserProfileId.Value } : null; } }
        public TransactionType TransactionType { get { return new TransactionType { Id = TransactionTypeId }; } }

        public long? UserProfileId { set; get; }
        [Required]
        public long TransactionTypeId { set; get; }
        [Required]
        public Decimal Amount { set; get; }
    }
}
