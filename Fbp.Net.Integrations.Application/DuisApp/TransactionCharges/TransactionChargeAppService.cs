﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.TransactionCharges.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionCharges
{
    public class TransactionChargeAppService : ITransactionChargeAppService
    {
        private readonly ITransactionChargeRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<TransactionCharge, TransactionChargeDto, CreateTransactionChargeInput,
            UpdateTransactionChargeInput, ITransactionChargeRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public TransactionChargeAppService(ITransactionChargeRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<TransactionCharge, TransactionChargeDto, CreateTransactionChargeInput,
            UpdateTransactionChargeInput, ITransactionChargeRepository>(ownRepository);
        }
        public TransactionChargeDto Create(CreateTransactionChargeInput input)
        {
            TransactionChargeDto result = _coreAppService.Create(input);
            return result;
        }

        public TransactionChargeDto Update(UpdateTransactionChargeInput input)
        {
            TransactionChargeDto result = _coreAppService.Update(input);
            return result;
        }
    }
}
