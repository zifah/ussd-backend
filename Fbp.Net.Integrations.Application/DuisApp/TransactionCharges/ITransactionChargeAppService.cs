﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.TransactionCharges.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionCharges
{
    public interface ITransactionChargeAppService : ICoreAppService<TransactionChargeDto, CreateTransactionChargeInput, UpdateTransactionChargeInput>
    {

    }
}
