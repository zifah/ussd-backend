﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.WeakPasswords.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.WeakPasswords
{
    public interface IWeakPasswordAppService : ICoreAppService<WeakPasswordDto, CreateWeakPasswordInput, UpdateWeakPasswordInput>
    {

    }
}
