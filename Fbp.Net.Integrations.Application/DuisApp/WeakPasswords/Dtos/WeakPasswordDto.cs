﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.WeakPasswords;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.WeakPasswords.Dtos
{
    [AutoMapFrom(typeof(WeakPassword))]
    public class WeakPasswordDto : EntityDto<long>
    {
        public string Password { set; get; } 
        public bool IsDeleted { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
    }
}
