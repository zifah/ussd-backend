﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.WeakPasswords.Dtos
{
    public class CreateWeakPasswordInput : IInputDto
    {
        public CreateWeakPasswordInput()
        {

        }
        [Required]
        public string Password { set; get; }
        public bool IsDeleted { set; get; }
    }
}
