﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.WeakPasswords.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.WeakPasswords
{
    public class WeakPasswordAppService : IWeakPasswordAppService
    {
        private readonly IWeakPasswordRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<WeakPassword, WeakPasswordDto, CreateWeakPasswordInput,
            UpdateWeakPasswordInput, IWeakPasswordRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public WeakPasswordAppService(IWeakPasswordRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<WeakPassword, WeakPasswordDto, CreateWeakPasswordInput,
            UpdateWeakPasswordInput, IWeakPasswordRepository>(ownRepository);
        }
        public WeakPasswordDto Create(CreateWeakPasswordInput input)
        {
            WeakPasswordDto result = _coreAppService.Create(input);
            return result;
        }

        public WeakPasswordDto Update(UpdateWeakPasswordInput input)
        {
            WeakPasswordDto result = _coreAppService.Update(input);
            return result;
        }
    }
}
