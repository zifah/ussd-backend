﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.NetworkPrefixes;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs
{
    public class PhoneNetworkLogAppService : IPhoneNetworkLogAppService
    {
        private readonly IPhoneNetworkLogRepository _ownRepository;
        private readonly INetworkPrefixRepository _networkPrefixRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<PhoneNetworkLog, PhoneNetworkLogDto, CreatePhoneNetworkLogInput,
            UpdatePhoneNetworkLogInput, IPhoneNetworkLogRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public PhoneNetworkLogAppService(IPhoneNetworkLogRepository ownRepository, INetworkPrefixRepository networkPrefixRepository)
        {
            _ownRepository = ownRepository;
            _networkPrefixRepository = networkPrefixRepository;
            _coreAppService = new CoreAppService<PhoneNetworkLog, PhoneNetworkLogDto, CreatePhoneNetworkLogInput,
            UpdatePhoneNetworkLogInput, IPhoneNetworkLogRepository>(ownRepository);
        }
        public PhoneNetworkLogDto Create(CreatePhoneNetworkLogInput input)
        {
            PhoneNetworkLogDto result = _coreAppService.Create(input);
            return result;
        }

        public PhoneNetworkLogDto Update(UpdatePhoneNetworkLogInput input)
        {
            PhoneNetworkLogDto result = _coreAppService.Update(input);
            return result;
        }

        /// <summary>
        /// Phone number in standard format: +2348012345678
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        public PhoneNetworkLogDto GetPhoneNetwork(string msisdn)
        {
            var raw = _ownRepository.GetPhoneNetworkLog(msisdn);
            var result = Mapper.Map<PhoneNetworkLogDto>(raw);
            return result;
        }

        public string GetPhoneNetworkFromPrefix(string msisdn)
        {
            string result = null;

            if (msisdn != null && msisdn.Length == 11)
            {
                var networkPrefix = _networkPrefixRepository.GetPhoneNetwork(msisdn);

                if(networkPrefix != null)
                {
                    result = networkPrefix.Network;
                }
            }

            return result;
        }
    }
}
