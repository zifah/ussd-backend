﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs
{
    public interface IPhoneNetworkLogAppService : ICoreAppService<PhoneNetworkLogDto, CreatePhoneNetworkLogInput, UpdatePhoneNetworkLogInput>
    {
        /// <summary>
        /// This msisdn is in +234... format
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        PhoneNetworkLogDto GetPhoneNetwork(string msisdn);
        
        /// <summary>
        /// MSISDN in 11-digit format e.g. 08012...
        /// <para>Will return null if no network is found that matches the prefix of the supplied number</para>
        /// </summary>
        /// <returns></returns>
        string GetPhoneNetworkFromPrefix(string msisdn);
    }
}
