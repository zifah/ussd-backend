﻿using Abp.Application.Services.Dto;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos
{
    public class CreatePhoneNetworkLogInput : IInputDto
    {
        public CreatePhoneNetworkLogInput()
        {

        }

        [Required]
        public string Msisdn { set; get; }
        public string AccountNumber { set; get; }
        public bool IsTransactionInitiator { set; get; }
        [Required]
        public string Network { set; get; }
    }
}
