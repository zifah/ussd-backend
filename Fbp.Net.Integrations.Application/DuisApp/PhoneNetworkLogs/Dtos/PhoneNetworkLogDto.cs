﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.PhoneNetworkLogs.Dtos
{
    [AutoMapFrom(typeof(PhoneNetworkLog))]
    public class PhoneNetworkLogDto : EntityDto<long>
    {
        public string Msisdn { set; get; }
        public string AccountNumber { set; get; }
        public bool IsTransactionInitiator { set; get; }
        public string Network { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
    }
}
