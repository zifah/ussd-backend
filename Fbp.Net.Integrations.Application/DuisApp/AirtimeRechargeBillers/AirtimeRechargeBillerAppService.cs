﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers
{
    public class AirtimeRechargeBillerAppService : IAirtimeRechargeBillerAppService
    {
        private readonly IAirtimeRechargeBillerRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<AirtimeRechargeBiller, AirtimeRechargeBillerDto, CreateAirtimeRechargeBillerInput,
            UpdateAirtimeRechargeBillerInput, IAirtimeRechargeBillerRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public AirtimeRechargeBillerAppService(IAirtimeRechargeBillerRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<AirtimeRechargeBiller, AirtimeRechargeBillerDto, CreateAirtimeRechargeBillerInput,
            UpdateAirtimeRechargeBillerInput, IAirtimeRechargeBillerRepository>(ownRepository);
        }
        public AirtimeRechargeBillerDto Create(CreateAirtimeRechargeBillerInput input)
        {
            AirtimeRechargeBillerDto result = _coreAppService.Create(input);
            return result;
        }

        public AirtimeRechargeBillerDto Update(UpdateAirtimeRechargeBillerInput input)
        {
            AirtimeRechargeBillerDto result = _coreAppService.Update(input);
            return result;
        }

        public AirtimeRechargeBillerDto GetByNetwork(string networkName)
        {
            var raw = _ownRepository.GetByNetwork(networkName);
            var result = Mapper.Map<AirtimeRechargeBillerDto>(raw);
            return result;
        }
    }
}
