﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers
{
    public interface IAirtimeRechargeBillerAppService : ICoreAppService<AirtimeRechargeBillerDto, CreateAirtimeRechargeBillerInput, UpdateAirtimeRechargeBillerInput>
    {
        AirtimeRechargeBillerDto GetByNetwork(string networkName);
    }
}
