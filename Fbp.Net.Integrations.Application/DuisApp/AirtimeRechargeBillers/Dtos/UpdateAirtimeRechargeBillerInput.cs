﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers.Dtos
{
    public class UpdateAirtimeRechargeBillerInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
        public BillPaymentConfig Biller { get { return BillerId == null ? null : new BillPaymentConfig { Id = BillerId.Value }; } }
        public string Network { set; get; }
        public string TopupPlanCode { set; get; }
        public long? BillerId { set; get; }
        public bool? IsActive { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
