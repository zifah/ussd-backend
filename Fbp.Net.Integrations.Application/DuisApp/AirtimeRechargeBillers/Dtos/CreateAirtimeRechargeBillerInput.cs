﻿using Abp.Application.Services.Dto;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers.Dtos
{
    public class CreateAirtimeRechargeBillerInput : IInputDto
    {
        public CreateAirtimeRechargeBillerInput()
        {
            IsActive = true;
        }

        public BillPaymentConfig Biller { get { return new BillPaymentConfig { Id = BillerId }; } }
        [Required]
        public string Network { set; get; }
        [Required]
        public string TopupPlanCode { set; get; }
        [Required]
        public long BillerId { set; get; }
        public bool IsActive { set; get; }
    }
}
