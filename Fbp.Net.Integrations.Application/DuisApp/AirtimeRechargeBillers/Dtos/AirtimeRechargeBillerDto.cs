﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.AirtimeRechargeBillers.Dtos
{
    [AutoMapFrom(typeof(AirtimeRechargeBiller))]
    public class AirtimeRechargeBillerDto : EntityDto<long>
    {
        public BillPaymentConfigDto Biller { set; get; }
        public string Network { set; get; }
        public string TopupPlanCode { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
        public bool IsActive { set; get; }
    }
}
