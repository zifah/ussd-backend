﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.UserProfiles
{
    public interface IUserProfileAppService : ICoreAppService<UserProfileDto, CreateUserProfileInput, UpdateUserProfileInput>
    {

    }
}
