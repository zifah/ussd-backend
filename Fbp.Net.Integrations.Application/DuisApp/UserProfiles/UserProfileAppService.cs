﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.UserProfiles
{
    public class UserProfileAppService : IUserProfileAppService
    {
        private readonly IUserProfileRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<UserProfile, UserProfileDto, CreateUserProfileInput,
            UpdateUserProfileInput, IUserProfileRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public UserProfileAppService(IUserProfileRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<UserProfile, UserProfileDto, CreateUserProfileInput,
            UpdateUserProfileInput, IUserProfileRepository>(ownRepository);
        }
        public UserProfileDto Create(CreateUserProfileInput input)
        {
            UserProfileDto result = _coreAppService.Create(input);
            return result;
        }

        public UserProfileDto Update(UpdateUserProfileInput input)
        {
            UserProfileDto result = _coreAppService.Update(input);
            return result;
        }
    }
}
