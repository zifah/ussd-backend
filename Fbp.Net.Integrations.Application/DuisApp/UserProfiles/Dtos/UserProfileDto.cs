﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos
{
    [AutoMapFrom(typeof(UserProfile))]
    public class UserProfileDto : EntityDto<long>
    {
        public string Name { set; get; }
        public string Description { set; get; } 
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
    }
}
