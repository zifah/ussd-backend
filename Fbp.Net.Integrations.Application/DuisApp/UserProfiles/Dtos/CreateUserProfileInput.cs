﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos
{
    public class CreateUserProfileInput : IInputDto
    {
        public CreateUserProfileInput()
        {

        }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Description { set; get; }
    }
}
