﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.TransactionTypes.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionTypes
{
    public interface ITransactionTypeAppService : ICoreAppService<TransactionTypeDto, CreateTransactionTypeInput, UpdateTransactionTypeInput>
    {
        TransactionTypeDto GetByName(string transactionTypeName);
    }
}
