﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionTypes.Dtos
{
    public class UpdateTransactionTypeInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
        public decimal? DefaultDailyAmount { set; get; }
        public decimal? DefaultDailyCount { set; get; }
        public decimal? MinTransactionAmount { set; get; }
        public decimal? DefaultMaxTransactionAmount { set; get; }
        public decimal? MaximumDailyAmount { set; get; }
        public decimal? MaximumDailyCount { set; get; }
        public decimal? MaximumTransactionAmount { set; get; }
        public decimal? MaximumBeforeTokenAmount { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
