﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionTypes.Dtos
{
    public class CreateTransactionTypeInput : IInputDto
    {
        public CreateTransactionTypeInput()
        {

        }

        public string Name { set; get; }
        public decimal DefaultDailyAmount { set; get; }
        public decimal DefaultDailyCount { set; get; }
        public decimal MinTransactionAmount { set; get; }
        public decimal DefaultMaxTransactionAmount { set; get; }
        public decimal MaximumDailyAmount { set; get; }
        public decimal MaximumDailyCount { set; get; }
        public decimal MaximumTransactionAmount { set; get; }
        public decimal MaximumBeforeTokenAmount { set; get; }
    }
}
