﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.TransactionTypes.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.TransactionTypes
{
    public class TransactionTypeAppService : ITransactionTypeAppService
    {
        private readonly ITransactionTypeRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<TransactionType, TransactionTypeDto, CreateTransactionTypeInput,
            UpdateTransactionTypeInput, ITransactionTypeRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public TransactionTypeAppService(ITransactionTypeRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<TransactionType, TransactionTypeDto, CreateTransactionTypeInput,
            UpdateTransactionTypeInput, ITransactionTypeRepository>(ownRepository);
        }
        public TransactionTypeDto Create(CreateTransactionTypeInput input)
        {
            TransactionTypeDto result = _coreAppService.Create(input);
            return result;
        }

        public TransactionTypeDto Update(UpdateTransactionTypeInput input)
        {
            TransactionTypeDto result = _coreAppService.Update(input);
            return result;
        }

        public TransactionTypeDto GetByName(string transactionTypeName)
        {
            var raw = _ownRepository.GetByName(transactionTypeName);
            var result = Mapper.Map<TransactionTypeDto>(raw);
            return result;
        }
    }
}
