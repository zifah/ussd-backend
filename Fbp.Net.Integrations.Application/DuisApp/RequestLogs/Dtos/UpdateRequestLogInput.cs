﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos
{
    public class UpdateRequestLogInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }

        public string Msisdn { set; get; }
        public string AccountNumber { set; get; }
        public string Url { set; get; }
        public string HttpMethod { set; get; }
        public decimal? Amount { set; get; }
        public string ClientReference { set; get; }
        public string ResponseCode { set; get; }
        public string ResponseMessage { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
