﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.RequestLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos
{
    [AutoMapFrom(typeof(RequestLog))]
    public class RequestLogDto : EntityDto<long>
    {
        public string Msisdn { set; get; }
        public string AccountNumber { set; get; }
        public string Url { set; get; }
        public virtual string HttpMethod { set; get; }
        public decimal? Amount { set; get; }
        public string ClientReference { set; get; }
        public string ResponseCode { set; get; }
        public string ResponseMessage { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
    }
}
