﻿using Abp.Application.Services.Dto;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos
{
    public class CreateRequestLogInput : IInputDto
    {
        public CreateRequestLogInput()
        {

        }

        public string Msisdn { set; get; }
        public string AccountNumber { set; get; }
        [Required]
        public string Url { set; get; }
        public string HttpMethod { set; get; }
        public decimal? Amount { set; get; }
        public string ClientReference { set; get; }
        public string ResponseCode { set; get; }
        public string ResponseMessage { set; get; }
        public string OriginatingAddress { set; get; }
    }
}
