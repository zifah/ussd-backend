﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs
{
    public interface IRequestLogAppService : ICoreAppService<RequestLogDto, CreateRequestLogInput, UpdateRequestLogInput>
    {

    }
}
