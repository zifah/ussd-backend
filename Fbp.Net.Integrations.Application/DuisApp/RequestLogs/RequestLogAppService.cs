﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.NetworkPrefixes;
using Fbp.Net.Integrations.DuisApp.RequestLogs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.RequestLogs
{
    public class RequestLogAppService : IRequestLogAppService
    {
        private readonly IRequestLogRepository _ownRepository;
        private readonly INetworkPrefixRepository _networkPrefixRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<RequestLog, RequestLogDto, CreateRequestLogInput,
            UpdateRequestLogInput, IRequestLogRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public RequestLogAppService(IRequestLogRepository ownRepository, INetworkPrefixRepository networkPrefixRepository)
        {
            _ownRepository = ownRepository;
            _networkPrefixRepository = networkPrefixRepository;
            _coreAppService = new CoreAppService<RequestLog, RequestLogDto, CreateRequestLogInput,
            UpdateRequestLogInput, IRequestLogRepository>(ownRepository);
        }
        public RequestLogDto Create(CreateRequestLogInput input)
        {
            RequestLogDto result = _coreAppService.Create(input);
            return result;
        }

        public RequestLogDto Update(UpdateRequestLogInput input)
        {
            RequestLogDto result = _coreAppService.Update(input);
            return result;
        }
    }
}
