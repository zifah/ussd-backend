﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.ApiClients.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fbp.Net.Integrations.DuisApp.ApiClients
{
    public class ApiClientAppService : IApiClientAppService
    {
        private readonly IApiClientRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<ApiClient, ApiClientDto, CreateApiClientInput,
            UpdateApiClientInput, IApiClientRepository> _coreAppService;
        
        public ApiClientAppService(IApiClientRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<ApiClient, ApiClientDto, CreateApiClientInput,
            UpdateApiClientInput, IApiClientRepository>(ownRepository);
        }
        public ApiClientDto Create(CreateApiClientInput input)
        {
            ApiClientDto result = _coreAppService.Create(input);
            return result;
        }

        public ApiClientDto Update(UpdateApiClientInput input)
        {
            ApiClientDto result = _coreAppService.Update(input);
            return result;
        }

        public ApiClientDto GetByName(string name)
        {
            var theClient = _ownRepository.GetByName(name);
            return Mapper.Map<ApiClientDto>(theClient);
        }
    }
}
