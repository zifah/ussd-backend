﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.ApiClients.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.ApiClients
{
    public interface IApiClientAppService : ICoreAppService<ApiClientDto, CreateApiClientInput, UpdateApiClientInput>
    {
        ApiClientDto GetByName(string name);
    }
}
