﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.ApiClients.Dtos
{
    public class UpdateApiClientInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
        public string Name { set; get; }
        public string IpAddress { set; get; }
        public string SharedKey { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }

        public bool? IsActive { set; get; }
        public bool? RequiresAuth { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
