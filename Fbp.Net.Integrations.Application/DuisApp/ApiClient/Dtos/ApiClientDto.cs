﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Fbp.Net.Integrations.DuisApp.ApiClients.Dtos
{
    [AutoMapFrom(typeof(ApiClient))]
    public class ApiClientDto : EntityDto<long>
    {
        public string Name { set; get; }
        public string IpAddress { set; get; }
        public string SharedKey { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }

        public bool IsActive { set; get; }
        public bool RequiresAuth { set; get; }
    }
}
