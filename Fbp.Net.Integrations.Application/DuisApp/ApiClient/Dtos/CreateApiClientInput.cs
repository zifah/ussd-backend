﻿using Abp.Application.Services.Dto;

namespace Fbp.Net.Integrations.DuisApp.ApiClients.Dtos
{
    public class CreateApiClientInput : IInputDto
    {
        public CreateApiClientInput()
        {
            RequiresAuth = true;
        }

        public string Name { set; get; }
        public string IpAddress { set; get; }
        public string SharedKey { set; get; }
        public long? LastModifierUserId { set; get; }
        public bool IsActive { set; get; }
        public bool RequiresAuth { set; get; }
    }
}
