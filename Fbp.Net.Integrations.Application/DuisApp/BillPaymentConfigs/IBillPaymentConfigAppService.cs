﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs
{
    public interface IBillPaymentConfigAppService : ICoreAppService<BillPaymentConfigDto, CreateBillPaymentConfigInput, UpdateBillPaymentConfigInput>
    {
        BillPaymentConfigDto GetByCode(string billerCode);

        /// <summary>
        /// Returns the list of all active billers who are not hidden
        /// </summary>
        /// <returns></returns>
        IList<BillPaymentConfigDto> GetVisibleBillers();

        IList<BillPaymentConfigDto> GetAll();

        BillPaymentConfigDto GetByName(string billerName);
    }
}
