﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos
{
    [AutoMapFrom(typeof(BillPaymentConfig))]
    public class BillPaymentConfigDto : EntityDto<long>
    {
        public string BillerCode { set; get; }
        public string BillerName { set; get; }
        public string ProductName { set; get; }
        public string PaymentParametersJson { set; get; }
        public string ProviderName { set; get; }
        /// <summary>
        /// <para>Format: Fully-qualified class name,Assembly name</para>
        /// </summary>
        public string ProviderFullyQualifiedName { set; get; }
        public string JsonPlans { set; get; }
        public IList<BillerPlan> BillerPlans { set; get; }
        public BillerPlan GetSelectedPlan(string planCode)
        {
            return BillerPlans.SingleOrDefault(x => x.Code == planCode) ?? BillerPlans.SingleOrDefault(x => x.IsDefault);
        }
        public string BillerSuspenseAccount { set; get; }

        public string ProviderClassFqn
        {
            get
            {
                return ProviderFullyQualifiedName == null ? null : ProviderFullyQualifiedName.Split(',')[0] ;
            }
        }

        public string ProviderAssembly
        {
            get
            {
                return ProviderFullyQualifiedName == null ? null : ProviderFullyQualifiedName.Split(',')[1];
            }
        }

        /// <summary>
        /// e.g. phone number for topup; smart card number for cable TV; tolling account number for tolling
        /// </summary>
        public string CustomerIdFieldName { set; get; }
        public decimal SurchargeAmount { set; get; }
        public bool RequiresCustomerIdValidation { set; get; }
        public bool HasPlans { set; get; }
        public bool IsProviderTransactor { set; get; }
        public DateTime CreationTime { set; get; }
        public bool SaveValidationResponse { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
        public bool IsActive { set; get; }
        public string ShortName { set; get; }
        public bool IsHidden { set; get; }
        public string Category { get; set; }
    }
}
