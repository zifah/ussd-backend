﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos
{
    public class CreateBillPaymentConfigInput : IInputDto
    {
        public CreateBillPaymentConfigInput()
        {
            IsActive = true;
        }

        public string BillerCode { set; get; }
        public string BillerName { set; get; }
        public string ProductName { set; get; }
        public string PaymentParametersJson { set; get; }
        public string ProviderName { set; get; }
        public string ProviderFullyQualifiedName { set; get; }
        public string BillerSuspenseAccount { set; get; }

        /// <summary>
        /// e.g. phone number for topup; smart card number for cable TV; tolling account number for tolling
        /// </summary>
        public string CustomerIdFieldName { set; get; }
        public decimal SurchargeAmount { set; get; }
        public bool RequiresCustomerIdValidation { set; get; }
        public string JsonPlans { set; get; }
        public bool HasPlans { set; get; }
        public bool IsProviderTransactor { set; get; }
        public bool IsActive { set; get; }
        public string ShortName { set; get; }
        public bool IsHidden { set; get; }
        public string Category { get; set; }
        public bool SaveValidationResponse { set; get; }
    }
}
