﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.BillPaymentConfigs
{
    public class BillPaymentConfigAppService : IBillPaymentConfigAppService
    {
        private readonly IBillPaymentConfigRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<BillPaymentConfig, BillPaymentConfigDto, CreateBillPaymentConfigInput,
            UpdateBillPaymentConfigInput, IBillPaymentConfigRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public BillPaymentConfigAppService(IBillPaymentConfigRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<BillPaymentConfig, BillPaymentConfigDto, CreateBillPaymentConfigInput,
            UpdateBillPaymentConfigInput, IBillPaymentConfigRepository>(ownRepository);
        }
        public BillPaymentConfigDto Create(CreateBillPaymentConfigInput input)
        {
            BillPaymentConfigDto result = _coreAppService.Create(input);
            return result;
        }

        public BillPaymentConfigDto Update(UpdateBillPaymentConfigInput input)
        {
            BillPaymentConfigDto result = _coreAppService.Update(input);
            return result;
        }

        public BillPaymentConfigDto GetByCode(string billerCode)
        {
            var raw = _ownRepository.GetByCode(billerCode);
            var result = Mapper.Map<BillPaymentConfigDto>(raw);
            return result;
        }

        public BillPaymentConfigDto GetByName(string billerName)
        {
            var raw = _ownRepository.GetByName(billerName);
            var result = Mapper.Map<BillPaymentConfigDto>(raw);
            return result;
        }

        public IList<BillPaymentConfigDto> GetVisibleBillers()
        {
            var visibleBillers = _ownRepository.GetAll().Where(x => x.IsActive && !x.IsHidden).ToList();

            var result = Mapper.Map<IList<BillPaymentConfigDto>>(visibleBillers);

            return result;
        }

        public IList<BillPaymentConfigDto> GetAll()
        {
            var allBillers = _ownRepository.GetAll().ToList();

            var result = Mapper.Map<IList<BillPaymentConfigDto>>(allBillers);

            return result;
        }
    }
}
