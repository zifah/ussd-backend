﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Adverts.Dtos
{
    public class CreateAdvertInput : IInputDto
    {
        public CreateAdvertInput()
        {

        }


        public string Name { set; get; }
        public string Content { set; get; }
        /// <summary>
        /// Property-value pairs which would exclude a user from getting this ad
        /// </summary>
        public string UserExcludeDictionaryJson { set; get; }
        public string ApplicableModulesListJson { set; get; }
        public long? LastModifierUserId { set; get; }
        public bool IsActive { set; get; }
    }
}
