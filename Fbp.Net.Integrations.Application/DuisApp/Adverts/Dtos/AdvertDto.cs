﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.Adverts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Adverts.Dtos
{
    [AutoMapFrom(typeof(Advert))]
    public class AdvertDto : EntityDto<long>
    {
        public string Name { set; get; }
        public string Content { set; get; }
        /// <summary>
        /// Property-value pairs which would exclude a user from getting this ad
        /// </summary>
        public string UserExcludeDictionaryJson { set; get; }
        public string ApplicableModulesListJson { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
        public Dictionary<string, string> UserExcludeDictionary { set; get; }

        public List<string> ApplicableModulesList { set; get; }

        public bool IsActive { set; get; }
    }
}
