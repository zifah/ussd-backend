﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.Adverts.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Adverts
{
    public interface IAdvertAppService : ICoreAppService<AdvertDto, CreateAdvertInput, UpdateAdvertInput>
    {
        IList<AdvertDto> GetAdverts(string module, Enrolments.Dtos.EnrolmentDto user);
    }
}
