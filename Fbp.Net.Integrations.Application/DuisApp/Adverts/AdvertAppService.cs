﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.Adverts.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Adverts
{
    public class AdvertAppService : IAdvertAppService
    {
        private readonly IAdvertRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<Advert, AdvertDto, CreateAdvertInput,
            UpdateAdvertInput, IAdvertRepository> _coreAppService;
        
        public AdvertAppService(IAdvertRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<Advert, AdvertDto, CreateAdvertInput,
            UpdateAdvertInput, IAdvertRepository>(ownRepository);
        }
        public AdvertDto Create(CreateAdvertInput input)
        {
            AdvertDto result = _coreAppService.Create(input);
            return result;
        }

        public AdvertDto Update(UpdateAdvertInput input)
        {
            AdvertDto result = _coreAppService.Update(input);
            return result;
        }

        public IList<AdvertDto> GetAdverts(string module, Enrolments.Dtos.EnrolmentDto user)
        {
            var allAdverts = _ownRepository.GetAll().ToList();
            var applicable = allAdverts.Where(x => x.IsActive
                && (x.ApplicableModulesList == null || x.ApplicableModulesList.Contains(module) || x.ApplicableModulesList.Count == 0)).ToList();
            var finalList = new List<Advert>();

            foreach (var ad in applicable)
            {
                finalList.Add(ad);
                foreach (var pair in ad.UserExcludeDictionary)
                {
                    string propertyName = pair.Key;
                    string checkValue = pair.Value;

                    var enrolmentDtoType = typeof(Enrolments.Dtos.EnrolmentDto);
                    var currentProperty = enrolmentDtoType.GetProperty(propertyName);

                    if (currentProperty != null)
                    {
                        string realValue = Convert.ToString(currentProperty.GetValue(user));

                        if (realValue == checkValue)
                        {
                            finalList.Remove(ad);
                            break;
                        }
                    }
                }
            }

            return Mapper.Map<IList<AdvertDto>>(finalList);
        }
    }
}
