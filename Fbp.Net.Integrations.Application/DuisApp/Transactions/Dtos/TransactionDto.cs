﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Transactions.Dtos
{
    [AutoMapFrom(typeof(Transaction))]
    public class TransactionDto : EntityDto<long>
    {
        public decimal Amount { set; get; }
        public decimal ChargeAmount { set; get; }
        public string ClientRefId { set; get; }
        public string RefId { set; get; }
        public string Type { set; get; }
        public long UserId { set; get; }
        public string UserMsisdn { set; get; }
        public string UserEmail { set; get; }
        public string UserFullName { set; get; }
        public string Status { set; get; }
        public string StatusCode { set; get; }
        public bool Success { set; get; }
        public bool IsReversed { set; get; }
        public int ReversalAttempts { set; get; }
        public decimal DrAmount { set; get; }
        public string DrSource { set; get; }
        public string DrDestination { set; get; }
        public string DrDestinationCode { set; get; }
        public string DrRefId { set; get; }
        public int DrRefIdNumeric { get { try { return Convert.ToInt32(DrRefId); } catch { return 0; } } }
        public string DrNarration { set; get; }
        public string DrResponseCode { set; get; }
        public string DrResponseDesc { set; get; }
        public DateTime? DrReqTime { set; get; }
        public DateTime? DrRespTime { set; get; }
        public string ProcessorName { set; get; }
        /// <summary>
        /// Will be biller code for bill payments
        /// </summary>
        public string ProcessorCode { set; get; }
        public string ProcessorOptionCode { set; get; }
        public string ProcessorValidationOutput { set; get; }
        public bool IsViaDirectCode { set; get; }
        /// <summary>
        /// e.g. DSTV card number for bill payments; dest phone number for airtime recharge
        /// </summary>
        public string ProcessorUserId { set; get; }
        public string ProcessorRefId { set; get; }
        public string ProcessorRespCode { set; get; }
        public string ProcessorRespDesc { set; get; }
        public string ProcessorResponseBody { set; get; }
        public DateTime? ProcessorReqTime { set; get; }
        public DateTime? ProcessorRespTime { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? ReversalCutoffTime { set; get; }
        public DateTime? ReversalTime { set; get; }
    }
}
