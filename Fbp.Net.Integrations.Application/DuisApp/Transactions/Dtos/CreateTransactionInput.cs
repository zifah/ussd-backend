﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Transactions.Dtos
{
    public class CreateTransactionInput : IInputDto
    {
        public CreateTransactionInput()
        {
        }

        [Required]
        public decimal Amount { set; get; }
        public decimal ChargeAmount { set; get; }
        [Required]
        public string ClientRefId { set; get; }
        public string RefId { set; get; }
        [Required]
        public string Type { set; get; }
        [Required]
        public long UserId { set; get; }
        [Required]
        public string UserMsisdn { set; get; }
        public string UserEmail { set; get; }
        public string UserFullName { set; get; }
        public string Status { set; get; }
        public string StatusCode { set; get; }
        public bool? Success { set; get; }
        public bool IsReversed { set; get; }
        public int ReversalAttempts { set; get; }
        public decimal DrAmount { set; get; }
        public string DrSource { set; get; }
        public string DrDestination { set; get; }
        public string DrDestinationCode { set; get; }
        public string DrRefId { set; get; }
        public string DrNarration { set; get; }
        public string DrResponseCode { set; get; }
        public string DrResponseDesc { set; get; }
        public DateTime? DrReqTime { set; get; }
        public DateTime? DrRespTime { set; get; }
        public string ProcessorName { set; get; }
        /// <summary>
        /// Will be biller code for bill payments
        /// </summary>
        public string ProcessorCode { set; get; }
        public string ProcessorOptionCode { set; get; }
        public string ProcessorValidationOutput { set; get; }
        public bool IsViaDirectCode { set; get; }
        /// <summary>
        /// e.g. DSTV card number for bill payments; dest phone number for airtime recharge
        /// </summary>
        public string ProcessorUserId { set; get; }
        public string ProcessorRefId { set; get; }
        public string ProcessorRespCode { set; get; }
        public string ProcessorRespDesc { set; get; }
        public string ProcessorResponseBody { set; get; }
        public DateTime? ProcessorReqTime { set; get; }
        public DateTime? ProcessorRespTime { set; get; }
        public DateTime? ReversalTime { set; get; }
        public bool ReverseExceptionally { set; get; }
    }
}
