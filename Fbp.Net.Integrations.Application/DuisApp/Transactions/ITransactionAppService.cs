﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Transactions
{
    public interface ITransactionAppService : ICoreAppService<TransactionDto, CreateTransactionInput, UpdateTransactionInput>
    {
        LimitHolder IsLimitExceeded(decimal amount, EnrolmentDto enrolment, string transactionType, ref string limitExceededName, bool useTokenLimit = false);
        decimal? GetTransactionCharge(string transactionTypeName, long? profileId);
        IList<TransactionDto> GetPendingReversals(string successfulDebitCode, int daysBacklog);
        TransactionDto GetByClientRef(string clientRef);
        TransactionDto GetByRefId(string refId);
        IList<TransactionDto> GetLatestTransactions(long userId, int count);
        IList<TransactionDto> GetLatestSuccessfulTransactions(long userId, int count);
    }
}
