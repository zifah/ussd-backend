﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.Enrolments;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Fbp.Net.Integrations.DuisApp.TransactionCharges;
using Fbp.Net.Integrations.DuisApp.TransactionLimits;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using Fbp.Net.Integrations.DuisApp.TransactionTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Transactions
{
    public class TransactionAppService : ITransactionAppService
    {
        private readonly ITransactionRepository _ownRepository;
        private readonly ITransactionTypeRepository _txnTypeRepo;
        private readonly ITransactionLimitRepository _txnLimitRepo;
        private readonly ITransactionChargeRepository _txnChargeRepo;

        private static readonly object _locker = new object();
        private readonly CoreAppService<Transaction, TransactionDto, CreateTransactionInput,
            UpdateTransactionInput, ITransactionRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public TransactionAppService(ITransactionRepository ownRepository,
            ITransactionTypeRepository txnTypeRepo, ITransactionLimitRepository txnLimitRepo, ITransactionChargeRepository txnChargeRepo)
        {
            _ownRepository = ownRepository;
            _txnTypeRepo = txnTypeRepo;
            _txnLimitRepo = txnLimitRepo;
            _txnChargeRepo = txnChargeRepo;
            _coreAppService = new CoreAppService<Transaction, TransactionDto, CreateTransactionInput,
            UpdateTransactionInput, ITransactionRepository>(ownRepository);
        }
        public TransactionDto Create(CreateTransactionInput input)
        {
            //TODO: Generate RRN from the ID before savings; convert RRN to Int if ID value is long
            TransactionDto result = _coreAppService.Create(input);

            result = Update(new UpdateTransactionInput
            {
                Id = result.Id,
                DrRefId = GenerateDebitReference(result.Id)
            });

            return result;
        }

        private string GenerateDebitReference(long transactionId)
        {
            return (transactionId % Int32.MaxValue).ToString();
        }

        public TransactionDto Update(UpdateTransactionInput input)
        {
            TransactionDto result = _coreAppService.Update(input);
            return result;
        }

        public LimitHolder IsLimitExceeded(decimal amount, EnrolmentDto enrolment, string transactionType, ref string limitExceededName, bool useTokenLimit = false)
        {
            var result = false;
            var userId = enrolment.Id;
            long? profileId = enrolment.Profile == null ? null : (long?)enrolment.Profile.Id;

            var defaultTypeLimits = _txnTypeRepo.GetByName(transactionType);
            var defaultGlobalLimits = _txnTypeRepo.GetByName(ApplicationConstants.AllTransactions);

            // Get custom user/profile limit for this transaction type
            var customUserLimits = _txnLimitRepo.GetCustomLimits(userId, profileId);
            var customTxnTypeLimit = customUserLimits.SingleOrDefault(x => x.TransactionType.Id.Equals(defaultTypeLimits.Id));
            var customGlobalLimit = customUserLimits.SingleOrDefault(x => x.TransactionType.Id.Equals(defaultGlobalLimits.Id));

            var useDefaultForTransactionTypeLimit = customTxnTypeLimit == null;
            var useDefaultForGlobalLimit = customGlobalLimit == null;

            var minTxnAmount = defaultTypeLimits.MinTransactionAmount;
            var maxTxnAmount = useDefaultForTransactionTypeLimit ? defaultTypeLimits.DefaultMaxTransactionAmount : customTxnTypeLimit.MaxTransactionAmount;
            var allowedTotalTypeAmount = useDefaultForTransactionTypeLimit ? defaultTypeLimits.DefaultDailyAmount : customTxnTypeLimit.DailyAmount;
            var allowedTotalTypeCount = useDefaultForTransactionTypeLimit ? defaultTypeLimits.DefaultDailyCount : customTxnTypeLimit.DailyCount;
            var allowedTotalGlobalAmount = useDefaultForGlobalLimit ? defaultGlobalLimits.DefaultDailyAmount : customGlobalLimit.DailyAmount;
            var allowedTotalGlobalCount = useDefaultForGlobalLimit ? defaultGlobalLimits.DefaultDailyCount : customGlobalLimit.DailyCount;

            var maxWithoutTokenTypeAmount = useDefaultForTransactionTypeLimit || customTxnTypeLimit.MaximumBeforeTokenAmount == null ? defaultTypeLimits.MaximumBeforeTokenAmount : customTxnTypeLimit.MaximumBeforeTokenAmount.Value;
            var maxWithoutTokenAggregateAmount = useDefaultForGlobalLimit || customGlobalLimit.MaximumBeforeTokenAmount == null ? defaultGlobalLimits.MaximumBeforeTokenAmount : customGlobalLimit.MaximumBeforeTokenAmount.Value;

            //Limits to use
            // Spool all user's transactions within the past twenty four hours
            var last24HoursTransactions = _ownRepository.GetTransactionsLast24Hours(userId);
            var successfulTransactions = last24HoursTransactions.Where(x => x.Success);
            var typeSuccessfulTransactions = successfulTransactions.Where(x => x.Type == defaultTypeLimits.Name);

            var limitHolder = new LimitHolder
            {
                MinimumAmount = minTxnAmount,
                MaximumAmount = maxTxnAmount,
                CountLeft = (int)allowedTotalTypeCount
            };

            if (amount < minTxnAmount)
            {
                result = true;
                limitExceededName = "Transaction magnitude limit";
                limitHolder.IsExceededLimitMinimum = amount < minTxnAmount;
            }

            else
            {
                var sumOfSuccessfulTransactions = successfulTransactions.Sum(x => x.Amount);
                var countOfSuccessfulTransactions = successfulTransactions.Count();
                var sumOfSuccessfulTypeTransactions = typeSuccessfulTransactions.Sum(x => x.Amount);
                var countOfSuccessfulTypeTransactions = typeSuccessfulTransactions.Count();

                var resultantGlobalAmountPast24 = sumOfSuccessfulTransactions + amount;
                var resultantTypeAmountPast24 = sumOfSuccessfulTypeTransactions + amount;

                var resultantGlobalCountPast24 = countOfSuccessfulTransactions + 1;
                var resultantTypeCountPast24 = countOfSuccessfulTypeTransactions + 1;

                limitHolder.CountLeft = Math.Min((int)allowedTotalGlobalCount - countOfSuccessfulTransactions,
                    (int)allowedTotalTypeCount - countOfSuccessfulTypeTransactions);

                limitHolder.MaximumAmount = Math.Min(maxTxnAmount, Math.Min(allowedTotalGlobalAmount - sumOfSuccessfulTransactions,
                    allowedTotalTypeAmount - sumOfSuccessfulTypeTransactions));

                limitHolder.DefaultCountLeft = Math.Min((int)defaultGlobalLimits.DefaultDailyCount - countOfSuccessfulTransactions,
                    (int)defaultTypeLimits.DefaultDailyCount - countOfSuccessfulTypeTransactions);

                limitHolder.DefaultMaxAmount = Math.Min(maxTxnAmount, Math.Min(defaultGlobalLimits.DefaultDailyAmount - sumOfSuccessfulTransactions,
                    defaultTypeLimits.DefaultDailyAmount - sumOfSuccessfulTypeTransactions));

                limitHolder.MaximumBeforeToken = Math.Min(limitHolder.MaximumAmount,
                    Math.Max(0,
                    Math.Min(maxWithoutTokenAggregateAmount - sumOfSuccessfulTransactions,
                    maxWithoutTokenTypeAmount - sumOfSuccessfulTypeTransactions)));

                limitHolder.IsTokenLimitExceeded = useTokenLimit && (maxWithoutTokenAggregateAmount < resultantGlobalAmountPast24 ||
                    maxWithoutTokenTypeAmount < resultantTypeAmountPast24);

                if (resultantGlobalCountPast24 > allowedTotalGlobalCount || resultantTypeCountPast24 > allowedTotalTypeCount)
                {
                    result = true;
                    limitExceededName = "Transaction frequency limit";
                }

                else if (resultantGlobalAmountPast24 > allowedTotalGlobalAmount
                    || resultantTypeAmountPast24 > allowedTotalTypeAmount
                    || amount > maxTxnAmount)
                {
                    result = true;
                    limitExceededName = "Transaction volume limit";
                }
            }

            limitHolder.IsLimitExceeded = result;

            return limitHolder;
        }

        public decimal? GetTransactionCharge(string transactionTypeName, long? profileId)
        {
            var charge = _txnChargeRepo.GetByTransactionType(transactionTypeName, profileId);

            return charge == null ? null : (decimal?)charge.Amount;
        }

        public IList<TransactionDto> GetPendingReversals(string successfulDebitCode, int daysBacklog)
        {
            // var transactions = _ownRepository.GetDebitedButFailed(successfulDebitCode, new string[] { ApplicationConstants.AirtimeRecharge, ApplicationConstants.BillPayment });
            var transactions = _ownRepository.GetForReversal(successfulDebitCode, 
                new string[] { ApplicationConstants.AirtimeRecharge, ApplicationConstants.BillPayment }, daysBacklog);

            var result = Mapper.Map<IList<TransactionDto>>(transactions);

            return result;
        }

        public TransactionDto GetByClientRef(string clientRef)
        {
            Transaction transaction = _ownRepository.GetByClientRef(clientRef);

            var result = Mapper.Map<TransactionDto>(transaction);

            return result;
        }

        public IList<TransactionDto> GetLatestTransactions(long userId, int count)
        {
            var raw = _ownRepository.GetLatestTransactions(userId, count);

            var result = Mapper.Map<IList<TransactionDto>>(raw);

            return result;
        }

        public IList<TransactionDto> GetLatestSuccessfulTransactions(long userId, int count)
        {
            var raw = _ownRepository.GetLatestSuccessfulTransactions(userId, count);
            var result = Mapper.Map<IList<TransactionDto>>(raw);
            return result;
        }

        public TransactionDto GetByRefId(string refId)
        {
            Transaction transaction = _ownRepository.GetByRefId(refId);
            return Mapper.Map<TransactionDto>(transaction);
        }
    }
}
