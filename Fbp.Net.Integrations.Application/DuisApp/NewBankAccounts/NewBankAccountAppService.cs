﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts
{
    public class NewBankAccountAppService : INewBankAccountAppService
    {
        private readonly INewBankAccountRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<NewBankAccount, NewBankAccountDto, CreateNewBankAccountInput,
            UpdateNewBankAccountInput, INewBankAccountRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public NewBankAccountAppService(INewBankAccountRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<NewBankAccount, NewBankAccountDto, CreateNewBankAccountInput,
            UpdateNewBankAccountInput, INewBankAccountRepository>(ownRepository);
        }
        public NewBankAccountDto Create(CreateNewBankAccountInput input)
        {
            NewBankAccountDto result = _coreAppService.Create(input);
            return result;
        }

        public NewBankAccountDto Update(UpdateNewBankAccountInput input)
        {
            NewBankAccountDto result = _coreAppService.Update(input);
            return result;
        }

        public IList<NewBankAccountDto> GetByPhone(string phone)
        {
            var resultRaw = _ownRepository.GetByPhone(phone);
            var result = Mapper.Map<IList<NewBankAccountDto>>(resultRaw);
            return result;
        }


    }
}
