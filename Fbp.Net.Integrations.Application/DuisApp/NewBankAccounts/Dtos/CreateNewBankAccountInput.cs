﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos
{
    public class CreateNewBankAccountInput : IInputDto
    {
        public CreateNewBankAccountInput()
        {
        }

        [Required]
        public string FirstName { set; get; }
        [Required]
        public string LastName { set; get; }
        public string AccountNumber { set; get; }
        public string CustomerId { set; get; }
        public string SchemeCode { set; get; }
        public string Bvn { set; get; }
        [Required]
        public DateTime Birthday { set; get; }
        [Required]
        public string PhoneNumber { set; get; }
        public string Email { set; get; }
        public string ReferrerCode { set; get; }
        /// <summary>
        /// Or SolId
        /// </summary>
        public string BranchCode { set; get; }
        public string CbaResponse { set; get; }
        public CreationStatus? CreationStatus { set; get; }
    }
}
