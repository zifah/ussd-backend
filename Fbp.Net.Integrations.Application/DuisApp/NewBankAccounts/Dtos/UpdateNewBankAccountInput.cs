﻿using Abp.Runtime.Validation;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos
{
    public class UpdateNewBankAccountInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }

       
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string AccountNumber { set; get; }
        public string CustomerId { set; get; }
        public string SchemeCode { set; get; }
        public string Bvn { set; get; }
        public DateTime? Birthday { set; get; }
        public string PhoneNumber { set; get; }
        public string Email { set; get; }
        public string ReferrerCode { set; get; }
        /// <summary>
        /// Or SolId
        /// </summary>
        public string BranchCode { set; get; }
        public string CbaResponse { set; get; }
        public CreationStatus? CreationStatus { set; get; }

        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
