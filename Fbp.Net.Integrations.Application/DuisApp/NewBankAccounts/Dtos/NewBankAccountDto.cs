﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos
{
    [AutoMapFrom(typeof(NewBankAccount))]
    public class NewBankAccountDto : EntityDto<long>
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string AccountNumber { set; get; }
        public string CustomerId { set; get; }
        public string SchemeCode { set; get; }
        public string Bvn { set; get; }
        public DateTime Birthday { set; get; }
        public string PhoneNumber { set; get; }
        public string Email { set; get; }
        public string ReferrerCode { set; get; }
        public string BranchCode { set; get; }
        public string CbaResponse { set; get; }
        public CreationStatus? CreationStatus { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
    }
}
