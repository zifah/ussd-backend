﻿using Fbp.Net.Integrations.DuisApp.NewBankAccounts.Dtos;
using System.Collections.Generic;

namespace Fbp.Net.Integrations.DuisApp.NewBankAccounts
{
    public interface INewBankAccountAppService : ICoreAppService<NewBankAccountDto, CreateNewBankAccountInput, UpdateNewBankAccountInput>
    {
        IList<NewBankAccountDto> GetByPhone(string phone);
    }
}
