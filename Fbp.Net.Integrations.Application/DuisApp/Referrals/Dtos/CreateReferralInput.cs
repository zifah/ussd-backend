﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Referrals.Dtos
{
    public class CreateReferralInput : IInputDto
    {
        /// <summary>
        /// <para>IsActive is instantiated to true by default</para>
        /// </summary>
        public CreateReferralInput()
        {
            IsActive = true;
        }

        [Required]
        public long ReferreeUserId { set; get; }
        [Required]
        public string ReferreePhone { set; get; }
        public string RefereeAccount { set; get; }
        public string RefereeCustomerId { set; get; }
        [Required]
        public string ReferrerPhone { set; get; }
        [Required]
        public string Product { set; get; }
        public bool? IsActive { set; get; }
    }
}
