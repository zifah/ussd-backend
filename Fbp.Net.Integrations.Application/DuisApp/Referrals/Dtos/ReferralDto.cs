﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.Referrals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Referrals.Dtos
{
    [AutoMapFrom(typeof(Referral))]
    public class ReferralDto : EntityDto<long>
    {
        public long ReferreeUserId { set; get; }
        public string ReferreePhone { set; get; }
        public string RefereeAccount { set; get; }
        public string RefereeCustomerId { set; get; }
        public string ReferrerPhone { set; get; }
        public string Product { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { set; get; }
        public long? LastModifierUserId { set; get; }
        public bool IsActive { set; get; }
    }
}
