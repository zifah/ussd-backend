﻿using AutoMapper;
using Fbp.Net.Integrations.DuisApp.Referrals.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Referrals
{
    public class ReferralAppService : IReferralAppService
    {
        private readonly IReferralRepository _ownRepository;
        private static readonly object _locker = new object();
        private readonly CoreAppService<Referral, ReferralDto, CreateReferralInput,
            UpdateReferralInput, IReferralRepository> _coreAppService;

        static int seed = Environment.TickCount;
        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public ReferralAppService(IReferralRepository ownRepository)
        {
            _ownRepository = ownRepository;
            _coreAppService = new CoreAppService<Referral, ReferralDto, CreateReferralInput,
            UpdateReferralInput, IReferralRepository>(ownRepository);
        }
        public ReferralDto Create(CreateReferralInput input)
        {
            ReferralDto result = _coreAppService.Create(input);
            return result;
        }

        public ReferralDto Update(UpdateReferralInput input)
        {
            ReferralDto result = _coreAppService.Update(input);
            return result;
        }

        public IList<ReferralDto> GetReferrals(string referrerPhone)
        {
            var raw = _ownRepository.GetReferrals(referrerPhone);
            return Mapper.Map<IList<ReferralDto>>(raw);
        }
    }
}
