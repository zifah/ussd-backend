﻿using Abp.Application.Services;
using Fbp.Net.Integrations.DuisApp.Referrals.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Referrals
{
    public interface IReferralAppService : ICoreAppService<ReferralDto, CreateReferralInput, UpdateReferralInput>
    {
        IList<ReferralDto> GetReferrals(string refferPhone);
    }
}
