﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using Fbp.Net.Integrations.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Enrolments.Dtos
{
    public class UpdateEnrolmentInput : IUpdaterDto, ICustomValidate
    {
        [Range(1, long.MaxValue)] //Data annotation attributes work as expected.
        public long Id { set; get; }
        public string Msisdn { set; get; }
        public string Network { set; get; }
        public string AccountNumber { set; get; }
        public string Password { set; get; }
        public DateTime? FirstLoginTime { set; get; }
        public DateTime? LastLoginTime { set; get; }
        public int? FailedLoginAttempts { set; get; }
        public EnrolStatus? Status { set; get; }
        public DateTime? EnrolmentTime { set; get; }
        public DateTime? LastLoginAttemptTime { set; get; }
        public UserProfile Profile { get { return ProfileId.HasValue ? new UserProfile {  Id = ProfileId.Value } : null; } }
        public long? ProfileId { set; get; }
        public string OldPasswords { set; get; }
        public string LockoutReason { set; get; }
        public virtual bool? HasToken { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Comment { set; get; }
        public virtual DateTime? FinacleCreationTime { set; get; }
        public virtual UserType? UserType { set; get; }

        //Custom validation method. It's called by ABP after data annotation validations.
        public void AddValidationErrors(List<ValidationResult> results)
        {
            Utility.AddValidationErrors(this, results);
        }
    }
}
