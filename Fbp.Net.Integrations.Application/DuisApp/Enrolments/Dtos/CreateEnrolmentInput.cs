﻿using Abp.Application.Services.Dto;
using Fbp.Net.Integrations.DuisApp.UserProfiles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Enrolments.Dtos
{
    public class CreateEnrolmentInput : IInputDto
    {
        public CreateEnrolmentInput()
        {

        }

        public string Msisdn { set; get; }
        public string Network { set; get; }
        public string BankCustomerId { set; get; }
        public string AccountNumber { set; get; }
        public string Password { set; get; }
        public DateTime? EnrolmentTime { set; get; }
        public EnrolStatus? Status { set; get; }
        public long? ProfileId { set; get; }
        public UserProfile Profile { get { return ProfileId.HasValue ? new UserProfile { Id = ProfileId.Value } : null; } }
        public string OldPasswords { set; get; }
        public string LockoutReason { set; get; }


        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Comment { set; get; }
        public virtual DateTime? FinacleCreationTime { set; get; }
        public virtual UserType UserType { set; get; }
    }
}
