﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Fbp.Net.Integrations.DuisApp.UserProfiles.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Enrolments.Dtos
{

    [AutoMapFrom(typeof(Enrolment))]
    public class EnrolmentDto : EntityDto<long>
    {
        public string Msisdn { set; get; }
        public string Network { set; get; }
        public UserProfileDto Profile { set; get; }
        public string BankCustomerId { set; get; }
        public DateTime CreationTime { set; get; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime? LastLoginAttemptTime { set; get; }
        public long? LastModifierUserId { get; set; }
        public string AccountNumber { set; get; }
        public string Password { set; get; }
        public DateTime? FirstLoginTime { set; get; }
        public DateTime? LastLoginTime { set; get; }
        public int FailedLoginAttempts { set; get; }
        public EnrolStatus Status { set; get; }
        public DateTime? EnrolmentTime { set; get; }
        public string OldPasswords { set; get; }
        public bool IsActive { set; get; }
        public bool ShouldResetPassword { set; get; }
        public string LockoutReason { set; get; }
        public virtual bool? HasToken { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Comment { set; get; }
        public virtual DateTime? FinacleCreationTime { set; get; }
        public virtual UserType UserType { set; get; }
        public virtual string FullName { set; get; }
    }
}
