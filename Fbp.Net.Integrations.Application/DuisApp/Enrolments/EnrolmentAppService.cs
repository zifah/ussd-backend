﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using Fbp.Net.Integrations.DuisApp.WeakPasswords;
using Fbp.Net.Integrations.DuisApp.Blacklists;

namespace Fbp.Net.Integrations.DuisApp.Enrolments
{
    public class EnrolmentAppService : IEnrolmentAppService
    {
        private readonly IEnrolmentRepository _ownRepository;
        private readonly CoreAppService<Enrolment, EnrolmentDto, CreateEnrolmentInput,
            UpdateEnrolmentInput, IEnrolmentRepository> _coreAppService;
        private readonly IWeakPasswordRepository _weakPasswordRepository;
        private readonly IBlacklistRepository _blacklistRepository;

        public EnrolmentAppService(IEnrolmentRepository ownRepository,
            IWeakPasswordRepository weakPasswordRepository, IBlacklistRepository blacklistRepository)
        {
            _ownRepository = ownRepository;
            _weakPasswordRepository = weakPasswordRepository;
            _blacklistRepository = blacklistRepository;

            _coreAppService = new CoreAppService<Enrolment, EnrolmentDto, CreateEnrolmentInput,
            UpdateEnrolmentInput, IEnrolmentRepository>(_ownRepository);
        }

        public EnrolmentDto Create(CreateEnrolmentInput input)
        {
            input.OldPasswords = input.Password;

            if (input.Status == EnrolStatus.Activated)
            {
                input.EnrolmentTime = DateTime.Now;
            }

            EnrolmentDto result = _coreAppService.Create(input);

            return result;
        }

        public EnrolmentDto Update(UpdateEnrolmentInput input)
        {
            EnrolmentDto result = _coreAppService.Update(input);
            return result;
        }

        public EnrolmentDto GetByMsisdn(string msisdn, UserType userType)
        {
            var raw = _ownRepository.GetByMsisdn(msisdn, userType);
            var result = Mapper.Map<EnrolmentDto>(raw);
            return result;
        }

        public EnrolmentDto GetByAccount(string accountNumber, UserType userType)
        {
            var raw = _ownRepository.GetByAccount(accountNumber, userType);
            var result = Mapper.Map<EnrolmentDto>(raw);
            return result;
        }

        public EnrolmentDto GetByMsisdnAcct(string msisdn, string accountNumber, UserType userType)
        {
            Enrolment resultRaw = null;

            var allMatches = _ownRepository.GetByMsisdnAcct(msisdn, accountNumber, userType);

            if (allMatches.Count == 1)
            {
                resultRaw = allMatches[0];
            }

            else
            {
                // pick an existing entry where the account number and msisdn will remain as is
                resultRaw = allMatches.SingleOrDefault(x => x.Msisdn == msisdn && x.AccountNumber == accountNumber);

                if (resultRaw == null)
                {
                    resultRaw = allMatches.SingleOrDefault(x => x.Status == EnrolStatus.UpdateMode);
                }
            }

            var result = Mapper.Map<EnrolmentDto>(resultRaw);
            return result;
        }

        public EnrolmentDto GetByCustomerId(string customerId)
        {
            var raw = _ownRepository.GetByCustomerId(customerId);

            var result = Mapper.Map<EnrolmentDto>(raw);

            return result;
        }
        public bool IsPasswordWeak(string password)
        {
            var weakPassword = _weakPasswordRepository.GetByPassword(password);

            bool isPasswordWeak = weakPassword != null;

            return isPasswordWeak;
        }

        public bool IsBlacklisted(string msisdn, string accountNumber)
        {
            bool result = true;

            var retrieved = _blacklistRepository.GetByAccountMSISDN(accountNumber, msisdn);

            if (retrieved.Where(x => x.IsActive).Count() == 0)
            {
                result = false;
            }

            return result;

        }
    }
}