﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Fbp.Net.Integrations.DuisApp.Enrolments.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations.DuisApp.Enrolments
{
    public interface IEnrolmentAppService : ICoreAppService<EnrolmentDto, CreateEnrolmentInput, UpdateEnrolmentInput>
    {
        EnrolmentDto GetByMsisdnAcct(string msisdn, string accountNumber, UserType userType);
        EnrolmentDto GetByMsisdn(string msisdn, UserType userType);
        EnrolmentDto GetByAccount(string accountNumber, UserType userType);
        EnrolmentDto GetByCustomerId(string customerId);
        bool IsPasswordWeak(string password);
        bool IsBlacklisted(string msisdn, string accountNumber);
    }
}
