﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fbp.Net.Integrations
{
    public class ApplicationConstants
    {
        // General
        public static readonly string AppSettingLogFilePath = "LogFilePath";

        // Duis 
        public const string IntrabankTransfer = "Intrabank Transfer";
        public const string InterbankTransfer = "Interbank Transfer";
        public const string BillPayment = "Bill Payment";
        public const string BalanceEnquiry = "Balance Enquiry";
        public const string AirtimeRecharge = "Airtime Recharge";
        public const string CardlessCashout = "Cardless Cashout";
        public const string OtpGeneration = "Otp Generation";
        public const string Service = "Service";
        public const string AllTransactions = "All Transactions";

        public const string AlertSubscription = "Alert Subscription";
        public const string CardControl = "Card Control";
        public const string TokenIssuance = "Token Issuance";
        public const string InstantBanking = "Instant Banking";

        // Services
        public const string AccountEnquiry = "Account Enquiry";

        // Mobile Money
        /// <summary>
        /// <para>From mobile money to regular bank account (intra)</para>
        /// </summary>
        public const string MobileMoneyIntrabankTransfer = "MobileMoney IntrabankTransfer";
        /// <summary>
        /// <para>From mobile money to regular bank account (inter)</para>
        /// </summary>
        public const string MobileMoneyInterbankTransfer = "MobileMoney InterbankTransfer";
        /// <summary>
        /// <para>From mobile money to another mobile account (intra)</para>
        /// </summary>
        public const string MobileMoneyIntramobileTransfer = "MobileMoney IntramobileTransfer";
        /// <summary>
        /// <para>From mobile money to another mobile account (inter)</para>
        /// </summary>
        public const string MobileMoneyIntermobileTransfer = "MobileMoney IntermobileTransfer";
        public const string MobileMoneyAirtimeRecharge = "MobileMoney AirtimeRecharge";
        public const string MobileMoneyBillPayment = "MobileMoney BillPayment";
        public const string MobileMoneyCashout = "MobileMoney Cashout";
        
        public const string ValueSavingsAccountStarters = "6";
        public const string ValueCurrentAccountStarters = "4,5,9";

    }
}
