﻿using Abp.Modules;
using Fbp.Net.Integrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Duis
{
    [DependsOn(typeof(DuisDataModule), typeof(IntegrationsApplicationModule))]
    public class DuisReversalsModule : AbpModule
    {
        public override void Initialize()
        {
            LoggingSystem.LogMessage("Started initializing DuisReversalsModule");
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            LoggingSystem.LogMessage("Finished initializing DuisReversalsModule");
        }
    }
}