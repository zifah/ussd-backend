﻿using Abp;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs;
using Fbp.Net.Integrations.DuisApp.BillPaymentConfigs.Dtos;
using Fbp.Net.Integrations.DuisApp.Transactions;
using Fbp.Net.Integrations.DuisApp.Transactions.Dtos;
using FidelityBank.CoreLibraries.Fincore;
using FidelityBank.CoreLibraries.Interfaces;
using FidelityBank.CoreLibraries.Interfaces.BillPayment;
using Newtonsoft.Json;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Duis.Reversals
{
    public partial class DuisReversals : ServiceBase
    {
        private Timer _timer;
        private static readonly AbpBootstrapper _bootstrapper = AbpDIManager.AbpBootstraper;
        private static IList<BillPaymentConfigDto> _allBillers = new List<BillPaymentConfigDto>();
        private static DateTime _serviceStartTime;
        /// <summary>
        /// <para>Wtih a value of 1, the system instruct reverse eligible transactions not older than the first second of (yesterday) 1 day ago</para>
        /// </summary>
        private const string _confDaysBacklog = "DaysBacklogToReverse";

        public DuisReversals()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _serviceStartTime = DateTime.Now;
            _timer = new Timer(ReverseBillingTransactions, null, 0, Timeout.Infinite);
        }

        protected override void OnStop()
        {
            _timer.Dispose();
        }

        private void ReverseBillingTransactions(object o)
        {
            var transactionAppService = _bootstrapper.IocManager.Resolve<ITransactionAppService>();
            var billingConfigAppService = _bootstrapper.IocManager.Resolve<IBillPaymentConfigAppService>();

            if (_allBillers.Count == 0 || DateTime.Now - _serviceStartTime > TimeSpan.FromHours(12))
            {
                _allBillers = billingConfigAppService.GetAll();
                _serviceStartTime = DateTime.Now;
            }

            int daysBacklogToReverse = Convert.ToInt32(ConfigurationManager.AppSettings[_confDaysBacklog]);
            // spool first 100 transactions which were successfully debited but transaction is not successful and is not reversed
            var pendingReversals = transactionAppService.GetPendingReversals("00", daysBacklogToReverse).OrderBy(x => x.ReversalAttempts);

            // requery the biller with the transaction
            foreach (var transaction in pendingReversals)
            {
                var biller = _allBillers.Single(x => x.BillerCode.Equals(transaction.ProcessorCode));
                var billingProvider = GetBillingProcessor(biller.ProviderClassFqn, biller.ProviderAssembly);


                var input = new BillPaymentIn()
                {
                    ProviderParametersJson = biller.PaymentParametersJson,
                    ReferenceNumber = transaction.RefId
                };

                var requeryResponse = billingProvider.RequeryBillPayment(input);

                if (new BillPaymentStatus[] { BillPaymentStatus.Failed, BillPaymentStatus.Successful }.Contains(requeryResponse.Status))
                {
                    // reverse if failed
                    var updateInput = new UpdateTransactionInput
                    {
                        Id = transaction.Id,
                        ProcessorRespCode = requeryResponse.ResponseCode,
                        Success = requeryResponse.IsSuccessful,
                        ProcessorRefId = requeryResponse.ProviderReference,
                        ProcessorResponseBody = string.Format("{0},{1}", requeryResponse.ProviderRawResponse, transaction.ProcessorResponseBody)
                    };

                    if (requeryResponse.Status == BillPaymentStatus.Failed)
                    {
                        updateInput.IsReversed = ReverseDebit(transaction);

                        if (updateInput.IsReversed.Value)
                        {
                            updateInput.ReversalTime = DateTime.Now;
                        }
                    }

                    // update the transaction status.
                    transactionAppService.Update(updateInput);
                }

                else
                {
                    // if status in unknown
                    var updateInput = new UpdateTransactionInput
                    {
                        Id = transaction.Id,
                        ProcessorRespCode = requeryResponse.ResponseCode,
                        ReversalAttempts = transaction.ReversalAttempts + 1
                    };

                    // update the transaction status.
                    transactionAppService.Update(updateInput);
                }
            }

            // trigger in the next five minutes
            _timer.Change(5 * 60 * 1000, Timeout.Infinite);
        }

        private bool ReverseDebit(TransactionDto transaction)
        {
            var fincoreSystem = new FincoreSystem(ConfigurationManager.AppSettings["FincoreResponsesFilePath"]);

            FtRequest request = new FtRequest
            {
                Amount = transaction.Amount,
                ChargeAmount = transaction.ChargeAmount,
                FromAcct = transaction.DrSource,
                ToAcct = transaction.DrDestination,
                Narration = transaction.DrNarration,
                RetrievalRefNum = Convert.ToInt32(transaction.DrRefId),
                ToBankCode = transaction.DrDestinationCode,
                TransactionTime = transaction.DrReqTime.Value
            };

            var response = fincoreSystem.ReverseFundsTransfer(request);

            //00: Successful; 94: Duplicate code
            if (new string[] { "00", "94" }.Contains(response.ResponseCode))
            {
                return true;
            }

            return false;
        }

        private IBillPaymentProvider GetBillingProcessor(string fullyQualifiedClassName, string assemblyName)
        {
            var processorInstance = (IBillPaymentProvider)Activator.CreateInstance(assemblyName, fullyQualifiedClassName).Unwrap();
            var container = new Container(x => x.For<IBillPaymentProvider>().Use(processorInstance));
            var processor = container.GetInstance<IBillPaymentProvider>();
            return processor;
        }
    }
}
