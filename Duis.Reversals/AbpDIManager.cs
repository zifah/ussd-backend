﻿using Abp;
using Fbp.Net.Integrations;
using Fbp.Net.Integrations.DuisApp.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Duis
{
    /// <summary>
    /// Summary description for AbpDIManager
    /// </summary>
    public class AbpDIManager
    {
        private static AbpBootstrapper _abpBootstrapper = null;
        //private static string AbpSessionConstant = "$AbpSingleton$";
        //private static HttpApplicationState currentSession = HttpContext.Current.Application;

        public AbpDIManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static AbpBootstrapper AbpBootstraper
        {
            get
            {
                //_abpBootstrapper = currentSession[AbpSessionConstant] as AbpBootstrapper;
                LoggingSystem.LogMessage("Started getting Nhibernate session.");
                if (_abpBootstrapper == null)
                {
                    _abpBootstrapper = new AbpBootstrapper();
                }

                if (!_abpBootstrapper.IocManager.IsRegistered(typeof(TransactionAppService)))
                {
                    LoggingSystem.LogMessage("Started NHibernate session initialization.");
                    _abpBootstrapper.Initialize();
                    LoggingSystem.LogMessage("Completed NHibernate session initialization.");
                }

                //currentSession[AbpSessionConstant] = _abpBootstrapper;     
                LoggingSystem.LogMessage("Finished getting Nhibernate session.");
                return _abpBootstrapper;
            }
        }

        public static void Initialize()
        {
            _abpBootstrapper = new AbpBootstrapper();
            _abpBootstrapper.Initialize();
        }
    }
}